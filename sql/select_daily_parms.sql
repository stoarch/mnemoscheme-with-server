﻿select * from
(SELECT avg( params.value ) as temp_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_query >= '2009-08-1')and( date_query <= '2009-08-20')
	and ( put.id = 6 ) and ( params.param_kind_id = 1 )
group by params.chanel_id, dy, params.param_kind_id
order by dy
) as temp_query,
(SELECT avg( params.value ) as pres_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_query >= '2009-08-1')and( date_query <= '2009-08-20')
	and ( put.id = 6 ) and ( params.param_kind_id = 2 )
group by params.chanel_id, dy, params.param_kind_id
order by dy
) as pres_query,
(SELECT avg( params.value ) as flr_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_query >= '2009-08-1')and( date_query <= '2009-08-20')
	and ( put.id = 6 ) and ( params.param_kind_id = 3 )
group by params.chanel_id, dy, params.param_kind_id
order by dy
)  as flr_query
where (temp_query.dy = pres_query.dy ) and ( pres_query.dy = flr_query.dy ) and ( temp_query.chanel_id = pres_query.chanel_id) and (pres_query.chanel_id = flr_query.chanel_id)
	and (temp_query.chanel_id = 16)
order by temp_query.chanel_id;
