unit interpreter_schedulePreparerLanguage;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ common ]--
        Classes, SysUtils,
        //--[ interfaces ]--
        interface_schedule,
        interface_scheduleList,
        interface_schedulePreparerLanguageInterpreter,
        interface_schedulePreparerLanguageLexer,
        interface_queryArchiveValues,
        interface_queryArchiveRealValue,
        interface_queryModBusSingleValue,
        interface_queryModBusArchiveValue,
        interface_queryRealValue,
        interface_queryValues,
        interface_variables
        ;


    type
        EInterpreterError = class( Exception );

        TSchedulePreparerLanguageInterpreter = class( TCreatableObject, ISchedulePreparerLanguageInterpreter )
            private
              function GetCurrentArchiveQuery: IQueryArchiveValues;
              procedure SetCurrentArchiveQuery(const Value: IQueryArchiveValues);
              function GetCurrentQuery: IQueryValues;
              procedure SetCurrentQuery(const Value: IQueryValues);
            private
              FCurrentArchiveQuery : IQueryArchiveValues;
              FCurrentLexer : ISchedulePreparerLanguageLexer;
              FCurrentSchedule : ISchedule;
              FCurrentQuery : IQueryValues;
              FSchedules : IScheduleList;
              FVariables : IVariables;


              function GetCurrentLexer() : ISchedulePreparerLanguageLexer;
              function GetCurrentSchedule() : ISchedule;

              procedure SetCurrentSchedule( const value : ISchedule );

              property CurrentArchiveQuery : IQueryArchiveValues read GetCurrentArchiveQuery write SetCurrentArchiveQuery;
              property CurrentLexer : ISchedulePreparerLanguageLexer read GetCurrentLexer;
              property CurrentSchedule : ISchedule read GetCurrentSchedule write SetCurrentSchedule;
              property CurrentQuery : IQueryValues read GetCurrentQuery write SetCurrentQuery;


              procedure ErrorOfParsing(message: string);

              function GetVariables() : IVariables;

            public
                constructor Create();override;

                function Execute( fileName : string ) : boolean;

                function GetSchedule(index: integer): ISchedule;
                function getSchedulesCount: Integer;

                property Schedule[ index : integer ] : ISchedule read GetSchedule;
                property SchedulesCount : Integer read getSchedulesCount;

                property Variables : IVariables read GetVariables;
        end;

implementation

uses const_token, ivk103_factories, const_ivk103_guids, interface_token,
  interface_scheduleRegularity, const_languageInterpreter,
  language_factories, interface_language, environment_language,
  const_syntax_ruleid, iterator_syntaxTree, interface_scheduleGenerator,
  interface_syntaxTreeIterator, const_syntax_tokens, factory_schedule;

{ TSchedulePreparerLanguageInterpreter }

procedure TSchedulePreparerLanguageInterpreter.ErrorOfParsing( message : string );
begin
    raise EInterpreterError.Create( message );
end;

function TSchedulePreparerLanguageInterpreter.Execute(
  fileName: string): boolean;
    var
        language : ILanguage;
        iterator : ISyntaxTreeIterator;
        stream : TFileStream;
        aschedule : ISchedule;
        generator : IScheduleGenerator;
begin
    Assert( FileExists( fileName ), 'Unable to parse script file' );

    language := LanguageEnvironment.GetLanguage();

    Assert( Assigned( language ), 'No language is defined in LanguageEnvironment' );

    stream := TFileStream.Create( fileName, fmOpenRead, fmShareDenyWrite );

    try
      FSchedules.clear;

      if( language.Parse( stream ) )then
      begin
          iterator := language.SyntaxTree.Iterator;

          while not iterator.AtEnd do
          begin
              Assert( iterator.TokenId = TOKEN_SCHEDULE, 'Must be schedule at each iteration' );

              generator := ScheduleFactory.MakeScheduleGenerator();

              aschedule := generator.Execute( iterator );

              Assert( Assigned( aschedule ), 'Unable to generate schedule' );

              FSchedules.Add( aschedule );
          end;
      end;
      
    finally
        FreeAndNil( stream );
    end;

    result := true;
end;

function TSchedulePreparerLanguageInterpreter.GetSchedule(
  index: integer): ISchedule;
begin
    result := FSchedules[index];
end;

function TSchedulePreparerLanguageInterpreter.getSchedulesCount: Integer;
begin
    result := FSchedules.Count;
end;

function TSchedulePreparerLanguageInterpreter.GetCurrentLexer: ISchedulePreparerLanguageLexer;
begin
    result := FCurrentLexer;
end;

function TSchedulePreparerLanguageInterpreter.GetCurrentSchedule: ISchedule;
begin
    result := FCurrentSchedule;
end;

procedure TSchedulePreparerLanguageInterpreter.SetCurrentSchedule(
  const value: ISchedule);
begin
    FCurrentSchedule := value;
end;

function TSchedulePreparerLanguageInterpreter.GetVariables: IVariables;
begin
    result := FVariables;
end;

function TSchedulePreparerLanguageInterpreter.GetCurrentArchiveQuery: IQueryArchiveValues;
begin
    result := FCurrentArchiveQuery;
end;

procedure TSchedulePreparerLanguageInterpreter.SetCurrentArchiveQuery(
  const Value: IQueryArchiveValues);
begin
    FCurrentArchiveQuery := value;
end;

function TSchedulePreparerLanguageInterpreter.GetCurrentQuery: IQueryValues;
begin
    result := FCurrentQuery;
end;

procedure TSchedulePreparerLanguageInterpreter.SetCurrentQuery(
  const Value: IQueryValues);
begin
    FCurrentQuery := value;
end;

constructor TSchedulePreparerLanguageInterpreter.Create;
begin
  inherited;

    FSchedules := ScheduleFactory.CreateInstance( CLSID_ScheduleList ) as IScheduleList;
    FVariables := LanguageFactory.MakeVariableList();
end;

end.
