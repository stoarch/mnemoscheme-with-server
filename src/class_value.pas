unit class_value;

interface
	uses
		//--[ classes ]--
		class_creatableObject,
		//--[ interfaces ]--
		interface_value
		;

	type
		TValue = class( TCreatableObject, IValue )
				private
				  FDate: TDateTime;
				  FValue: variant;
			public
				procedure Assign( const value : IValue );

				function GetDate() : TDateTime;
				function GetValue() : variant;

				procedure SetDate(const Value: TDateTime);
				procedure SetValue(const Value: variant);

				property Date : TDateTime read GetDate write SetDate;
				property Value: variant read GetValue write SetValue;
		  end;

implementation

{ TValue }

procedure TValue.Assign(const value: IValue);
begin
	FDate := value.Date;
    FValue := value.Value;
end;

function TValue.GetDate: TDateTime;
begin
	result	:=	FDate;
end;

function TValue.GetValue: variant;
begin
	result := FValue;
end;

procedure TValue.SetDate(const Value: TDateTime);
begin
  FDate := Value;
end;

procedure TValue.SetValue(const Value: variant);
begin
  FValue := Value;
end;

end.
 