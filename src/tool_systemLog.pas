unit tool_systemLog;

interface

	type
    	SystemLog = class
        	class procedure Open(fileName : string);
            class procedure Close();

            class procedure Write( amessage : string );
        	class procedure WriteNoTime( amessage : string );
        end;

implementation

uses
	//--[ common ]--
    classes, sysUtils, windows
    ;

var
	g_log : TFileStream;
    g_logFileName : string;

{ SystemLog }

class procedure SystemLog.Close;
begin
	FreeAndNil( g_log );
end;

class procedure SystemLog.Open(fileName: string);
begin
	g_log := TFileStream.Create( fileName, fmCreate );
	g_logFileName := fileName;
end;

procedure OpenLog();
begin
	//g_log := TFileStream.Create( g_logFileName, fmOpenWrite + fmShareDenyWrite );
end;

procedure WriteToFile( value : string );
    var
    	str : TStringStream;
begin
	str := TStringStream.Create( value );
    try
    	g_log.Seek(0, soFromEnd);
		g_log.CopyFrom(str, length(value));
    finally
		FreeAndNil( str );
    end;
end;

class procedure SystemLog.Write(amessage: string);
	var
        value : string;
begin
	//OpenLog();

	value := Format( '%s::%s',
    			[
                	FormatDateTime('dd.mm.yyyy hh:nn:ss',now),
                    amessage
                ]
    		) + #13#10;

    WriteToFile( value );

    //Close();
end;


class procedure SystemLog.WriteNoTime(amessage: string);
begin
    WriteToFile( amessage );
end;

initialization
finalization
	SystemLog.Close();
end.
