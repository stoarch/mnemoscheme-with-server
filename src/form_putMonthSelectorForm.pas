unit form_putMonthSelectorForm;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sComboBox, sButton,
  //--[ data ]--
  data_put,
  //--[ list ]--
  list_put
  ;

type
  TPutMonthSelectorForm = class(TForm)
    applyButton: TsButton;
    putCombo: TsComboBox;
    discardButton: TsButton;
    monthCombo: TsComboBox;
  private
  	m_list : TPutList;

    procedure FillPuts;
    function getMonth: integer;
    function getPutData: TPutData;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    procedure SetPutList( const list : TPutList );

    property data : TPutData read getPutData;
    property month : integer read getMonth;
  end;

var
  PutMonthSelectorForm: TPutMonthSelectorForm;

implementation

{$R *.dfm}

{ TPutMonthSelectorForm }

constructor TPutMonthSelectorForm.Create(AOwner: TComponent);
begin
  inherited;

  m_list := TPutList.Create();
end;

destructor TPutMonthSelectorForm.Destroy;
begin
	FreeAndNil( m_list );

  inherited;
end;

procedure TPutMonthSelectorForm.FillPuts;
	var
    	i : integer;
begin
	with putCombo.Items do
    begin
    	BeginUpdate();
        Clear();
        for i := 1 to m_list.count do
        begin
			Add( m_list[i].Caption );
        end;
    	EndUpdate();
    end;
end;

function TPutMonthSelectorForm.getMonth: integer;
begin
	result := monthCombo.ItemIndex + 1;
end;

function TPutMonthSelectorForm.getPutData: TPutData;
	var
    	data : TPutData;
begin
	data := TPutData.Create();
    data.Assign( m_list.Put[ putCombo.ItemIndex + 1 ] );
	result := data;
end;

procedure TPutMonthSelectorForm.SetPutList(const list: TPutList);
begin
	m_list.Assign( list );
    FillPuts();
end;

end.
