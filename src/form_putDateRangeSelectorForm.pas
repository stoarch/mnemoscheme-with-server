unit form_putDateRangeSelectorForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, form_putSelector, sSkinProvider, StdCtrls, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, sComboBox, sButton;

type
  TPutDateRangeSelectorForm = class(TPutSelectorForm)
    dateEndEdit: TsDateEdit;
  private
    function getEndDate: TDateTime;
    function getStartDate: TDateTime;
    { Private declarations }
  public
    { Public declarations }
    property StartDate : TDateTime read getStartDate;
    property EndDate : TDateTime read getEndDate;
  end;

var
  PutDateRangeSelectorForm: TPutDateRangeSelectorForm;

implementation

{$R *.dfm}

{ TPutDateRangeSelectorForm }

function TPutDateRangeSelectorForm.getEndDate: TDateTime;
begin
	result := dateEndEdit.Date; 
end;

function TPutDateRangeSelectorForm.getStartDate: TDateTime;
begin
	result := dateTime;
end;

end.
