unit list_put;

interface
	uses
        //--[ classes ]--
        class_mnemoschemeItemList,
        //--[ data ]--
        data_put,
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_putData,
        interface_putList
        ;

    type
    	TPutList = class( TMnemoschemeItemList, IPutList )
            private
            protected
                function MakeData: IMnemoschemeItemData;override;

        	public

                function getPutData(index: integer): IPutData;

                property Put[ index : integer ] : IPutData read getPutData;default;
        end;

implementation

uses Classes, tool_ms_factories, const_put, const_guids;

{ TPutList }

function TPutList.MakeData(): IMnemoschemeItemData;
begin
    result := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IMnemoschemeItemData;
end;



function TPutList.getPutData(index: integer): IPutData;
begin
	result := Values[index] as IPutData;
end;


end.
