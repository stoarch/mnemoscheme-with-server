unit class_universalDataLoader;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        class_key,
        //--[ data ]--
        Zdataset,
        //--[ interfaces ]--
        interface_channelValueLoader,
        interface_interfaceList,
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChannelValue,
        interface_universalDataLoader,
        //--[ collections ]--
        XPCollections,
        //--[ storage ]--
        storage_interfaceHashTable
        ;

type
    TUniversalDataLoader = class( TCreatableObject, IUniversalDataLoader )
    private
        FLastRecord : integer;
        FList : IInterfaceList;
        FCurrentChannel: IMnemoschemeItemChanelData;

    protected
          FChannels : TIntegerInterfaceHashTable;
          m_queriedChanelsHash : TStringInterfaceHashTable;

          FLoader : IChannelValueLoader;

          function HasData() : boolean;
          function HasMoreData() : boolean;

          function FindChanelById(index: integer): IMnemoschemeItemChanelData;virtual;

          procedure CheckChannelExistense(chanel: IMnemoschemeItemChanelData);
          procedure ClearQueriedChanelsStatus;
          function IsQueriedCurrentChannel: boolean;
          procedure LoadCurrentChannelValueFromDB;
          procedure LoadRecordsToLast;
          procedure MarkCurrentChannelAsQueried;

          function GetChannelFromCurrentID: IMnemoschemeItemChanelData;
          function GetCurrentChannelParamKind: integer;
          function GetKeyForCurrentChannelIndexAndParamKind: TKey;

          function GetCurrentChannelIndex(): Integer;
          function GetCurrentParamKind() : integer;

          procedure LoadChanelValue(value : IMnemoschemeItemChannelValue);
          procedure LogWrite( message : string );

          procedure MoveToNextChannelID();

          procedure PrepareQuery;

          procedure PrepareLoader();virtual;

          function GetQuery() : TZQuery;virtual;
          function GetQuerySQL: String;

          function GetCurrentChannel():  IMnemoschemeItemChanelData;
          procedure SetCurrentChannel( const value : IMnemoschemeItemChanelData );

          procedure FillChannels();virtual;

          property CurrentChannel: IMnemoschemeItemChanelData read GetCurrentChannel write
              SetCurrentChannel;

          property Query : TZQuery read GetQuery;
    public
        constructor Create();override;
        destructor Destroy();override;

        function Execute() : boolean;

        function GetLastRecord() : Integer;
        procedure SetLastRecord( const value : integer );

        function GetDataList() : IInterfaceList;
        procedure SetDataList( const value : IInterfaceList );

        property LastRecord : Integer read GetLastRecord write SetLastRecord;

        property DataList : IInterfaceList read GetDataList write SetDataList;
    end;

implementation

uses module_dataMain, SysUtils, tool_systemLog, tool_ms_factories,
  interface_commonFactory, const_guids;

{ TUniversalDataLoader }

constructor TUniversalDataLoader.Create;
begin
  inherited;

  m_queriedChanelsHash := TStringInterfaceHashTable.Create();

  PrepareLoader;

  FChannels := TIntegerInterfaceHashTable.Create();
end;

destructor TUniversalDataLoader.Destroy;
begin
  if assigned( FChannels ) then
      FreeAndNil( FChannels );

  if Assigned( m_queriedChanelsHash ) then
    FreeAndNil( m_queriedChanelsHash );

  inherited;
end;

function TUniversalDataLoader.GetCurrentChannel: IMnemoschemeItemChanelData;
begin
    result := FCurrentChannel;
end;

function TUniversalDataLoader.GetCurrentChannelIndex: Integer;
begin
    result := Query.FieldByName( 'chanel_id' ).AsInteger;
end;

function TUniversalDataLoader.GetCurrentParamKind: integer;
begin
  Result := Query.FieldByName('param_kind_id').AsInteger;
end;


function TUniversalDataLoader.GetQuery: TZQuery;
begin
    raise Exception.Create( 'Not implemented in base abstract class' );

    result := nil;
end;


function TUniversalDataLoader.GetQuerySQL: String;
begin
  //TODO: Move sql strings to class utility
  Result := 'select * from params where ( id > %d ) and ( date_query >= current_date ) order by date_query desc';
end;


function TUniversalDataLoader.HasData: boolean;
begin
  Result := Query.RecordCount > 0;
end;


function TUniversalDataLoader.HasMoreData: boolean;
begin
  Result := not Query.eof;
end;


procedure TUniversalDataLoader.LoadChanelValue(value: IMnemoschemeItemChannelValue);
begin
    FLoader.Query := Query;
    FLoader.Value := Value;
    FLoader.Kind := GetCurrentParamKind();

    FLoader.Execute();
end;


procedure TUniversalDataLoader.LogWrite(message: string);
begin
	SystemLog.Write( message );
end;


procedure TUniversalDataLoader.MoveToNextChannelID;
begin
    Query.Next();
end;


procedure TUniversalDataLoader.PrepareLoader;
begin
    FLoader := GetCommonFactory().CreateInstance( CLSID_ChannelValueLoader ) as IChannelValueLoader;
end;


procedure TUniversalDataLoader.PrepareQuery;
begin
  Query.SQL.Text := Format( GetQuerySQL(), [fLastRecord] );
end;


procedure TUniversalDataLoader.SetCurrentChannel(
  const value: IMnemoschemeItemChanelData);
begin
    FCurrentChannel := value;
end;



procedure TUniversalDataLoader.SetLastRecord(const value: integer);
begin
    FLastRecord := value;
end;


function TUniversalDataLoader.GetChannelFromCurrentID() : IMnemoschemeItemChanelData;
begin
    result := FindChanelById( GetCurrentChannelIndex() );
end;


function TUniversalDataLoader.GetCurrentChannelParamKind(): integer ;
begin
    result := Query.FieldByName( 'param_kind_id' ).AsInteger;
end;


function TUniversalDataLoader.GetKeyForCurrentChannelIndexAndParamKind(): TKey;
begin
    result := TKey.Create( GetCurrentChannelIndex(), GetCurrentChannelParamKind() );
end;


function TUniversalDataLoader.FindChanelById( index : integer ): IMnemoschemeItemChanelData;
begin
    result := FChannels.get( index ) as IMnemoschemeItemChanelData;
end;


function TUniversalDataLoader.Execute: boolean;
begin
    result := false;

    PrepareQuery;
    try
        Query.Open();

        if( HasData )then
        begin
            LoadRecordsToLast();

            result := true;
        end;

    except
        //TODO: Split checking for several exception kinds
        on e:exception do
            LogWrite('Error:' + e.message);
    end;
end;


procedure TUniversalDataLoader.LoadRecordsToLast;
begin
    ClearQueriedChanelsStatus;

    while HasMoreData do
    begin
        LoadCurrentChannelValueFromDB;

        MoveToNextChannelID;
    end;
end;


procedure TUniversalDataLoader.ClearQueriedChanelsStatus;
begin
  m_queriedChanelsHash.clear();
end;


procedure TUniversalDataLoader.LoadCurrentChannelValueFromDB;
begin
  currentChannel	:= GetChannelFromCurrentID();

  CheckChannelExistense(currentChannel);

  if( not IsQueriedCurrentChannel() )then
    begin
      LoadChanelValue( currentChannel.Value );
      MarkCurrentChannelAsQueried();
    end;
end;


procedure TUniversalDataLoader.MarkCurrentChannelAsQueried();
    var
        key : TKey;
begin
    key := GetKeyForCurrentChannelIndexAndParamKind();

   m_queriedChanelsHash.Put( key.caption, key );
end;


function TUniversalDataLoader.IsQueriedCurrentChannel(): boolean;
begin
  result := m_queriedChanelsHash.exists( GetKeyForCurrentChannelIndexAndParamKind().Caption );
end;


procedure TUniversalDataLoader.CheckChannelExistense(chanel: IMnemoschemeItemChanelData);
begin
  if( not assigned( chanel ) )then
      raise Exception.Create('Unable to find chanel ' + IntToStr( GetCurrentChannelIndex() ) );
end;


procedure TUniversalDataLoader.FillChannels;
begin
    raise Exception.Create( 'Not implemented in base class' );
end;



function TUniversalDataLoader.GetDataList: IInterfaceList;
begin
    result := FList;
end;

function TUniversalDataLoader.GetLastRecord: Integer;
begin
    result := FLastRecord;
end;

procedure TUniversalDataLoader.SetDataList(const value: IInterfaceList);
begin
    FList := value;
    FillChannels();
end;

end.
