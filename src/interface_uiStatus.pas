unit interface_uiStatus;

interface
	uses
      	//--[ types ]--
        type_tasks
        ;

	const
      	 IID_IUIStatus : TGUID = '{66DD1AE0-0792-467E-883A-8BE6D6299EED}';

	type
      	IUIStatus = interface
        	['{66DD1AE0-0792-467E-883A-8BE6D6299EED}']

            function getSubTaskProgress() : TTaskProgress;
            function getTaskProgress() : TTaskProgress;

            procedure setStatus( const value : string );
            procedure setSubTaskProgress( const value : TTaskProgress );
            procedure setTaskProgress( const value : TTaskProgress );

            procedure stepSubTaskProgress();
            procedure stepTaskProgress();

            property Status : string write setStatus;

            property SubTaskProgress : TTaskProgress read getSubTaskProgress write setSubTaskProgress;
            property TaskProgress : TTaskProgress read getTaskProgress write setTaskProgress;
        end;

implementation

end.
