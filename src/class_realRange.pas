unit class_realRange;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ interfaces ]--
        interface_realRange
        ;

	type
    	TRealRange = class( TCreatableObject, IRealRange )
            private
              FMax: Real;
              FMin: Real;
            published

        	public
             //--[ methods ]--
             procedure Assign( const value : IRealRange );
             function Contains( value : real ) : boolean;

              function GetMax: Real;
              function GetMin: Real;

              procedure SetMax(const Value: Real);
              procedure SetMin(const Value: Real);

             //--[ properties ]--
          	 property Min : Real read GetMin write SetMin;
             property Max : Real read GetMax write SetMax;

             function ToString() : string;
        end;

implementation

    uses SysUtils;
{ TRealRange }

procedure TRealRange.Assign(const value: IRealRange);
begin
	FMin := value.Min;
    FMax := value.Max;
end;

function TRealRange.Contains(value: real): boolean;
begin
	result := (value > Min) and (value < Max);
end;

function TRealRange.GetMax: Real;
begin
    result := FMax;
end;

function TRealRange.GetMin: Real;
begin
    result := FMin;
end;

procedure TRealRange.SetMax(const Value: Real);
begin
  FMax := Value;
end;

procedure TRealRange.SetMin(const Value: Real);
begin
  FMin := Value;
end;

function TRealRange.ToString: string;
begin
    result := Format( 'Min:%d Max:%d', [Min,Max]);
end;

end.
