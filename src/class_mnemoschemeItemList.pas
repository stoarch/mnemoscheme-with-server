unit class_mnemoschemeItemList;

interface
    uses
        //--[ classes ]--
        class_creatableInterfaceList,
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_mnemoschemeItemList
        ;

    type
        TMnemoschemeItemList = class( TCreatableInterfaceList, IMnemoschemeItemList )
        private
        protected
            function MakeData() : IMnemoschemeItemData;virtual;
        public
              procedure Add( const value : IMnemoschemeItemData );reintroduce;overload;
              procedure Assign( const list : IMnemoschemeItemList );

              function IndexOf(const value: IMnemoschemeItemData ): integer;reintroduce;overload;
              function IndexByCaption(const caption: string): integer;

              function FindById( index : integer ) : IMnemoschemeItemData;
              function FindByCaption(const caption: string): IMnemoschemeItemData;
              function FindByName( const name : string ) : IMnemoschemeItemData;

              function GetData( index : integer ) : IMnemoschemeItemData;
              procedure SetData( index : integer; value : IMnemoschemeItemData );

              property Items[ index : integer ] : IMnemoschemeItemData read getData write setData;
        end;

implementation

uses tool_ms_factories, const_guids, classes, interface_wellData, SysUtils;

{ TMnemoschemeItemList }

function TMnemoschemeItemList.FindByCaption(const caption: string): IMnemoschemeItemData;
    var
        i : integer;
begin
    result := nil;

    for i := 1 to count do
    begin
        if( Items[i].Caption = caption )then
        begin
          result := Items[i];
          break;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('Unable to find item');
end;

function TMnemoschemeItemList.IndexOf(const value: IMnemoschemeItemData): integer;
    var
        i : integer;
        tempValue : IWellData;
begin
    result := -1;

    tempValue := value as IWellData;

    for i := 1 to count do
    begin
        if( Items[i].Equals( tempValue ) )then
        begin
          result := i;
          exit;
        end;
    end;
end;


procedure TMnemoschemeItemList.Add(const value: IMnemoschemeItemData);
begin
	inherited add( value );
end;

procedure TMnemoschemeItemList.Assign(const list: IMnemoschemeItemList);
	var
    	i : integer;
        data : IMnemoschemeItemData;
begin
	Clear();
    for i := 1 to list.count do
    begin
		data := MakeData();
        data.Assign( list.Items[i] );
        Add( data );
    end;
end;

function TMnemoschemeItemList.FindById(index: integer): IMnemoschemeItemData;
	var
    	i : integer;
begin
	result := nil;
    for i := 1 to count do
    begin
		if( Items[i].Index = index )then
        begin
        	result := Items[i];
            break;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('Unable to find item');
end;

function TMnemoschemeItemList.FindByName(const name: string): IMnemoschemeItemData;
  var
    i : integer;
    ucName : string;
begin
  result := nil;
  ucName := AnsiUpperCase(name);
  for i := 1 to count do
  begin
    if( Items[i].Name = ucName )then
    begin
      result := Items[i];
      break;
    end;
  end;

  if( result = nil )then
    raise EListError.Create('Unable to find item ' + name );
end;


function TMnemoschemeItemList.GetData(
  index: integer): IMnemoschemeItemData;
begin
    result := Values[index] as IMnemoschemeItemData;
end;

procedure TMnemoschemeItemList.SetData(index: integer;
  value: IMnemoschemeItemData);
begin
    Values[index] := value;
end;

function TMnemoschemeItemList.MakeData: IMnemoschemeItemData;
begin
    result := GetCommonFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IMnemoschemeItemData;
end;

function TMnemoschemeItemList.IndexByCaption(
  const caption: string): integer;
    var
        i : integer;
begin
    result := -1;

    for i := 1 to count do
    begin
        if( Items[i].Caption = caption )then
        begin
          result := i;
          break;
        end;
    end;

    if( result = -1 )then
    	raise EListError.Create('Unable to find item');
end;

end.
