unit interface_parameterValueLoader;

interface
    uses
        //--[ db ]--
        ZDataset,
        //--[ interfaces ]--
        interface_mnemoschemeItemChannelValue
        ;


const
    IID_IParameterValueLoader : TGUID = '{83376EA0-1300-463B-8538-C38206A88A4D}';

type
    IParameterValueLoader = interface
        ['{83376EA0-1300-463B-8538-C38206A88A4D}']

        function Execute() : boolean;

        function GetQuery() : TZQuery;
        function GetValue() : IMnemoschemeItemChannelValue;

        procedure SetQuery(const Value: TZQuery);
        procedure SetValue(const Value: IMnemoschemeItemChannelValue );

        property Query : TZQuery read GetQuery write SetQuery;
        property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
    end;

implementation

end.
 