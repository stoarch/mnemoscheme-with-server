unit class_wellCalculator;

interface

    uses
      //--[ classes ]--
      class_creatableObject,
      //--[ interfaces ]-
      interface_wellsCalculator,
      interface_wellList,
      //--[ lists ]--
      list_well
      ;


type
    TWellsCalculator = class( TCreatableObject, IWellsCalculator )
    private
      FWells: IWellList;
    public
        function GetWells() : IWellList;
        procedure SetWells( const value : IWellList );

        property Wells : IWellList read FWells write SetWells;

        function GetTotalSumFlowRate() : double;
        function GetTotalCurrentFlowRate() : double;
    end;

implementation

{ TWellsCalculator }

function TWellsCalculator.GetTotalCurrentFlowRate: double;
    var
        i : integer;
        totalWI : double;
begin
  //TODO: Implement ForEach on list
  totalWI := 0.0;

  for i := 1 to Wells.Count do
  begin
    //TODO: Remove indirection
    totalWI := totalWI + Wells[i].Chanels[1].Value.CurrentFlowRate.Value;
  end;

  result := totalWI;
end;

function TWellsCalculator.GetTotalSumFlowRate: double;
    var
        i : integer;
        totalSWI : double;
begin
  totalSWI := 0.0; //HACK: SumWaterIntake

  for i := 1 to Wells.Count do
  begin
    //TODO: Remove indirection
    totalSWI := totalSWI + Wells[i].Chanels[1].Value.FlowRate.Value;
  end;

  result := totalSWI;
end;

function TWellsCalculator.GetWells: IWellList;
begin
    result := FWells;
end;

procedure TWellsCalculator.SetWells(const Value: IWellList);
begin
  FWells := Value;
end;

end.
