unit data_alarms;

interface

type
	TAlarmState = ( asTemperature, asPressure, asFlowRate );
	TAlarmStateSet = set of TAlarmState;

implementation

end.
