unit interface_wellDataLoader;

interface
    uses
      //--[ data ]--
      data_wellChanel,
      //--[ interfaces ]--
      interface_universalDataLoader,
      interface_wellList
     ;

const
    IID_IWellDataLoader : TGUID = '{0F54CD0B-428F-4AB7-B5B8-2B7A51051705}';

type

    IWellDataLoader = interface( IUniversalDataLoader )
        ['{0F54CD0B-428F-4AB7-B5B8-2B7A51051705}']
        function getWells() : IWellList;
        procedure setWells( const value : IWellList );

        property Wells : IWellList read getWells write setWells;
    end;

implementation

end.
 