unit list_Well;

interface
	uses
        //--[ classes ]--
        class_mnemoschemeItemList,
        //--[ common ]--
        SysUtils,
        //--[ data ]--
        data_Well,
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_wellData,
        interface_wellList
        ;

    type
    	TWellList = class( TMnemoschemeItemList, IWellList )
            private
            protected
                function MakeData: IMnemoschemeItemData;override;

        	public

                function getWellData(index: integer): IWellData;

                property Well[ index : integer ] : IWellData read getWellData;default;
        end;

implementation

uses Classes, tool_ms_factories, interface_commonFactory, const_well,
  const_guids;

{ TWellList }


function TWellList.getWellData(index: integer): IWellData;
begin
	result := Values[index] as IWellData;
end;



function TWellList.MakeData: IMnemoschemeItemData;
begin
    result := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IMnemoschemeItemData;
end;

end.
