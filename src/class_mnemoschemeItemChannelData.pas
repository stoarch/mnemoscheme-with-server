unit class_mnemoschemeItemChannelData;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChannelValue
        ;
type
    TMnemoschemeItemChannelData = class( TCreatableObject, IMnemoschemeItemChanelData )
    private
        FActive: boolean;
        FCaption: string;
        FIndex: integer;
        FValue: IMnemoschemeItemChannelValue;


    public
        constructor Create();override;
        destructor Destroy();override;


        procedure Assign( const value : IMnemoschemeItemChanelData );virtual;

        function GetActive: boolean;
        function GetCaption: string;
        function GetIndex: integer;
        function GetValue: IMnemoschemeItemChannelValue;

        procedure SetActive(const Value: boolean);
        procedure SetCaption(const Value: string);
        procedure SetIndex(const Value: integer);
        procedure SetValue(const Value: IMnemoschemeItemChannelValue);

        property Active : boolean read GetActive write SetActive;
        property Caption : string read GetCaption write SetCaption;
        property Index : integer read GetIndex write SetIndex;

        function Equals( source : IMnemoschemeItemChanelData ) : boolean;


        property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
    end;

implementation

uses tool_ms_factories, interface_commonFactory, const_put, const_guids;

{ TMnemoschemeItemChannelData }

procedure TMnemoschemeItemChannelData.Assign(const value: IMnemoschemeItemChanelData);
begin
	Active := value.Active;
    Caption := value.Caption;
    Index := value.Index;
    FValue.Assign( value.Value );
end;

constructor TMnemoschemeItemChannelData.Create;
begin
  inherited;

    FValue := GetCommonFactory().CreateInstance( CLSID_ChannelValue ) as IMnemoschemeItemChannelValue;
end;

destructor TMnemoschemeItemChannelData.Destroy;
begin
   Assert( FRefCount = 0, 'Destroying of usable interfaced object' );

  inherited;
end;

function TMnemoschemeItemChannelData.Equals(
  source: IMnemoschemeItemChanelData): boolean;
begin
    result :=
        (
            ( Active = source.Active )
           and ( Caption = source.Caption )
           and ( Index = source.Index )
           and ( Value.Equals( source.Value ) )
        );
end;

function TMnemoschemeItemChannelData.GetActive: boolean;
begin
    result := FActive;
end;

function TMnemoschemeItemChannelData.GetCaption: string;
begin
    result := FCaption;
end;

function TMnemoschemeItemChannelData.GetIndex: integer;
begin
    result := FIndex;
end;

function TMnemoschemeItemChannelData.GetValue: IMnemoschemeItemChannelValue;
begin
    result := FValue;
end;

procedure TMnemoschemeItemChannelData.SetActive(const Value: boolean);
begin
    FActive := value;
end;

procedure TMnemoschemeItemChannelData.SetCaption(const Value: string);
begin
    FCaption := value;
end;

procedure TMnemoschemeItemChannelData.SetIndex(const Value: integer);
begin
    FIndex := value;
end;

procedure TMnemoschemeItemChannelData.SetValue(const Value: IMnemoschemeItemChannelValue);
begin
  FValue := Value;
end;

end.
 