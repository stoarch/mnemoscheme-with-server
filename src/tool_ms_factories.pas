unit tool_ms_factories;

interface

    uses
        //--[ interfaces ]--
        interface_commonFactory
        ;

function GetCommonFactory(): ICommonFactory;
function GetPutFactory() : ICommonFactory;
function GetWellFactory() : ICommonFactory;

implementation
    uses
      //--[ constants ]--
      const_guids,
      const_put,
      const_well,
      //--[ tools ]--
      tool_environment
      ;

function GetCommonFactory(): ICommonFactory;
begin
    result := Environment.GetInstance( CLSID_CommonFactory ) as ICommonFactory;

    Assert( assigned( result ), 'Common factory is not set' );
end;


function GetPutFactory(): ICommonFactory;
begin
  Result := Environment.GetInstance( CLSID_PutFactory ) as ICommonFactory;

  Assert( assigned( Result ), 'Put factory does not set' );
end;

function GetWellFactory(): ICommonFactory;
begin
    result := Environment.GetInstance( CLSID_WELLSFACTORY ) as ICommonFactory;

    Assert( assigned( result ), 'Wells factory is not set' );
end;


end.
 