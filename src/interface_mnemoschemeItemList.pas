unit interface_mnemoschemeItemList;

interface
    uses
       //--[ interfaces ]--
       interface_interfaceList,
       interface_mnemoschemeItemData
       ;

    const
        IID_IMnemoschemeItemList : TGUID = '{4574F16F-02AA-4386-BA25-AD5D4B222D44}';

    type
        IMnemoschemeItemList = interface( IInterfaceList )
            ['{4574F16F-02AA-4386-BA25-AD5D4B222D44}']

              procedure Add( const value : IMnemoschemeItemData );
              procedure Assign( const list : IMnemoschemeItemList );

              function IndexOf(const value: IMnemoschemeItemData ): integer;
              function IndexByCaption(const caption: string): integer;

              function FindById( index : integer ) : IMnemoschemeItemData;
              function FindByCaption(const caption: string): IMnemoschemeItemData;
              function FindByName( const name : string ) : IMnemoschemeItemData;

              function GetData( index : integer ) : IMnemoschemeItemData;
              procedure SetData( index : integer; value : IMnemoschemeItemData );

              property Items[ index : integer ] : IMnemoschemeItemData read getData write setData;
        end;

implementation

end.
 