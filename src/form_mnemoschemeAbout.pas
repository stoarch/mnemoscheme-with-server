unit form_mnemoschemeAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sLabel, StdCtrls, sButton, acPNG, ExtCtrls, sBevel,
  sSkinProvider;

type
  TMnemoschemeAboutForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    sBevel1: TsBevel;
    Image1: TImage;
    sButton1: TsButton;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sWebLabel1: TsWebLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MnemoschemeAboutForm: TMnemoschemeAboutForm;

implementation

{$R *.dfm}

end.
