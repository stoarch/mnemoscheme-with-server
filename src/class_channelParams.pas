unit class_channelParams;

interface
	uses
    	//--[ classes ]--
        class_creatableObject,
        class_realRange,
        //--[ interfaces ]--
        interface_channelParams,
        interface_commonFactory,
        interface_realRange,
    	//--[ containers ]--
        XPCollections
        ;

//TODO: Refactor ChannelParams - Make classes: ChannelValue, ChannelSettings and move ChannelValue list ot TPutChanelData
	type
    	TChannelParams = class( TCreatableObject, IChannelParams )
          private
    		FPressureRange: IRealRange;
    		FTemperatureRange: IRealRange;
    		FFlowRateRange: IRealRange;
            FHeatRateRange: IRealRange;

          published
          public
            constructor Create();override;
            destructor Destroy();override;

            procedure Assign( const data : IChannelParams );

            function GetFlowRateRange: IRealRange;
            function GetHeatRateRange: IRealRange;
            function GetPressureRange: IRealRange;
            function GetTemperatureRange: IRealRange;

            property TemperatureRange : IRealRange read GetTemperatureRange;
            property PressureRange : IRealRange read GetPressureRange;
            property FlowRateRange : IRealRange read GetFlowRateRange;
            property HeatRateRange : IRealRange read GetHeatRateRange;
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils,
        //--[ constants ]--
        const_guids,
        //--[ tools ]--
        tool_environment
        , tool_ms_factories;

{ TPUTParams }

procedure TChannelParams.Assign(const data: IChannelParams);
begin
    FFlowRateRange.Assign( data.FlowRateRange );
    FPressureRange.Assign( data.PressureRange );
    FTemperatureRange.Assign( data.TemperatureRange );
    FHeatRateRange.Assign( data.HeatRateRange );
end;

constructor TChannelParams.Create;
    var
      factory : ICommonFactory;
begin
  inherited;

     factory := GetCommonFactory();

    FFlowRateRange := factory.CreateInstance( CLSID_RealRange ) as IRealRange;
    FTemperatureRange := factory.CreateInstance( CLSID_RealRange ) as IRealRange;
    FPressureRange := factory.CreateInstance( CLSID_RealRange ) as IRealRange;
    FHeatRateRange := factory.CreateInstance( CLSID_RealRange ) as IRealRange;
end;


destructor TChannelParams.Destroy;
begin
  inherited;
end;


function TChannelParams.GetFlowRateRange: IRealRange;
begin
    result := FFlowRateRange;
end;

function TChannelParams.GetHeatRateRange: IRealRange;
begin
    Result := FHeatRateRange;
end;

function TChannelParams.GetPressureRange: IRealRange;
begin
    Result := FPressureRange;
end;

function TChannelParams.GetTemperatureRange: IRealRange;
begin
    result := FTemperatureRange;
end;


end.
