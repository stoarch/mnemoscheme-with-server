unit interface_universalDataLoader;

interface
    uses
        //--[ interfaces ]--
        interface_interfaceList
        ;

const
    IID_IUniversalDataLoader : TGUID = '{ECBC9A77-2278-4153-9F18-B371E757B294}';

type
    IUniversalDataLoader = interface
        ['{ECBC9A77-2278-4153-9F18-B371E757B294}']

        function Execute() : boolean;

        function GetLastRecord() : Integer;
        procedure SetLastRecord( const value : integer );

        function GetDataList() : IInterfaceList;
        procedure SetDataList( const value : IInterfaceList );

        property LastRecord : Integer read GetLastRecord write SetLastRecord;

        property DataList : IInterfaceList read GetDataList write SetDataList;
    end;



implementation

end.
 