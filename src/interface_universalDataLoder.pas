unit interface_universalDataLoder;

interface
    uses
        //--[ collections ]--
        XPCollections
        ;

const
    IID_IUniversalDataLoader : TGUID = '{ECBC9A77-2278-4153-9F18-B371E757B294}';

type
    IUniversalDataLoader = interface
        ['{ECBC9A77-2278-4153-9F18-B371E757B294}']

        function Execute() : boolean;

        function GetLastRecord() : Integer;
        procedure SetLastRecord( const value : integer );

        function GetDataList() : TXDList;
        procedure SetDataList( const value : TXDList );

        property LastRecord : Integer read GetLastRecord write SetLastRecord;

        property DataList : TXDList read GetDataList write SetDataList;
    end;

type


implementation

end.
 