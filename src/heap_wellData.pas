unit heap_wellData;

interface

uses
  EzdslBse, EzdslSup, EzdslPQu,
  data_Well
  ;

	type
    	TWellHeapItem = record
        	priority : integer;
            value : TWellData;
        end;
        PWellHeapItem = ^TWellHeapItem;

//TODO: Commonize heap classes
        TWellHeap = class
        	private
              	FQueue : TPriorityQueue;
                FTopPriority : integer; //Top readen priority

    			function getCount: integer;
    			function getIsEmpty: boolean;
    			function getTopPriority: integer;

            public
              	constructor Create();
                destructor Destroy();override;

                property Count : integer read getCount;
                property IsEmpty : boolean read getIsEmpty;

                property TopPriority : integer read getTopPriority;

                function Pop() : TWellData;
                procedure Push( const value : TWellData; priority : integer );
        end;

implementation

	uses
      	SysUtils
        ;


    const
      	EmptyWellHeapItem : TWellHeapItem = (priority:0;value:nil);

procedure DisposeWellItem( adata : pointer );
begin
  	if( assigned( adata ))then
    	if( assigned( PWellHeapItem(adata)^.value ) )then
        	TWellData( PWellHeapItem(adata)^.value ).Free;

	SafeFreeMem(aData, succ(sizeof(PWellHeapItem(aData)^)));
end;

function CompareWellItem(Data1, Data2 : pointer) : integer;
begin
  if (Data1 = nil) then
    if (Data2 = nil) then
      Result := 0
    else
      Result := -1
  else
    if (Data2 = nil) then
      Result := 1
    else
      Result := PWellHeapItem(Data1)^.priority -
      			PWellHeapItem(Data2)^.priority
      ;
end;

function HeapItemNew( avalue : TWellData; apriority : integer ) : PWellHeapItem;
	var
      	item : TWellHeapItem;
begin
  item.value := avalue;
  item.priority := apriority;

  SafeGetMem(Result, succ(sizeof(item)));
  PWellHeapItem(Result)^ := item;
end;

{ TWellHeap }

constructor TWellHeap.Create;
begin
	inherited;

	FQueue := TPriorityQueue.Create(True);
    FQueue.DisposeData := DisposeWellItem;
    FQueue.Compare := CompareWellItem;
end;

destructor TWellHeap.Destroy;
begin
	FreeAndNil( FQueue );
    
	inherited;
end;

function TWellHeap.getCount: integer;
begin
	Result := FQueue.Count;
end;

function TWellHeap.getIsEmpty: boolean;
begin
	Result := FQueue.IsEmpty;
end;

function TWellHeap.getTopPriority: integer;
begin
  result := FTopPriority;
end;

function TWellHeap.Pop: TWellData;
	var
      	item : TWellHeapItem;
begin
	if( isEmpty )then
    	result := nil
    else
    	begin
            item := PWellHeapItem(FQueue.Pop)^;
            
    		result := item.value;
            FTopPriority := item.priority;
        end;
end;

procedure TWellHeap.Push(const value: TWellData; priority: integer);
begin
	FQueue.Append(HeapItemNew(value, priority));
end;

end.
