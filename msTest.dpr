program msTest;

uses
  Forms,
  sysUtils,
  TestFramework,
  GuiTestRunner,
  test_main in 'src\test\test_main.pas',
  form_main in 'src\form_main.pas' {Mnemoscheme},
  class_opcDataAccess in 'src\class_opcDataAccess.pas',
  OPCutils in '..\..\library_new\OPC\Client\OPCutils.pas',
  class_channelParams in 'src\class_channelParams.pas',
  class_putView in 'src\class_putView.pas',
  frame_channelView in 'src\frame_channelView.pas' {ChannelViewFrame: TFrame},
  frame_putView in 'src\frame_putView.pas' {PutViewFrame: TFrame},
  frame_channelInfoView in 'src\frame_channelInfoView.pas' {ChannelInfoViewFrame: TFrame},
  frame_putInfoView in 'src\frame_putInfoView.pas' {PutInfoViewFrame: TFrame},
  class_realRange in 'src\class_realRange.pas',
  data_alarms in 'src\data_alarms.pas',
  class_opcItem in 'src\class_opcItem.pas',
  OPCAutomation_TLB in '..\..\library_new\OPCAutomation_TLB.pas',
  XPFramework in '..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  XPCollections in '..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  OPCtypes in '..\..\library_new\OPC\OPCtypes.pas',
  OPC_AE in '..\..\library_new\OPC\OPC_AE.pas',
  OPCCOMN in '..\..\library_new\OPC\OPCCOMN.pas',
  OPCDA in '..\..\library_new\OPC\OPCDA.pas',
  OpcError in '..\..\library_new\OPC\OPCerror.pas',
  OPCHDA in '..\..\library_new\OPC\OPCHDA.pas',
  OPCSEC in '..\..\library_new\OPC\OPCSEC.pas',
  opc_error_strings in '..\..\library_new\OPC\opc_error_strings.pas',
  form_logo in 'src\form_logo.pas' {LogoForm},
  module_dataMain in 'src\module_dataMain.pas' {MSDataModule: TDataModule},
  list_wellChanel in 'src\list_wellChanel.pas',
  data_put in 'src\data_put.pas',
  data_wellChanel in 'src\data_wellChanel.pas',
  list_put in 'src\list_put.pas',
  AMAdvLed in '..\..\library_new\AmadvLed\AMAdvLed.pas',
  A3nalogGauge in '..\..\library_new\A3Nalog\A3nalogGauge.pas',
  storage_ObjectHashTable in '..\..\library\irlib\storage\hash\storage_ObjectHashTable.pas',
  storage_HashTable in '..\..\library\irlib\storage\hash\storage_HashTable.pas',
  DeCAL in '..\..\library\Collections\decal\DeCAL.pas',
  SuperStream in '..\..\library\Collections\decal\SuperStream.pas',
  DeCALIO in '..\..\library\Collections\decal\DeCALIO.pas',
  mwFixedRecSort in '..\..\library\Collections\decal\mwFixedRecSort.pas',
  storage_iterator in '..\..\library\irlib\storage\iterators\storage_iterator.pas',
  form_mnemoschemeAbout in 'src\form_mnemoschemeAbout.pas' {MnemoschemeAboutForm},
  class_channelValue in 'src\class_channelValue.pas',
  const_well in 'src\const_well.pas',
  class_valueMatrix in 'src\class_valueMatrix.pas',
  class_valueList in 'src\class_valueList.pas',
  class_value in 'src\class_value.pas',
  form_graphViewForm in 'src\form_graphViewForm.pas' {GraphViewForm},
  form_dateRangeSelectorForm in 'src\form_dateRangeSelectorForm.pas' {SelectDateRangeForm},
  form_tableView in 'src\form_tableView.pas' {TableViewForm},
  tool_systemLog in 'src\tool_systemLog.pas',
  tool_environment in '..\..\library\irlib\tools\tool_environment.pas',
  storage_interfaceHashTable in '..\..\library\irlib\storage\hash\storage_interfaceHashTable.pas',
  interface_uiStatus in 'src\interface_uiStatus.pas',
  const_guids in 'src\const_guids.pas',
  type_tasks in 'src\type_tasks.pas',
  tool_settings in '..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\library\irlib\tools\tool_localSystem.pas',
  JclFileUtils,
  form_monthDaySelector in 'src\form_monthDaySelector.pas' {MonthDaySelectorForm},
  tool_wellSettingsLoader in 'src\tool_wellSettingsLoader.pas',
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm},
  class_creatableObject in '..\..\library\irlib\classes\class_creatableObject.pas',
  class_creatableInterfaceList in '..\..\library.delphi\library\irlib\classes\class_creatableInterfaceList.pas',
  interface_interfaceList in '..\..\library.delphi\library\irlib\interfaces\interface_interfaceList.pas',
  interface_list in '..\..\library.delphi\library\irlib\interfaces\lists\interface_list.pas',
  interface_commonIterator in '..\..\library.delphi\library\irlib\interfaces\iterators\interface_commonIterator.pas',
  storage_interfaceList in '..\..\library.delphi\library\irlib\storage\list\storage_interfaceList.pas',
  storage_commonList in '..\..\library.delphi\library\irlib\storage\list\storage_commonList.pas',
  interface_value in 'src\interface_value.pas',
  interface_valueList in 'src\interface_valueList.pas',
  interface_valueMatrix in 'src\interface_valueMatrix.pas',
  tool_wellDataSaver in 'src\tool_wellDataSaver.pas';

{$R *.res}

begin
    Settings.Open( 'ms.settings' );

	SystemLog.Open( 'mstest.log' );

    msDataModule.LoadSettings();

    msDatamodule.Open();

  	Application.Initialize;

    MSDataModule := TMSDataModule.Create(nil);

  	GuiTestRunner.RunRegisteredTests();

	FreeAndNil( msDataModule );

    SystemLog.Write( 'All test done' );
end.
