unit class_putDataLoader;

interface
    uses
      //--[ classes ]--
      class_creatableObject,
      class_key,
      class_universalDataLoader,
      //--[ common ]--
      ZDataset,
      //--[ data ]--
      data_putChanel,
      //--[ interfaces ]--
      interface_putDataLoader,
      interface_putList,
      //--[ lists ]--
      list_put,
      //--[ storages ]--
      storage_ObjectHashTable
      ;

type
    TPutDataLoader = class( TUniversalDataLoader, IPutDataLoader, IPutDataLoader2 )
        private

            FLoadActiveOnly : boolean;
        protected
            function GetQuery() : TZQuery;override;

            procedure PrepareLoader;override;

            procedure FillChannels;override;

        public
            constructor Create();override;
            destructor Destroy();override;


            function GetPutList() : IPutList;
            procedure SetPutList( const value : IPutList );


            property PutList : IPutList read GetPutList write SetPutList;

          procedure SetLoadActiveOnly( value : boolean );
          function GetLoadActiveOnly():Boolean;

          property LoadActiveOnly : Boolean read GetLoadActiveOnly write SetLoadActiveOnly;
    end;

implementation

uses module_dataMain, SysUtils, tool_systemLog, const_put
  , classes_chanelParameterLoaders, tool_ms_factories, const_guids,
  interface_commonFactory, interface_parameterValueLoader;

{ TPutDataLoader }


procedure TPutDataLoader.PrepareLoader;
    var
        factory : ICommonFactory;
begin
  inherited;

  factory := GetCommonFactory();

  FLoader.AddParameterLoader( TEMPERATURE_ID, factory.CreateInstance( CLSID_TemperatureValueLoader ) as IParameterValueLoader );
  FLoader.AddParameterLoader( PRESSURE_ID, factory.CreateInstance( CLSID_PressureValueLoader ) as IParameterValueLoader );
  FLoader.AddParameterLoader( FLOWRATE_ID, factory.CreateInstance( CLSID_FlowrateValueLoader ) as IParameterValueLoader );
  FLoader.AddParameterLoader( HEATRATE_ID, factory.CreateInstance( CLSID_HeatRateValueLoader ) as IParameterValueLoader );
end;


procedure TPutDataLoader.SetPutList(const Value: IPutList);
begin
  DataList := Value;

  FillChannels();
end;


function TPutDataLoader.GetPutList: IPutList;
begin
    result := DataList as IPutList;
end;



function TPutDataLoader.GetQuery: TZQuery;
begin
    result := msDataModule.queryData;
end;

constructor TPutDataLoader.Create;
begin
  inherited;

end;

destructor TPutDataLoader.Destroy;
begin

  inherited;
end;

procedure TPutDataLoader.FillChannels;
  var
      i, j : integer;
begin
  FChannels.clear();

  for i := 1 to PutList.Count do
    for j := 1 to PutList[i].count do
        FChannels.put( PutList[i][j].Index, PutList[i][j] );
end;

function TPutDataLoader.GetLoadActiveOnly: Boolean;
begin
  Result := FLoadActiveOnly;
end;

procedure TPutDataLoader.SetLoadActiveOnly(value: boolean);
begin
  FLoadActiveOnly := value;
end;

end.
