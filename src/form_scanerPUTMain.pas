unit form_scanerPUTMain;

//TODO: Implement 3 timers for 3 priority
//TODO: Implement database logic in async thread

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinManager, sSkinProvider, Trayicon, StdCtrls, sMemo,
  sComboBox, sLabel, ComCtrls, acProgressBar, Buttons, sBitBtn, iComponent,
  iVCLComponent, iCustomComponent, iSwitchLed, iniFiles,
  OleServer, OPCAutomation_TLB, Grids, DBGrids,
  sStatusBar, Menus, sPageControl, sButton, DB,
  //--[ data ]--
  data_put,
  data_putChanel,
  //--[ list ]--
  list_put,
  //--[ opc ]--
  class_opcDataAccess,
  //--[ tools ]--
  tool_systemLog, ExtCtrls, sGauge, acPNG, sPanel, TeEngine, Series,
  TeeProcs, Chart, sCheckBox, sSpeedButton
  ;

type
  TPutScanerMainForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    SkinManager: TsSkinManager;
    trayIcon: TTrayIcon;
    OPCServer: TOPCServer;
    sPageControl1: TsPageControl;
    sTabSheet1: TsTabSheet;
    progresLabel: TStaticText;
    queryAllNowButton: TsBitBtn;
    inProgressBar: TsProgressBar;
    progressBar: TsProgressBar;
    durationLabel: TsLabelFX;
    timeLabel: TsLabelFX;
    ConnectButton: TsBitBtn;
    logMemo: TsMemo;
    clearLogButton: TsBitBtn;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    statusBar: TsStatusBar;
    refreshTimer: TTimer;
    tickTimer: TTimer;
    timeGauge: TsGauge;
    watchDogTimer: TTimer;
    watchdogErrorPanel: TsPanel;
    Image1: TImage;
    sLabel6: TsLabel;
    Contents1: TMenuItem;
    autoqueryButton: TsSpeedButton;
    procedure ConnectButtonClick(Sender: TObject);
    procedure clearLogButtonClick(Sender: TObject);
    procedure base_values_buttonClick(Sender: TObject);
    procedure base_ccc_values_buttonClick(Sender: TObject);
    procedure queryAllNowButtonClick(Sender: TObject);
    procedure autoQueryOnLEDChange(Sender: TObject);
    procedure refreshTimerTimer(Sender: TObject);
    procedure tickTimerTimer(Sender: TObject);
    procedure trayIconClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure watchDogTimerTimer(Sender: TObject);
    procedure Contents1Click(Sender: TObject);
  private
  	m_querying : boolean;

    m_startTime : TDateTime;

    FTicks : integer;
    FQueried : boolean;

    procedure HandleLog(sender: TObject; const value: string);
    procedure ShowAboutForm;
    procedure ShowHint(message: string);
    procedure ShowWorkState(state: string);
    procedure ShowMemoryUsage;

  protected
    procedure ConnectToOPCServer;
    procedure QueryData;
    procedure QueryPutParams;
    procedure QueryOPCData;
    function getOPCDA: TOPCDataAccess;
    function ReadPutSettings: TPutList;
    procedure HandleError(sender: TObject; const error: string);
    procedure HandleInitProgress(sender: TObject);
    procedure HandleFinalProgress(sender: TObject);
    procedure HandleStepProgress(sender : Tobject);
    procedure HandleUnableToConnect(sender: TObject);
    procedure HandleViewQueriedPut(sender: TObject;
      const acaption: string);
    procedure Log(message: string);
    function GetCurrentServer: string;
    procedure LoadServerList;
    procedure SaveCurrentPutData(data: TPutData);
    procedure InsertBaseValues;
    procedure InsertCCCBaseValues;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( aowner : TComponent );override;
  end;

var
  PutScanerMainForm: TPutScanerMainForm;

implementation

uses
    //--[ data ]--
    module_dataMain,
	//--[ opc ]--
    opc_queryExecutor,
    //--[ tools ]--
    tool_putDataSaver,
    tool_putSettingsLoader
    , form_aboutPutScaner, tool_functions;

const
   settings_file = 'ms.settings';

const
    TEMPERATURE_ID = 1;
    PRESSURE_ID = 2;
    FLOWRATE_ID = 3;

{$R *.dfm}
function TPutScanerMainForm.GetCurrentServer: string;
begin
	result := 'Vzljot.OpcDaVzljotServer.1';
end;


procedure TPutScanerMainForm.ConnectToOPCServer();
begin
	try
      if( autoqueryButton.Down )then
      begin
        QueryData();
      end;
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.ConnectToOPCServer->'+#10#13+ e.message );
    end;
end;

procedure TPutScanerMainForm.QueryData;
begin
	WatchDogTimer.Enabled := true;
    timeGauge.MaxValue := WatchDogTimer.Interval div 1000;
    timeGauge.Progress := 0;

    ShowWorkState( 'Query data' );
    ShowHint( 'Query data started...' );
    Log( '===================================' );
    Log( 'Query data started...' );

	//TickTimer.Enabled := false;
	refreshTimer.Enabled := false;
    FQueried := true;

	try
      queryAllNowButton.Enabled := false;

      QueryPutParams;
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.QueryData->'+#10#13+ e.Message );
    end;
end;

procedure TPutScanerMainForm.QueryPutParams;
begin
	try
      if( m_querying )then
        exit;

      m_querying := true;

      QueryOPCData;

      m_querying := false;
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.QueryPutParams->'+#10#13+ e.message );
    end;
end;

procedure TPutScanerMainForm.Log( message : string );
	var
    	value : string;
begin
	value := Format( '%s -- %s', [FormatDateTime('hh:nn:ss', now), message] );
	logMemo.Lines.Add( value );
    if( logMemo.Lines.Count > 100 )then
    	logMemo.Lines.Delete(0);

    logMemo.SelStart := length( logMemo.lines.Text );
    logMemo.SelLength := 1;

    SystemLog.Write( message );
end;

procedure TPutScanerMainForm.HandleError(sender : TObject; const error : string);
begin
	Log( 'Error:' + error );
    ShowHint( 'Error:' + error );
    ShowWorkState( 'Error occured' );
end;

procedure TPutScanerMainForm.HandleInitProgress( sender : TObject );
begin
	m_startTime := now;

    ShowHint( 'Executor query started...' );
    Log( 'Executor query started...' );

	progressBar.Position := 0;
    progressBar.Max := g_executor.PutList.Count;
    progressBar.Step := 1;

    inProgressBar.Style := pbstMarquee;
end;

procedure TPutScanerMainForm.HandleFinalProgress( sender : TObject );
begin
	inProgressBar.Style := pbstNormal;

    queryAllNowButton.Enabled := true;
    inProgressBar.Position := 0;

    durationLabel.Caption := Format( '����� ������ %s', [FormatDateTime('hh:nn:ss', now - m_startTime)] );
    timeLabel.Caption := Format( '� %s', [FormatDateTime( 'hh:nn:ss', now )]);

    timeGauge.Progress := 0;
    timeGauge.MaxValue := refreshTimer.Interval div 1000;
    refreshTimer.Enabled := true;
    watchDogTimer.enabled := false;
    FQueried := false;

    Log(' Complete.' );
    ShowHint( 'Data querying finished' );
    ShowWorkState( 'Wait for query' );
end;

procedure TPutScanerMainForm.HandleStepProgress(sender : Tobject);
begin
	try
		progressBar.StepIt();
        try
        	g_synchronizer.BeginRead();
            if( g_executor.CurrentPut.Active )then
            begin
                ShowHint( 'Query of ' + g_executor.CurrentPut.Caption );
                Log( 'Query of ' + g_executor.CurrentPut.Caption );

    			SaveCurrentPutData( g_executor.CurrentPut );
            end;
        finally
        	g_synchronizer.EndRead();
        end;
    except
    	on e:exception do
        	Log( e.message );
    end;
end;

procedure TPutScanerMainForm.SaveCurrentPutData( data : TPutData );
begin
	if( assigned( data ) )then
	if( data.Active )then
    begin
    	Log( 'Saving data...' );
        ShowHint( 'Saving data...' );

  		PutDataSaver.Save( data );

        ShowHint( 'Data saved' );
        Log( 'Data saved.' );
    end;
end;

procedure TPutScanerMainForm.HandleUnableToConnect( sender : TObject );
begin
	Log( 'Unable to connect::' + g_executor.ErrorMessage );
    ShowHint( 'Unable to connect' );
    ShowWorkState( 'Error occured' );
end;

procedure TPutScanerMainForm.HandleViewQueriedPut( sender : TObject; const acaption : string );
begin
	Log( 'Query put ' + acaption );
    ShowHint( 'Query put ' + acaption );
end;

procedure TPutScanerMainForm.QueryOPCData;
begin
	try
      if( not assigned( g_executor ))then
      begin
      	ShowHint( 'Prepare executor...');
        Log( 'Prepare executor...' );

        g_executor := TQueryExecutor.Create(true);
        g_executor.FreeOnTerminate := true;
        g_executor.SetOPCDA( getOPCDA() );
        g_executor.ServerName := GetCurrentServer();

        g_executor.OnError := HandleError;
        g_executor.OnInitProgress := HandleInitProgress;
        g_executor.OnFinalProgress := HandleFinalProgress;
        g_executor.OnStepProgress := HandleStepProgress;
        g_executor.OnUnableToConnect := HandleUnableToConnect;
        g_executor.OnViewQueriedPut := HandleViewQueriedPut;
        g_executor.OnLogOutput := HandleLog;
      end;

      ShowHint( 'Query started...' );
      Log( 'Query started...' );

      g_executor.PutList := readPutSettings;
      g_executor.Resume();
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.QueryOPCData->'+#10#13+ e.Message );
    end;
end;


procedure TPutScanerMainForm.HandleLog( sender : TObject; const value : string );
begin
	Log(value);
end;


function TPutScanerMainForm.getOPCDA() : TOPCDataAccess;
begin
	result := TOPCDataAccess.Create();
end;

function TPutScanerMainForm.ReadPutSettings() : TPutList;
begin
	Result := PutSettingsLoader.ReadPutSettings( [psoChanel, psoOPC], false );
end;

constructor TPutScanerMainForm.Create(aowner: TComponent);
begin
  inherited;

  FTicks := 0;

  timeGauge.MinValue := 0;
  timeGauge.MaxValue := RefreshTimer.Interval div TickTimer.Interval;

  SetBounds( Screen.WorkAreaWidth - Width, Screen.WorkAreaHeight - Height, Width, Height );

  try
  	LoadServerList();
  except
  	on e:exception do
  		Log( 'ERROR::TPutScanerMainForm.Create->'+#10#13+ e.message );
  end;

  ShowMemoryUsage();

  ConnectButtonClick(self);
  autoQueryButton.Down := true;
end;

procedure TPutScanerMainForm.LoadServerList();
begin
end;

procedure TPutScanerMainForm.ConnectButtonClick(Sender: TObject);
begin
	try
      ConnectToOPCServer();

      autoQueryButton.Enabled := true;
      queryAllNowButton.Enabled := true;
      ConnectButton.Enabled := false;
    except
    	on e:exception do
        	Log( 'ERROR:' + e.message );
    end;
end;

procedure TPutScanerMainForm.clearLogButtonClick(Sender: TObject);
begin
	logMemo.Lines.Clear();
end;

procedure TPutScanerMainForm.base_values_buttonClick(Sender: TObject);
begin
	try
		InsertBaseValues();
    except
    	on e:exception do
        	Log( 'ERROR:' + e.message );
    end;
end;

procedure TPutScanerMainForm.InsertBaseValues();
	procedure AppendOPCData( kindId : integer; value : string );
    begin
      with MSDataModule, opc_params_table do
      begin
          Append();
          FieldByName('param_kind_id').AsInteger := kindId;
          FieldByName('value').AsString := value;
          Post();
      end;
    end;
begin
	try
      AppendOPCData(1,'');
      AppendOPCData(2,'');
      AppendOPCData(3,'');
      AppendOPCData(4,'');
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.InsertBaseValues->'+#10#13+ e.message );
    end;
end;


procedure TPutScanerMainForm.base_ccc_values_buttonClick(Sender: TObject);
begin
	try
		InsertCCCBaseValues();
    except
    	on e:exception do
    		Log( 'ERROR:' + e.message );
    end;
end;

procedure TPutScanerMainForm.InsertCCCBaseValues();
	procedure AppendCCCData( kindId : integer; min, max : real );
    begin
      with msDataModule, chanel_control_settings_table do
      begin
      	Append();
        FieldByName('param_kind_id').AsInteger := kindId;
        FieldByName('min').AsFloat := min;
        FieldByName('max').AsFloat := max;
        Post();
      end;
    end;
begin
	try
      AppendCCCData(1,10,60);
      AppendCCCData(2,0,1);
      AppendCCCData(3,0,1500);
      AppendCCCData(4,0,100);
    except
    	on e:exception do
        	raise Exception.Create( 'TPutScanerMainForm.InsertCCCBaseValues->'+#10#13+ e.message );
    end;
end;


procedure TPutScanerMainForm.queryAllNowButtonClick(Sender: TObject);
begin
	QueryData();
end;

procedure TPutScanerMainForm.autoQueryOnLEDChange(Sender: TObject);
begin
	RefreshTimer.Enabled := ( autoQueryButton.Down );
end;

procedure TPutScanerMainForm.refreshTimerTimer(Sender: TObject);
begin
	if( FQueried )then
        exit;

    FQueried := true;

	QueryData();
end;

procedure TPutScanerMainForm.tickTimerTimer(Sender: TObject);
	{$J+}
	const
    	c_day_secs = 24*60*60;
        c_ticks : integer = 0;
        MEM_TICKS = 20;
	var
    	time : TDateTime;
begin
	c_ticks := c_ticks + 1;
    if( c_ticks = MEM_TICKS )then
    begin
		ShowMemoryUsage();
        c_ticks := 0;
    end;

	if( not FQueried )then
      if( RefreshTimer.Enabled )then
      begin
          timeGauge.Progress := timeGauge.Progress + 1;
          timeGauge.ForeColor := clGray;

          time := ( refreshTimer.Interval - timeGauge.Progress*tickTimer.Interval )/(c_day_secs*1000);
          timeGauge.Hint := '�������� ' + FormatDateTime('hh:nn:ss', time );

          ShowHint( 'Wait for next query...' );
      end;

    if( WatchDogTimer.Enabled )then
    begin
      timeGauge.Progress := timeGauge.Progress + 1;
      timeGauge.ForeColor := clBlue;

      time := ( watchDogTimer.Interval - timeGauge.Progress*tickTimer.Interval )/(c_day_secs*1000);
      timeGauge.Hint := '�� ����� ������ ' + FormatDateTime('hh:nn:ss', time );

      ShowHint( 'Wait for query finish...' );
    end;
end;

procedure TPutScanerMainForm.ShowMemoryUsage();
	{$J+}
    const
    	index : integer = 0;
        GRAPH_INTERVAL = 30;//intervals

	var
        i : integer;
    	smemUsage : string;
        memus : Currency;
        memusMB : currency;
begin
	memus := CurrentMemoryUsage;
    memusMB := memus/1048576;

	smemUsage := 'Usd. mem.: ' + Format('%6.3f',[memusMB]) + ' ��';

	StatusBar.Panels[3].Text := smemUsage;

    index := index + 1;
end;

procedure TPutScanerMainForm.trayIconClick(Sender: TObject);
begin
	Visible := not Visible;
    SkinManager.RepaintForms();
end;

procedure TPutScanerMainForm.Exit1Click(Sender: TObject);
begin
	Close();
end;

procedure TPutScanerMainForm.About1Click(Sender: TObject);
begin
	ShowAboutForm();
end;

procedure TPutScanerMainForm.ShowAboutForm();
	var
    	form : TAboutForm;
begin
	form := TAboutForm.Create(self);
    try
		form.ShowModal();    	
    finally
      FreeAndNil( form );
    end;
end;


procedure TPutScanerMainForm.watchDogTimerTimer(Sender: TObject);
begin
	if( FQueried )then
    begin
    	watchdogErrorPanel.Show();
        watchdogErrorPanel.Update();

    //TODO: Implement mindly logic:
    //		1. If put is not answered - setup flag "INVALID"
    //		2. For new invalid puts set "ByPassCount" to 3
    //		3. At each query loop decrement "ByPassCount" and no query "Invalid" puts
    //		4. When "ByPassCount" is zero drop flag "Invalid" for next query
    //		5. If all puts is invalid then kill OPC server and drop all puts "Invalid" flag
        try

       	  KillProcess( 'OpcDaVzljotServer.exe' );

          g_executor.Suspend();

          g_executor.OnError := nil;
          g_executor.OnInitProgress := nil;
          g_executor.OnFinalProgress := nil;
          g_executor.OnStepProgress := nil;
          g_executor.OnUnableToConnect := nil;
          g_executor.OnViewQueriedPut := nil;
          g_executor.OnLogOutput := nil;


          g_executor.Resume();
          g_executor.Terminate();

          g_executor := nil;
        except
        	on e:Exception do
            	Log('Error:' + e.message );
        end;

    	FQueried := false;
        timeGauge.Progress := 0;
        refreshTimer.Enabled := true;
        inProgressBar.Style := pbstNormal;
        progressBar.Position := 0;
        watchDogTimer.Enabled := false;
        queryAllNowButton.Enabled := false;

        ShowHint( 'Watchdog executed!');
        Log( 'Watchdog executed!' );
        ShowWorkState( 'Watchdog alarm' );

        watchdogErrorPanel.Hide();
    end;
end;

procedure TPutScanerMainForm.ShowWorkState( state : string );
begin
	statusBar.Panels[1].Text := state;
end;

procedure TPutScanerMainForm.ShowHint( message : string );
begin
	statusBar.Panels[2].Text := message;
end;



procedure TPutScanerMainForm.Contents1Click(Sender: TObject);
begin
	Application.HelpCommand(HELP_CONTENTS,0);
    //Application.HelpSystem.ShowTableOfContents();
end;

end.
