unit interface_putList;

interface

    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemList,
        interface_putData
        ;

    const
        IID_IPutList : TGUID = '{136CD0BC-199C-4FA7-A148-D677E52B367A}';

    type
      IPutList = interface( IMnemoschemeItemList )
            ['{136CD0BC-199C-4FA7-A148-D677E52B367A}']

              function getPutData(index: integer): IPutData;

              property Put[ index : integer ] : IPutData read getPutData;default;
      end;

implementation

end.
 