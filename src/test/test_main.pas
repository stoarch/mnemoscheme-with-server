{$define DEBUG_PUTREADING}
unit test_main;

interface
	uses
    	//--[ common ]--
        classes,
        //--[ forms ]--
        form_main,
        //--[ testing ]--
        GuiTesting, TestFramework
        ;

	type
        TMnemoschemeAccessor = class( TMnemoscheme );

    	TMnemoschemeTest = class( TGuiTestCase )
        	private
            	m_form : TMnemoschemeAccessor;

        	protected
            	procedure SetUp();override;

        	public
            	procedure TearDown();override;

            published
                procedure Test_ReadPutSettings();
        end;


implementation

	uses
    	//--[ common ]--
        sysUtils,
        //--[ data ]--
        data_put, data_putChanel,
        //--[ tools ]--
        tool_putDataSaver
        , list_put;

{ TMnemoschemeTest }

procedure TMnemoschemeTest.SetUp;
begin
	inherited;

    m_form := TMnemoschemeAccessor.Create(nil);
end;

procedure TMnemoschemeTest.TearDown;
begin
    FreeAndNil( m_form );

	inherited;
end;


procedure TMnemoschemeTest.Test_ReadPutSettings;
    var
        form : TMnemoschemeAccessor;
        list : TPutList;
begin
    form := TMnemoschemeAccessor.CreateForTest();

    try
      list := form.ReadPutSettings();

      CheckEquals( 18, list.count, 'Not all put is readen' );

    finally
      FreeAndNil( form );
    end;
end;

initialization
    RegisterTest( 'TMnemoschemeTest', TMnemoschemeTest.suite );

finalization

end.
 