object PutViewFrame: TPutViewFrame
  Left = 0
  Top = 0
  Width = 886
  Height = 753
  TabOrder = 0
  inline Channel1_View: TChannelViewFrame
    Left = 29
    Top = 98
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 0
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  object sToolBar1: TsToolBar
    Left = 0
    Top = 0
    Width = 875
    Height = 62
    ButtonHeight = 54
    ButtonWidth = 55
    Caption = 'sToolBar1'
    Images = ImageList
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    SkinData.SkinSection = 'TOOLBAR'
    object graphToolButton: TToolButton
      Left = 0
      Top = 2
      Hint = #1055#1086#1082#1072#1079#1072#1090#1100' '#1075#1088#1072#1092#1080#1082
      Caption = 'graphToolButton'
      DropdownMenu = graphPopupMenu
      ImageIndex = 0
    end
    object tableToolButton: TToolButton
      Left = 55
      Top = 2
      Hint = #1055#1086#1082#1072#1079#1072#1090#1100' '#1090#1072#1073#1083#1080#1094#1091
      Caption = 'tableToolButton'
      DropdownMenu = tablePopupMenu
      ImageIndex = 1
    end
    object lcmTitle: TiLCDMatrix
      Left = 110
      Top = 2
      Width = 483
      Height = 54
      CellsWidth = 3
      CellsShowOff = True
      Text = 'Gagarina - PUT 12'
      OuterMarginLeft = 2
      OuterMarginTop = 2
      OuterMarginRight = 2
      OuterMarginBottom = 2
      Characters = <
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '4,4,4,4,4,0,4'
        end
        item
          Data = '10,10,0,0,0,0,0'
        end
        item
          Data = '0,10,31,10,31,10,0'
        end
        item
          Data = '4,15,20,14,5,30,4'
        end
        item
          Data = '25,26,2,4,8,11,19'
        end
        item
          Data = '12,18,20,8,21,18,13'
        end
        item
          Data = '4,4,0,0,0,0,0'
        end
        item
          Data = '2,4,8,8,8,4,2'
        end
        item
          Data = '8,4,2,2,2,4,8'
        end
        item
          Data = '0,4,21,14,21,4,0'
        end
        item
          Data = '0,4,4,31,4,4,0'
        end
        item
          Data = '0,0,0,0,12,12,16'
        end
        item
          Data = '0,0,0,31,0,0,0'
        end
        item
          Data = '0,0,0,0,12,12,0'
        end
        item
          Data = '1,1,2,4,8,16,16'
        end
        item
          Data = '14,17,19,21,25,17,14'
        end
        item
          Data = '4,12,4,4,4,4,14'
        end
        item
          Data = '14,17,1,2,4,8,31'
        end
        item
          Data = '14,17,1,6,1,17,14'
        end
        item
          Data = '2,6,10,18,31,2,2'
        end
        item
          Data = '31,16,30,1,1,17,14'
        end
        item
          Data = '14,17,16,30,17,17,14'
        end
        item
          Data = '31,1,1,2,4,4,4'
        end
        item
          Data = '14,17,17,14,17,17,14'
        end
        item
          Data = '14,17,17,15,1,17,14'
        end
        item
          Data = '0,12,12,0,12,12,0'
        end
        item
          Data = '0,12,12,0,12,12,16'
        end
        item
          Data = '2,4,8,16,8,4,2'
        end
        item
          Data = '0,0,31,0,31,0,0'
        end
        item
          Data = '8,4,2,1,2,4,8'
        end
        item
          Data = '14,17,1,2,4,0,4'
        end
        item
          Data = '14,17,19,21,23,16,15'
        end
        item
          Data = '14,17,17,31,17,17,17'
        end
        item
          Data = '30,17,17,30,17,17,30'
        end
        item
          Data = '14,17,16,16,16,17,14'
        end
        item
          Data = '30,17,17,17,17,17,30'
        end
        item
          Data = '31,16,16,30,16,16,31'
        end
        item
          Data = '31,16,16,30,16,16,16'
        end
        item
          Data = '14,17,16,19,17,17,14'
        end
        item
          Data = '17,17,17,31,17,17,17'
        end
        item
          Data = '14,4,4,4,4,4,14'
        end
        item
          Data = '1,1,1,1,17,17,14'
        end
        item
          Data = '17,18,20,24,20,18,17'
        end
        item
          Data = '16,16,16,16,16,16,31'
        end
        item
          Data = '17,27,21,21,17,17,17'
        end
        item
          Data = '17,25,21,19,17,17,17'
        end
        item
          Data = '14,17,17,17,17,17,14'
        end
        item
          Data = '30,17,17,30,16,16,16'
        end
        item
          Data = '14,17,17,17,17,14,1'
        end
        item
          Data = '30,17,17,30,17,17,17'
        end
        item
          Data = '14,17,16,14,1,17,14'
        end
        item
          Data = '31,4,4,4,4,4,4'
        end
        item
          Data = '17,17,17,17,17,17,14'
        end
        item
          Data = '17,17,17,17,17,10,4'
        end
        item
          Data = '17,17,17,17,21,27,17'
        end
        item
          Data = '17,10,4,4,4,10,17'
        end
        item
          Data = '17,17,17,10,4,4,4'
        end
        item
          Data = '31,1,2,4,8,16,31'
        end
        item
          Data = '12,8,8,8,8,8,12'
        end
        item
          Data = '0,16,8,4,2,1,0'
        end
        item
          Data = '6,2,2,2,2,2,6'
        end
        item
          Data = '4,10,17,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,31'
        end
        item
          Data = '6,4,2,0,0,0,0'
        end
        item
          Data = '0,0,14,1,15,17,15'
        end
        item
          Data = '16,16,30,17,17,17,30'
        end
        item
          Data = '0,0,15,16,16,16,15'
        end
        item
          Data = '1,1,15,17,17,17,15'
        end
        item
          Data = '0,0,14,17,31,16,14'
        end
        item
          Data = '3,4,31,4,4,4,4'
        end
        item
          Data = '0,0,15,17,15,1,14'
        end
        item
          Data = '16,16,22,25,17,17,17'
        end
        item
          Data = '4,0,12,4,4,4,14'
        end
        item
          Data = '2,0,6,2,2,18,12'
        end
        item
          Data = '16,16,18,20,24,20,18'
        end
        item
          Data = '12,4,4,4,4,4,14'
        end
        item
          Data = '0,0,26,21,21,21,21'
        end
        item
          Data = '0,0,22,25,17,17,17'
        end
        item
          Data = '0,0,14,17,17,17,14'
        end
        item
          Data = '0,0,30,17,30,16,16'
        end
        item
          Data = '0,0,15,17,15,1,1'
        end
        item
          Data = '0,0,11,12,8,8,8'
        end
        item
          Data = '0,0,14,16,14,1,30'
        end
        item
          Data = '4,4,31,4,4,4,3'
        end
        item
          Data = '0,0,17,17,17,19,13'
        end
        item
          Data = '0,0,17,17,17,10,4'
        end
        item
          Data = '0,0,17,17,21,21,10'
        end
        item
          Data = '0,0,17,10,4,10,17'
        end
        item
          Data = '0,0,17,17,15,1,14'
        end
        item
          Data = '0,0,31,2,4,8,31'
        end
        item
          Data = '3,4,4,8,4,4,3'
        end
        item
          Data = '4,4,4,4,4,4,4'
        end
        item
          Data = '24,4,4,2,4,4,24'
        end
        item
          Data = '8,21,2,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '6,9,28,8,28,9,6'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '0,0,0,0,0,0,0'
        end
        item
          Data = '4,0,4,4,4,4,4'
        end
        item
          Data = '4,15,20,20,20,15,4'
        end
        item
          Data = '6,9,8,30,8,8,31'
        end
        item
          Data = '0,17,14,17,14,17,0'
        end
        item
          Data = '17,10,31,4,31,4,4'
        end
        item
          Data = '4,4,4,0,4,4,4'
        end
        item
          Data = '14,17,12,19,14,1,30'
        end
        item
          Data = '10,0,0,0,0,0,0'
        end
        item
          Data = '14,17,23,25,23,17,14'
        end
        item
          Data = '4,2,14,10,6,0,14'
        end
        item
          Data = '0,0,9,18,9,0,0'
        end
        item
          Data = '0,0,0,14,2,0,0'
        end
        item
          Data = '0,0,0,14,0,0,0'
        end
        item
          Data = '14,17,29,27,29,27,14'
        end
        item
          Data = '31,0,0,0,0,0,0'
        end
        item
          Data = '6,9,6,0,0,0,0'
        end
        item
          Data = '0,4,14,4,0,14,0'
        end
        item
          Data = '12,2,4,8,14,0,0'
        end
        item
          Data = '12,2,4,2,12,0,0'
        end
        item
          Data = '7,6,8,0,0,0,0'
        end
        item
          Data = '0,17,17,17,25,22,16'
        end
        item
          Data = '15,29,29,13,5,5,5'
        end
        item
          Data = '0,0,0,4,0,0,0'
        end
        item
          Data = '0,0,0,0,4,2,12'
        end
        item
          Data = '4,12,4,4,4,0,0'
        end
        item
          Data = '14,10,14,0,14,0,0'
        end
        item
          Data = '0,0,18,9,18,0,0'
        end
        item
          Data = '9,10,4,11,21,7,1'
        end
        item
          Data = '17,18,4,14,17,2,7'
        end
        item
          Data = '24,9,26,5,11,23,1'
        end
        item
          Data = '4,0,4,8,16,17,14'
        end
        item
          Data = '8,4,14,17,31,17,17'
        end
        item
          Data = '2,4,14,17,31,17,17'
        end
        item
          Data = '4,10,14,17,31,17,17'
        end
        item
          Data = '13,18,14,17,31,17,17'
        end
        item
          Data = '10,0,14,17,31,17,17'
        end
        item
          Data = '4,0,14,17,31,17,17'
        end
        item
          Data = '11,20,20,22,28,20,23'
        end
        item
          Data = '15,16,16,16,15,2,14'
        end
        item
          Data = '8,4,31,16,30,16,31'
        end
        item
          Data = '2,4,31,16,30,16,31'
        end
        item
          Data = '4,10,31,16,30,16,31'
        end
        item
          Data = '10,0,31,16,30,16,31'
        end
        item
          Data = '8,4,14,4,4,4,14'
        end
        item
          Data = '2,4,14,4,4,4,14'
        end
        item
          Data = '4,10,14,4,4,4,14'
        end
        item
          Data = '10,0,14,4,4,4,14'
        end
        item
          Data = '14,9,9,29,9,9,14'
        end
        item
          Data = '13,18,17,25,21,19,17'
        end
        item
          Data = '8,4,14,17,17,17,14'
        end
        item
          Data = '2,4,14,17,17,17,14'
        end
        item
          Data = '4,10,14,17,17,17,14'
        end
        item
          Data = '13,22,14,17,17,17,14'
        end
        item
          Data = '10,0,14,17,17,17,14'
        end
        item
          Data = '0,0,0,10,4,10,0'
        end
        item
          Data = '1,14,19,21,25,14,16'
        end
        item
          Data = '8,4,17,17,17,17,14'
        end
        item
          Data = '2,4,17,17,17,17,14'
        end
        item
          Data = '4,10,17,17,17,17,14'
        end
        item
          Data = '10,0,17,17,17,17,14'
        end
        item
          Data = '2,4,17,17,10,4,4'
        end
        item
          Data = '28,8,14,9,14,8,28'
        end
        item
          Data = '14,17,30,18,17,25,22'
        end
        item
          Data = '8,4,14,1,15,17,15'
        end
        item
          Data = '2,4,14,1,15,17,15'
        end
        item
          Data = '4,10,14,1,15,17,15'
        end
        item
          Data = '13,18,14,1,15,17,15'
        end
        item
          Data = '10,0,14,1,15,17,15'
        end
        item
          Data = '4,0,14,1,15,17,15'
        end
        item
          Data = '0,0,26,5,15,20,15'
        end
        item
          Data = '0,15,16,16,15,2,14'
        end
        item
          Data = '8,4,14,17,31,16,15'
        end
        item
          Data = '2,4,14,17,31,16,15'
        end
        item
          Data = '4,10,14,17,31,16,15'
        end
        item
          Data = '10,0,14,17,31,16,15'
        end
        item
          Data = '8,4,0,4,4,4,4'
        end
        item
          Data = '2,4,0,4,4,4,4'
        end
        item
          Data = '4,10,0,4,4,4,4'
        end
        item
          Data = '0,10,0,4,4,4,4'
        end
        item
          Data = '19,12,18,14,17,17,14'
        end
        item
          Data = '13,18,0,22,25,17,17'
        end
        item
          Data = '8,4,0,14,17,17,14'
        end
        item
          Data = '2,4,0,14,17,17,14'
        end
        item
          Data = '4,10,0,14,17,17,14'
        end
        item
          Data = '13,22,0,14,17,17,14'
        end
        item
          Data = '10,0,0,14,17,17,14'
        end
        item
          Data = '0,4,0,31,0,4,0'
        end
        item
          Data = '1,14,17,17,17,14,16'
        end
        item
          Data = '8,4,0,17,17,19,13'
        end
        item
          Data = '2,4,0,17,17,19,13'
        end
        item
          Data = '4,10,0,17,17,19,13'
        end
        item
          Data = '10,0,0,17,17,19,13'
        end
        item
          Data = '2,4,17,17,15,1,6'
        end
        item
          Data = '16,28,18,18,28,16,16'
        end>
    end
  end
  inline Channel2_View: TChannelViewFrame
    Left = 463
    Top = 98
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 2
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1090#1088#1091#1073#1086#1087#1088#1086#1074#1086#1076
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  inline Channel4_View: TChannelViewFrame
    Left = 463
    Top = 347
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 3
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1090#1088#1091#1073#1086#1087#1088#1086#1074#1086#1076
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  inline Channel3_view: TChannelViewFrame
    Left = 29
    Top = 347
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 4
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  inline Channel6_view: TChannelViewFrame
    Left = 463
    Top = 596
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 5
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Caption = #1054#1073#1088#1072#1090#1085#1099#1081' '#1090#1088#1091#1073#1086#1087#1088#1086#1074#1086#1076
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  inline Channel5_view: TChannelViewFrame
    Left = 29
    Top = 596
    Width = 412
    Height = 231
    Color = 15266548
    ParentColor = False
    TabOrder = 6
    inherited lblCaption: TPDJRotoLabel
      Height = 231
      Align = alLeft
    end
    inherited temperatureGroup: TsGroupBox
      Left = 32
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited temperatureGauge: TiThermometer
        PositionMax_2 = 120.000000000000000000
      end
    end
    inherited pressureGroup: TsGroupBox
      Left = 146
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited pressureGauge: TiAngularGauge
        PointerStyle = iagpsTriangle
        PositionMax_2 = 1.600000000000000000
        SectionEnd1_2 = 0.300000000000000000
        SectionEnd2_2 = 0.700000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 9
            Margin = -15
            Color = clBlue
            DisabledColor = clGray
            Style = 3
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
    inherited flowRateGroup: TsGroupBox
      Left = 314
      Font.Color = clBlue
      SkinData.CustomFont = True
      inherited flowRateGauge: TiLinearGauge
        PositionMax_2 = 1500.000000000000000000
        SectionEnd1_2 = 300.000000000000000000
        SectionEnd2_2 = 1000.000000000000000000
        SectionEnd3_2 = 0.000000000000000000
        SectionEnd4_2 = 0.000000000000000000
        Pointers = <
          item
            Visible = True
            Enabled = True
            Size = 10
            Margin = 5
            Color = 4194304
            DisabledColor = clGray
            Position = 0.000000000000000000
            UserCanMove = False
          end>
      end
    end
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 577
    Top = 353
  end
  object ImageList: TImageList
    Height = 48
    Width = 48
    Left = 577
    Top = 411
    Bitmap = {
      494C010102000400040030003000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000C00000003000000001001000000000000048
      0000000000000000000000000000000000000000FF7FFF7FFF7FFF7FFE7FFF7F
      FF7FDF7FDF7BDE7BFE7BFE77FE77DE7BDE7BFE7BDD77FE7BDE7BDE7BDE7FDE7F
      DE7BFF7FDD77DE7BFF7FDE77DE77FE7BDE7BDE7BDE7BFE7BDE77FF7BDE7BDD7B
      FE7FFF7FFF7FFF7FFF7FFF7BFF7BFF7FFF7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFE7FFE7BBD77
      9D735B6F5B6B5A6B59637B675A6759675A6B59677B6B5A6B5A675A6B5A675A67
      5A6B39677B6B5A6739635A675A6B5A675A675A6B5A6B5A675A675A635A677A6F
      7A6F9C73DE7BDF7FFE7BFF7FFF7FFF7FFF7F0000000000000000000000000000
      D65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65A
      D65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65A
      D65AD65A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFE7FDD7B9B733863
      D6569552744E734A744A74467446734A734A7346734A534A544A744A744A744A
      7446744A73467446944A744A744A734A734A534A534A7346944A744A5346744E
      9552D75A19639C73DD7BFF7FFF7FFF7FFF7F0000000000000000000000002104
      1863BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77
      BD77BD77BD77BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7FFF7FFF7FBD775A6BB5525342
      323A323A7442333A753E7436743A743E543E753E543E553E543E533A743E743E
      743E543E743E533E333A543E543E743E753E543E543E543A543A543A75425442
      333E323A7442B6525A6BDE7BFF7FFF7FFF7F0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77DE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FDF7FDF7B5B67B64E543EB742
      1A47FA423B473C433C435C3F3C3F3C433C471C433C471C433C433B471A43193F
      1A3F193F19431A471B4B1C431C433C3F3C3F1C433C475D433C3F3C3F3C43FA42
      F9421A43D7423236B54E5B6BBE77FF7FFF7F0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77BD77
      BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7F7C6FD856753E3B4F1A47
      1A43DF531B373D3B3D375D333D333D373D3B3D3B3D3B3D373D3BD936B7325B3F
      5B3F5C433B437D4F9F535E433D375E373D373E371D373D373D335D373C333D43
      7F4F1B3F193F5B53B546B6529D73DF7BFF7F0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67DE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7FDD773A6395461A4FBF5BBF57
      3B43FF5B3C3B3C333D333D2F3C2F5D333D2F3C2F5D331C2F3D3BDF5F9F5B9E53
      DF575C479E4F9E575D4F5E3B3D333C2F3D333D333D333D333C2F5D333C333D43
      D93ABF57BF5739473953954A185FDD77FE7F0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77EBD77BD77BD77BD77BD77BD77BD77BD77DE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7FBC6FD74E7D573C479F4F9F4F
      9F4B5D3F9F433D2F3D2B1C2B3D2F3D2F1D2B5D2F1B273D2F1C339F577E533C43
      BF4B5D3F7E479F539F4F5F3B3D2F1C2F3C2F3D2B3D2B3D2B1C2B3C2B1C33BF4F
      3D431B3F9F4F9E4F5B4B7D5BF8529B6BFD7B0000000000000000000000002104
      1863DE7B9452EF3D9C73BD77F77EBD77BD77BD77BD77BD77BD77BD77DE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7F7C6BD842FA3A1C2F1C2F9F3F
      3D331C2F1D2B1E271E271D2B1D2FFD2A1D2B3C271C271D2B1D2F3E3BBF4BFC2E
      1D2B1D2B1C2B1B2F1C2F1D2B1D2B1D2B1C2B1D271D271D2B1D2B1C271D2F9F47
      9F4B5F3FFC321C33FB32FB36D93E9C63FE770000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77EBD77BD77BD77BD77BD77BD77DE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7F5B5FFB3E3D2F3E2B3E2B5E33
      1D2B3E2B3E2B5F2B3F273F2B3F2F3F2F3E2B3D275F2B3E2B1E2B3E2F5F333E27
      3F273F273F2B3E2B3E2F3F2B3F2B3E2B5E2B3D275F2B3F2B3F2B5F2B3D2B3D33
      1D2F3E331E2B3E2B3E2F3E331C3B5C5BFE770000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67DE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7F3B571B331D273E233E231D27
      1D271E271E231E231E233E231E235F2B1D231D233E271E231E233E231D1F3F23
      3F1F1F231F231E271E271E271E233E1F3D1F3E237F2F3E231E1F3E233D233D23
      3D231D1F3E231E1F3F231F23FC2E3A53FF7B0000000000000000000000002104
      1863DE7B7B6F3967BD77BD77F77EBD77BD77BD77BD77DE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B1A53DA26FC1AFD16DC16FC1E
      DC1EDC1AFC1AFC1ADC1AFD1AFC1A5E27FB1AFC1ADC16DD1ADD1A1D1BFC16FD16
      FD16FD1AFD1ADD1ADD1ADD1ADD1AFD16FC16DC1A3F27FD1ADD16DC1AFC1AFB16
      FC16FC1A1E1BFD16FE16FE16DB261A53DE7B0000000000000000000000002104
      1863DE7BF75E734E9C73BD77F77EBD77BD77BD77DE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B3A57DA26FD16FD16DC16FD1A
      DC16DC16DC16DC1ADC1ADD1ADC165F2BDB1EFD1ADD16FE1ADD1ADC16FC16FD16
      FD16FC16DC16DC16DC1ADD1ADD16DD16DD1ADC1A3F2BFE1EFE1ADC1ADC1ADC1A
      DC16DC16DD16DD1ADD16FD16BA221A4FDE7B0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77EBD77BD77DE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD7B3A53BA26BC16DC16DC16DC16
      DC16DC16DC16DC1ABC1ADD1ABC1A3F2BBB1EBC1ADD16DC16DC16BC16BC16DC16
      DC16DC16DC16DC1ADC1ADC16DD16DD1ADC1ABC1E1F2BBC1ABD1ABC1ADC1ADC1A
      DC1ADC1ABC16DD1ADD16DD16BA1E1A53DE7B0000000000000000000000002104
      1863DE7BBD77BD77BD77BD77F77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B3A539A269C1ABC1ABC16BC16
      BC1ABC16BC16BB1ABB1ABD169C1A1E2BBA1EBB1ABC1ABB16BC1ABC1EBC1ABC1E
      BB1ABC1ABC1ABC1ABC1ABC1ABC1ABC1ABB1ABB1A1E2BBC1EBC1ABC1ABB1ABC1A
      BB16BC1ABC1ABC1ABC16BC1A9A221A53DF7B0000000000000000000000002104
      1863DE7B1863734EBD77BD77F77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD7B3A5799269C1EBD1E9B1ABC1A
      BB1ABC1A9C1ABC1ABC1E9D1A9C1E1E2FBB22BB1E9B1ABC1E9B1ABC1E9B1E9B1E
      BC1E9C1E9C1E9C1E9C1EBC1ABB1ABC1ABB1E9A1E1E2FBB1E9B1ABC1E9B1ABB1A
      BC1E9C1ABC1EBC1A9C1ABC1E9926FA4EDF7B0000000000000000000000002104
      1863DE7B5A6B1863BD77BD77F77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD7B3A57992A7B1A9B1A9B1E791A
      9B229B1E9B1E9C1E9C1E9C1E9B1EFD2A9A1E9B1E7B1A9B169B1A9B1E7A1E9B22
      7B1E9B1E9B1E9B1E7B1E9B1E9B1E7B1A9B1E9A1EFD2A9B1E9B1A7A1A9A1EBB1E
      9B1E9B1E7B1A9B1E9B1E791A7826FA4EDF770000000000000000000000002104
      1863DE7BBD77BD77BD77DE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B1A5758267A1A9A1A9A1E9A1E
      7A1E7B227A1E7B1A7C1E7B1E7A1EFD2E791E7A1A9B1E9B1E7B1E7A1E7A1E7A1E
      7B1E7A1E791A7A1A7B1E7B1E7B1E7A1E7A1E7A1EDD2E7A1E9A1E9A1E791A7A1A
      7A1A7B1E7B1E7A1A7A1E7A1E782AFA4EDF770000000000000000000000002104
      1863DE7BDE7BDE7BBD77DE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7BFA52792A9C229B1E9B1E7A1A
      7B1E7B229B229C229C227B227B22FE3679229A227A1E9A227A1E7B227B227C22
      9B1E9A229A1E9B1A9B1A7A1E7A229B227B1E7A22FE327A227A1E9A229A1E9B1E
      9B1E9B1A9C227A1E7B229A1E7826FA4EDF770000000000000000000000002104
      1863DE7BB556EF3D9C73DE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF771B5738265A1E591A5A1A7B1E
      5A1E591A5A1E5A1A5A1E5A1E591EDD32581E581E791E581E79225A1E5A1E3A1A
      5A1E581A791A7916791A591E591E7A1E591A591EDD2E591E591E591E591A5A1A
      5A1A5A1A5A1A591E591E591A5826FA4EFF7B0000000000000000000000000000
      1863DE7BBD77BD77DE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7B1B57592E5A2279227922591E
      591E7A227A225A1A5A1E7B225922DD32592659225922591E791E7A2259227A22
      7A227B1E7B1E5A1E5A1E7A2659225A1E7A225922DC325A227B225A22591E7A22
      7A227B225A1E5A225A22591E582AF94EDF770000000000000000000000000000
      1863DE7BDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B1957362E592A792E792E792E
      592659267A269B227A1E7A265A26DD32792659267A267A227A225822792A792E
      59265A227B267B265A265A265A265A227A267922FD325A225A225A227B267A26
      5922592A59267A267B265A22782EF952FF7B0000000000000000000000000000
      1863FF7FBD77BD77DE7BDE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DD7B5A67B84A7F5FFF6BFF6BDF63
      DC42382A161E3822371E381E381EBB3237223822381E381E37225732DF67FF67
      3F4F3626161E3822382218223926391E381E381EBC2E39221822381E5822161E
      582A783678361726181E381E3626D852DE7B0000000000000000000000000000
      1863FF7FB55610429C73DE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FD7F19637E675E5B36361532BB46
      FF6B3F535832582E792A7A2A7A2ADC3A782E592A792A792A782EDF671C53783A
      DF635F537832592A7A2A5A2A7B2A5A267B2A7A2AFD3A7A2E7A2E792A592A7936
      BF5FFF73DF67BB3A592A7B2A582EF956DE7F0000000000000000000000000000
      1863FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7B1A5BB321B51DB41DB521B521
      3836FF6FBA46372A592A392638261D4F5F579A3A372E362E1E4FBF67B425B41D
      382EFF6BB93E382659225A2639225A265926382ABB3A382A382A3826572A7F5F
      7F5FD5291E4BBF6357323822372AF956DE7F0000000000000000000000000000
      1863FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7BFA56B41DB619D61DB61D9619
      D6219942FF737836162617269B3AFF6F3E57FF6FBB46773ADF6B9B42D621F721
      F6211D4FDF67782E592A58265926592637267836FF6B5F53362E362ADB42DF6B
      F431B41DD521DF67DA423726372AD852DF7F0000000000000000000000000000
      1863FF7F3967D65ABD77DE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7BFB5AB425B519D61DB619B619
      B51DB31D7F579F639942BA429F5FDF67152E1D53DF6BBF67BF5F372A38223922
      38227832FF675E53DB3EFD3EFC3AFC3AFC42BF63FF6FDF6B7F5B983EDF63FD4A
      B521B61DB51DDB4ADF63992E7832F956DE7F0000000000000000000000000000
      1863FF7F3967F75EBD77DE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      DE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7BFA5AB421B519B519D619F71D
      172238267932FF6B3D57B946FF6F3F53162A5736DF67FF6BDA3E582639225926
      382637261D4BFF67983A9932BA369832B83AFF6F1B57FA4EFF6B3C53FF6B5932
      1826F821F721152EFF6778321426D856DF7F0000000000000000000000000000
      1863FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7B
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7FFA56B41DF719171E39265926
      5A267A2A592AFB42FF6BFF6F9F63DC42792E372A9A36BA36572A59265926592E
      572A582A9936FF6B3D53DB3EBB3ABA3A7E57DF6FDA4E5636BF63FF6F3D4F582E
      5A2A7B2A392A572EDF633D4F55361A5FBF7B0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BDE7BDE7BFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DF7FFA5A16265A267A2A7B2E7A2A
      7A2A5A267B2E782EDA3E1D4F5736FD42792A792A792A792E792E7A2A7B2A5932
      582E792E792E5E57DF67FD461D4BFB4ABF675E5BFB4A58329A361D4F5732582E
      792A5A2A7B2E582E3D53DF67D84A3B63DF7F0000000000000000000000000000
      1863FF7FB5561042BD77DE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B1A5B58369B327A329A2E7A2E
      7B2E9B327A2E7A2E792E993679321D4779329A2E7A2E99329A327A2A7B2A7B32
      7A2E7A2E782EBA3EFF6FFC4A9942D94EFF73D942FC4699327A327A32792E9A32
      9A2E7B2E7A327A32BA3EFF739C673967DF7F0000000000000000000000000000
      3967FF7F9C737B6FDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B1A5B993A9B329A329A329B32
      9B369B329B367A329B327A329A361D4799369A329B329A327A329C329C329C32
      9B329A32993699369F5FFF6BFB529F67DF6B983A1D479A369A329A32BC329A32
      9A2E7A329B367A3279367E5B5B5F185FDE7F0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B195BBA3E9B369B369B3A9A36
      BB369B369B369B369B369B369A363E4B993A9A369A369A36BA36BB36BB36BB36
      BB369A369A36993AFC46FF6F7E5BFF733D57983A3E4BBB3A9B36BB369B36BB36
      BB329A369A369B3A9A369A3A983EF85AFE7B0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BDE7BDE7BFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B195BBA42BC3ABB3ABB3EBB3A
      BB3ABC36BB36BB3ABB3ABB3ABB3A3F4FBA3EBA3ABB3ABB3ABB3ABB3ABB3ABB3A
      BB3ABB3ABB3E9A3A993E5F57FF73BF67B942993E3E4FBB3EBB3ABB3EBB3ABB36
      BB36BB3ABA3ABB3ABB3ABC3ABA3E1957FE7B0000000000000000000000000000
      3967FF7FB556EF3DBD77DE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7B195BDB42BC3EBC3EDC42BB3E
      DB3EDC3EDC3ABC3EBB3EBC3EBB3E5F53BB42DB3EDB3EDC3EDC3EDC3EDC3EDC3E
      DC3EBC3EDC3EBB3EDB42DB42FC4ABA46B942BA425F53DC3EBC3EDC42BB3EDC3A
      DB3ADB3EBB3EDB3EBC3EDC3EBA42195BDE7B0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BDE7BF77EDE7BDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B195FDB46DC42DC42DC46DB42
      DC42DC42DC3EDC42DC42DC42DC425F53DB46DB42DB42DC42DC42DC42DC42DC42
      DC42DC42DC42BB3EDB42DB46DB46FC4ADB46DA465F53DC42DC42DC42DB42DC3E
      DC3EDB42DB42DC42DC42DC42DB46195BDE7B0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BDE7BF77EDE7BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B3A5FFB4AFC46DC46FC46DC46
      FC46FC42FC42FC46DC46DC46FC465F57FB4ADB46DC46FC46FC46FC46FC46FD46
      FD46FD46DC46FC46FD46DC46DC46DB46DB46DB4A5F57FC46DC46DC46FC46FC42
      FC42FC46DB46DC46DC46FD46DB4A195FDE7B0000000000000000000000000000
      3967FF7FBD777B6FDE7BDE7BF77EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B3A5FFB4EFC4AFC4AFC4AFC4A
      FC4AFC46FC46FC4AFC46FC4AFC4A5F57FB4AFB4AFC4AFC4AFC4AFC4AFC4AFC46
      FC46FC46FC46FD4AFC46FC461D4BFC4AFC4AFB4A5F5BFC4AFC46FC4AFC4AFC46
      FC46FC4AFC4AFC4AFC4AFD4AFB4A1A5FDE7B0000000000000000000000000000
      3967FF7FD65A524ABD77DE7BF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67FF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DE7B3A63FB52FC4EFC4EFC4EFC4E
      FC4A1C4BFC4A1C4FFC4AFD4EFC4E5F5BFB4EFC4EFC4AFC4AFC4AFC4AFC4AFC4A
      FC4A1D4FFC4AFC4AFC4A1D4BFC4A1D4BFC4AFB4E5E5B1C4FFC4AFC4EFC4E1C4B
      1C4B1C4FFC4EFC4EFC4EFD4EFB4E3A5FFE7B0000000000000000000000000000
      3967FF7FDE7BDE7BDE7BFF7FF77EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FE7F3A631B571C531C531D531C4F
      1C4F1D4F1C4F1D53FC4E1D531D537F5F1C531C4F1C4F1C4F1C4F1C4F1C4F1C4F
      1C4F1C4F1C4F1D531D531C4F3D531C4F1D531C537F5F1D531C4F1C4F1D531D4F
      1D4F1C4F1C4F1D531C531D53FB523B63FF7F0000000000000000000000000000
      3967FF7FDE7BDE7BFF7FFF7FF77EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7F5B673C5B1C571D571C531C53
      1C533D531D531D531D531D531D539F631C531C531C531C531C531C531C531C53
      1C531C531C531C531C531D531D531D531C531C539F631D571D531D531D531D53
      1D531C531C531D531D571C533C5B3B63FF7B0000000000000000000000000000
      3967FF7F39679452DE7BFF7FF77EFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FBE731A5B3C5B3D5B1C573D5B
      3D573D573D573D573D573D571D579F633C573D573D573D573D573D573D573D57
      3D573D573D573D573D573D573D573D573D571C579F633D573D571D573D573D57
      3C573D573D571D571D573D5B1B5B9E6FFF7F0000000000000000000000000000
      3967FF7F7B6F3967DE7BFF7FF77E7E677E677E677E677E677E677E677E677E67
      7E677E677E677E677E677E677E677E677E677E677E677E677E677E677E67DE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7B3B633C5F3D5F3C5B3D5B
      3C5B3D5B3D5B3D5B3D5B3D5B3D5B9F673D5B3D5B3D5B3D5B3D5B3D5B3D5B3D5B
      3D5B3D5B3D5B3D5B3D5B3D5B3D5B3D5B3D5B3C5B9F673D5B3D5B3D5B3D5B3D5B
      3D5B3D5B3D5B3D5F3D5B3C5F3C5FFF7BFF7F0000000000000000000000000000
      3967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7B
      FF7FD65A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FBE733C635D635D5F5D5F
      5C5F5D5F5D5F3D5F5D5F5D5F5D5F9F675D5F5E5F5E5F5E5F5E5F5E5F5D5F5E5F
      5D635D635D635D635D635D635D635D635D635D639F6B5D5F5D5F5D5F5D5F5D5F
      5D5F5C5F5D5F5D633C5F3C63BF73FF7BFF7F0000000000000000000000000000
      3967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7B
      FF7FD65A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7F7D6B5D675D635D63
      5D635D635D635D635D635D635D637E6B5D635D635D635D635D635D635D635D63
      5D635D635D635D635D635D635D635D635D635D639E6B7D675D635D635D635D63
      5D637D635D635D635C677D6BFF7BFF7FFF7F0000000000000000000000000000
      3967FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7BDE7BDE7B
      FF7FD65A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7B9E6F7D675C67
      7D677D677D677D675D675D677D677D6B7D677D677D677D677D677D677D677D67
      7D677D677D677D677D677D677D677D677D675D677D6B7D675D677D677D677D67
      7D677D677D677D679E6FDF7BFF7FFF7FFF7F0000000000000000000000000000
      5A6BFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FDE7BDE7B
      FF7FF75E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7BBF739E6F
      9E6B9D6B9D6B9E6B7E6B7E6B7E6F7D6B9D6B7D6B7D6B7D6B7D6B7D6B7D6B7D6B
      7D6B7D6B7D6B7D6B7D6B7D6B7D6B7D6B7D6B7D6B7D6B7E6F7E6B7E6B9E6B9D6B
      9D6B9D6B9E6FBF73DF7BFF7FFF7FFF7FFF7F0000000000000000000000000000
      D65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65A
      D65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65AD65A
      D65AD65A00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7FFF7FFF7B
      FF77DF77DF77DF77DF77DF77DF77BF77DF77DF77DF77DF77DF77DF77DF77DF73
      DF73DF73DF73DF73DF73DF77DF77DF77DF77DF77DF77DF77DF77DF77DF77DF77
      DF77DF77FF7BFF7FFF7FFF7FFF7FFF7FFE7F0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000C0000000300000000100010000000000800400000000000000000000
      000000000000000000000000FFFFFF00800000000000FFFFFFFFFFFF00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000003F000000000000000000000000000000000000FC00
      0000003F000000000000000000000000000000000000FC000000003F00000000
      0000000000000000000000000000FC000000003F000000000000000000000000
      000000000000FC000000007F000000000000000000000000000000000000FFFF
      FFFFFFFF00000000000000000000000000000000000000000000000000000000
      000000000000}
  end
  object graphPopupMenu: TPopupMenu
    Left = 506
    Top = 261
    object temperatureGraphMenuItem: TMenuItem
      Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072
      OnClick = temperatureGraphMenuItemClick
    end
    object pressureGraphPopupMenu: TMenuItem
      Caption = #1044#1072#1074#1083#1077#1085#1080#1077
      OnClick = pressureGraphPopupMenuClick
    end
    object FlowRateGraphMenuItem: TMenuItem
      Caption = #1056#1072#1089#1093#1086#1076
      OnClick = FlowRateGraphMenuItemClick
    end
  end
  object tablePopupMenu: TPopupMenu
    Left = 498
    Top = 312
    object temperatureTableMenuItem: TMenuItem
      Caption = #1058#1077#1084#1087#1077#1088#1072#1090#1091#1088#1072
      OnClick = temperatureTableMenuItemClick
    end
    object pressureTableMenuItem: TMenuItem
      Caption = #1044#1072#1074#1083#1077#1085#1080#1077
      OnClick = pressureTableMenuItemClick
    end
    object FlowRateTableMenuItem: TMenuItem
      Caption = #1056#1072#1089#1093#1086#1076
      OnClick = FlowRateTableMenuItemClick
    end
  end
end
