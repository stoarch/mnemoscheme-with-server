unit class_mnemoschemeItemChanelList;

interface
    uses
        //--[ classes ]--
        class_creatableInterfaceList,
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChanelList
        ;

    type
        TMnemoschemeItemChanelList = class( TCreatableInterfaceList, IMnemoschemeItemChanelList )
        protected
            function MakeData() : IMnemoschemeItemChanelData;virtual;
        public
              constructor Create();reintroduce;overload;

              procedure Add( value : IMnemoschemeItemChanelData );reintroduce;overload;virtual;
              procedure Assign( const data : IMnemoschemeItemChanelList );
              function FindById( index : integer ) : IMnemoschemeItemChanelData;

              function GetCount() : integer;

              function getChanelData(index: integer): IMnemoschemeItemChanelData;
              procedure setChanelData(index: integer; const Value: IMnemoschemeItemChanelData);

              property Chanels[index : integer] : IMnemoschemeItemChanelData read getChanelData write setChanelData;
        end;

implementation
    uses
        //--[ common ]--
        SysUtils
        ;

{ TMnemoschemeItemChanelList }

procedure TMnemoschemeItemChanelList.Add(
  value: IMnemoschemeItemChanelData);
	var
    	adata : IMnemoschemeItemChanelData;
begin
	adata := MakeData();
    adata.Assign(value);

	inherited Add( adata );
end;

procedure TMnemoschemeItemChanelList.Assign(
  const data: IMnemoschemeItemChanelList);
	var
    	i : integer;
        adata : IMnemoschemeItemChanelData;
begin
	Clear();

    for i := 1 to data.Count do
    begin
    	adata := MakeData();
        adata.Assign( data[i] );
        Add( adata );
    end;
end;

constructor TMnemoschemeItemChanelList.Create;
begin
  inherited Create();
end;

function TMnemoschemeItemChanelList.FindById(
  index: integer): IMnemoschemeItemChanelData;
	var
    	i : integer;
begin
	result := nil;

    for i := 1 to count do
    begin
		if( Chanels[i].Index = index )then
        begin
        	result := Chanels[i];
            break;
        end;
    end;
end;

function TMnemoschemeItemChanelList.getChanelData(
  index: integer): IMnemoschemeItemChanelData;
begin
	result := Values[index] as IMnemoschemeItemChanelData;
end;

function TMnemoschemeItemChanelList.GetCount: integer;
begin
    result := inherited Count;
end;

function TMnemoschemeItemChanelList.MakeData: IMnemoschemeItemChanelData;
begin
    raise Exception.Create( 'Not implemented MakeData' );
end;

procedure TMnemoschemeItemChanelList.setChanelData(index: integer;
  const Value: IMnemoschemeItemChanelData);
begin
	Values[index] := value;
end;

end.
