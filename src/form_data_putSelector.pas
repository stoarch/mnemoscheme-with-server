unit form_data_putSelector;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  sComboBox, sButton, sSkinProvider,
  //--[ data ]--
  data_put,
  //--[ list ]--
  list_put, sEdit, sSpinEdit
  ;

type
  TDateSelectorForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    applyButton: TsButton;
    dateSelector: TsDateEdit;
    discardButton: TsButton;
    tpkTime: TsTimePicker;
  private

    function getDate: TDateTime;
    function getTime: TDateTime;

    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    property date : TDateTime read getDate;
    property time : TDateTime read getTime;
  end;

var
  DateSelectorForm: TDateSelectorForm;

implementation

{$R *.dfm}

{ TPutSelectorForm }

function TDateSelectorForm.getDate: TDateTime;
begin
	result := dateSelector.Date;
end;

function TDateSelectorForm.getTime : TDateTime;
begin
	result := tpkTime.Time;
end;


constructor TDateSelectorForm.Create(AOwner: TComponent);
begin
  inherited;

  dateSelector.Date := now;
end;

destructor TDateSelectorForm.Destroy;
begin
  inherited;
end;

end.
