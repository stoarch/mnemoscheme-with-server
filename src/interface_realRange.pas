unit interface_realRange;

interface

const
    IID_IRealRange : TGUID = '{85311CEE-6202-438C-8B70-0A889E15361B}';

type
    IRealRange = interface
        ['{85311CEE-6202-438C-8B70-0A889E15361B}']
             procedure Assign( const value : IRealRange );
             function Contains( value : real ) : boolean;

              function GetMax: Real;
              function GetMin: Real;

              procedure SetMax(const Value: Real);
              procedure SetMin(const Value: Real);

             //--[ properties ]--
          	 property Min : Real read GetMin write SetMin;
             property Max : Real read GetMax write SetMax;

             function ToString() : string;
    end;

implementation

end.
 