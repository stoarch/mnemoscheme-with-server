unit frame_wellChannelView;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Mask,
  sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit, A3nalogGauge, sGroupBox,
  sFrameAdapter, iThermometer, iSevenSegmentDisplay, iSevenSegmentAnalog,
  iComponent, iVCLComponent, iCustomComponent, iPositionComponent,
  iScaleComponent, iGaugeComponent, iAngularGauge, iAngularLogGauge,
  iLinearGauge, iLed, iLedDiamond, ImgList, sLabel, math, PDJRotoLabel,
  //--[ data ]--
  data_alarms,
  data_putChanel,
  data_wellChanel,
  //--[ interfaces ]--
  interface_mnemoschemeItemChannelValue,
  interface_wellChannelData,
  interface_wellChannelParams
  ;

type

  TWellChannelViewFrame = class(TFrame)
    vplusGroup: TsGroupBox;
    sFrameAdapter1: TsFrameAdapter;
    VPlusValue: TiSevenSegmentAnalog;
    VPlusValid: TiLedDiamond;
    ImageList: TImageList;
    VPlusDate: TsLabel;
    vminusGroup: TsGroupBox;
    VMinusDate: TsLabel;
    VMinusValue: TiSevenSegmentAnalog;
    VMinusValid: TiLedDiamond;
    sGroupBox1: TsGroupBox;
    VCurrentDate: TsLabel;
    VCurrentValue: TiSevenSegmentAnalog;
    VCurrentValid: TiLedDiamond;
  private
    FParams: IWellChannelParams;
    FChannelCaption: string;
    FActive: boolean;
    FData: IWellChannelData;
    FChanelNo: integer;
    FValue : IMnemoschemeItemChannelValue;
    FNeedUpdateView: boolean;

    procedure SetChannelCaption(const Value: string);
    procedure SetBackwardFlowRate;
    procedure SetFlowRate;
    procedure SetActive(const Value: boolean);
    procedure SetData(const Value: IWellChannelData);
    procedure SetChanelNo(const Value: integer);
    procedure UpdateValue;
    function GetActive: boolean;
    procedure SetNeedUpdateView(const Value: boolean);
    procedure SetCurrentFlowRate;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;


    procedure UpdateView();

    //--[ properties ]--
    property Active : boolean read GetActive write SetActive;
    property ChannelCaption : string read FChannelCaption write SetChannelCaption;
    property ChanelNo : integer read FChanelNo write SetChanelNo;
    property Params : IWellChannelParams read FParams;

    property Data : IWellChannelData read FData write SetData;

    property NeedUpdateView : boolean read FNeedUpdateView write SetNeedUpdateView;
  end;

implementation

uses tool_systemLog, tool_ms_factories, interface_commonFactory,
  const_well, const_guids;

{$R *.dfm}
	const
    	DEFAULT_SEGMENT_COLOR : TColor = $0039FB82;

{ TFrame1 }

constructor TWellChannelViewFrame.Create(AOwner: TComponent);
    var
        factory : ICommonFactory;
begin
  inherited;

  factory := GetWellFactory();

  FData := factory.CreateInstance( CLSID_MnemoschemeItemChanelData ) as IWellChannelData;

  FParams := factory.CreateInstance( CLSID_WellChanelParams ) as IWellChannelParams;
  FValue :=  factory.CreateInstance( CLSID_MnemoschemeItemChanelValue ) as IMnemoschemeItemChannelValue;
end;

destructor TWellChannelViewFrame.Destroy;
begin
  inherited;
end;

procedure TWellChannelViewFrame.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

procedure TWellChannelViewFrame.SetChannelCaption(const Value: string);
begin
  FChannelCaption := Value;
end;


procedure TWellChannelViewFrame.UpdateView;
begin
  UpdateValue();

  Refresh();
end;

procedure TWellChannelViewFrame.UpdateValue();
begin
  SetBackwardFlowRate();
  SetFlowRate();
  SetCurrentFlowRate();
end;


procedure TWellChannelViewFrame.SetData(const Value: IWellChannelData);
begin
  FData.Assign( Value );

  if( NeedUpdateView )then
  	UpdateView();
end;

procedure TWellChannelViewFrame.SetChanelNo(const Value: integer);
begin
  FChanelNo := Value;
end;

function TWellChannelViewFrame.GetActive: boolean;
begin
	result := FData.Active;
end;

procedure TWellChannelViewFrame.SetNeedUpdateView(const Value: boolean);
begin
  FNeedUpdateView := Value;
end;


procedure TWellChannelViewFrame.SetBackwardFlowRate;
begin
    VMinusValue.Value := FData.Value.BackwardFlowRate.Value;
    VMinusValid.Active := FData.Value.BackwardFlowRate.IsValid;
    VMinusDate.Caption := DateTimeToStr( FData.Value.BackwardFlowRate.DateReceived );
end;

procedure TWellChannelViewFrame.SetFlowRate;
begin
    VPlusValue.Value := FData.Value.FlowRate.Value;
    VPlusValid.Active := FData.Value.FlowRate.IsValid;
    VPlusDate.Caption := DateTimeToStr( FData.Value.FlowRate.DateReceived );
end;

procedure TWellChannelViewFrame.SetCurrentFlowRate;
begin
    VCurrentValue.Value := FData.Value.CurrentFlowRate.Value;
    VCurrentValid.Active := FData.Value.CurrentFlowRate.IsValid;
    VCurrentDate.Caption := DateTimeToStr( FData.Value.CurrentFlowRate.DateReceived );
end;


end.
