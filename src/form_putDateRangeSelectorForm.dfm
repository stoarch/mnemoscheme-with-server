inherited PutDateRangeSelectorForm: TPutDateRangeSelectorForm
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 187
  PixelsPerInch = 96
  TextHeight = 13
  inherited applyButton: TsButton
    Top = 136
  end
  inherited dateSelector: TsDateEdit
    BoundLabel.Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072':'
  end
  inherited discardButton: TsButton
    Top = 136
  end
  object dateEndEdit: TsDateEdit [4]
    Left = 10
    Top = 102
    Width = 262
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Color = clWhite
    EditMask = '!99/99/9999;1; '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    TabOrder = 4
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
end
