unit form_tableView;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FR_DSet, DB, kbmMemTable, FR_DBSet, FR_Class, StdCtrls, sLabel,
  Grids, DBGrids, Buttons, sBitBtn, sSkinProvider, sDialogs,
  //--[ classes ]--
  class_valueMatrix,
  //--[ interfaces ]--
  interface_commonFactory,
  interface_valueMatrix
  ;

type
  TTableViewForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    DBGrid1: TDBGrid;
    tableLabel: TsLabel;
    report: TfrReport;
    dataSet: TfrDBDataSet;
    DataSource: TDataSource;
    dataTable: TkbmMemTable;
    closeButton: TsBitBtn;
    headerDataset: TfrUserDataset;
    rowDataset: TfrUserDataset;
    dlgSelectExportFile: TsSaveDialog;
    printButton: TsBitBtn;
    exportButton: TsBitBtn;
    procedure reportGetValue(const ParName: String; var ParValue: Variant);
    procedure closeButtonClick(Sender: TObject);
    procedure printButtonClick(Sender: TObject);
    procedure exportButtonClick(Sender: TObject);
  private
    FValueMatrix: IValueMatrix;
    FParamCaption: string;
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    procedure SeIValueMatrix(const Value: IValueMatrix);
    procedure SetEndDate(const Value: TDateTime);
    procedure SetParamCaption(const Value: string);
    procedure SetStartDate(const Value: TDateTime);
    procedure ExportCurrentMatrix;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    property ValueMatrix : IValueMatrix read FValueMatrix write SeIValueMatrix;
    property StartDate : TDateTime read FStartDate write SetStartDate;
    property EndDate : TDateTime read FEndDate write SetEndDate;
    property ParamCaption : string read FParamCaption write SetParamCaption;
  end;

var
  TableViewForm: TTableViewForm;

implementation

uses
    //--[ common ]--
    ComObj,
    //--[ constants ]--
    const_guids,
    //--[ tools ]--
    tool_environment
    , tool_ms_factories;

{$R *.dfm}

{ TTableViewForm }

constructor TTableViewForm.Create(AOwner: TComponent);
begin
  inherited;

  FValueMatrix := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;
end;

destructor TTableViewForm.Destroy;
begin
  inherited;
end;

procedure TTableViewForm.SeIValueMatrix(const Value: IValueMatrix);
	var
    	i,j : integer;
begin
  FValueMatrix.Assign( Value );

  dataTable.Fields.Clear();
  dataTable.FieldDefs.Clear();
  dataTable.FieldDefs.Add('Date', ftDateTime );
  for i := 1 to value.count do
  begin
  	dataTable.FieldDefs.Add( 'Value' + intToStr(i), ftFloat );
  end;
  dataTable.CreateTable();
  with(dataTable.FieldByName('Date') as TDateTimeField)do
  begin
  	DisplayFormat := 'dd.mm.yyyy hh:nn';
    DisplayLabel := '�����';
  end;

  for i := 1 to value.count do
  begin
	with( dataTable.FieldByName( 'Value' + intToStr(i) ) as TFloatField )do
    begin
        DisplayFormat := '###.###';
        DisplayLabel := '����. ' + intToStr(i);
    end;
  end;

  dataTable.Active := true;

  dataTable.First();
  for j := 1 to value.List[1].Count do
  begin
    dataTable.Append();
    dataTable.FieldByName('Date').AsDateTime := value.List[1].Value[j].Date;

    for i := 1 to value.count do
    begin
        dataTable.FieldByName('Value'+intToStr(i)).AsFloat := value.List[i].Value[j].Value;
    end;

  	dataTable.Post();
  end;

  headerDataset.RangeEndCount := value.Count;
  rowDataset.RangeEndCount := value.Count;
end;

procedure TTableViewForm.reportGetValue(const ParName: String;
  var ParValue: Variant);
  	var
    	aname : String;
        index : integer;
begin
	aname := AnsiUpperCase(ParName);

	if( aname = 'PUT' )then
    begin
    	ParValue := tableLabel.Caption;
        exit;
    end;

    if( aname = 'STARTDATE' )then
    begin
    	ParValue := FormatDateTime( 'dd.mm.yyyy', StartDate );
        exit;
    end;

    if( aname = 'ENDDATE' )then
    begin
    	ParValue := FormatDateTime( 'dd.mm.yyyy', EndDate );
        exit;
    end;

	index := headerDataset.RecNo;

    if ParName = 'CHANELHDR' then
    begin
      ParValue := Format( '%s%d', [ParamCaption, index+1] );
      exit;
    end;

    if( ParName = 'DATE' )then
    begin
    	ParValue := ValueMatrix.List[1].Value[dataTable.RecNo].Date;
        exit;
    end;

    index := rowDataset.RecNo + 1;
    if( index < 1 )or( index > ValueMatrix.Count )then
    	exit;
        
    if( ParName = 'VALUE' )then
    begin
    	ParValue := ValueMatrix.List[index].Value[dataTable.RecNo].Value;
        exit;
    end;

end;

procedure TTableViewForm.SetEndDate(const Value: TDateTime);
begin
  FEndDate := Value;
end;

procedure TTableViewForm.SetParamCaption(const Value: string);
begin
  FParamCaption := Value;
end;

procedure TTableViewForm.SetStartDate(const Value: TDateTime);
begin
  FStartDate := Value;
end;

procedure TTableViewForm.closeButtonClick(Sender: TObject);
begin
	ModalResult := mrOk;
end;

procedure TTableViewForm.printButtonClick(Sender: TObject);
begin
	report.LoadFromFile('.\reports\report_tableView.frf');
	if( report.PrepareReport() )then
    	report.ShowPreparedReport();	
end;

procedure TTableViewForm.exportButtonClick(Sender: TObject);
begin
	ExportCurrentMatrix();
end;

procedure TTableViewForm.ExportCurrentMatrix();
	var
    	excel : Variant;
        sheet : Variant;
        i, j  : integer;
begin
	if( not dlgSelectExportFile.Execute() )then
    begin
      exit;
    end;

	excel	:=	CreateOleObject( 'Excel.Application' );
    excel.EnableEvents	:=	false;

    excel.WorkBooks.Add();
    sheet	:=	excel.Workbooks[1].Worksheets[1];

    sheet.Cells[ 1, 1 ].Value	:=	tableLabel.Caption	+	'  -  ' + Caption;

   	sheet.Cells[ 2, 1 ].Value	:=	'Date';

    for i := 1 to ValueMatrix.Count do
    begin
        sheet.Cells[ 2, i+1 ].Value	:=	ValueMatrix.List[i].Title + intToStr( i );

		for j := 1 to ValueMatrix.List[i].Count do
        begin
        	sheet.Cells[ j+2, 1 ].Value	:=	ValueMatrix.List[i].Value[j].Date;
			sheet.Cells[ j+2, i+1 ].Value	:=	ValueMatrix.List[i].Value[j].Value;
        end;
    end;

    excel.Workbooks[1].SaveAs( dlgSelectExportFile.FileName );
    excel.Visible	:=	true;
end;


end.
