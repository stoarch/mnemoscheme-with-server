unit frame_putInfoView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, frame_channelInfoView, StdCtrls, sGroupBox, sFrameAdapter;

type
  TPutInfoViewFrame = class(TFrame)
    putGroupCaption: TsGroupBox;
    sFrameAdapter1: TsFrameAdapter;
    Channel1_infoView: TChannelInfoViewFrame;
    Channel2_infoView: TChannelInfoViewFrame;
    Channel3_infoView: TChannelInfoViewFrame;
    Channel4_infoView: TChannelInfoViewFrame;
    Channel5_infoView: TChannelInfoViewFrame;
    Channel6_infoView: TChannelInfoViewFrame;
  private
    FCount: integer;
    FActive: boolean;
    FPutCaption: string;
    function getChannelView(index: integer): TChannelInfoViewFrame;
    procedure SetCount(const Value: integer);
    procedure SetActive(const Value: boolean);
    procedure SetPutCaption(const Value: string);
    { Private declarations }
  public
    { Public declaration }
    //--[ methods ]--
    procedure UpdateAlarmState();
    procedure UpdateView();

    //--[ properties ]--
    property Active : boolean read FActive write SetActive;
    property Count : integer read FCount write SetCount;
    property ChannelView[ index : integer ] : TChannelInfoViewFrame read getChannelView;
    property PutCaption : string read FPutCaption write SetPutCaption;
  end;

implementation

{$R *.dfm}

{ TPutInfoViewFrame }

function TPutInfoViewFrame.getChannelView(
  index: integer): TChannelInfoViewFrame;
  	var
    	compName : string;
begin
    compName := Format( 'channel%d_infoView', [index] );
    result := FindComponent( compName ) as TChannelInfoViewFrame;
end;

procedure TPutInfoViewFrame.SetActive(const Value: boolean);
var
  i: Integer;
begin
  FActive := Value;

  for i := 1 to Count do
  begin
    ChannelView[i].Active := value;
    ChannelView[i].ChanelNo := i;
  end;
end;

procedure TPutInfoViewFrame.SetCount(const Value: integer);
begin
  FCount := Value;

  case count of
  	1 : Height := 94;
    2 : Height := 163;
    3 : Height := 230;
    4 : Height := 299;
    5 : Height := 369;
    6 : Height := 437;
  end;
end;

procedure TPutInfoViewFrame.SetPutCaption(const Value: string);
begin
  FPutCaption := Value;

  putGroupCaption.Caption := value;
end;

procedure TPutInfoViewFrame.UpdateAlarmState;
var
  j: Integer;
begin
	for j := 1 to Count do
    begin
      ChannelView[j].UpdateAlarmState();
    end;
end;

procedure TPutInfoViewFrame.UpdateView;
var
  i: Integer;
begin
	for i := 1 to Count do
    begin
      ChannelView[i].UpdateView();
    end;
end;

end.
