object MonthDaySelectorForm: TMonthDaySelectorForm
  Left = 573
  Top = 205
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1077#1085#1100
  ClientHeight = 232
  ClientWidth = 255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mclDay: TsMonthCalendar
    Left = 5
    Top = 4
    Width = 245
    Height = 185
    BevelWidth = 1
    BorderWidth = 3
    Caption = ' '
    TabOrder = 0
    SkinData.SkinSection = 'PANEL'
  end
  object bbtSelect: TsBitBtn
    Left = 5
    Top = 196
    Width = 88
    Height = 29
    Caption = #1042#1099#1073#1088#1072#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 1
    SkinData.SkinSection = 'BUTTON'
  end
  object bbtDiscard: TsBitBtn
    Left = 161
    Top = 196
    Width = 88
    Height = 29
    Cancel = True
    Caption = #1054#1090#1082#1072#1079#1072#1090#1100#1089#1103
    ModalResult = 2
    TabOrder = 2
    SkinData.SkinSection = 'BUTTON'
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 476
    Top = 268
  end
end
