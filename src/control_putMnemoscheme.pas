unit control_putMnemoscheme;
//TODO: Move strings to .lng file
//TODO: Load the put's in background thread

interface

    uses
        //--[ common ]--
        classes, ComCtrls, controls, graphics, ExtCtrls, XPCollections, types,
        //--[ alpha skins ]--
        sComboBox, slabel, sPanel, sGroupBox, sSplitter, sToolbar,
        sSkinProvider, sSkinManager, sCustomComboEdit, sConst,
        //--[ controls ]--
        control_commonMnemoscheme,
        //--[ data ]--
        data_put,
        //--[ interfaces ]--
        interface_putData,
        interface_putList,
        //--[ flex ]--
        FlexBase, FlexUtils, FlexControls, FlexProps,
        //--[ frames ]--
        frame_putView,
        frame_putInfoView,
        //--[ jedi ]--
        JclSysUtils, JclStrings, JclStringLists, JclContainerIntf,
        JclVectors, JclAlgorithms,
        //--[ lists ]--
        list_put
        ;

type
    TWorkMode = ( wmSummer, wmWinter );
    TDisplayKind	= ( dkPressure, dkFlowRate, dkTemperature );

    TPutMnemoscheme = class( TCommonMnemoscheme )
    private
      rgpActiveParms: TsRadioGroup;
      rgpWorkMode: TsRadioGroup;

      FOnViewChanged: TNotifyEvent;
      FOnActiveParamsChanged: TNotifyEvent;
      FOnWorkModeChanged: TNotifyEvent;

      FDisplayKind: TDisplayKind;

      FinfoView : TPutInfoViewFrame;
      Fview : TPutViewFrame;


    procedure SetOnViewChanged(const Value: TNotifyEvent);
    procedure rgpActiveParmsClick(Sender: TObject);
    procedure SetOnActiveParamsChanged(const Value: TNotifyEvent);
    procedure rgpWorkModeClick(sender: TObject);
    procedure SetOnWorkModeChanged(const Value: TNotifyEvent);

    procedure Fire_ActiveParamsChanged;
    procedure Fire_WorkModeChanged;
    procedure SetPutList(const Value: IPutList);
    function GetPutName(index: integer): string;
    function FindPut(name: string): TPutViewFrame;
    function getPutCount: integer;
    function getPutView(index: integer): TPutViewFrame;
    function getPutIndex(name: string): integer;
    procedure SetDisplayKind(const Value: TDisplayKind);
    procedure rgpActiveParmsChanging(Sender: TObject; NewIndex: Integer;
      var AllowChange: Boolean);
      procedure SetupActiveParmsSelector;

    function getPutLED(index: integer): TFlexControl;

	property PutLED[ index : integer ] : TFlexControl read getPutLED;
    function CreatePutViewFrame( chanelCount : integer ): TPutViewFrame;
    function FindPutData: IPutData;

    procedure UpdatePutSettings(put: TPutViewFrame; data: IPutData);
    procedure UpdatePutState(put: TPutViewFrame; data: IPutData);
    function GetPutList: IPutList;
    procedure SetupWorkModeSelector;
    procedure UpdatePutDetailsView(putNo: integer);
    procedure PlaceActiveDetails(onControl: TFlexControl);

  protected
      procedure CreateControls;override;

      function GetItemIndex(index: Integer): integer;override;
      function GetItemCaption( index : integer ): string;override;
      function MakeViewTab( aindex : integer; acaption : String ): TTabSheet;override;

      procedure Handle_TabChanged(sender: TObject); override;

      procedure MnemoschemePanelMouseMove(Sender: TObject; Shift : TShiftState;
            X,Y : integer );override;

      procedure MnemoschemePanelMouseUp(Sender: TObject;
                Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;

      procedure SetupControls;override;
      procedure UpdateMnemoschemeView;override;
  public
    constructor Create( AOwner : TComponent );override;
      procedure SetupInfoView;


    procedure UpdateAlarmState;
    procedure UpdateSettings();override;
    procedure UpdateViews();override;

    property DisplayKind : TDisplayKind read FDisplayKind write SetDisplayKind;

    property PutCount : integer read getPutCount;

    property PutList : IPutList read GetPutList write SetPutList;
    property PutView[ index : integer ] : TPutViewFrame read getPutView;

    destructor Destroy; override;

  published
      property OnActiveParamsChanged : TNotifyEvent read FOnActiveParamsChanged write SetOnActiveParamsChanged;
      property OnViewChanged : TNotifyEvent read FOnViewChanged write SetOnViewChanged;
      property OnWorkModeChanged : TNotifyEvent read FOnWorkModeChanged write SetOnWorkModeChanged;
  end;

implementation

uses
  SysUtils, Math, Forms, class_mnemoschemeItemChannelData;

  //TODO: Move to settings
const
   WINTER_MODEL	=	'.\models\MainPipeline.fxd';
   SUMMER_MODEL	=	'.\models\MainPipeline_summer.fxd';

{ TPutMnemoscheme }



procedure TPutMnemoscheme.CreateControls();
begin
  inherited;

  rgpActiveParms := TsRadioGroup.Create(Self);
  rgpWorkMode := TsRadioGroup.Create(Self);
  FinfoView := TPutInfoViewFrame.Create(self);
end;

procedure TPutMnemoscheme.SetupControls;
begin
  inherited;


  SetupActiveParmsSelector;
  SetupWorkModeSelector;
  SetupInfoView();

  cbbZoomFactor.ItemIndex := cbbZoomFactor.Items.IndexOf('75%');
  ZoomBy(75);
end;

procedure TPutMnemoscheme.SetupInfoView;
begin
    with FinfoView do
    begin
        Name := 'InfoView';
        Parent := self;
        Left := pnlInfo.Left + 8;
        Top := pnlInfo.Top + 178;
        TabOrder := 3;
        Anchors := [];
        Visible := False;
    end;
end;

procedure TPutMnemoscheme.SetupActiveParmsSelector;
begin
  with rgpActiveParms do
  begin
    Name := 'rgpActiveParms';
    Parent := pnlInfo;
    Left := 8;
    Top := 66;
    Width := 135;
    Height := 105;
    Caption := '�������� ���������:';
    ParentBackground := False;
    TabOrder := 2;
    ItemIndex := 1;
    Items.Clear;
    Items.Add('��������');
    Items.Add('������');
    Items.Add('�����������');

    OnClick := rgpActiveParmsClick;
    OnChanging := rgpActiveParmsChanging;
  end;
end;

procedure TPutMnemoscheme.SetupWorkModeSelector;
begin
  with rgpWorkMode do
  begin
    Name := 'rgpWorkMode';
    Parent := pnlInfo;
    Left := 148;
    Top := 66;
    Width := 102;
    Height := 65;
    Caption := '����� ������:';
    ParentBackground := False;
    TabOrder := 3;
    Items.Clear;
    Items.Add('����');
    Items.Add('����');
    ItemIndex := 0;
    OnClick := rgpWorkModeClick;
  end;
end;



procedure TPutMnemoscheme.SetOnViewChanged(const Value: TNotifyEvent);
begin
  FOnViewChanged := Value;
end;

procedure TPutMnemoscheme.rgpActiveParmsClick(Sender: TObject);
begin
    UpdateMnemoschemeView();
    Fire_ActiveParamsChanged();
end;

procedure TPutMnemoscheme.Fire_ActiveParamsChanged;
begin
    if( Assigned( FOnActiveParamsChanged ))then
        FOnActiveParamsChanged(self);
end;

procedure TPutMnemoscheme.Fire_WorkModeChanged;
begin
    if( assigned( FOnWorkModeChanged ))then
        FOnWorkModeChanged(self);
end;

procedure TPutMnemoscheme.SetOnActiveParamsChanged(
  const Value: TNotifyEvent);
begin
  FOnActiveParamsChanged := Value;
end;

procedure TPutMnemoscheme.rgpWorkModeClick( sender : TObject );
begin

	if rgpWorkMode.ItemIndex = Ord( wmWinter ) then
		MnemoschemePanel.LoadFromFile(SUMMER_MODEL)
	else
		MnemoschemePanel.LoadFromFile(WINTER_MODEL)
	;

    UpdateMnemoschemeView();

    Fire_WorkModeChanged();
end;


procedure TPutMnemoscheme.SetOnWorkModeChanged(const Value: TNotifyEvent);
begin
  FOnWorkModeChanged := Value;
end;

procedure TPutMnemoscheme.SetPutList(const Value: IPutList);
  var
    aputList : IPutList;
    aput : IPutData;
begin
  FDataList := Value;

  aPutList := GetPutList();
  if( aputList = nil ) then exit;

  aput := aputList[1];

  UpdatePutDetailsView(1);
end;


function TPutMnemoscheme.GetItemIndex(index: Integer): integer;
begin
  Result := StrToInt( StrAfter('��� ', CaptionList[index]) );
end;


function TPutMnemoscheme.GetItemCaption( index : integer ): string;
begin
  Result := PutList[index].caption + ' - ��� ' + Format( '%0:2d', [index] );
end;


function TPutMnemoscheme.MakeViewTab( aindex : integer; acaption : String ): TTabSheet;
var
  tab: TTabSheet;
begin
      tab := TTabSheet.Create( functionPages );
      tab.Caption := acaption;

      tab.PageControl := functionPages;
      tab.Parent := functionPages;

      tab.Name := GetPutName(aindex) + 'Tab';

      Result := tab;
end;

procedure TPutMnemoscheme.Handle_TabChanged( sender : TObject );
    var
        data : IPutData;
begin
    FinfoView.Hide();
    
    if( functionPages.ActivePageIndex = 0 )then
    begin
        UpdateMnemoschemeView();
        exit;
    end;

    data := FindPutData;

    if( functionPages.ActivePage.ControlCount > 0 )then
    begin
        UpdatePutState(
            functionPages.ActivePage.Controls[0] as TPutViewFrame,
            data
        );

        exit;
    end;

    Fview := CreatePutViewFrame( data.Count );

    UpdatePutState(
        Fview,
        data
    );
end;

function TPutMnemoscheme.FindPutData: IPutData;
begin
  Result := PutList[ functionPages.ActivePageIndex ] as IPutData;
end;

procedure TPutMnemoscheme.UpdatePutState(put: TPutViewFrame; data: IPutData);
begin
  put.NeedUpdateView := false;

  UpdatePutSettings(put, data);
  //TODO: Move protocol for data assignment to one method
  put.PutData := data;
  put.UpdateData( data );
  put.UpdateView();
  put.UpdateAlarmState();
end;

function TPutMnemoscheme.CreatePutViewFrame( chanelCount : integer ): TPutViewFrame;
var
  put: TPutViewFrame;
begin
  put := TPutViewFrame.Create(self, chanelCount);
  put.Align := alClient;
  put.Name := GetPutName( functionPages.ActivePageIndex );
  put.Parent := functionPages.ActivePage;
  Result := put;
end;


function TPutMnemoscheme.GetPutName( index : integer ): string;
begin
	result := Format( 'Put%d_View', [index] );
end;

procedure TPutMnemoscheme.UpdateSettings;
	var
    	i : integer;
        data : IPutData;
        put : TPutViewFrame;
begin
    for i := 1 to PutList.Count do
    begin
    	data := PutList[i];
        put := FindPut( data.name );

        if( not Assigned( put ) )then
            Continue;

        UpdatePutSettings(put, data);

		put.NeedUpdateView := false;
        put.UpdateData( data );
        put.NeedUpdateView := true;
    end;

    UpdateViews();
end;

procedure TPutMnemoscheme.UpdatePutSettings(put: TPutViewFrame; data:
    IPutData);
var
  j: integer;
begin
  for j := 1 to data.Count do
      begin
        //TODO: Move to TChannelParams

        with put.ChannelView[j].ParamRanges do
          begin
          //TODO: Remove indirection
              TemperatureRange.Min := data.Chanels[j].Params.TemperatureRange.Min;
              TemperatureRange.Max := data.Chanels[j].Params.TemperatureRange.Max;

              PressureRange.Min := data.Chanels[j].Params.PressureRange.Min;
              PressureRange.Max := data.Chanels[j].Params.PressureRange.Max;

              FlowRateRange.Min := data.Chanels[j].Params.FlowRateRange.Min;
              FlowRateRange.Max := data.Chanels[j].Params.FlowRateRange.Max;
          end;
      end;
end;

procedure TPutMnemoscheme.UpdateViews;
var
  i: Integer;
begin
  for i := 1 to PutCount do
  begin
    //TODO: Make TNullPutView singleton/fake
    if( not assigned( putView[i] ))then
        continue;

    PutView[i].UpdateView;
  end;
end;


function TPutMnemoscheme.FindPut(name: string): TPutViewFrame;
begin
	result := FindComponent( Format( '%s_View', [name] )) as TPutViewFrame;
end;


function TPutMnemoscheme.getPutCount: integer;
begin
	result := PutList.Count;
end;


function TPutMnemoscheme.getPutView(index: integer): TPutViewFrame;
begin
  result := FindComponent( GetPutName(index) ) as TPutViewFrame;
end;


procedure TPutMnemoscheme.UpdateMnemoschemeView;
var
  i: integer;
  j: integer;
  index: integer;
  temperature_viewName: string;
  pressure_viewName: string;
  flowrate_viewName : string;
  temperatureView : TFlexText;
  pressureView : TFlexText;
  flowrateView : TFlexText;

  procedure SetTemperature();
  begin
     temperature_viewName := Format( 'put%d_ch%d_value_temperature',
                  [
                    Index,
                    j
                  ]
            );

     temperatureView :=  MnemoschemePanel.FindControl( temperature_viewName ) as TFlexText;

     if assigned( temperatureView ) then
        temperatureView.TextProp.Text :=  Format( '%4.1f', [PutList[i].Chanels[j].Value.Temperature.Value]);
  end;

  procedure SetPressure();
  begin
     pressure_viewName := Format( 'put%d_ch%d_value_pressure',
                  [
                    Index,
                    j
                  ]
            );

     pressureView :=  MnemoschemePanel.FindControl( pressure_viewName ) as TFlexText;

     if assigned( pressureView ) then
        pressureView.TextProp.Text :=  Format( '%4.1f', [PutList[i].Chanels[j].Value.Pressure.Value]);
  end;

  procedure SetFlowrate();
  begin
     flowrate_viewName := Format( 'put%d_ch%d_value_flowrate',
                  [
                    Index,
                    j
                  ]
            );

     flowrateView :=  MnemoschemePanel.FindControl( flowrate_viewName ) as TFlexText;

     if assigned( flowrateView ) then
        flowrateView.TextProp.Text :=  Format( '%4.1f', [PutList[i].Chanels[j].Value.Flowrate.Value]);
  end;

begin
  if( not assigned( PutList ))then
    exit;

    //TODO: Simplify this
  try
    for i := 1 to PutList.Count do
    begin
       if( not PutList[i].Active )then continue;

       index  := StrToInt( System.Copy( PutList[i].Name, 4, 10 ));

       for j := 1 to PutList[i].Count do
       begin
         SetTemperature();
         SetPressure();
         SetFlowrate();
       end;
    end;
  except
  end;
end;


function TPutMnemoscheme.getPutIndex( name : string ): integer;
  var
    i : integer;
begin
  name := AnsiUpperCase(name);
  result := -1;
  for i := 1 to PutList.count do
    if( PutList[i].Name = name )then
    begin
      result := i;
      exit;
    end;
end;

procedure TPutMnemoscheme.SetDisplayKind(const Value: TDisplayKind);
begin
  FDisplayKind := Value;
  
  rgpActiveParms.ItemIndex := ord(value);
end;


procedure TPutMnemoscheme.rgpActiveParmsChanging(Sender: TObject;
  NewIndex: Integer; var AllowChange: Boolean);
begin
	FDisplayKind	:=	TDisplayKind( NewIndex );
end;


procedure TPutMnemoscheme.MnemoschemePanelMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var
    putName : string;
    putIndex : integer;
begin
  if assigned( MnemoschemePanel.MouseSubControl )then
  begin
    with MnemoschemePanel do
    begin
      putName := MouseSubControl.Name;

      putName := System.Copy(
                        putName, 1,
                        Pos( 'View', putName ) - 1
                    );

          //TODO: Move string processing to function
      putIndex := getPutIndex(
                    putName
                );


      if( putIndex > 0 )and( putIndex <= getPutCount() )then
      begin
        functionPages.ActivePageIndex := putIndex;
            Handle_TabChanged(functionPages);
      end;
	  end;
	end
end;

procedure TPutMnemoscheme.UpdateAlarmState();
var
  i: Integer;
  led : TFlexControl;
begin
    //TODO: Move alarm checking to class independent from TPutView
	for i := 1 to PutList.count do
	begin
	  led := PutLED[i];
	  if( not assigned( led ))then continue;

      if( not assigned( PutView[i] ))then
        continue;
        
	  if( PutView[i].AlarmOn ) and ( PutView[i].Active ) then
	  begin
		  PutView[i].UpdateAlarmState();

		  TBrushProp(led.Props['Brush']).Color := clRed;
	  end
	  else
	  begin
		  TBrushProp(led.Props['Brush']).Color := clGreen;
	  end;
   end;
end;

function TPutMnemoscheme.getPutLED(index: integer): TFlexControl;
	var
		caption : string;
begin
  caption := Format( 'Put%dView', [index] );
  result := MnemoschemePanel.FindControl( caption );
end;

function TPutMnemoscheme.GetPutList: IPutList;
begin
    result := FDataList as IPutList;
end;

constructor TPutMnemoscheme.Create(AOwner: TComponent);
begin
  inherited;

  MnemoschemePanel.LoadFromFile(WINTER_MODEL);
  Fview := nil;
end;

procedure TPutMnemoscheme.MnemoschemePanelMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
    var
        putName : string;
  putNo: Integer;
begin
	if assigned( MnemoschemePanel.MouseSubControl )then
	begin
	  with MnemoschemePanel do
	  begin
		  putName := MouseSubControl.Name;
          if( Pos( 'Put', putName ) > 0 )then
          begin
            putNo := getPutIndex(
                    System.Copy(
                        putName, 1,
                        Pos( 'View', putName ) - 1
                    )
                );
            if( putNo > 0 )then
                UpdatePutDetailsView(
                    putNo
                );
            PlaceActiveDetails( MouseSubControl );
          end;
      end;
    end
    else
    begin
      FinfoView.Hide();
    end;
end;

procedure TPutMnemoscheme.UpdatePutDetailsView( putNo : integer );
	var
		i : integer;
        put : IPutData;
begin
  //TODO: Make function for finding put by index of name
  put := PutList[ putNo ] as IPutData;

  FinfoView.Count := put.Count;
  FinfoView.Active := put.Active;
  FinfoView.PutCaption := put.Caption;

  for i := 1 to FinfoView.Count do
  //TODO: Move assign one level up
  begin
	 FinfoView.ChannelView[i].Params.Assign(
		put[i].Params
	 );
	 FinfoView.ChannelView[i].Value.Assign(
		put[i].Value
	 );

	 FinfoView.ChannelView[i].ChannelCaption :=
		put[i].Caption;
  end;

  FinfoView.UpdateView();
end;

procedure TPutMnemoscheme.PlaceActiveDetails( onControl : TFlexControl );
	const
	  LED_WIDTH = 15;
	  LED_HEIGHT = 15;
var
  newLeft: Integer;
  newTop: Integer;
  _top: Integer;
  _left: Integer;
  ptWork : TPoint;
  rcPut: TRect;
begin
    rcPut := Rect(
                  onControl.Left,
                  onControl.Top,
                  onControl.Width + onControl.Left,
                  onControl.Height + onControl.Top
              );

    MnemoschemePanel.TransformRect( rcPut );

  _left := tabMnemoscheme.Width - FinfoView.Width;
  _top := functionPages.Height - FinfoView.Height - 20;

  newLeft := Min(rcPut.Right, _left);
  newTop := Min(rcPut.Bottom, _top);

  if (newLeft = _left) then
	 newLeft := rcPut.left - FinfoView.Width;

  if (newTop = _top) then
	 newTop := rcPut.Top - FinfoView.Height;

  ptWork.X := newLeft;
  ptWork.Y := newTop;

  ptWork := tabMnemoscheme.ClientToScreen(ptWork);

  ptWork := ScreenToClient(ptWork);

  ptWork.X := Max(0, ptWork.X);
  ptWork.Y := Max(0, ptWork.Y);

  FinfoView.Top := ptWork.Y;//newTop;
  FinfoView.Left := ptWork.X;//newLeft;
  FinfoView.Visible := true;

  FinfoView.Refresh();
  FinfoView.Update();

  Application.ProcessMessages();
end;

destructor TPutMnemoscheme.Destroy();
begin
  inherited;
end;


end.
