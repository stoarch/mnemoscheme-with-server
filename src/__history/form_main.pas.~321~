unit form_main;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, DBOleCtl, isAnalogLibrary_TLB, sSkinProvider, sSkinManager,
  OleServer, OPCAutomation_TLB, sComboBoxes, StdCtrls, sComboBox, Buttons,
  sBitBtn, A3nalogGauge, AMAdvLed, ComCtrls, sStatusBar, ExtCtrls,
  Math, sLabel, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, Series, TeEngine, TeeProcs, Chart, Led1, AMHLEDVecStd,
  sGroupBox, IniFiles,
  //--[ containers ]--
  XPCollections,
  //--[ frames ]--
  frame_putInfoView,
  frame_putView,
  //--[ opc ]--
  class_opcDataAccess, acProgressBar, sEdit, sSpinEdit
  ;

type
  TMnemoscheme = class(TForm)
    sSkinManager1: TsSkinManager;
    sSkinProvider1: TsSkinProvider;
    stateLED: TAMAdvLed;
    RefreshTimer: TTimer;
    OPCServer: TOPCServer;
    functionPages: TPageControl;
    Panel1: TPanel;
    TabSheet1: TTabSheet;
    tabMnemoscheme: TTabSheet;
    ConnectButton: TsBitBtn;
    serversCombo: TsComboBox;
    sLabel2: TsLabel;
    Image1: TImage;
    put9_LED: TAMHLEDVecStd;
    put7_led: TAMHLEDVecStd;
    put5_led: TAMHLEDVecStd;
    put3_led: TAMHLEDVecStd;
    put6_led: TAMHLEDVecStd;
    put4_led: TAMHLEDVecStd;
    put8_led: TAMHLEDVecStd;
    put10_led: TAMHLEDVecStd;
    put12_led: TAMHLEDVecStd;
    put11_led: TAMHLEDVecStd;
    alarmTimer: TTimer;
    put1_led: TAMHLEDVecStd;
    put2_led: TAMHLEDVecStd;
    detailsTimer: TTimer;
    Put1_View: TPutViewFrame;
    PutDetailsView: TPutInfoViewFrame;
    put2_tabSheet: TTabSheet;
    put2_view: TPutViewFrame;
    put3_tabPage: TTabSheet;
    put3_view: TPutViewFrame;
    put4_tabsheet: TTabSheet;
    put4_view: TPutViewFrame;
    put5_tabsheet: TTabSheet;
    put5_view: TPutViewFrame;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    put6_view: TPutViewFrame;
    put7_view: TPutViewFrame;
    put8_view: TPutViewFrame;
    put9_view: TPutViewFrame;
    put10_view: TPutViewFrame;
    put11_view: TPutViewFrame;
    put12_view: TPutViewFrame;
    progressBar: TsProgressBar;
    durationLabel: TsLabelFX;
    timeLabel: TsLabelFX;
    progresLabel: TsLabel;
    time: TsTimePicker;
    procedure FormCreate(Sender: TObject);
    procedure ConnectButtonClick(Sender: TObject);
    procedure RefreshTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure put9_LEDClick(Sender: TObject);
    procedure put9_LEDMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure alarmTimerTimer(Sender: TObject);
    procedure detailsTimerTimer(Sender: TObject);
  private
    procedure FinalProgress;
    procedure UpdateAllPutViews;
    procedure StepProgress;
    procedure ShowUnableToConnect;
  protected
	var
    	m_opcda : TOPCDataAccess;
    	m_activePutLed: TAMHLEDVecStd;
    	m_alarmPutLed: TAMHLEDVecStd;
    	m_querying: Boolean;
        m_itemsToAlarm : TXDList;

        startTime : TTime;

    procedure ConnectToOPCServer;

    function GetCurrentServer: string;
    procedure ShowHint(message: string);
    procedure ActivatePutDetails;
    procedure PlaceActiveDetails;
    function getPutView(index: integer): TPutViewFrame;
    procedure QueryPutParams;
    procedure UpdatePutDetailsView;
    procedure ReadPutSettings;
    procedure ReadPutViewSettings;
    function getIniFile(): TIniFile;
    function ReadString(item, section, defValue: string): String;
    function ReadBoolean(item, section: string; defValue: boolean): boolean;
    function ReadInteger(item, section: string; defValue: integer): integer;
    function WriteInteger(item, section: string; Value: integer): integer;
    function WriteBoolean(item, section: string; value: boolean): boolean;
    function WriteString(item, section, value: string): String;
    procedure SetupOPCDA;
    function getPutLED(index: integer): TAMHLEDVecStd;
    procedure QueryData;
    procedure QueryOPCData;
    procedure InitProgress;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    function getItem( index : integer ) : Variant;

    property OPCDA : TOPCDataAccess read m_opcda;
    property CurrentServer : string read GetCurrentServer;
    property PutLED[ index : integer ] : TAMHLEDVecStd read getPutLED;
    property PutView[ index : integer ] : TPutViewFrame read getPutView;
  end;

var
  Mnemoscheme: TMnemoscheme;

implementation

uses
  ActiveX;


{$R *.dfm}

const
	PUT_COUNT = 12;

    settings_file = 'ms.settings';

type
	TQueryExecutor = class( TThread )
      private
        m_mnemoscheme : TMnemoscheme;
        m_opcda : TOPCDataAccess;
    FServerName: string;

    	function getItem( index : integer ): Variant;
    	procedure CopyOPCDASettings(source, destination: TOPCDataAccess);
    procedure SetServerName(const Value: string);
    procedure Initialize;
      protected
      	procedure Execute();override;
      public
        constructor Create( CreateSuspended : boolean; Mnemoscheme : TMnemoscheme );

        property ServerName : string read FServerName write SetServerName;
    end;


var
  g_executor: TQueryExecutor;
  g_synchronizer : TMultiReadExclusiveWriteSynchronizer;

{TMnemoscheme}
procedure TMnemoscheme.put9_LEDClick(Sender: TObject);
begin
  functionPages.ActivePageIndex := TAMHLEDVecStd( Sender ).Tag;
  putDetailsView.Hide();
  detailsTimer.Enabled := false;
end;

procedure TMnemoscheme.put9_LEDMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  m_activePutLed := Sender as TAMHLEDVecStd;
  detailsTimer.Enabled := true;
end;

procedure TMnemoscheme.ActivatePutDetails();
var
  i: Integer;
begin
	if( m_activePutLed = nil )then
    	exit;

  UpdatePutDetailsView();

  PlaceActiveDetails;
end;

procedure TMnemoscheme.alarmTimerTimer(Sender: TObject);
var
  i: Integer;
  led : TAMHLEDVecStd;
begin
   time.Value := now;

   for i := 1 to PUT_COUNT do
   begin
     if( PutView[i].AlarmOn ) and ( PutView[i].Active ) then
     begin
     	PutView[i].UpdateAlarmState();

    	led := PutLED[i];
        assert( assigned( led ), Format( 'led%d', [i] ));
        led.LEDColorOff := clBlack;
        led.LEDColorOn := clRed;
        led.LEDActive := not led.LEDActive;
     end;
   end;

   PutDetailsView.UpdateAlarmState();
end;

procedure TMnemoscheme.ConnectButtonClick(Sender: TObject);
begin
	ConnectToOPCServer();
end;

procedure TMnemoscheme.ConnectToOPCServer();
begin
  QueryData();
  RefreshTimer.Enabled := true;
end;

constructor TMnemoscheme.Create(AOwner: TComponent);
	const
    	QUERYINTERVAL_ITEM = 'QueryInterval';
        MNEMOSCHEME_SECTION = 'Mnemoscheme';
begin
  inherited;
  m_opcda := TOPCDataAccess.Create();
  m_itemsToAlarm := TXDList.Create(TXDObject);

  RefreshTimer.Interval := ReadInteger( QUERYINTERVAL_ITEM, MNEMOSCHEME_SECTION, 2000 );

  ReadPUTSettings;

  SetupOPCDA();

  functionPages.ActivePageIndex := 0;
end;

procedure TMnemoscheme.SetupOPCDA();
var
  i: Integer;
  j: Integer;
  k : integer;
begin
	K := 1;
	for i := 1 to PUT_COUNT do
    begin
    	for j := 1 to PutView[i].Count do
        begin
            if( PutView[i].ChannelView[j].temperatureOPC = '' )
              or ( PutView[i].ChannelView[j].PressureOPC = '' )
              or ( PutView[i].ChannelView[j].FlowRateOPC = '' )
            then
            	continue;

            m_opcda.AddItem( PutView[i].ChannelView[j].temperatureOPC );
            m_opcda.AddItem( PutView[i].ChannelView[j].PressureOPC );
            m_opcda.AddItem( PutView[i].ChannelView[j].FlowRateOPC );

            inc( k );
        end;
    end;
end;

destructor TMnemoscheme.Destroy;
begin
	FreeAndNil( m_opcda );
    FreeAndNil( m_itemsToAlarm );
  inherited;
end;

procedure TMnemoscheme.UpdateAllPutViews;
var
  i: Integer;
begin
  for i := 1 to PUT_COUNT do
  begin
    PutView[i].UpdateView;
  end;
end;

procedure TMnemoscheme.QueryOPCData;
begin
  if( not assigned( g_executor ))then
  	g_executor := TQueryExecutor.Create(true,self);

  ConnectButton.Enabled := false;
  StateLED.LedOn := true;

  g_executor.ServerName := CurrentServer;
  g_executor.Resume();
end;

procedure TMnemoscheme.QueryData;
begin
  QueryPutParams;
  UpdatePutDetailsView;
end;

procedure TMnemoscheme.ReadPutViewSettings;
const
	TEMP_MIN_ITEM = 'TemperatureMin';
    TEMP_MAX_ITEM = 'TemperatureMax';
    PRESS_MIN_ITEM = 'PressureMin';
    PRESS_MAX_ITEM = 'PressureMax';
    FLOWRATE_MIN_ITEM = 'FlowRateMin';
    FLOWRATE_MAX_ITEM = 'FlowRateMax';

    FLOWRATE_OPC_ITEM = 'FlowRateOPC';
    PRESSURE_OPC_ITEM = 'PressureOPC';
    TEMPERATURE_OPC_ITEM = 'TemperatureOPC';

var
  i, j: Integer;
  section : string;
begin
  for j := 1 to PUT_COUNT do
    for i := 1 to PutView[j].Count do
    begin
      with PutView[j].ChannelView[i],Params do
      begin
        section := Format('PUT%dCHANEL%d',[j,i]);

        TemperatureRange.Min := ReadInteger(TEMP_MIN_ITEM, section, 0 );
        TemperatureRange.Max := ReadInteger(TEMP_MAX_ITEM, section, 0 );
        TemperatureOPC := ReadString( TEMPERATURE_OPC_ITEM, section, '' );

        PressureRange.Min := ReadInteger(PRESS_MIN_ITEM, section, 0);
        PressureRange.Max := ReadInteger(PRESS_MAX_ITEM, section, 0);
        PressureOPC := ReadString( PRESSURE_OPC_ITEM, section, '' );

        FlowRateRange.Min := ReadInteger(FLOWRATE_MIN_ITEM, section, 0);
        FlowRateRange.Max := ReadInteger(FLOWRATE_MAX_ITEM, section, 0);
        FlowRateOPC := ReadString( FLOWRATE_OPC_ITEM, section, '' );
      end;
    end;
end;

function TMnemoscheme.ReadString( item, section : string; defValue : string ): String;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    result := ini.ReadString( section, item, defValue );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.WriteString( item, section : string; value : string ): String;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    ini.WriteString( section, item, Value );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.ReadBoolean( item, section : string; defValue : boolean ): boolean;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    result := ini.ReadBool( section, item, defValue );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.WriteBoolean( item, section : string; value : boolean ): boolean;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    ini.WriteBool( section, item, Value );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.ReadInteger( item, section : string; defValue : integer): integer;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    result := ini.ReadInteger( section, item, defValue );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.WriteInteger( item, section : string; Value : integer): integer;
	var
    	ini : TIniFile;
begin
  ini := getIniFile();
  try
    ini.WriteInteger( section, item, value );
  finally
  	FreeAndNil( ini );
  end;
end;

function TMnemoscheme.getIniFile(): TIniFile;
begin
	result := TIniFile.Create(ExtractFilePath(ParamStr(0))+settings_file);
end;

function TMnemoscheme.getItem(index: integer): Variant;
begin
	result := m_opcda.Item[index];
end;

procedure TMnemoscheme.ReadPutSettings;
const
	PUT_CAPTION_ITEM = 'Caption';
    PUT_ACTIVE_ITEM  = 'Active';
    PUT_COUNT_ITEM   = 'Count';
var
  i: Integer;
  putSection : string;
  acaption : string;
  aactive : boolean;
  acount : integer;
begin
  //TODO: ������ ��������� �� �����
  for i := 1 to PUT_COUNT do
  begin
    putSection := 'PUT' + intToStr(i);

    acaption := ReadString( PUT_CAPTION_ITEM, putSection, '' );
    aactive := ReadBoolean( PUT_ACTIVE_ITEM, putSection, false );
    acount  := ReadInteger( PUT_COUNT_ITEM, putSection, 0 );;

    if( acaption = '' )or
    	( acount = 0 )
    then
    	continue;

    PutView[i].PutCaption := acaption;
    PutView[i].Count := acount;
    PutView[i].Active := aactive;
  end;

  ReadPutViewSettings;

  Put1_View.UpdateView;
end;

procedure TMnemoscheme.QueryPutParams;
begin
  if( m_querying )then
  	exit;

  m_querying := true;

  QueryOPCData;

  m_querying := false;
end;

procedure TMnemoscheme.PlaceActiveDetails;
var
  newLeft: Integer;
  led: TAMHLEDVecStd;
  newTop: Integer;
  atop: Integer;
  aleft: Integer;
begin
  aleft := tabMnemoscheme.Width - putDetailsView.Width;
  atop := tabMnemoscheme.Height - putDetailsView.Height;

  led := m_activePutLed;

  newLeft := Min(led.Left + put9_LED.Width, aleft);
  newTop := Min(led.Top + put9_LED.Height, atop);

  if (newLeft = aleft) then
    newLeft := led.left - putDetailsView.Width;

  if (newTop = atop) then
    newTop := led.Top - putDetailsView.Height;

  newLeft := Max(0, newLeft);
  newTop := Max(0, newTop);

  putDetailsView.Top := newTop;
  putDetailsView.Left := newLeft;
  putDetailsView.Visible := true;
end;

procedure TMnemoscheme.detailsTimerTimer(Sender: TObject);
begin
	ActivatePutDetails();
end;

procedure TMnemoscheme.ShowHint( message : string );
begin
  //tatusBar.Panels[2].Text := message;
end;

procedure TMnemoscheme.FormCreate(Sender: TObject);
	var
    	serverList 	: Variant;
  		I			: Integer;
begin
	serverList := OPCServer.GetOPCServers();

    serversCombo.Items.BeginUpdate();
    serversCombo.Items.Clear();

    for I := VarArrayLowBound( serverList, 1 ) to VarArrayHighBound( serverList, 1 ) do
       serversCombo.Items.Add( serverList[ i ] );

    serversCombo.Items.EndUpdate();

    SetBounds( Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight );
end;

procedure TMnemoscheme.FormDestroy(Sender: TObject);
begin
  if( assigned( m_opcda ) )then
	m_opcda.Disconnect();
end;

function TMnemoscheme.GetCurrentServer: string;
begin
	result := serversCombo.Items[ serversCombo.ItemIndex ];
end;

function TMnemoscheme.getPutLED(index: integer): TAMHLEDVecStd;
	var
    	caption : string;
begin
  caption := Format( 'put%d_led', [index] );
  result := FindComponent( caption ) as TAMHLEDVecStd;
end;

function TMnemoscheme.getPutView(index: integer): TPutViewFrame;
begin
  result := nil; //TODO: ������� NULL ������ ��� TPutViewFrame
  result := FindComponent( Format( 'Put%d_View', [index] )) as TPutViewFrame;
end;

procedure TMnemoscheme.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
	putDetailsView.Hide();
    detailsTimer.Enabled := false;
end;

procedure TMnemoscheme.RefreshTimerTimer(Sender: TObject);
begin
	if( m_opcda.Count = 0 )then
    	exit;

  QueryData;
end;

procedure TMnemoscheme.UpdatePutDetailsView();
	var
    	i : integer;
begin
  if( m_activePutLed = nil )then
  	exit;

  PutDetailsView.Count := PutView[ m_activePutLed.Tag ].Count;
  PutDetailsView.Active := PutView[ m_activePutLed.Tag ].Active;
  PutDetailsView.PutCaption := PutView[ m_activePutLed.Tag ].PutCaption;

  //TODO: ������� ����� ��������� ��� ���������� ����
  g_synchronizer.BeginRead();
  for i := 1 to PutDetailsView.Count do
  begin
    PutDetailsView.ChannelView[i].Params :=
      PutView[ m_activePutLed.Tag ].ChannelView[i].Params;

    PutDetailsView.ChannelView[i].ChannelCaption :=
      PutView[ m_activePutLed.Tag ].ChannelView[i].ChannelCaption;
  end;

  PutDetailsView.UpdateView();
  g_synchronizer.EndRead();
end;

procedure TMnemoscheme.InitProgress;
begin
    progressBar.Min := 1;
    progressBar.Max := PUT_COUNT;
    progressBar.Position := 1;
    progressBar.Step := 1;

    progresLabel.Caption := '����� ������...';
    application.ProcessMessages();

    startTime := Now();
end;

procedure TMnemoscheme.FinalProgress;
begin
    progressBar.Position := 0;
    progresLabel.Caption := '����� ��������';

    durationLabel.Caption := Format( '����� ������: %s', [FormatDateTime( 'hh:nn:ss:zzz', Now-startTime )] );

    timeLabel.Caption := FormatDateTime( '� hh:nn:ss', Now );
    sSkinManager1.RepaintForms();

  	UpdateAllPutViews;
end;

procedure TMnemoscheme.StepProgress();
begin
  progressBar.StepIt();
  Application.ProcessMessages();
end;

procedure TMnemoscheme.ShowUnableToConnect();
begin
	ShowHint( '�� ���� �����������' );
end;


{ TQueryExecutor }

constructor TQueryExecutor.Create(CreateSuspended: boolean;
  Mnemoscheme: TMnemoscheme);
begin
  inherited Create( True );
  m_mnemoscheme := Mnemoscheme;

  m_opcda := TOPCDataAccess.Create();
  CopyOPCDASettings( m_mnemoscheme.OPCDA, m_opcda );
end;

procedure TQueryExecutor.CopyOPCDASettings( source, destination : TOPCDataAccess);
	var
    	i : integer;
begin
    destination.Clear();
    for i := 1 to source.Count do
    begin
      destination.AddItem(source.OPCItem[i].Caption);
    end;
end;

function TQueryExecutor.getItem( index : integer ): Variant;
begin
	result := m_opcda.Item[index];
end;

procedure TQueryExecutor.SetServerName(const Value: string);
begin
  FServerName := Value;
end;

procedure TQueryExecutor.Initialize();
	const
      RPC_C_AUTHN_LEVEL_NONE = 1;
      RPC_C_IMP_LEVEL_IMPERSONATE = 3;
      EOAC_NONE = 0;

	var
    	HR : HRESULT;
begin
      HR := CoInitialize(nil);
(*
      HR := CoInitializeSecurity(
        nil,                    // points to security descriptor
        -1,                     // count of entries in asAuthSvc
        nil,                    // array of names to register
        nil,                    // reserved for future use
        RPC_C_AUTHN_LEVEL_NONE, // the default authentication level for proxies
        RPC_C_IMP_LEVEL_IMPERSONATE,// the default impersonation level for proxies
        nil,                    // used only on Windows 2000
        EOAC_NONE,              // additional client or server-side capabilities
        nil                     // reserved for future use
        );
      if Failed(HR) then
      begin
        raise EOPCDCOMInitializationError.Create( 'Security is not initialized' );
      end;
*)
end;

procedure TQueryExecutor.Execute;
label
	done;
var
  floatV: Variant;
  settings: TFormatSettings;
  i, j,k : integer;
  startTime : TTime;
  fFlowRate, fTemperature, fPressure : real;
begin
  initialize();

  while True do
  begin
    if( not m_opcda.Connected )then
  	  try
  		m_opcda.Connect( ServerName );
  	  except
		Synchronize( self, m_mnemoscheme.ShowUnableToConnect  );
        Suspend();
  	  end;


    settings.DecimalSeparator := '.';

    if( Terminated )then
      break;

    Synchronize( self, m_mnemoscheme.InitProgress );

    k := 1;
    for i := 1 to PUT_COUNT do
    begin
      if( m_mnemoscheme.PutView[ i ].Active )then
      for j := 1 to m_mnemoscheme.PutView[i].Count do
      begin
        if( not m_mnemoscheme.PutView[i].ChannelView[j].Active )then
          continue;

        if( Terminated )then
          goto done;

        floatV := getItem(k);
        fTemperature := StrToFloat( floatV, settings );

		if( Terminated )then
          goto done;

        floatV := getItem(k+1);
        fPressure := StrToFloat( floatV, settings );

		if( Terminated )then
          goto done;

        floatV := getItem(k+2);
        fFlowRate := StrToFloat( floatV, settings );

		if( Terminated )then
          goto done;

        g_synchronizer.BeginWrite();
        m_mnemoscheme.PutView[i].ChannelView[j].Params.Temperature := fTemperature;
        m_mnemoscheme.PutView[i].ChannelView[j].Params.Pressure := fPressure;
        m_mnemoscheme.PutView[i].ChannelView[j].Params.FlowRate := fFlowRate;
        g_synchronizer.EndWrite();

        k := k + 3;
      end;

      if( Terminated )then
        goto done;

      Synchronize( self, m_mnemoscheme.StepProgress );
    end;

    if( Terminated )then
      break;

    Synchronize( self, m_mnemoscheme.FinalProgress );

	Suspend();

    if( Terminated )then
      break;
  end;

done:
  CoUninitialize();
end;

initialization
    g_synchronizer := TMultiReadExclusiveWriteSynchronizer.Create();

finalization
    if( assigned( g_synchronizer ))then
    	FreeAndNil( g_synchronizer );

	if( assigned( g_executor ))then
    	FreeAndNil( g_executor );

end.
