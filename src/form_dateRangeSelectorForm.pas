unit form_dateRangeSelectorForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit, sButton,
  sSkinProvider;

type
  TSelectDateRangeForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    selectButton: TsButton;
    startDateEdit: TsDateEdit;
    endDateEdit: TsDateEdit;
    discardButton: TsButton;
    procedure selectButtonClick(Sender: TObject);
    procedure discardButtonClick(Sender: TObject);
  private
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    procedure SetEndDate(const Value: TDateTime);
    procedure SetStartDate(const Value: TDateTime);
    { Private declarations }
  public
    { Public declarations }
    property StartDate : TDateTime read FStartDate write SetStartDate;
    property EndDate : TDateTime read FEndDate write SetEndDate;
  end;

var
  SelectDateRangeForm: TSelectDateRangeForm;

implementation

{$R *.dfm}

procedure TSelectDateRangeForm.selectButtonClick(Sender: TObject);
begin
	StartDate := StartDateEdit.Date;
    EndDate := EndDateEdit.Date;
    ModalResult := mrOk;
end;

procedure TSelectDateRangeForm.SetEndDate(const Value: TDateTime);
begin
  FEndDate := Value;
  EndDateEdit.Date := value;
end;

procedure TSelectDateRangeForm.SetStartDate(const Value: TDateTime);
begin
  FStartDate := Value;
  StartDateEdit.Date := value;
end;

procedure TSelectDateRangeForm.discardButtonClick(Sender: TObject);
begin
	ModalResult := mrCancel;
end;

end.
