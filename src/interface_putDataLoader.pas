unit interface_putDataLoader;

interface
    uses
      //--[ data ]--
      data_putChanel,
      //--[ interfaces ]--
      interface_putList,
      interface_universalDataLoader
     ;
    const
        IID_IPutDataLoader : TGUID = '{042CF65A-A7C6-45F2-B9AA-CF9D6A02EB6A}';

    type
        IPutDataLoader = interface( IUniversalDataLoader )
            ['{042CF65A-A7C6-45F2-B9AA-CF9D6A02EB6A}']

            function GetPutList() : IPutList;
            procedure SetPutList( const value : IPutList );


            property PutList : IPutList read GetPutList write SetPutList;
        end;

        IPutDataLoader2 = interface( IPutDataLoader )
          ['{5CBA3657-DC97-41E6-A90D-781D343DAC85}']

          procedure SetLoadActiveOnly( value : boolean );
          function GetLoadActiveOnly():Boolean;

          property LoadActiveOnly : Boolean read GetLoadActiveOnly write SetLoadActiveOnly;
        end;


implementation

end.
 