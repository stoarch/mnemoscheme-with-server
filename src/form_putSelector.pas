unit form_putSelector;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  sComboBox, sButton, sSkinProvider,
  //--[ data ]--
  data_put,
  //--[ list ]--
  list_put
  ;

type
  TPutSelectorForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    applyButton: TsButton;
    putCombo: TsComboBox;
    dateSelector: TsDateEdit;
    discardButton: TsButton;
  private
  	m_list : TPutList;

    function getDateTime: TDateTime;
    function getPutData: TPutData;
    procedure FillPuts;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    procedure SetPutList( const list : TPutList );

    property data : TPutData read getPutData;
    property dateTime : TDateTime read getDateTime;
  end;

var
  PutSelectorForm: TPutSelectorForm;

implementation

{$R *.dfm}

{ TPutSelectorForm }

function TPutSelectorForm.getDateTime: TDateTime;
begin
	result := dateSelector.Date;
end;

function TPutSelectorForm.getPutData: TPutData;
	var
    	data : TPutData;
begin
	data := TPutData.Create();
    data.Assign( m_list.Put[ putCombo.ItemIndex + 1 ] );
	result := data;
end;

procedure TPutSelectorForm.SetPutList(const list: TPutList);
begin
	m_list.Assign( list );
    FillPuts();
end;

procedure TPutSelectorForm.FillPuts();
	var
    	i : integer;
begin
	with putCombo.Items do
    begin
    	BeginUpdate();
        Clear();
        for i := 1 to m_list.count do
        begin
			Add( m_list[i].Caption );
        end;

    	EndUpdate();
    end;
end;


constructor TPutSelectorForm.Create(AOwner: TComponent);
begin
  inherited;

  m_list := TPutList.Create();
end;

destructor TPutSelectorForm.Destroy;
begin
	FreeAndNil( m_list );

  inherited;
end;

end.
