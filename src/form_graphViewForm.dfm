object GraphViewForm: TGraphViewForm
  Left = 294
  Top = 56
  Width = 835
  Height = 678
  Caption = #1043#1088#1072#1092#1080#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    827
    650)
  PixelsPerInch = 96
  TextHeight = 13
  object graphChart: TChart
    Left = 7
    Top = 9
    Width = 600
    Height = 595
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      #1043#1088#1072#1092#1080#1082#1080' '#1090#1077#1084#1087#1077#1088#1072#1090#1091#1088#1099' '#1055#1059#1058' '#1093#1093)
    Legend.Alignment = laBottom
    ScaleLastPage = False
    View3D = False
    TabOrder = 0
    Anchors = [akLeft, akTop, akRight, akBottom]
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 4227072
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object sBitBtn1: TsBitBtn
    Left = 719
    Top = 607
    Width = 103
    Height = 32
    Anchors = [akRight, akBottom]
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 1
    TabOrder = 1
    SkinData.SkinSection = 'BUTTON'
  end
  object printButton: TsBitBtn
    Left = 612
    Top = 608
    Width = 103
    Height = 32
    Anchors = [akRight, akBottom]
    Caption = #1055#1077#1095#1072#1090#1100
    TabOrder = 2
    OnClick = printButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object dataGrid: TDBGrid
    Left = 611
    Top = 9
    Width = 210
    Height = 595
    Anchors = [akTop, akRight, akBottom]
    DataSource = dataSource
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object graphValsVisible: TsCheckBox
    Left = 7
    Top = 613
    Width = 137
    Height = 19
    Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1085#1072' '#1075#1088#1072#1092#1080#1082#1077
    Anchors = [akLeft, akBottom]
    TabOrder = 4
    OnClick = graphValsVisibleClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object marksVisible: TsCheckBox
    Left = 144
    Top = 613
    Width = 121
    Height = 19
    Caption = #1052#1077#1090#1082#1080' '#1085#1072' '#1075#1088#1072#1092#1080#1082#1077
    Anchors = [akLeft, akBottom]
    TabOrder = 5
    OnClick = marksVisibleClick
    SkinData.SkinSection = 'CHECKBOX'
    ImgChecked = 0
    ImgUnchecked = 0
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 403
    Top = 325
  end
  object dataSource: TDataSource
    DataSet = dataTable
    Left = 666
    Top = 389
  end
  object dataTable: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 653
    Top = 323
  end
end
