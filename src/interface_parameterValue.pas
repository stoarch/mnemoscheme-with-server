unit interface_parameterValue;

interface

const
    IID_IParameterValue : TGUID = '{C88DAE32-C7C1-450C-81B6-18EE313EE573}';
type
    IParameterValue = interface
        ['{C88DAE32-C7C1-450C-81B6-18EE313EE573}']

        procedure Assign( const data : IParameterValue );
        procedure Clear();
        

        function GetValue() : Single;
        procedure SetValue( const avalue : Single );

        function GetDateReceived() : TDateTime;
        procedure SetDateReceived( const avalue : TDateTime );

        function GetIsValid() : boolean;
        procedure SetIsValid( const avalue : boolean );


        property DateReceived : TDateTime read GetDateReceived write SetDateReceived;
        property IsValid : boolean read GetIsValid write SetIsValid;
        property Value : Single read GetValue write SetValue;
    end;

implementation

end.
 