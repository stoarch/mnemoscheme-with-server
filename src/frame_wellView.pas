unit frame_wellView;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Mask,
  sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit, A3nalogGauge, sGroupBox,
  math, sFrameAdapter, ImgList, ComCtrls, ToolWin, sToolBar,
  Menus, iComponent, iVCLComponent, iCustomComponent, DateUtils,
  iLCDMatrix, sLabel, DB, kbmMemTable,
  sCheckBox, Grids, DBGrids,
  //--[ classes ]--
  class_valueList,
  class_valueMatrix,
  //--[ data ]--
  data_Well,
  //--[ interfaces ]--
  interface_value,
  interface_valueList,
  interface_valueMatrix,
  interface_wellData,
  //--[ frames ]--
  frame_channelView,
  frame_wellChannelView

  ;

type
  TWellViewFrame = class(TFrame)
    sFrameAdapter1: TsFrameAdapter;
    sToolBar1: TsToolBar;
    graphToolButton: TToolButton;
    ImageList: TImageList;
    tableToolButton: TToolButton;
    graphPopupMenu: TPopupMenu;
    FlowRateGraphMenuItem: TMenuItem;
    tablePopupMenu: TPopupMenu;
    FlowRateTableMenuItem: TMenuItem;
    Channel1_View: TWellChannelViewFrame;
    captionView: TsLabel;
    graphChart: TChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    Series4: TLineSeries;
    dataGrid: TDBGrid;
    graphValsVisible: TsCheckBox;
    marksVisible: TsCheckBox;
    dataSource: TDataSource;
    dataTable: TkbmMemTable;
    dataTableDate: TDateTimeField;
    dataTableValue: TFloatField;
    VsumPlus_MenuItem: TMenuItem;
    vsumMinus_menuItem: TMenuItem;
    VplusSumTableMenuItem: TMenuItem;
    VminusSumTableMenuItem: TMenuItem;
    procedure FlowRateGraphMenuItemClick(Sender: TObject);
    procedure FlowRateTableMenuItemClick(Sender: TObject);
    procedure graphValsVisibleClick(Sender: TObject);
    procedure marksVisibleClick(Sender: TObject);
    procedure VsumPlus_MenuItemClick(Sender: TObject);
    procedure vsumMinus_menuItemClick(Sender: TObject);
    procedure VplusSumTableMenuItemClick(Sender: TObject);
    procedure VminusSumTableMenuItemClick(Sender: TObject);
  private
    FCount: integer;
    FActive: boolean;
    FWellCaption: string;
    FData: IWellData;
    FNeedUpdateView: boolean;
    procedure SetCount(const Value: integer);
    function getChannelView(index: integer): TWellChannelViewFrame;
    procedure SetWellCaption(const Value: string);
    procedure SetData(const Value: IWellData);
    procedure ShowGraph(value: IValueMatrix; acaption, atitle: string; parmCaption : string );
    function GetFlowRateListFor(chanelID: integer; startDate, endDate : TDateTime; kind : integer ): IValueList;
    function GetFlowRateMatrix( startDate, endDate : TDateTime; kind : integer  ): IValueMatrix;
    procedure GetDateRange(var startDate, endDate: TDateTime);
    procedure ShowTable(value: IValueMatrix; acaption, atitle: string; startDate, endDate : TDateTime; paramCaption : string);
    procedure SetNeedUpdateView(const Value: boolean);
    procedure UpdateGraphAndTable( value : IValueMatrix );
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent; acount : integer );reintroduce;overload;
    destructor Destroy();override;

    procedure UpdateView();
    procedure UpdateGraphs;

    //--[ properties ]--
    property Active : boolean read FActive;
    property ChannelView[ index : integer ] : TWellChannelViewFrame read getChannelView;
    property Count : integer read FCount write SetCount;
    property WellCaption : string read FWellCaption write SetWellCaption;

    property Data : IWellData read FData write SetData;

    procedure UpdateData(const data: IWellData);

    property NeedUpdateView : boolean read FNeedUpdateView write SetNeedUpdateView;
  end;

implementation

uses
	form_graphViewForm,
    module_dataMain,
    class_value,
  	form_dateRangeSelectorForm,
    form_tableView,
    tool_systemLog
    , tool_ms_factories, interface_commonFactory, const_well, const_guids;

{$R *.dfm}

{ TWellViewFrame }
const
    MAX_COUNT = 1;

    FLOWRATE_VPLUSSUM = 1;
    FLOWRATE_VMINUSSUM = 2;
    FLOWRATE_CURRENT = 3;

constructor TWellViewFrame.Create(AOwner: TComponent; acount : integer);
var
  i: Integer;
begin
  inherited Create( aowner );

  FData := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IWellData;

  Count := acount;

  //TODO: Move to PrepareWellChannelsView
  for i := 1 to Count do
  begin
    with ChannelView[ i ] do
    begin
    	ChannelCaption := IntToStr( i );
        ChanelNo := i;
    end;
  end;
end;

destructor TWellViewFrame.Destroy;
begin
  inherited;
end;

function TWellViewFrame.getChannelView(index: integer): TWellChannelViewFrame;
begin
    result := self.FindComponent( Format( 'Channel%d_View', [index] )) as TWellChannelViewFrame;
end;

procedure TWellViewFrame.SetCount(const Value: integer);
	var
    	i : integer;
begin
  FCount := Max( 0, Min( Value, MAX_COUNT ) );

  for i := 1 to value do
  begin
    ChannelView[ i ].Show();
  end;

  for i := Value + 1 to MAX_COUNT do
  begin
    ChannelView[ i ].Hide();
  end;
end;

procedure TWellViewFrame.SetData(const Value: IWellData);
	var
    	i : integer;
begin
  FData.Assign( Value );
  WellCaption := FData.Caption;
  FActive := FData.Active;
  SetCount( Value.Count );

  //TODO: Refactor this method
  for i := 1 to Count do
  begin
	ChannelView[i].Data := FData[i];
  end;

  captionView.Caption := value.Caption;
end;

procedure TWellViewFrame.SetWellCaption(const Value: string);
begin
  FWellCaption := Value;
end;

procedure TWellViewFrame.UpdateView;
var
  i: Integer;
begin
    for i := 1 to Count do
    begin
      ChannelView[ i ].UpdateView();
    end;

    Refresh();
end;

procedure TWellViewFrame.UpdateData( const data : IWellData );
begin
	FData.Assign( data );
    FActive := FData.Active;
end;

procedure TWellViewFrame.UpdateGraphs();
begin
    try
        UpdateGraphAndTable( GetFlowRateMatrix( Now, IncDay(Now, 1), FLOWRATE_CURRENT ) );
    except

      on E:EAbort do
        ;//nothin
   end;
end;


procedure TWellViewFrame.UpdateGraphAndTable( value : IValueMatrix );
	var
    	i,j : integer;
        series : TLineSeries;
begin
  graphChart.SeriesList.Clear();

  
  for i := 1 to value.count do
  begin
    with( dataTable.FieldByName( 'Value' + intToStr(i) ) as TFloatField )do
    begin
        DisplayFormat := '###.##';
        DisplayLabel := '����. ' + intToStr(i);
    end;
  end;


  dataTable.Active := true;

  dataTable.First();
  for j := 1 to value.List[1].Count do
  begin
    dataTable.Append();
    dataTable.FieldByName('Date').AsDateTime := value.List[1].Value[j].Date;

    for i := 1 to value.count do
    begin
        dataTable.FieldByName('Value'+intToStr(i)).AsFloat := value.List[i].Value[j].Value;
    end;

  	dataTable.Post();
  end;

  for i := 1 to value.Count do
  begin
    series := TLineSeries.Create(self);
    series.Title := value.List[i].Title;
    series.XValues.DateTime := true;


    series.LinePen.Width := 2;

    series.Pointer.Style := psCircle;
    series.Pointer.HorizSize := 3;
    series.Pointer.VertSize := 3;

    for j := 1 to value.List[i].count do
    begin
      series.AddXY( value.List[i].Value[j].Date, value.List[i].Value[j].Value );
    end;


    graphChart.AddSeries(series);
  end;
end;

//TODO: Refactor get...ListFor

function TWellViewFrame.GetFlowRateListFor( chanelID : integer; startDate, endDate : TDateTime; kind : integer ): IValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %d ) and (date_query >= %s) and (date_query <= %s) and (param_kind_id = %d) order by date_query;';
    var
    	adata : IValue;
        factory : ICommonFactory;
begin
	result := nil;

    factory := GetCommonFactory();

    with msDataModule do
    begin
    	wellQuery.SQL.Text := Format( c_sql, [chanelId, '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',  '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''', kind ] );
        wellQuery.Open();
        if( wellQuery.RecordCount > 0 )then
        begin
        	result := factory.CreateInstance( CLSID_ValueList ) as IValueList;
		    result.Title := 'V' + IntToStr(chanelId);

            while not wellQuery.eof do
            begin
				adata := factory.CreateInstance( CLSID_Value ) as IValue;
                adata.Date := wellQuery.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := wellQuery.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                wellQuery.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;



function TWellViewFrame.GetFlowRateMatrix( startDate, endDate : TDateTime; kind : integer ): IValueMatrix;
  var
    adata : IValueList;
    value : IValueMatrix;
    i	 : integer;
begin
    value := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;

    for i := 1 to Count do
    begin
        try
          adata := GetFlowRateListFor( Data.Chanels[i].Index, startDate, endDate, kind );
          adata.Title := 'V' + IntToStr(i);

          value.Add( adata );
        except
            on e:EListError do
                Abort;//Log error message
        end;
    end;
    result := value;
end;


procedure TWellViewFrame.ShowGraph( value : IValueMatrix; acaption : string; atitle : string; parmCaption : string );
	var
        graphForm : TGraphViewForm;
        i : integer;
begin
        graphForm := TGraphViewForm.Create(self);
        try
            for i := 1 to value.Count do
            begin
              value.List[i].Title := parmCaption + IntToStr(i);
            end;

            graphForm.ValueMatrix := value;
            graphForm.Caption := acaption;
            graphForm.graphChart.Title.Text.Text := atitle;
            graphForm.ParamCaption := parmCaption;

            graphForm.ShowModal();
        finally
          FreeAndNil( graphForm );
        end;
end;

procedure TWellViewFrame.GetDateRange( var startDate, endDate : TDateTime ) ;
 var
    selector : TSelectDateRangeForm;
begin
	selector := TSelectDateRangeForm.Create(self);
    try
    	selector.startDate := now;
        selector.endDate := now + 1;
    	if( selector.ShowModal() = mrOk )then
        begin
			startDate := selector.StartDate;
            endDate := selector.EndDate;
        end
        else
        	Abort();
    finally
      FreeAndNil( selector );
    end;
end;


procedure TWellViewFrame.FlowRateGraphMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_CURRENT ),
        '������� �������',
        '������ �� �������� - ' + Data.Caption, 'V' );
end;


procedure TWellViewFrame.ShowTable( value : IValueMatrix; acaption : string; atitle : string; startDate, endDate : TDateTime; paramCaption : string  );
	var
        tableForm : TTableViewForm;
begin
        tableForm := TTableViewForm.Create(self);
        try
            tableForm.ValueMatrix := value;
            tableForm.Caption := acaption;
            tableForm.tableLabel.caption := atitle;
            tableForm.StartDate := startDate;
            tableForm.EndDate := endDate;
            tableForm.ParamCaption := ParamCaption;
            tableForm.ShowModal();
        finally
          FreeAndNil( tableForm );
        end;
end;


procedure TWellViewFrame.FlowRateTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

    ShowTable(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_CURRENT ),
        '������� ��������', '�������� - ' + Data.Caption,
        startDate, endDate,
        'V' );
end;

procedure TWellViewFrame.SetNeedUpdateView(const Value: boolean);
	var
    	i : integer;
begin
  FNeedUpdateView := Value;

  for i := 1 to count do
  begin
  	ChannelView[i].NeedUpdateView := value;
  end;
end;

procedure TWellViewFrame.graphValsVisibleClick(Sender: TObject);
	var
    	i : integer;
begin
	for i := 1 to graphChart.SeriesList.Count do
    begin
    	graphChart.SeriesList[i-1].Marks.Visible := graphValsVisible.Checked;
    end;
end;

procedure TWellViewFrame.marksVisibleClick(Sender: TObject);
	var
    	i : integer;
begin
	for i := 1 to graphChart.SeriesList.Count do
    begin
		( graphChart.SeriesList[i-1] as TLineSeries ).Pointer.Visible := marksVisible.Checked;
    end;
end;

procedure TWellViewFrame.VsumPlus_MenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_VPLUSSUM ),
        '������� �������',
        '������ �� �������� - ' + Data.Caption, 'V+ ���������' );
end;

procedure TWellViewFrame.vsumMinus_menuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_VMINUSSUM ),
        '������� �������',
        '������ �� �������� - ' + Data.Caption, 'V- c��������' );
end;


procedure TWellViewFrame.VplusSumTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

    ShowTable(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_VPLUSSUM ),
        '������� ��������: ��������� V+', '�������� - ' + Data.Caption,
        startDate, endDate,
        'V' );
end;


procedure TWellViewFrame.VminusSumTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

    ShowTable(
        GetFlowRateMatrix( startDate, endDate, FLOWRATE_VMINUSSUM ),
        '������� ��������: V-', '�������� - ' + Data.Caption,
        startDate, endDate,
        'V' );
end;

end.
