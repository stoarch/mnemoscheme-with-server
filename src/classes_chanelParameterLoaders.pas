unit classes_chanelParameterLoaders;

//TODO: Make channel parameter value loader common by RTTI (property name)

interface
    uses
      //--[ classes ]--
      class_creatableObject,
      //--[ data ]--
      ZDataset,
      //--[ interfaces ]--
      interface_mnemoschemeItemChannelValue,
      interface_parameterValueLoader,
      //--[ storage ]--
      storage_ObjectHashTable
      ;

type
    TParameterValueLoader = class( TCreatableObject, IParameterValueLoader )
    private
      FValue: IMnemoschemeItemChannelValue;
      FQuery: TZQuery;


    protected
        function GetCurrentDateQuery() : TDateTime;
        function GetCurrentValidation() : boolean;
        function GetCurrentValue() : Single;
    public
        function Execute() : boolean;virtual;

        function GetQuery() : TZQuery;
        function GetValue() : IMnemoschemeItemChannelValue;

        procedure SetQuery(const Value: TZQuery);
        procedure SetValue(const Value: IMnemoschemeItemChannelValue );

        property Query : TZQuery read GetQuery write SetQuery;
        property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
    end;

    TTemperatureValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;

    TPressureValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;

    TFlowRateValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;

    TBackwardFlowRateValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;

    TCurrentFlowRateValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;

    THeatRateValueLoader = class( TParameterValueLoader, IParameterValueLoader )
    public
        function Execute() : boolean;override;
    end;



implementation

function TParameterValueLoader.Execute: boolean;
begin
    result := false;
end;

function TParameterValueLoader.GetCurrentDateQuery: TDateTime;
begin
  Result := Query.FieldByName('date_query').AsDateTime;
end;

function TParameterValueLoader.GetCurrentValidation: boolean;
begin
  Result := Query.FieldByName('valid').AsBoolean;
end;

function TParameterValueLoader.GetCurrentValue: Single;
begin
    //TODO: Make magic strings to consts
  Result := Query.FieldByName('value').AsFloat;
end;

function TParameterValueLoader.GetQuery: TZQuery;
begin
    result := FQuery;
end;

function TParameterValueLoader.GetValue: IMnemoschemeItemChannelValue;
begin
    result := FValue;
end;

procedure TParameterValueLoader.SetQuery(const Value: TZQuery);
begin
  FQuery := Value;
end;

procedure TParameterValueLoader.SetValue(const Value: IMnemoschemeItemChannelValue);
begin
  FValue := Value;
end;

{ TTemperatureValueLoader }

function TTemperatureValueLoader.Execute: boolean;
begin
  //TODO: Remove indirection
  with Value do
  begin
    Temperature.Value 			:= GetCurrentValue;
    Temperature.DateReceived 	:= GetCurrentDateQuery;
    Temperature.IsValid 		:= GetCurrentValidation;
  end;
  result := true;
end;

{ TPressureValueLoader }

function TPressureValueLoader.Execute: boolean;
begin
  with Value do
  begin
      Pressure.Value 			:= GetCurrentValue;
      Pressure.DateReceived := GetCurrentDateQuery;
      Pressure.IsValid 	:= GetCurrentValidation;
  end;

  result := true;
end;

{ THeatRateValueLoader }

function THeatRateValueLoader.Execute: boolean;
begin
    with Value do
    begin
      HeatRate.Value 			:= GetCurrentValue;
      HeatRate.DateReceived		:= GetCurrentDateQuery;
      HeatRate.IsValid := GetCurrentValidation;
    end;

    result := true;
end;

{ TFlowRateValueLoader }

function TFlowRateValueLoader.Execute: boolean;
begin
    with Value do
    begin
      FlowRate.Value 			:= GetCurrentValue;
      FlowRate.DateReceived 		:= GetCurrentDateQuery;
      FlowRate.IsValid := GetCurrentValidation;
    end;

    result := true;
end;

{ TBackwardFlowRateValueLoader }

function TBackwardFlowRateValueLoader.Execute: boolean;
begin
    with Value do
    begin
      BackwardFlowRate.Value 			:= GetCurrentValue;
      BackwardFlowRate.DateReceived 		:= GetCurrentDateQuery;
      BackwardFlowRate.IsValid := GetCurrentValidation;
    end;

    Result := true;
end;

{ TCurrentFlowRateValueLoader }

function TCurrentFlowRateValueLoader.Execute: boolean;
begin
    with Value do
    begin
      CurrentFlowRate.Value 			:= GetCurrentValue;
      CurrentFlowRate.DateReceived 		:= GetCurrentDateQuery;
      CurrentFlowRate.IsValid := GetCurrentValidation;
    end;

    result := true;
end;

end.
