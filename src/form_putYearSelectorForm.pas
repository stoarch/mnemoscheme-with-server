unit form_putYearSelectorForm;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sComboBox,
  //--[ data ]--
  data_put,
  //--[ list ]--
  list_put
  ;

type
	//TODO: Refactor hierarchy of PutxxxSelectorForm
  TPutYearSelectorForm = class(TForm)
    putCombo: TsComboBox;
    applyButton: TsButton;
    discardButton: TsButton;
    yearCombo: TsComboBox;
  private
  	m_list : TPutList;

    procedure FillPuts;
    function getPutData: TPutData;
    function getYear: integer;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    procedure SetPutList( const list : TPutList );

    property data : TPutData read getPutData;
    property year : integer read getYear;
  end;

var
  PutYearSelectorForm: TPutYearSelectorForm;

implementation

{$R *.dfm}

{ TPutYearSelectorForm }

constructor TPutYearSelectorForm.Create(AOwner: TComponent);
begin
  inherited;

  m_list := TPutList.Create();
end;

destructor TPutYearSelectorForm.Destroy;
begin
	FreeAndNil( m_list );

  inherited;
end;

procedure TPutYearSelectorForm.FillPuts;
	var
    	i : integer;
begin
	with putCombo.Items do
    begin
    	BeginUpdate();
        Clear();
        for i := 1 to m_list.count do
        begin
			Add( m_list[i].Caption );
        end;
    	EndUpdate();
    end;
end;

function TPutYearSelectorForm.getPutData: TPutData;
	var
    	data : TPutData;
begin
	data := TPutData.Create();
    data.Assign( m_list.Put[ putCombo.ItemIndex + 1 ] );
	result := data;
end;

function TPutYearSelectorForm.getYear: integer;
begin
	result := yearCombo.ItemIndex + 2009;
end;

procedure TPutYearSelectorForm.SetPutList(const list: TPutList);
begin
	m_list.Assign( list );
    FillPuts();
end;

end.
