unit class_opcDataAccess;

interface

uses
  //--[ classes ]--
  class_opcItem,
  //--[ common ]--
  ComObj, SysUtils, Variants, ActiveX, Windows,
  //--[ dcl ]--
  XPCollections,
  //--[ opc ]--
  OPCutils, OPCDA, opcTypes
 ;

type
	EOPCAddGroup = class( Exception );
    EOPCAddItem = class( Exception );
    EOPCAdviseError = class( Exception );
    EOPCNotGoodItem = class( Exception );
    EOPCReadItem = class( Exception );
    EOPCDCOMInitializationError = class( Exception );
    EOPCRemoteServerError = class( Exception );

	TOPCDataAccess = class
    	private
        	m_server : OPCDA.IOPCServer;
        	m_group : IOPCItemMgt;
            m_items  : Variant;
            m_adviseSink : IAdviseSink;
            m_asyncConnection : integer;

            m_opcItems : TXDList;

            groupHandle : cardinal;
    		FQueryInterval: integer;


    		function getCount: integer;
    		function getItem(i : integer): Variant;
    		procedure SetQueryInterval(const Value: integer);

    		function getOPCItem( index : integer ): TOPCItem;

        public
        	constructor Create();
            destructor Destroy();override;

            procedure AddItem( opcCaption : string );
            procedure Connect( serverName : string );
            procedure Disconnect();
            procedure Advise();
            procedure UnAdvise();

            property Count : integer read getCount;
            property Item[ i : integer ] : Variant read getItem;
            property OPCItem[ index : integer ] : TOPCItem read getOPCItem;
            property QueryInterval : integer read FQueryInterval write SetQueryInterval;
    end;

implementation

	const
        QUERY_INTERVAL = 1000;//3000 - needed

type
  // class to receive IDataObject data change advises
  TOPCAdviseSink = class(TInterfacedObject, IAdviseSink)
  public
    procedure OnDataChange(const formatetc: TFormatEtc;
                            const stgmed: TStgMedium); stdcall;
    procedure OnViewChange(dwAspect: Longint; lindex: Longint); stdcall;
    procedure OnRename(const mk: IMoniker); stdcall;
    procedure OnSave; stdcall;
    procedure OnClose; stdcall;
  end;

  // class to receive IConnectionPointContainer data change callbacks
  TOPCDataCallback = class(TInterfacedObject, IOPCDataCallback)
  public
    function OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
      pErrors: PResultList): HResult; stdcall;
    function OnCancelComplete(dwTransid: DWORD; hGroup: OPCHANDLE):
      HResult; stdcall;
  end;

// TOPCAdviseSink methods

// OPC standard says this is the only method we need to fill in
procedure TOPCAdviseSink.OnDataChange(const formatetc: TFormatEtc;
                                      const stgmed: TStgMedium);
var
  PG: POPCGROUPHEADER;
  PI1: POPCITEMHEADER1ARRAY;
  PI2: POPCITEMHEADER2ARRAY;
  PV: POleVariant;
  I: Integer;
  PStr: PWideChar;
  NewValue: string;
  WithTime: Boolean;
  ClientHandle: OPCHANDLE;
  Quality: Word;
begin
  // the rest of this method assumes that the item header array uses
  // OPCITEMHEADER1 or OPCITEMHEADER2 records,
  // so check this first to be defensive
  if (formatetc.cfFormat <> OPCSTMFORMATDATA) and
      (formatetc.cfFormat <> OPCSTMFORMATDATATIME) then Exit;
  // does the data stream provide timestamps with each value?
  WithTime := formatetc.cfFormat = OPCSTMFORMATDATATIME;

  PG := GlobalLock(stgmed.hGlobal);
  if PG <> nil then
  begin
    // we will only use one of these two values, according to whether
    // WithTime is set:
    PI1 := Pointer(PAnsiChar(PG) + SizeOf(OPCGROUPHEADER));
    PI2 := Pointer(PI1);
    if Succeeded(PG.hrStatus) then
    begin
      for I := 0 to PG.dwItemCount - 1 do
      begin
        if WithTime then
        begin
          PV := POleVariant(PAnsiChar(PG) + PI1[I].dwValueOffset);
          ClientHandle := PI1[I].hClient;
          Quality := (PI1[I].wQuality and OPC_QUALITY_MASK);
        end
        else begin
          PV := POleVariant(PAnsiChar(PG) + PI2[I].dwValueOffset);
          ClientHandle := PI2[I].hClient;
          Quality := (PI2[I].wQuality and OPC_QUALITY_MASK);
        end;
        if Quality = OPC_QUALITY_GOOD then
        begin
          // this test assumes we're not dealing with array data
          if TVarData(PV^).VType <> VT_BSTR then
          begin
            NewValue := VarToStr(PV^);
          end
          else begin
            // for BSTR data, the BSTR image follows immediately in the data
            // stream after the variant union;  the BSTR begins with a DWORD
            // character count, which we skip over as the BSTR is also
            // NULL-terminated
            PStr := PWideChar(PAnsiChar(PV) + SizeOf(OleVariant) + 4);
            NewValue := WideString(PStr);
          end;
          if WithTime then
          begin
            Writeln('New value for item ', ClientHandle, ' advised:  ',
                    NewValue, ' (with timestamp)');
          end
          else begin
            Writeln('New value for item ', ClientHandle, ' advised:  ',
                    NewValue);
          end;
        end
        else begin
          Writeln('Advise received for item ', ClientHandle,
                  ' , but quality not good');
        end;
      end;
    end;
    GlobalUnlock(stgmed.hGlobal);
  end;
end;

procedure TOPCAdviseSink.OnViewChange(dwAspect: Longint; lindex: Longint);
begin
end;

procedure TOPCAdviseSink.OnRename(const mk: IMoniker);
begin
end;

procedure TOPCAdviseSink.OnSave;
begin
end;

procedure TOPCAdviseSink.OnClose;
begin
end;

// TOPCDataCallback methods

function TOPCDataCallback.OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
var
  ClientItems: POPCHANDLEARRAY;
  Values: POleVariantArray;
  Qualities: PWORDARRAY;
  I: Integer;
  NewValue: string;
begin
  Result := S_OK;
  ClientItems := POPCHANDLEARRAY(phClientItems);
  Values := POleVariantArray(pvValues);
  Qualities := PWORDARRAY(pwQualities);
  for I := 0 to dwCount - 1 do
  begin
    if Qualities[I] = OPC_QUALITY_GOOD then
    begin
      NewValue := VarToStr(Values[I]);
      Writeln('New callback for item ', ClientItems[I], ' received, value:  ',
              NewValue);
    end
    else begin
      Writeln('Callback received for item ', ClientItems[I],
              ' , but quality not good');
    end;
  end;
end;

function TOPCDataCallback.OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
begin
  Result := OnDataChange(dwTransid, hGroup, hrMasterquality, hrMastererror,
    dwCount, phClientItems, pvValues, pwQualities, pftTimeStamps, pErrors);
end;

function TOPCDataCallback.OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
  pErrors: PResultList): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;

function TOPCDataCallback.OnCancelComplete(dwTransid: DWORD;
  hGroup: OPCHANDLE): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;




{ TOPCDataAccess }
procedure TOPCDataAccess.AddItem(opcCaption: string);
begin
	m_opcItems.Add( refobj(TOPCItem.Create(opcCaption), true))
end;

procedure TOPCDataAccess.Advise;
	var
    	HR : HRESULT;
begin
  m_adviseSink := TOPCAdviseSink.Create();
  HR := GroupAdviseTime(m_group, m_adviseSink, m_asyncConnection);
  if Failed(HR) then
  begin
 	raise EOPCAdviseError.Create( 'Unable to advise' );
  end;
end;

procedure TOPCDataAccess.Connect(serverName: string);
	var
    	HR : HRESULT;
        itemType : word;
        serverInfo : PCoServerInfo;
        qiIf : PMultiQIArray;
        pIID : PGUID;
  		i: Integer;
        handle : cardinal;
begin
  m_server := CreateComObject(ProgIDToClassID(serverName)) as IOPCServer;

  if( Failed( HR ) )then
     raise EOPCRemoteServerError.Create( 'Unable to connect to remote server' );

  HR := ServerAddGroup(m_server,'Vzljot',True,QueryInterval,0, m_group, groupHandle);
  if Failed(HR) then
  begin
    raise EOPCAddGroup.Create( 'Unable to add to group' );
  end;

  for i := 1 to Count do
  begin
    HR := GroupAddItem(m_group, OPCItem[i].Caption, 0, VT_EMPTY, handle, ItemType);

    if Failed(HR) then
    begin
      raise EOPCAddItem.Create( 'Unable to add to group' );
    end;

    OPCItem[i].Handle := handle;
  end;
end;

constructor TOPCDataAccess.Create;
begin
	FQueryInterval := QUERY_INTERVAL;
    m_opcItems := TXDList.Create(TXDObject);
end;

destructor TOPCDataAccess.Destroy;
begin
	FreeAndNil( m_opcItems );
	FreeAndNil( m_server );

	inherited;
end;

procedure TOPCDataAccess.Disconnect;
begin
	if( assigned( m_server ) )then
		m_server.RemoveGroup(groupHandle,false);
    m_server := nil;
end;

function TOPCDataAccess.getCount: integer;
begin
	result := m_opcItems.Count;
end;

function TOPCDataAccess.getOPCItem( index : integer ): TOPCItem;
	var
    	xdItem : TXDObject;
begin
	m_opcItems.Item[ index-1 ].GetTyped(TXDObject, xdItem);

	result := xdItem.value as TOPCItem;
end;

function TOPCDataAccess.getItem(i : integer): Variant;
	var
    	HR : HRESULT;
        handle : cardinal;
        itemQuality : word;
        itemValue : string;
begin
  handle := getOPCItem( i ).Handle;

  HR := ReadOPCGroupItemValue(m_group, handle, ItemValue, ItemQuality);
  if Succeeded(HR) then
  begin
    if (ItemQuality and OPC_QUALITY_MASK) = OPC_QUALITY_GOOD then
    begin
    	result := itemValue;
        exit;
    end
    else begin
      //Writeln('Item 0 value was read synchronously, but quality was not good');
      raise EOPCNotGoodItem.Create( 'Item is not good' );
    end;
  end
  else begin
    //Writeln('Failed to read item 0 value synchronously');
    raise EOPCReadItem.Create( 'Unable to read the item' );
  end;

  result := itemValue;
end;

procedure TOPCDataAccess.SetQueryInterval(const Value: integer);
begin
  FQueryInterval := Value;
end;

procedure TOPCDataAccess.UnAdvise;
begin
    GroupUnadvise(m_group, m_AsyncConnection);
end;

end.
