unit form_putListSelector;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinProvider, StdCtrls, Buttons, sBitBtn, sAlphaListBox,
  sCheckListBox,
  //--[ list ]--
  list_put, sComboBox, sEdit, sSpinEdit, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit
  ;

type
  TPutListSelectorForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    clbPutList: TsCheckListBox;
    bbtAccept: TsBitBtn;
    bbtDiscard: TsBitBtn;
    speChanelNo: TsSpinEdit;
    cbxParms: TsComboBox;
    dedRangeStart: TsDateEdit;
    dedRangeEnd: TsDateEdit;
    procedure clbPutListClick(Sender: TObject);
  private
  	m_putList : TPutList;
    m_selectedPutList : TPutList;

    function getPutList: TPutList;
    function getSelectedPuts: TPutList;
    procedure setPutList(const Value: TPutList);
    procedure setSelectedPuts(const Value: TPutList);
    procedure UpdateUI;
    function getChanelNo: integer;
    function getParamKind: integer;
    function IsAnyChecked: boolean;
    function getDateEnd: TDateTime;
    function getDateStart: TDateTime;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    property ChanelNo 	: integer 	read getChanelNo;
    property DateEnd	: TDateTime read getDateEnd;
    property DateStart	: TDateTime read getDateStart;
    property ParamKind  : integer   read getParamKind;
    property PutList 	: TPutList 	read getPutList write setPutList;
    property SelectedPuts : TPutList read getSelectedPuts write setSelectedPuts;
  end;

var
  PutListSelectorForm: TPutListSelectorForm;

implementation

uses
  data_put;

{$R *.dfm}

{ TPutListSelectorForm }

constructor TPutListSelectorForm.Create(AOwner: TComponent);
begin
  inherited;

	m_putList := TPutList.Create();
    m_selectedPutList := TPutList.Create();
end;

destructor TPutListSelectorForm.Destroy;
begin
	if( assigned( m_putList ))then
    	FreeAndNil( m_putList );

    if( assigned( m_selectedPutList ))then
    	FreeAndNil( m_selectedPutList );

  inherited;
end;

function TPutListSelectorForm.getPutList: TPutList;
begin
	result := m_putList;
end;

function TPutListSelectorForm.getSelectedPuts: TPutList;
	var
      	i : integer;
begin
	m_selectedPutList.Clear();

    for i := 0 to clbPutList.Items.Count - 1 do
    begin
		if( clbPutList.Checked[i] )then
        	m_selectedPutList.Add( TPutData.Create( m_putList.Put[i+1] ) );
    end;

    Result := m_selectedPutList;
end;

procedure TPutListSelectorForm.setPutList(const Value: TPutList);
begin
	m_putList.Clear();
    m_putList.Assign( value );

    UpdateUI();
end;

procedure TPutListSelectorForm.UpdateUI();
	var
      	i : Integer;
begin
	clbPutList.Items.BeginUpdate();

    for i := 1 to m_putList.Count do
    begin
		clbPutList.Items.Add( m_putList.Put[i].Caption );
    end;

    clbPutList.Items.EndUpdate();
end;

procedure TPutListSelectorForm.setSelectedPuts(const Value: TPutList);
begin
	//TODO: Implement this
end;

function TPutListSelectorForm.getChanelNo: integer;
begin
	Result := speChanelNo.Value;
end;

function TPutListSelectorForm.getParamKind: integer;
begin
	result := cbxParms.ItemIndex + 1;
end;

procedure TPutListSelectorForm.clbPutListClick(Sender: TObject);
begin
	bbtAccept.Enabled := isAnyChecked;
end;

function TPutListSelectorForm.IsAnyChecked(): boolean;
	var
      	i : integer;
begin
	result := false;

    for i := 0 to clbPutList.Items.Count - 1 do
    begin
		if( clbPutList.Checked[ i ] )then
        begin
          result := true;
          exit;
        end;
    end;
end;


function TPutListSelectorForm.getDateEnd: TDateTime;
begin
	result := dedRangeEnd.Date;
end;

function TPutListSelectorForm.getDateStart: TDateTime;
begin
	result := dedRangeStart.Date;
end;

end.
