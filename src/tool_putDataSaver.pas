unit tool_putDataSaver;

interface
	uses
    	//--[ data ]--
        data_put
        ;

	type
    	PutDataSaver = class
        	class procedure Save( data : TPutData );
        end;

implementation

	uses
    	//--[ common ]--
        db, sysUtils, windows,
        //--[ constants ]--
        const_put,
    	//--[ data ]--
        module_dataMain
        ;



{ PutDataSaver }

class procedure PutDataSaver.Save(data: TPutData);
    const
      	c_sql = 'insert into params (chanel_id, param_kind_id, "value", date_query, "valid") ' +
        		' values ( %0:d, %1:d, %2:5.2f, ''%3:s'', %4:s ) ';

    function GetBoolStr( value : boolean ) : string;
    begin
      result := 'False';
      if value then
      	result	:= 'True';
    end;

    procedure SaveParams( index : integer; kind : integer; value : Single; isValid : boolean );
    	var
          	settings : TFormatSettings;
    begin
    	settings.DecimalSeparator := '.';

      with msDataModule, queryData do
      begin
        SQL.Text := Format( c_sql,
                      [
                          index,
                          kind,
                          value,
                          FormatDateTime( 'dd.mm.yyyy hh:nn:ss.zzz', Now ),
                          GetBoolStr( isValid )
                      ],
                      settings
                    );
        ExecSQL();
      end;
    end;

	var
    	i : integer;
begin
  	for i := 1 to data.Count do
    with data.Chanels[i], Value do
    begin
      	SaveParams( Index, TEMPERATURE_ID, 	Temperature, 	IsValidTemperature 	);
      	SaveParams( Index, PRESSURE_ID, 	Pressure, 		IsValidPressure		);
      	SaveParams( Index, FLOWRATE_ID, 	FlowRate, 		IsValidFlowRate		);
    end;
end;

//    procedure AppendParams( dateQuery : TDateTime; kindId : integer; value : real; valid : boolean );
//    begin
//    	with msDataModule do
//        begin
//        	if( not params_table.Active )then
//            	params_table.Open();
//
//			params_table.Append();
//            params_table.FieldByName('date_query').AsDateTime := dateQuery;
//            params_table.FieldByName('value').AsFloat := value;
//            params_table.FieldByName('param_kind_id').AsInteger := kindId;
//            params_table.FieldByName('valid').AsBoolean := valid;
//            params_table.Post();
//        end;
//    end;
//
//    var
//      	oldPutTableActive, oldParamsActive, oldChanelActive : boolean;
//begin
//	try
//      with MSDataModule do
//      begin
//        oldChanelActive		:=  chanel_table.Active;
//        oldParamsActive 	:= 	params_table.Active;
//      	oldPutTableActive 	:= 	put_table.Active;
//
//        try
//          if( not put_table.Active )then
//              put_table.Open();
//
//          params_table.Close();
//          put_table.First();
//          if( put_table.Locate( 'id', data.index, [loCaseInsensitive] ) )then
//          begin
//              if( not chanel_table.Active )then
//                  chanel_table.Open();
//
//            chanel_table.First();
//            for i := 1 to data.Count do
//            begin
//          		params_table.Close();
//
//                if( chanel_table.Locate('id', data.Chanels[i].index,[]))then
//                with data.Chanels[i].Value do
//                begin
//                    params_table.Open();
//
//                    AppendParams( now, TEMPERATURE_ID, Temperature, IsValidTemperature );
//                    AppendParams( now, PRESSURE_ID, Pressure, IsValidPressure );
//                    AppendParams( now, FLOWRATE_ID, FlowRate, IsValidFlowRate );
//                    AppendParams( now, HEATRATE_ID, HeatRate, IsValidHeatRate );
//                end;
//            end;
//          end;
//        finally
//          params_table.Active	:=	oldParamsActive;
//          chanel_table.Active	:=	oldChanelActive;
//          put_table.Active		:=	oldPutTableActive;
//        end;
//      end;
//    except
//    	on e:exception do
//        	raise Exception.Create( 'TPutScanerMainForm.SaveCurrentPutData->'+#10#13+ e.Message );
//    end;
//end;

end.
