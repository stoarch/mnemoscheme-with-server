unit tool_putSettingsLoader;

interface
	uses
    	//--[ data ]--
        data_put,
        data_putChanel,
        //--[ forms ]--
		form_progressDisplayer,
        //--[ interfaces ]--
        interface_putChanelData,
        interface_putData,
        interface_putList,
    	//--[ list ]--
        list_put
        ;

	type
      	TPutSettingsOption = ( psoChanel, psoOPC, psoLimits );
        TPutSettingsOptions = set of TPutSettingsOption;

    	PutSettingsLoader = class
//TODO: Optimize performance of reading
        	class function ReadPutSettings(
            					putCount : integer;
                                options  : TPutSettingsOptions = [psoChanel,psoOPC,psoLimits];
                                needInactive : Boolean = True
            				) : IPutList;overload;

        	class function ReadPutSettings(
            				options  : TPutSettingsOptions = [psoChanel,psoOPC,psoLimits];
                            needInactive : boolean = true
                           ) : IPutList;overload;

          private
            class procedure ReadChanelControlSettings(chData: IPutChanelData);
            class procedure ReadOPCSettings(chData: IPutChanelData);
            class procedure ReadPutViewSettings(data: IPutData; progress : TProgressDisplayerForm; options  : TPutSettingsOptions = [psoOPC,psoLimits]);
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils,
        //--[ constants ]--
        const_put,
        //--[ data ]--
		module_dataMain
        , interface_commonFactory, tool_ms_factories, const_guids;


{ PutSettingsLoader }

class function PutSettingsLoader.ReadPutSettings(
									options  	 : TPutSettingsOptions = [psoChanel,psoOPC,psoLimits];
									needInactive : boolean = true
								): IPutList;
begin
	result := ReadPutSettings( MSDataModule.put_table.RecordCount, options, needInactive );
end;


class procedure PutSettingsLoader.ReadChanelControlSettings( chData : IPutChanelData );
begin
	with msDatamodule do
    begin
    	if( not chanel_control_settings_table.Active )then
        	chanel_control_settings_table.Open();

      chanel_control_settings_table.First();
      while not chanel_control_settings_table.Eof do
      begin
        case( chanel_control_settings_table.FieldByName('param_kind_id').asInteger )of
            TEMPERATURE_ID :
                with chData.Params.TemperatureRange do
                begin
                    Min := chanel_control_settings_table.FieldByName('min').AsFloat;
                    Max := chanel_control_settings_table.FieldByName('max').AsFloat;
                end;

            PRESSURE_ID :
                with chData.Params.PressureRange do
                begin
                    Min := chanel_control_settings_table.FieldByName('min').AsFloat;
                    Max := chanel_control_settings_table.FieldByName('max').AsFloat;
                end;

            FLOWRATE_ID :
                with chData.Params.FlowRateRange do
                begin
                    Min := chanel_control_settings_table.FieldByName('min').AsFloat;
                    Max := chanel_control_settings_table.FieldByName('max').AsFloat;
                end;

            HEATRATE_ID :
                with chData.Params.HeatRateRange do
                begin
                    Min := chanel_control_settings_table.FieldByName('min').AsFloat;
                    Max := chanel_control_settings_table.FieldByName('max').AsFloat;
                end;
        end;

        chanel_control_settings_table.Next;
      end;
    end;
end;

class procedure PutSettingsLoader.ReadOPCSettings( chData : IPutChanelData );
begin
  with msDataModule do
  begin
  	if( not opc_params_table.Active )then
    	opc_params_table.Open();

    opc_params_table.First();
    while not opc_params_table.Eof do
    begin
      case opc_params_table.FieldByName('param_kind_id').AsInteger of
          TEMPERATURE_ID:
              begin
                chData.temperatureOPC := opc_params_table.FieldByName( 'value' ).AsString;
              end;

          PRESSURE_ID :
              begin
                chData.pressureOPC := opc_params_table.FieldByName( 'value' ).AsString;
              end;

          FLOWRATE_ID :
              begin
                chData.flowRateOPC := opc_params_table.FieldByName( 'value' ).AsString;
              end;

          HEATRATE_ID :
          	  begin
              	chData.heatRateOPC := opc_params_table.FieldByName( 'value' ).AsString;
              end;
      end;

      opc_params_table.Next();
    end;
  end;
end;

class procedure PutSettingsLoader.ReadPutViewSettings( data : IPutData; progress : TProgressDisplayerForm; options  : TPutSettingsOptions = [psoOPC,psoLimits] );
var
  i : Integer;
  chData : IPutChanelData;
  oldOPCActive, oldLimitsActive : boolean;
  factory : ICommonFactory;
begin
    factory := GetPutFactory();

	try
      with MSDataModule do
      begin
      		if( not chanel_table.Active )then
            	chanel_table.Open();

            oldOPCActive := chanel_control_settings_table.Active;
            oldLimitsActive	:=	opc_params_table.Active;

            if( not (psoOpc in options))then
            	opc_params_table.Close();

            if( not (psoLimits in options))then
            	chanel_control_settings_table.Close();

      	try
            chanel_table.First();

              chData := factory.CreateInstance( CLSID_MnemoschemeItemChanelData ) as IPutChanelData;
              data.Clear();

              progress.MaxValue := chanel_table.RecordCount;

              for i := 1 to chanel_table.RecordCount do
              begin
                  if( progress.IsCanceled )then
                  begin
                      Abort;
                  end;

                  progress.progress := i;
                  progress.SubMessage := '�������� ����� ' + IntToStr( i ) +  ' �� ' + IntToStr( chanel_table.RecordCount );

                  chData.caption := chanel_table.FieldByName('caption').AsString;
                  chData.active  := chanel_table.FieldByName('active').AsBoolean;
                  chData.index   := chanel_table.FieldByName('id').AsInteger;

                  if( psoOPC in options )then
                  	ReadOPCSettings( chData );

                  if( psoLimits in options )then
                      ReadChanelControlSettings( chData );

                  data.Add( chData );

                  chanel_table.next();
              end;
        finally
          	chanel_control_settings_table.Active := oldLimitsActive;
            opc_params_table.Active 			 := oldOPCActive;
        end;
      end;
    except
      	on e:EAbort do
        begin

        	raise;
        end;

    	on e:exception do
        	raise Exception.Create('TPutScanerMainForm.ReadPutViewSettings->'+#10#13+e.message);
    end;
end;

class function PutSettingsLoader.ReadPutSettings(putCount: integer; options  : TPutSettingsOptions; needInactive: Boolean): IPutList;
const
	PUT_CAPTION_ITEM = 'Caption';
    PUT_ACTIVE_ITEM  = 'Active';
    PUT_COUNT_ITEM   = 'Count';

    COMMON_SECTION = 'Mnemoscheme';
var
  i		: Integer;
  data 	: IPutData;
  progress	: TProgressDisplayerForm;
  oldParamsActive : boolean;
  factory : ICommonFactory;
begin
  	result 		:= nil;

    factory := GetPutFactory();
    
    oldParamsActive := msDataModule.params_table.Active;

  	progress	:= TProgressDisplayerForm.Create(nil);
    try
      	progress.Show();
        progress.WorkMessage := '�������� ������ �����...';

        with msDataModule do
        try
          result := GetPutFactory().CreateInstance( CLSID_PutList ) as IPutList;

          params_table.Close();

          if( not msDataModule.put_table.Active )then
            put_table.Open();

          if( not ( psoChanel in options ))then
          	put_table.DisableControls();

          i := 0;
          msDataModule.put_table.First();

          //TODO: Add progress indicator
          progress.TaskMax := putCount;
          while ( not msDataModule.put_table.eof ) and ( i < putCount )  do
          begin
            if( progress.IsCanceled )then
            begin
                Abort();
            end;

            if not( ( msDataModule.put_table.fieldByName('active').AsBoolean )
                    or( needInactive ) )
            then
                begin
                    msDataModule.put_table.Next();
                    continue;
                end;

          	progress.TaskProgress := i;

            i	 := i + 1;
            data := factory.CreateInstance( CLSID_MnemoschemeItemData ) as IPutData;

            data.Name 	 := msDataModule.put_table.fieldByName('name').AsString;
            data.Caption := msDataModule.put_table.fieldByName('caption').AsString;
            data.Active	 := msDataModule.put_table.fieldByName('active').AsBoolean;
            data.Index	 := msDataModule.put_table.fieldByName('id').AsInteger;

            progress.WorkMessage := '�������� ������ ' + data.Name + '...';

            if( psoChanel in options ) then
            	ReadPutViewSettings( data, progress, options );

            result.Add( data );

            msDataModule.put_table.Next();
          end;
        except
          	on e:EAbort do
            begin
            	if( assigned( result ))then
                	result := nil;
                raise;
            end;

            on e:exception do
                raise Exception.Create( 'TPutScanerMainForm.ReadPutSettings->'+#10#13+ e.message );
        end;
    finally
      	msDataModule.put_table.EnableControls;

        msDataModule.params_table.Active := oldParamsActive;

		if( assigned( progress ) )then
      		FreeAndNil( progress );
    end;
end;


end.
