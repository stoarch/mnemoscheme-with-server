object ProgressDisplayerForm: TProgressDisplayerForm
  Left = 581
  Top = 260
  BorderStyle = bsDialog
  Caption = #1054#1073#1088#1072#1073#1086#1090#1082#1072' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
  ClientHeight = 158
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Lucida Console'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    350
    158)
  PixelsPerInch = 96
  TextHeight = 16
  object lblWait: TsLabel
    Left = 14
    Top = 12
    Width = 321
    Height = 17
    AutoSize = False
    Caption = #1055#1086#1076#1086#1078#1076#1080#1090#1077' '#1087#1086#1078#1072#1083#1091#1081#1089#1090#1072'...'
    ParentFont = False
    WordWrap = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object lblSubMessage: TsLabel
    Left = 14
    Top = 33
    Width = 321
    Height = 17
    AutoSize = False
    Caption = #1048#1076#1077#1090' '#1086#1073#1088#1072#1073#1086#1090#1082#1072'...'
    ParentFont = False
    WordWrap = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object pbrWorkStatus: TsProgressBar
    Left = 18
    Top = 78
    Width = 313
    Height = 17
    Anchors = [akLeft, akBottom]
    TabOrder = 0
    SkinData.SkinSection = 'GAUGE'
  end
  object btnCancel: TsButton
    Left = 107
    Top = 106
    Width = 135
    Height = 37
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = #1054#1090#1082#1072#1079#1072#1090#1100#1089#1103
    TabOrder = 1
    OnClick = btnCancelClick
    SkinData.SkinSection = 'BUTTON'
  end
  object pbrTaskStatus: TsProgressBar
    Left = 18
    Top = 58
    Width = 313
    Height = 17
    Anchors = [akLeft, akBottom]
    TabOrder = 2
    SkinData.SkinSection = 'GAUGE'
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 224
    Top = 128
  end
end
