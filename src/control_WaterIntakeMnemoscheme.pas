unit control_WaterIntakeMnemoscheme;

interface
    uses
        //--[ common ]--
        classes, ComCtrls, controls, graphics, ExtCtrls, SysUtils,
        //--[ alpha skins ]--
        sComboBox, slabel, sPanel, sGroupBox, sSplitter, sToolbar,
        sSkinProvider, sSkinManager, sCustomComboEdit, sConst,
        sedit,
        //--[ controls ]--
        control_commonMnemoscheme,
        //--[ data ]--
        data_put, data_well,
        //--[ flex ]--
        FlexBase, FlexUtils, FlexControls, FlexProps,
        //--[ frames ]--
        frame_putView, frame_wellView,
        //--[ lists ]--
        list_well,
        //--[interfaces]--
        interface_commonFactory,
        interface_wellsCalculator,
        interface_wellData,
        interface_wellList,
        //--[Tools]--
        tool_environment,
        //--[constants]--
        const_well
        ;

type

 TWaterIntakeMnemoscheme = class(TCommonMnemoscheme)
 private

  TotalWaterIntakeView: TsEdit;
  totalSumWIView: TsEdit;

    procedure SetWellList(const Value: IWellList);
    function FindWell(name: string): TWellViewFrame; //TODO: Make IWellView->TWellViewFrame usage for common logic
    procedure UpdateCalculatedWellValues;
    function getWellCount: integer;
    function getWellView(index: integer): TWellViewFrame;
    function CreateWellsCalculator: IWellsCalculator;
    function CreateWellView(data: IWellData): TWellViewFrame;
    function FindWellIndexBy(data: IWellData): Integer;
    function GetCurrentData: IWellData;
    function GetWellsFactory: ICommonFactory;
    function getWellIndex(name: string): integer;


    procedure SetupTotalWaterIntakeView;
    procedure SetupTotalSumWaterIntakeView;
    procedure UpdateActiveWelLGraph;
    procedure UpdateWellViewState(well: TWellViewFrame; data: IWellData);
    function GetWellList: IWellList;


 protected
    procedure CreateControls;override;

    function GetItemCaption( index : integer ): string;override;
    function GetItemIndex( index : integer ) : integer;override;
    procedure Handle_TabChanged(Sender: TObject); override;

    function MakeViewTab( aindex : integer; acaption : String ): TTabSheet;override;
    procedure MnemoschemePanelMouseUp(Sender: TObject; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer); override;
    procedure SetupControls;override;

    procedure UpdateMnemoschemeView;override;
    procedure UpdateViews;override;


    property WellView[ index : integer ] : TWellViewFrame read getWellView;
    property WellCount : integer read getWellCount;

 public
  constructor create(AOwner: TComponent); override;
  property WellList: IWellList read GetWellList write SetWellList;

  function GetWellName( index : integer ): string;

  procedure UpdateSettings;override;
 end;



implementation
uses
  JclAlgorithms, JclBase, JclContainerIntf, JclVectors
  ;

{ TWaterIntakeMnemoscheme }

constructor TWaterIntakeMnemoscheme.create(AOwner: TComponent);
begin
  inherited;

  MnemoschemePanel.LoadFromFile( WATER_INTAKE_MODEL );
end;

procedure TWaterIntakeMnemoscheme.CreateControls;
begin
  inherited;

  TotalWaterIntakeView := TsEdit.Create(Self);
  totalSumWIView := TsEdit.Create(Self);
end;

procedure TWaterIntakeMnemoscheme.SetWellList(const Value: IWellList);
begin
  FDataList := Value;
end;


function TWaterIntakeMnemoscheme.GetCurrentData: IWellData;
begin
  Result := WellList.FindByCaption(functionPages.ActivePage.Caption) as IWellData;
end;

procedure TWaterIntakeMnemoscheme.UpdateWellViewState(well: TWellViewFrame;
    data: IWellData);
begin
  well.NeedUpdateView := false;
  well.Data := data;
  well.UpdateView();
end;

function TWaterIntakeMnemoscheme.CreateWellView(data: IWellData):
    TWellViewFrame;
var
  well: TWellViewFrame;
begin
  well := TWellViewFrame.Create(self, data.Count);
  well.Align := alClient;
  well.Name := GetWellName(FindWellIndexBy(data));

  well.Parent := functionPages.ActivePage;
  Result := well;
end;

function TWaterIntakeMnemoscheme.FindWellIndexBy(data: IWellData): Integer;
begin
  Result := WellList.IndexOf(data);
end;

function TWaterIntakeMnemoscheme.MakeViewTab( aindex : integer; acaption : String ): TTabSheet;
var
  tab: TTabSheet;
begin
    tab := TTabSheet.Create( functionPages );
    tab.Caption := aCaption;

    tab.PageControl := functionPages;
    tab.Parent := functionPages;

    tab.Name := GetWellName(aindex) + 'Tab';

    Result := tab;
end;

function TWaterIntakeMnemoscheme.GetWellName( index : integer ): string;
begin
	result := Format( 'Well%dView', [index] );
end;

procedure TWaterIntakeMnemoscheme.UpdateSettings();
	var
    	i, j : integer;
        data : IWellData;
        Well : TWellViewFrame;
begin
    for i := 1 to WellList.Count do
    begin
    	data := WellList[i];
        Well := FindWell( data.name );

        if not assigned( well ) then
            continue;

		for j := 1 to data.Count do
        begin
        	with Well.ChannelView[j].Params do
            begin
                FlowRateRange.Min := data.Chanels[j].Params.FlowRateRange.Min;
                FlowRateRange.Max := data.Chanels[j].Params.FlowRateRange.Max;
            end;
        end;

		Well.NeedUpdateView := false;
        Well.UpdateData( data );
        Well.NeedUpdateView := true;
    end;

    UpdateViews();
    UpdateCalculatedWellValues();
end;

function TWaterIntakeMnemoscheme.FindWell(name: string): TWellViewFrame;
begin
	result := FindComponent( Format( '%sView', [name] )) as TWellViewFrame;
end;

procedure TWaterIntakeMnemoscheme.UpdateViews;
var
  i: Integer;
begin
  for i := 1 to WellCount do
  begin
    if( not assigned( WellView[i] ))then
        continue;

    WellView[i].UpdateView;
  end;
end;

procedure TWaterIntakeMnemoscheme.UpdateCalculatedWellValues;
    var
        calc : IWellsCalculator;
begin
    //TODO: Create by factory
    calc := CreateWellsCalculator();
    calc.Wells := wellList;

  TotalWaterIntakeView.Text := Format( '%0:.2f', [calc.GetTotalCurrentFlowRate] );
  totalSumWIView.Text := Format( '%0:.2f', [calc.GetTotalSumFlowRate] );
end;

function TWaterIntakeMnemoscheme.getWellView(index: integer): TWellViewFrame;
begin
  result := FindComponent( GetWellName(index) ) as TWellViewFrame;
end;

function  TWaterIntakeMnemoscheme.getWellCount: integer;
begin
    result := WellList.Count;
end;

function TWaterIntakeMnemoscheme.CreateWellsCalculator: IWellsCalculator;
begin
  Result := GetWellsFactory().CreateInstance( CLSID_WellsCalculator ) as IWellsCalculator;
end;

function TWaterIntakeMnemoscheme.GetWellsFactory(): ICommonFactory;
begin
    result := Environment.GetInstance( CLSID_WELLSFACTORY ) as ICommonFactory;
end;


procedure TWaterIntakeMnemoscheme.UpdateMnemoschemeView;
	var
      	i		:	integer;
		  index		:	integer;

		  viewName	:	string;
		  view		:	TFlexText;
begin
	try
		for i := 1 to wellList.Count do
		begin
			  index	:=	getWellIndex( WellList[i].Name );

              //Display sum value
				viewName	:=	Format( 'Well%dView',
										  [
												Index
										  ]
							  );

				 view	:=	MnemoschemePanel.FindControl( viewName ) as TFlexText;

				 if assigned( view ) then
					  view.TextProp.Text	:=	Format( '%4.1f', [wellList[i].Chanels[1].Value.FlowRate.Value]);

                //Display current value
				viewName	:=	Format( 'Well%dCurView',
										  [
												Index
										  ]
							  );

				 view	:=	MnemoschemePanel.FindControl( viewName ) as TFlexText;

				 if assigned( view ) then
					  view.TextProp.Text	:=	Format( '%4.1f', [wellList[i].Chanels[1].Value.CurrentFlowRate.Value]);
		end;
	except
	end;
end;

function TWaterIntakeMnemoscheme.getWellIndex( name : string ): integer;
begin
	result := -1;
	try
		result	:=	StrToInt( System.Copy( Name, 5, length( name )))
	except
	end;
end;

procedure TWaterIntakeMnemoscheme.SetupControls;
begin
  inherited;

  SetupTotalWaterIntakeView;
  SetupTotalSumWaterIntakeView;

  cbbZoomFactor.ItemIndex := cbbZoomFactor.Items.IndexOf('100%');
  ZoomBy(100);
end;



procedure TWaterIntakeMnemoscheme.SetupTotalWaterIntakeView;
begin
  with TotalWaterIntakeView do
  begin
    Name := 'TotalWaterIntakeView';
    Parent := pnlInfo;
    Left := 8;
    Top := 142;
    Width := 199;
    Height := 21;
    Color := clWhite;
    ParentFont := False;
    ReadOnly := True;
    TabOrder := 2;
    Text := '0,00';
    BoundLabel.Active := True;
    BoundLabel.Caption := '������� ������:';
    BoundLabel.Indent := 0;
    BoundLabel.Font.Charset := 204;
    BoundLabel.Font.Color := 4276545;
    BoundLabel.Font.Height := -11;
    BoundLabel.Font.Name := 'Tahoma';
    BoundLabel.Font.Style := [];
    BoundLabel.Layout := sclTopLeft;
    BoundLabel.MaxWidth := 0;
    BoundLabel.UseSkinColor := True;
  end;
end;

procedure TWaterIntakeMnemoscheme.SetupTotalSumWaterIntakeView;
begin
  with totalSumWIView do
  begin
    Name := 'totalSumWIView';
    Parent := pnlInfo;
    Left := 9;
    Top := 181;
    Width := 199;
    Height := 21;
    Color := clWhite;
    ParentFont := False;
    ReadOnly := True;
    TabOrder := 3;
    Text := '0,00';
    BoundLabel.Active := True;
    BoundLabel.Caption := '��������� ������:';
    BoundLabel.Indent := 0;
    BoundLabel.Font.Charset := 204;
    BoundLabel.Font.Color := 4276545;
    BoundLabel.Font.Height := -11;
    BoundLabel.Font.Name := 'Tahoma';
    BoundLabel.Font.Style := [];
    BoundLabel.Layout := sclTopLeft;
    BoundLabel.MaxWidth := 0;
    BoundLabel.UseSkinColor := True;
  end;
end;




procedure TWaterIntakeMnemoscheme.MnemoschemePanelMouseUp(Sender: TObject;
    Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
	var
		wellName : string;
		wellIndex : integer;
begin
	if assigned( MnemoschemePanel.MouseSubControl )then
	begin
	  with MnemoschemePanel do
	  begin
		  wellName := AnsiUpperCase( MouseSubControl.Name );

          //HACK: Magic string to constant
          if( Pos( 'WELL', wellName ) = 0 )then
            exit;

		  wellIndex := getWellIndex( wellName );

		  if( wellIndex > 0 )and( wellIndex <= getWellCount() )then
          begin
            functionPages.ActivePage :=  TTabSheet(functionPages.FindChildControl(GetWellName(wellIndex) + 'Tab'));
            Handle_TabChanged(functionPages);

            UpdateActiveWelLGraph();
          end;
	  end;
	end;
end;

procedure TWaterIntakeMnemoscheme.UpdateActiveWelLGraph();
    var
        wellView : TWellViewFrame;
begin
    if( functionPages.ActivePage.ControlCount = 0 )then
        exit;

    wellView :=   (functionPages.ActivePage.Controls[0]) as TWellViewFrame;

    if( not assigned( wellView ))then
        exit;

    wellView.UpdateGraphs();
end;

procedure TWaterIntakeMnemoscheme.Handle_TabChanged(Sender: TObject);
begin
    if( functionPages.ActivePageIndex = 0 )then
    begin
      UpdateMnemoschemeView();

      exit;
    end;

    if( functionPages.ActivePageIndex > 0 )then
        UpdateActiveWelLGraph();

    if(functionPages.ActivePage.ControlCount > 0 )then
    begin
        UpdateWellViewState(
            functionPages.ActivePage.Controls[0] as TWellViewFrame,
            GetCurrentData
        );
        Exit;
    end;

    //TODO: Update state in any reason
    UpdateWellViewState(
        CreateWellView(GetCurrentData),
        GetCurrentData
    );

    UpdateActiveWelLGraph();
end;



function TWaterIntakeMnemoscheme.GetWellList: IWellList;
begin
    result := FDataList as IWellList;
end;

function TWaterIntakeMnemoscheme.GetItemCaption(index: integer): string;
begin
    result := WellList[index].Caption;
end;

function TWaterIntakeMnemoscheme.GetItemIndex(index: integer): integer;
begin
    result := WellList.IndexByCaption( CaptionList[index] );
end;

end.
