Mnemoscheme 2: Alternate

goal: Display the scada for hef (heat electro factory) for status of
  heat control points and water wells.

Applications:
  * ms.dpr - main scada interface with two mnemoschemes
  * putScaner.dpr - opc data scaner. By default uses Vzljot.OPC

Creator Afonin Vladimir (mailto: sortie@yandex.ru)

License: LGPL v.3

