unit class_putParams;

interface
	uses
    	//--[ containers ]--
        XPCollections
        ;

	type
    	TPUTParams = class
          private
            FPressure: real;
            FTemeprature: real;
            FFlowRate: real;
            procedure SetFlowRate(const Value: real);
            procedure SetPressure(const Value: real);
            procedure SetTemeprature(const Value: real);
          published
          public
          	property Temeprature : real read FTemeprature write SetTemeprature;
            property Pressure : real read FPressure write SetPressure;
            property FlowRate : real read FFlowRate write SetFlowRate;
        end;

implementation

{ TPUTParams }

procedure TPUTParams.SetFlowRate(const Value: real);
begin
  FFlowRate := Value;
end;

procedure TPUTParams.SetPressure(const Value: real);
begin
  FPressure := Value;
end;

procedure TPUTParams.SetTemeprature(const Value: real);
begin
  FTemeprature := Value;
end;

end.
