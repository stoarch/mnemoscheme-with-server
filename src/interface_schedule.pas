unit interface_schedule;

interface

    type
        ISchedule = interface
            ['{91806291-B420-407E-A00B-53133F66907E}']

            function GetCaption() : string;
            function GetCount() : integer;
            function GetRegularity() : IScheduleRegularity;

            procedure SetCaption( const value : string );

            property Caption : string read GetCaption write SetCaption;
            property Regularity : IScheduleRegularity read GetRegularity write SetRegularity;

            property ExecuteItems[ index : integer ] : TExecutableItem read GetExecutableItem;
            property ExecuteItemCount : integer read GetCount;
        end;

implementation

end.
 