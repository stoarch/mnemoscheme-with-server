unit class_wellChannelParams;

interface
	uses
    	//--[ classes ]--
        class_creatableObject,
        class_realRange,
    	//--[ containers ]--
        XPCollections,
        //--[ interfaces ]--
        interface_wellChannelParams,
        interface_realRange
        ;

	type
    	TWellChannelParams = class( TCreatableObject, IWellChannelParams )
          private
    		FFlowRateRange: IRealRange;
            FBackwardFlowRateRange : IRealRange;
            FCurrentFlowRateRange: IRealRange;

          published
          public
            constructor Create();override;
            destructor Destroy();override;

            procedure Assign( const data : IWellChannelParams );

            function GetBackwardFlowRateRange: IRealRange;
            function GetCurrentFlowRateRange: IRealRange;
            function GetFlowRateRange: IRealRange;

            procedure SetCurrentFlowRateRange(const Value: IRealRange);

            property FlowRateRange : IRealRange read GetFlowRateRange;
            property BackwardFlowRateRange : IRealRange read GetBackwardFlowRateRange;
            property CurrentFlowRateRange : IRealRange read GetCurrentFlowRateRange write SetCurrentFlowRateRange;
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils
        , tool_ms_factories, const_guids, interface_commonFactory;

{ TPUTParams }

procedure TWellChannelParams.Assign(const data: IWellChannelParams);
begin
    FFlowRateRange.Assign( data.FlowRateRange );
    FBackwardFlowRateRange.Assign( data.BackwardFlowRateRange );
end;

constructor TWellChannelParams.Create;
begin
  inherited;

    FFlowRateRange := GetCommonFactory().CreateInstance( CLSID_RealRange ) as IRealRange;
    FBackwardFlowRateRange := GetCommonFactory().CreateInstance( CLSID_RealRange ) as IRealRange;
end;

destructor TWellChannelParams.Destroy;
begin
  inherited;
end;



function TWellChannelParams.GetBackwardFlowRateRange: IRealRange;
begin
    Result := FBackwardFlowRateRange;
end;

function TWellChannelParams.GetCurrentFlowRateRange: IRealRange;
begin
    result := FCurrentFlowRateRange;
end;

function TWellChannelParams.GetFlowRateRange: IRealRange;
begin
    result := FFlowRateRange;
end;

procedure TWellChannelParams.SetCurrentFlowRateRange(
  const Value: IRealRange);
begin
  FCurrentFlowRateRange := Value;
end;

end.
