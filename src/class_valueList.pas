unit class_valueList;

interface
	uses
		//--[ classes ]--
		  class_creatableInterfaceList,
		  class_value,
		//--[ collections ]--
		  xpCollections,
		//--[ interfaces ]--
			interface_value,
			interface_valueList
		  ;

	type
		TValueList = class( TCreatableInterfaceList, IValueList )
			private
			 FTitle: string;

			public
				constructor Create();override;

				//--[ methods ]--
				procedure Add( const value : IValue );reintroduce;overload;
				procedure Assign( const alist : IValueList );

				//--[ property accessors ]--
				function getValue(index: integer): IValue;
				function getTitle() : string;

				procedure SetTitle(const Value: string);

				//--[ properties ]--
				property Title : string read GetTitle write SetTitle;
				property Value[ index : integer ] : IValue read getValue;
		  end;

implementation

uses tool_ms_factories, const_guids, interface_commonFactory;

{ TValueList }

procedure TValueList.Add(const value: IValue);
begin
	inherited add( value );
end;

procedure TValueList.Assign(const alist : IValueList);
	var
		i : integer;
		  adata : IValue;
          factory : ICommonFactory;
begin
     Clear();

     factory := GetCommonFactory();

	 for i := 1 to alist.count do
	 begin
		 adata := factory.CreateInstance( CLSID_Value ) as IValue;

         adata.Assign( alist.value[i] );
		 Add( adata );
	 end;
end;

constructor TValueList.Create;
begin
	inherited Create();
end;

function TValueList.getTitle: String;
begin
	Result	:=	FTitle;
end;

function TValueList.getValue(index: integer): IValue;
begin
	result := inherited Values[ index ] as IValue;
end;

procedure TValueList.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

end.
