unit data_Well;

interface
	uses
        //--[ classes ]--
        class_mnemoschemeItemData,
    	//--[ collections ]--
        xpCollections,
        //--[ data ]--
        data_WellChanel,
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChannelValue,
        interface_mnemoschemeItemChanelList,
        interface_wellChannelData,
        interface_wellChanelList,
        interface_wellData,
        //--[ list ]--
        list_WellChanel
        ;

	type
    	TWellData = class( TMnemoschemeItemData, IWellData )
            private
            protected
                function MakeList() : IMnemoschemeItemChanelList;override;

        	public

                function IWellData.GetChanels = GetWellChanels;
                procedure IWellData.SetChanels = SetWellChanels;

                function GetWellChanels(index: integer): IWellChannelData;
                procedure SetWellChanels(index: integer; const Value: IWellChannelData);
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils
        , Classes, tool_ms_factories, interface_commonFactory, const_well,
  const_guids;

{ TWellData }



function TWellData.GetWellChanels(index: integer): IWellChannelData;
begin
    result := Chanels[index] as IWellChannelData;
end;

function TWellData.MakeList: IMnemoschemeItemChanelList;
begin
    result := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemChanelList ) as IMnemoschemeItemChanelList;
end;


procedure TWellData.SetWellChanels(index: integer;
  const Value: IWellChannelData);
begin
    Chanels[index] := value;
end;

end.

