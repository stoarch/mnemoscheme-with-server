unit interface_putMnemoschemePreparer;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemePreparer
        ;

const
    IID_IPutMnemoschemePreparer : TGUID = '{CE3B530B-7952-4EAE-A981-54A586D1ACB6}';

type
    IPutMnemoschemePreparer = interface( IMnemoschemePreparer )
        ['{CE3B530B-7952-4EAE-A981-54A586D1ACB6}']

    end;

implementation

end.
 