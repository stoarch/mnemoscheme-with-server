unit interface_wellChanelList;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelList,
        interface_wellChannelData
        ;

    const
        IID_IWellChanelList : TGUID = '{90F0F2A7-7B7B-432E-B96F-6173CFFA16BC}';

    type
        IWellChanelList = interface( IMnemoschemeItemChanelList )
            ['{90F0F2A7-7B7B-432E-B96F-6173CFFA16BC}']
        end;

implementation

end.
 