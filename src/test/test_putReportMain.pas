unit test_putReportMain;

interface

uses
  //--[ common ]--
  Windows, SysUtils, Classes, TestFramework, TestExtensions,
  //--[ Forms ]--
  form_reportMain
  ;

type
  TPutReportMainFormAccessor = class( TPutReportsForm );

  TPutReportMainTest = class(TTestCase)
  protected
    FForm : TPutReportMainFormAccessor;

    procedure SetUp; override;
    procedure TearDown; override;

  published
    procedure TestGetAverageHourValueFor();
    procedure TestGetValueMatrix();
  end;

implementation

	uses
      	//--[ common ]--
		DateUtils, list_put, class_valueMatrix, data_put, tool_putSettingsLoader;

procedure TPutReportMainTest.Setup;
begin
	FForm := TPutReportMainFormAccessor.Create( nil );
end;

procedure TPutReportMainTest.TearDown;
begin
	FreeAndNil( FForm );
end;

procedure TPutReportMainTest.TestGetAverageHourValueFor;
begin
	CheckEquals( 1042, FForm.GetAverageHourValueFor( 1, 1, StrToDate( '23.12.2009' ), StrToTime( '01:00:00' ), 3) );
    CheckEquals( 126, Trunc( FForm.GetAverageHourValueFor( 3, 1, StrToDate( '23.12.2009' ), StrToTime( '05:00:00' ) , 3) ) );
end;

procedure TPutReportMainTest.TestGetValueMatrix;
	var
      	matrix  			: TValueMatrix;
        startDate, endDate 	: TDateTime;
        list, allPuts		: TPutList;
begin
  	allPuts := PutSettingsLoader.ReadPutSettings(2, false);

	list := TPutList.Create();
    list.Add( TPutData.Create( allPuts.Put[1] ) );
    list.Add( TPutData.Create( allPuts.Put[2] ) );

    startDate 	:= 	StrToDateTime( '29.12.2009' );
    endDate		:=	startDate + 1;

    FForm.m_chanelNo := 1;
    FForm.m_parmKind := 1;

    try
      try
      	matrix := FForm.GetValueMatrix( list, startDate, endDate, 1 );
        FForm.ShowGraphFor( matrix );
      finally
        	FreeAndNil( list );
            FreeAndNil( matrix );
      end;
    except
          Check( False, 'Unable to read value matrix' );
    end;
end;

initialization
  TestFramework.RegisterTest(TPutReportMainTest.Suite);

end.

