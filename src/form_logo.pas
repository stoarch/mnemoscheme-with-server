unit form_logo;


interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sLabel, StdCtrls, sSkinProvider, iThreadTimers, ComCtrls,
  acProgressBar, ExtCtrls, jpeg,
  //--[ interfaces ]--
  interface_uiStatus,
  //--[ types ]--
  type_tasks, pngimage
  ;

type
  TLogoForm = class(TForm)
	 sSkinProvider1: TsSkinProvider;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabelFX1: TsLabelFX;
    Image1: TImage;
    statusLabel: TsLabel;
    Image2: TImage;
    sLabel6: TsLabel;
    Image3: TImage;
  private
    procedure ShowToUser;
    { Private declarations }
  public
	 { Public declarations }
	 constructor Create(AOwner:TComponent);override;
	 destructor Destroy();override;

	 procedure AnimatedShow();
	 procedure AnimatedHide();

	 //--[ IUIStatus ]--
	 function getSubTaskProgress() : TTaskProgress;
	 function getTaskProgress() : TTaskProgress;

	 procedure setStatus( const value : string );
	 procedure setSubTaskProgress( const value : TTaskProgress );
	 procedure setTaskProgress( const value : TTaskProgress );

	 procedure stepSubTaskProgress();
    procedure stepTaskProgress();
  end;

var
  LogoForm: TLogoForm;

implementation

    uses
        math
        ;

type
    TUpdateThread = class( TThread )
        protected
            procedure Execute();override;
    end;

{$R *.dfm}


procedure TLogoForm.AnimatedHide;
begin
  //g_updater.Suspend();

  repeat
    AlphaBlendValue := Max( AlphaBlendValue  - 34, 50);
    Sleep(100);
  until( AlphaBlendValue = 50 );

  Hide();
end;

procedure TLogoForm.AnimatedShow;
begin
  Show();
  Update();

  repeat
    AlphaBlendValue := Min(AlphaBlendValue  + 34, 255);
    Update();
    Sleep(80);
  until( AlphaBlendValue >= 255 );

  //g_updater.Resume();
end;

constructor TLogoForm.Create(AOwner: TComponent);
begin
  inherited;

  //g_updater := TUpdateThread.Create(true);
end;

destructor TLogoForm.Destroy;
begin

  inherited;
end;

{ TUpdateThread }

procedure TUpdateThread.Execute;
begin
    repeat
        //Synchronize( Application.ProcessMessages );
        logoForm.Update();
        Sleep(100);
    until( Terminated );
end;

const
  	EMPTY_PROGRESS	:	TTaskProgress = ( Min:0; Max:0; Caption:'' );

//TODO: Implement this
function TLogoForm.getSubTaskProgress: TTaskProgress;
begin
	result := EMPTY_PROGRESS;
end;

function TLogoForm.getTaskProgress: TTaskProgress;
begin
	result := EMPTY_PROGRESS;
end;

procedure TLogoForm.setStatus(const value: string);
begin
	statusLabel.Caption := value;

    ShowToUser();
end;

procedure TLogoForm.setSubTaskProgress(const value: TTaskProgress);
begin
end;

procedure TLogoForm.setTaskProgress(const value: TTaskProgress);
begin
end;

procedure TLogoForm.stepSubTaskProgress;
begin

end;

procedure TLogoForm.stepTaskProgress;
begin
end;

procedure TLogoForm.ShowToUser();
begin
    Refresh();
    Update();
    Application.ProcessMessages();
end;


end.
