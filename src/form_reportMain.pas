unit form_reportMain;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, sSkinProvider, sSkinManager, FR_DSet, FR_DBSet, FR_Class,
  Grids, DBGrids, DB, ZAbstractRODataset, ZAbstractDataset, ZDataset,
  ExtCtrls, FR_View, ImgList, ComCtrls, ToolWin, sToolBar, Math,
  sStatusBar, FR_Desgn, kbmMemTable,
  //--[ classes ]--
  class_value,
  class_valueList,
  class_valueMatrix,
  //--[ data ]--
  data_put,
  //--[ list ]--
  list_put
  ;

type
  TPutReportsForm = class(TForm)
    sSkinManager1: TsSkinManager;
    sSkinProvider1: TsSkinProvider;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    Report1: TMenuItem;
    DailyReport: TMenuItem;
    MonthlyReport: TMenuItem;
    YearlyReport: TMenuItem;
    N1: TMenuItem;
    Intervalreport1: TMenuItem;
    report: TfrReport;
    chanel1Dataset: TfrDBDataSet;
    chanel1Query: TZQuery;
    chanel2Query: TZQuery;
    chanel3Query: TZQuery;
    chanel4Query: TZQuery;
    chanel2Dataset: TfrDBDataSet;
    chanel3Dataset: TfrDBDataSet;
    chanel4Dataset: TfrDBDataSet;
    headerDataset: TfrUserDataset;
    dataDataset: TfrUserDataset;
    reportPreview: TfrPreview;
    chanel1_DataSource: TDataSource;
    chanel2_DataSource: TDataSource;
    chanel3_DataSource: TDataSource;
    chanel4_DataSource: TDataSource;
    toolBar: TsToolBar;
    PrintReportButton: TToolButton;
    ImageList: TImageList;
    statusBar: TsStatusBar;
    ToolButton1: TToolButton;
    designReportButton: TToolButton;
    frDesigner1: TfrDesigner;
    mniCommonHourReport: TMenuItem;
    ktbFlowRateStore: TkbmMemTable;
    ktbFlowRateStoreid: TIntegerField;
    ktbFlowRateStorecaption: TStringField;
    ktbFlowRateStoreq1: TFloatField;
    ktbFlowRateStoreq2: TFloatField;
    ktbFlowRateStoreq3: TFloatField;
    ktbFlowRateStoreq4: TFloatField;
    ktbFlowRateStoreq5: TFloatField;
    ktbFlowRateStoreq6: TFloatField;
    dsFlowRateStore: TDataSource;
    executorQuery: TZQuery;
    dsExecutor: TDataSource;
    dstFlowRateStore: TfrDBDataSet;
    mniFlowRateCommonHourlyReport: TMenuItem;
    mniPressureCommonHourlyReport: TMenuItem;
    mniTemperatureCommonHourlyReport: TMenuItem;
    mniN2: TMenuItem;
    mniCommonDailyTemperatureReport: TMenuItem;
    mniCommonDailyPressureReport: TMenuItem;
    mniCommonDailyFlowRateReport: TMenuItem;
    mniN3: TMenuItem;
    mniComparePutValues: TMenuItem;
    procedure DailyReportClick(Sender: TObject);
    procedure reportGetValue(const ParName: String; var ParValue: Variant);
    procedure FormCreate(Sender: TObject);
    procedure MonthlyReportClick(Sender: TObject);
    procedure PrintReportButtonClick(Sender: TObject);
    procedure reportEndDoc;
    procedure Intervalreport1Click(Sender: TObject);
    procedure YearlyReportClick(Sender: TObject);
    procedure designReportButtonClick(Sender: TObject);
    procedure mniCommonHourReportClick(Sender: TObject);
    procedure mniCommonDailyTemperatureReportClick(Sender: TObject);
    procedure mniComparePutValuesClick(Sender: TObject);
  private
  protected
  	m_put 	: TPutData;
    m_date 	: TDateTime;
    m_time	: TDateTime;

    m_dateStart	: TDateTime;
    m_dateEnd 	: TDateTime;

    m_month 	: integer;
    m_year 		: integer;

    m_chanelNo	: integer;
    m_parmKind	: integer;

    procedure GenerateCommonDailyReport(adate: TDateTime; akind: integer);
    function GetAverageDayValueFor(putId, chanelId: integer;
      adate: TDateTime; akind: integer): real;
    procedure ShowComparePutValueReport( const list : TPutList );
    function SelectPutList( const sourceList : TPutList; var selectedList : TPutList ): boolean;
    function GetParmsKindStr(parmKind: integer): string;
    function GetValueMatrix( const list : TPutList; startDate, endDate: TDateTime; parmKind : integer): TValueMatrix;
    function GetValueListFor(chanelID: integer; startDate,  endDate: TDateTime; parmKind: integer): TValueList;
    procedure ShowGraphFor(const matrix: TValueMatrix);

    procedure MakeHourlyReport;
    procedure GenerateHourlyReport(const put: TPutData; adate: TDateTime );
    procedure GenerateMonthlyReport(const put : TPutData; month : integer );
    procedure MakeDailyReport;
    procedure PrintReport;
    procedure GenerateIntervalReport(const put: TPutData; dateStart,
      dateEnd: TDateTime);
    procedure MakeIntervalReport;
    function GetMonthName(month: integer): String;
    procedure GenerateYearReport(const put: TPutData; year: integer);
    procedure MakeYearReport;
    procedure GenerateCommonHourlyReport(adate : TDateTime; atime : TDateTime; akind : integer );
    function GetAverageHourValueFor(
    			putId, chanelId: integer;
                adate, atime: TDateTime;
                akind : integer
                ): real;
    { Private declarations }
  public

    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;
  end;

var
  PutReportsForm: TPutReportsForm;

implementation

uses module_dataMain, tool_putSettingsLoader, form_putSelector,
  form_putDateRangeSelectorForm, form_putMonthSelectorForm,
  form_putYearSelectorForm, form_data_putSelector, DateUtils,
    form_progressDisplayer, form_monthDaySelector, form_putListSelector,
      form_graphViewForm;

{$R *.dfm}
const
  	MAX_PUT_CHANEL_COUNT = 6; //TODO: Think about access to maximal count of put chanels


    TEMPERATURE	=	1;
    PRESSURE	=	2;
    FLOW_RATE 	= 	3;

procedure TPutReportsForm.DailyReportClick(Sender: TObject);
begin
	MakeHourlyReport();
end;

procedure TPutReportsForm.GenerateHourlyReport( const put : TPutData; adate : TDateTime );
	const
    	c_time_sql =
            'select * from ' +
            '(SELECT avg( params.value ) as temp_val, extract( hour from date_query ) as hr, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_trunc( ''day'', date_query ) = %1:s ) ' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 1 ) ' +
            'group by params.chanel_id, hr, params.param_kind_id ' +
            'order by hr ' +
            ') as temp_query, ' +
            '(SELECT avg( params.value ) as pres_val, extract( hour from date_query ) as hr, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_trunc( ''day'', date_query ) = %1:s ) ' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 2 ) ' +
            'group by params.chanel_id, hr, params.param_kind_id ' +
            'order by hr ' +
            ') as pres_query, ' +
            '(SELECT avg( params.value ) as fl_val, extract( hour from date_query ) as hr, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_trunc( ''day'', date_query ) = %1:s ) ' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 3 ) ' +
            'group by params.chanel_id, hr, params.param_kind_id ' +
            'order by hr ' +
            ')  as flr_query ' +
            'where (temp_query.hr = pres_query.hr ) and ( pres_query.hr = flr_query.hr ) and ( temp_query.chanel_id = pres_query.chanel_id) and (pres_query.chanel_id = flr_query.chanel_id) ' +
            '    and (temp_query.chanel_id = %2:d)'+
            'order by temp_query.chanel_id;';

        var
        	i : integer;
            compName : string;
            query : TZQuery;
begin
	for i := 1 to put.Count do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        	query.Close();
    end;

	for i := 1 to put.count do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        with query do
        begin
        	query.SQL.Text := Format( c_time_sql, [put.index, '''' + FormatDateTime( 'yyyy-mm-dd', adate) + '''', put[i].index ] );
        end;
        query.Open();
    end;

    headerDataset.RangeEndCount := put.Count;
    dataDataset.RangeEndCount := put.Count;

    report.LoadFromFile( '.\reports\hourly_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();
end;

procedure TPutReportsForm.MakeHourlyReport();
	var
    	selector : TPutSelectorForm;
        put : TPutData;
        date : TDateTime;
        list : TPutList;
begin
	selector := TPutSelectorForm.Create(self);
    try
    	list := PutSettingsLoader.ReadPutSettings();
    	selector.SetPutList( list );
		if( selector.ShowModal() = idOk )then
        begin
			put := selector.Data;
            date := selector.DateTime;
            m_put.Assign( put );
            m_date := date;

            GenerateHourlyReport( put, date );
        end;
    finally
      FreeAndNil( put );
      FreeAndNil( list );
      FreeAndNil( selector );
    end;
end;


procedure TPutReportsForm.reportGetValue(const ParName: String;
  var ParValue: Variant);
  	var
    	index : integer;
        query : TZQuery;
        aparam : String;
begin
    aparam := AnsiUpperCase(parName);

    if aparam = 'PARMKIND' then
    begin
      case m_parmKind of
      	TEMPERATURE : ParValue := 'T';
        PRESSURE	: ParValue := 'P';
        FLOW_RATE	: ParValue := 'Q';
      	else
        	ParValue := 'Z';
      end;
      exit;
    end;

    if aparam = 'PUT_NAME' then
    begin
      ParValue := m_put.Name;
      exit;
    end;

    if aparam = 'DATEEX' then
    begin
      ParValue := FormatDateTime( 'dd.mm.yyyy', m_date );
      exit;
    end;

    if aparam = 'TIMEEX' then
    begin
      ParValue := FormatDateTime( 'hh', m_time ) + '�. - ' + FormatDateTime( 'dd.mm.yyyy', m_date );
      exit;
    end;

    if aparam = 'DATESTART' then
    begin
      ParValue := FormatDateTime( 'dd.mm.yyyy', m_dateStart );
      exit;
    end;

    if aparam = 'DATEEND' then
    begin
      ParValue := FormatDateTime( 'dd.mm.yyyy', m_dateEnd );
      exit;
    end;

    if aparam = 'MONTH' then
    begin
    	ParValue := GetMonthName( m_month );
    	exit;
    end;

    if aparam = 'MONTHID' then
    begin
    	ParValue := GetMonthName( chanel1Query.FieldByName( 'mn' ).AsInteger );
    	exit;
    end;

    if aparam = 'YEAR' then
    begin
    	ParValue := m_year;
		exit;
    end;


	index := headerDataset.RecNo;

    if ParName = 'CHANEL_HEADER' then
    begin
      ParValue := Format( '����� %d', [index+1] );
      exit;
    end;

    if aparam = 'TEMPERATURE_HEADER' then
    begin
      ParValue := Format( 'T', [index+1] );
      exit;
    end;

    if aparam = 'PRESSURE_HEADER' then
    begin
      ParValue := Format( 'P', [index+1] );
      exit;
    end;

    if aparam = 'FLOWRATE_HEADER' then
    begin
      ParValue := Format( 'Q', [index+1] );
      exit;
    end;

    index := dataDataset.RecNo + 1;
	query := FindComponent( Format( 'chanel%dQuery',[index] ) ) as TZQuery;
    if( not assigned( query ))or( index > m_put.count )then
    begin
    	exit;
    end;

    if( index > 1 )then
    begin
    	query.First();
        query.MoveBy(chanel1Query.RecNo-1);
    end;

    if aparam = 'TEMPERATURE_CELL' then
    begin
      ParValue := Format( '%5.2f',[query.FieldByName('temp_val').AsFloat]);
      exit;
    end;

    if aparam = 'PRESSURE_CELL' then
    begin
      ParValue := Format( '%4.3f', [query.FieldByName('pres_val').AsFloat]);
      exit;
    end;

    if aparam = 'FLOWRATE_CELL' then
    begin
      ParValue := Format( '%4.0f', [query.FieldByName('fl_val').AsFloat]);
      exit;
    end;
end;

function TPutReportsForm.GetMonthName( month : integer ): String;
begin
	result := '';
	case month of
    	1 : result := '������';
    	2 : result := '�������';
    	3 : result := '����';
    	4 : result := '������';
    	5 : result := '���';
    	6 : result := '����';
    	7 : result := '����';
    	8 : result := '������';
    	9 : result := '��������';
       10 : result := '�������';
       11 : result := '������';
       12 : result := '�������';
    end;
end;

constructor TPutReportsForm.Create(AOwner: TComponent);
begin
  inherited;
	m_put := TPutData.Create();
end;

destructor TPutReportsForm.Destroy;
begin
	FreeAndNil( m_put );
  inherited;
end;

procedure TPutReportsForm.FormCreate(Sender: TObject);
begin
	SetBounds( Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight );
end;

procedure TPutReportsForm.MonthlyReportClick(Sender: TObject);
begin
	MakeDailyReport();
end;

procedure TPutReportsForm.MakeDailyReport();
	var
    	selector : TPutMonthSelectorForm;
        put : TPutData;
        month : integer;
        list : TPutList;
begin
	selector := TPutMonthSelectorForm.Create(self);
    try
    	list := PutSettingsLoader.ReadPutSettings();
    	selector.SetPutList( list );
		if( selector.ShowModal() = idOk )then
        begin
			put 	:= selector.Data;
            month 	:= selector.Month;
            m_put.Assign( put );

            m_month := month;

            GenerateMonthlyReport( put, month );
        end;
    finally
      FreeAndNil( put );
      FreeAndNil( list );
      FreeAndNil( selector );
    end;
end;


procedure TPutReportsForm.GenerateMonthlyReport( const put : TPutData; month : integer );
	const
    	c_time_sql =
          'select * from '+
          '(SELECT avg( params.value ) as temp_val, extract( month from date_query ) as mn, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id '+
          '    FROM put, chanel, params, param_kind '+
          '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) '+
          '        and ( put.id = %0:d ) and ( params.param_kind_id = 1 ) '+
          '    group by params.chanel_id, mn, dy, params.param_kind_id '+
          '    order by mn, dy ) as temp_query, '+
          '(SELECT avg( params.value ) as pres_val, extract( month from date_query ) as mn, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id '+
          '    FROM put, chanel, params, param_kind '+
          '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) '+
          '        and ( put.id = %0:d ) and ( params.param_kind_id = 2 ) '+
          '    group by params.chanel_id, mn, dy, params.param_kind_id '+
          '    order by mn, dy ) as pres_query, '+
          '(SELECT avg( params.value ) as fl_val, extract( month from date_query ) as mn, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id '+
          '    FROM put, chanel, params, param_kind '+
          '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) '+
          '        and ( put.id = %0:d ) and ( params.param_kind_id = 3 ) '+
          '    group by params.chanel_id, mn, dy, params.param_kind_id '+
          '    order by mn, dy ) as flr_query '+
          'where (temp_query.mn = pres_query.mn ) and ( pres_query.mn = flr_query.mn ) and ( temp_query.chanel_id = pres_query.chanel_id) '+
          '    and (pres_query.chanel_id = flr_query.chanel_id) and (temp_query.chanel_id = %2:d) and (temp_query.dy = pres_query.dy) '+
          '    and (pres_query.dy = flr_query.dy) and (temp_query.mn = %1:d)'+
          'order by temp_query.chanel_id;'
          ;

        var
        	i : integer;
            compName : string;
            query : TZQuery;
begin
	for i := 1 to 4 do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        	query.Close();
    end;

	for i := 1 to put.count do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        begin
        	query.SQL.Text := Format( c_time_sql, [put.index, month, put[i].index ] );
        	query.Open();
        end;
    end;

    headerDataset.RangeEndCount := put.Count;
    dataDataset.RangeEndCount := put.Count;

    report.LoadFromFile( '.\reports\daily_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();
end;



procedure TPutReportsForm.PrintReportButtonClick(Sender: TObject);
begin
	PrintReport();
end;

procedure TPutReportsForm.PrintReport();
begin
	reportPreview.Print();
end;


procedure TPutReportsForm.reportEndDoc;
begin
	statusBar.panels[2].Text := '1 �� ' + IntToStr( report.Pages.Count );
end;

procedure TPutReportsForm.Intervalreport1Click(Sender: TObject);
begin
	MakeIntervalReport();
end;

procedure TPutReportsForm.MakeIntervalReport();
	var
    	selector : TPutDateRangeSelectorForm;
        put : TPutData;
        dateStart, dateEnd : TDateTime;
        list : TPutList;
begin
	selector := TPutDateRangeSelectorForm.Create(self);
    try
    	list := PutSettingsLoader.ReadPutSettings();
    	selector.SetPutList( list );
		if( selector.ShowModal() = idOk )then
        begin
			put := selector.Data;
            dateStart := selector.StartDate;
            dateEnd := selector.EndDate;
            m_put.Assign( put );

            m_dateStart := dateStart;
            m_dateEnd := dateEnd;

            GenerateIntervalReport( put, dateStart, dateEnd );
        end;
    finally
      FreeAndNil( put );
      FreeAndNil( list );
      FreeAndNil( selector );
    end;
end;


procedure TPutReportsForm.GenerateIntervalReport( const put : TPutData; dateStart, dateEnd : TDateTime );
	const
    	c_time_sql =
            'select * from ' +
            '(SELECT avg( params.value ) as temp_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_query >= %1:s ) and ( date_query <= %2:s )' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 1 ) ' +
            'group by params.chanel_id, dy, params.param_kind_id ' +
            'order by dy ' +
            ') as temp_query, ' +
            '(SELECT avg( params.value ) as pres_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_query >= %1:s ) and ( date_query <= %2:s )' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 2 ) ' +
            'group by params.chanel_id, dy, params.param_kind_id ' +
            'order by dy ' +
            ') as pres_query, ' +
            '(SELECT avg( params.value ) as fl_val, extract( day from date_query ) as dy, params.param_kind_id, params.chanel_id ' +
            '  FROM put, chanel, params, param_kind ' +
            '  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and ' +
            '    ( date_query >= %1:s ) and ( date_query <= %2:s )' +
            '    and ( put.id = %0:d ) and ( params.param_kind_id = 3 ) ' +
            'group by params.chanel_id, dy, params.param_kind_id ' +
            'order by dy ' +
            ') as flr_query ' +
            'where (temp_query.dy = pres_query.dy ) and ( pres_query.dy = flr_query.dy ) and ( temp_query.chanel_id = pres_query.chanel_id) and (pres_query.chanel_id = flr_query.chanel_id) ' +
            '    and (temp_query.chanel_id = %3:d)'+
            'order by temp_query.chanel_id;';

        var
        	i : integer;
            compName : string;
            query : TZQuery;
begin
	for i := 1 to 4 do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        	query.Close();
    end;

	for i := 1 to put.count do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        begin
        	query.SQL.Text := Format( c_time_sql, [put.index, '''' + FormatDateTime( 'yyyy-mm-dd', dateStart ) + '''', '''' + FormatDateTime( 'yyyy-mm-dd', dateEnd ) + '''', put[i].index ] );
        	query.Open();
        end;
    end;

    headerDataset.RangeEndCount := put.Count;
    dataDataset.RangeEndCount := put.Count;

    report.LoadFromFile( '.\reports\interval_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();
end;


procedure TPutReportsForm.YearlyReportClick(Sender: TObject);
begin
	MakeYearReport();
end;

//TODO: Refactor report generation subs - make one object with strategy for data and report
procedure TPutReportsForm.MakeYearReport();
	var
    	selector : TPutYearSelectorForm;
        put : TPutData;
        list : TPutList;
        year : integer;
begin
	selector := TPutYearSelectorForm.Create(self);
    try
    	list := PutSettingsLoader.ReadPutSettings();
    	selector.SetPutList( list );
		if( selector.ShowModal() = idOk )then
        begin
			put := selector.Data;
            year := selector.Year;
            m_put.Assign( put );

            m_year := year;

            GenerateYearReport( put, year );
        end;
    finally
      FreeAndNil( put );
      FreeAndNil( list );
      FreeAndNil( selector );
    end;
end;


procedure TPutReportsForm.GenerateYearReport( const put : TPutData; year : integer );
	const
    	c_time_sql =
            'select * from ' +
            '(SELECT avg( params.value ) as temp_val, extract( month from date_query ) as mn, extract( year from date_query ) as yr, params.param_kind_id, params.chanel_id ' +
            '    FROM put, chanel, params, param_kind ' +
            '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) ' +
            '        and ( put.id = %0:d ) and ( params.param_kind_id = 1 ) ' +
            '    group by params.chanel_id, yr, mn, params.param_kind_id order by mn ) as temp_query, ' +
            '(SELECT avg( params.value ) as pres_val, extract( month from date_query ) as mn, extract( year from date_query ) as yr, params.param_kind_id, params.chanel_id ' +
            '    FROM put, chanel, params, param_kind ' +
            '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) ' +
            '        and ( put.id = %0:d ) and ( params.param_kind_id = 2 ) ' +
            '    group by params.chanel_id, yr, mn, params.param_kind_id order by mn ) as pres_query, ' +
            '(SELECT avg( params.value ) as fl_val, extract( month from date_query ) as mn, extract( year from date_query ) as yr, params.param_kind_id, params.chanel_id ' +
            '    FROM put, chanel, params, param_kind ' +
            '    where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) ' +
            '        and ( put.id = %0:d ) and ( params.param_kind_id = 3 ) ' +
            '    group by params.chanel_id, yr, mn, params.param_kind_id order by yr, mn  ) as flr_query ' +
            'where (temp_query.mn = pres_query.mn ) and ( pres_query.mn = flr_query.mn ) and ( temp_query.chanel_id = pres_query.chanel_id) ' +
            '    and (pres_query.chanel_id = flr_query.chanel_id) and (temp_query.chanel_id = %2:d) ' +
            '    and (temp_query.yr = %1:d) and (temp_query.yr = pres_query.yr) and (pres_query.yr = flr_query.yr) ' +
            'order by temp_query.chanel_id;';

            
        var
        	i : integer;
            compName : string;
            query : TZQuery;
begin
	for i := 1 to 4 do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        	query.Close();
    end;

	for i := 1 to put.count do
    begin
		compName := Format( 'chanel%dQuery', [i] );
    	query := FindComponent( compName ) as TZQuery;
        if( assigned( query ))then
        begin
        	query.SQL.Text := Format( c_time_sql, [put.index, m_year, put[i].index ] );
        	query.Open();
        end;
    end;

    headerDataset.RangeEndCount := put.Count;
    dataDataset.RangeEndCount := put.Count;

    report.LoadFromFile( '.\reports\year_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();
end;



procedure TPutReportsForm.designReportButtonClick(Sender: TObject);
begin
	report.DesignReport();
end;

procedure TPutReportsForm.mniCommonHourReportClick(Sender: TObject);
var
    	selector : TDateSelectorForm;
        date, time : TDateTime;
begin
	selector := TDateSelectorForm.Create(self);
    try
		if( selector.ShowModal() = idOk )then
        begin
			date := selector.Date;
            m_date 	:= date;
            time 	:= selector.Time;
            m_time	:=time;

			if( sender is TMenuItem )then
            	if(( sender as TMenuItem ).Tag in [1..3] )then
                begin
                    m_parmKind := (sender as TMenuItem).Tag;
            		GenerateCommonHourlyReport( date, time, m_parmKind );
                end;
        end;

    finally
      FreeAndNil( selector );
    end;
end;

procedure TPutReportsForm.GenerateCommonHourlyReport( adate : TDateTime; atime : TDateTime; akind : integer );
	var
      flVal : real;
      list  : TPutList;
      i, j	: integer;
      cmpN	: string;
      field : TFloatField;

      progress  	: TProgressDisplayerForm;
begin
  progress := TProgressDisplayerForm.Create( self );
  try
      progress.Show();

      if( not ktbFlowRateStore.Active )then
          ktbFlowRateStore.Open();

      ktbFlowRateStore.DeleteTable();
      ktbFlowRateStore.Open();

      list := PutSettingsLoader.ReadPutSettings(false);
      progress.TaskMax := list.Count;
      for i := 1 to list.count do
      begin
      	  progress.TaskProgress := i;

          ktbFlowRateStore.Append();
          ktbFlowRateStore.FieldByName('caption').AsString := list[i].Caption;

          progress.WorkMessage 	:= Format( '��������� %s ����...', [ list[i].Caption ]);
          progress.MaxValue 	:=	list[i].count;

          for j := 1 to list[i].Count do
          begin
          	  progress.progress := j;

              flVal	:= GetAverageHourValueFor( i, j, adate, atime, akind );

              cmpN   	:= 	Format( 'q%d', [j] );
              field 	:=	ktbFlowRateStore.FieldByName( cmpN ) as TFloatField;

              if( assigned( field ))and(flVal > 0)then
              begin
                field.AsFloat := RoundTo(flVal, -2);
              end;

              if( progress.IsCanceled )then
              begin
                abort;
              end;
          end;

          ktbFlowRateStore.Post();
      end;
  finally
    	FreeAndNil( progress );
  end;


    report.LoadFromFile( '.\reports\common_hourly_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();
end;

function TPutReportsForm.GetAverageHourValueFor( putId : integer; chanelId : integer; adate : TDateTime; atime : TDateTime; akind : integer ) : real;
const
	c_sql =
          'with ' +
          '    cnt(n) as ( ' +
          '        select count(*) ' +
          '        from put ' +
          '    ), ' +
          '    pid(id) as ( ' +
          '        select array( ' +
          '            select put.id ' +
          '            from put ' +
          '			   where put.active = true ' +
          '            order by put.id ' +
          '        ) ' +
          '    ), ' +
          ' ' +
          '    cid(n, c) as ( ' +
          '        select chanel.id, chanel.caption ' +
          '        from chanel, pid ' +
          '        where chanel.put_id = pid.id[ %0:d ] ' +
          '    ), ' +
          ' ' +
          '    aid(id) as ( ' +
          '        select array( ' +
          '            select n ' +
          '            from cid ' +
          '            order by n ' +
          '        ) ' +
          '    ), ' +
          ' ' +
          '    chld(chid, chcapt,puid,pcapt) as ( ' +
          '        select chanel.id as chid, chanel.caption as chcapt, put.id as puid, put.caption as pcapt ' +
          '        from chanel, aid, put ' +
          '            where ' +
          '                (chanel.id = aid.id[ %1:d ]) ' +
          '                and (put.id = chanel.put_id) ' +
          '    ) ' +
          ' ' +
          'select chld.chid, pcapt, avg(value) as value ' +
          'from ' +
          '    chld, params ' +
          'where ' +
          '   (chld.chid = params.chanel_id) ' +
          '    and ( date_trunc( ''day'', date_query ) = %2:s ) ' +
          '    and (extract(hour from date_query) = %3:d ) ' +
          '    and (params.param_kind_id = %4:d) ' +
          ' group by chid,pcapt ';

    VALUE_FIELD = 2;


begin
  result := -1;

  executorQuery.SQL.Text := Format( c_sql, [putId, chanelId, '''' + FormatDateTime('yyyy-mm-dd',adate) + '''' , HourOf(atime), akind] ) ;

  executorQuery.Open();
  if( executorQuery.RecordCount = 0 )then
  	exit;

  result := executorQuery.Fields[ VALUE_FIELD ].Value;
end;



procedure TPutReportsForm.mniCommonDailyTemperatureReportClick(
  Sender: TObject);
var
    	selector 	: TMonthDaySelectorForm;
        date		: TDateTime;
begin
	selector := TMonthDaySelectorForm.Create(self);
    try
		if( selector.ShowModal() = idOk )then
        begin
			date := selector.Date;
            m_date 	:= date;

			if( sender is TMenuItem )then
            	if(( sender as TMenuItem ).Tag in [1..3] )then
                begin
                    m_parmKind := (sender as TMenuItem).Tag;
            		GenerateCommonDailyReport( date, m_parmKind );
                end;
        end;

    finally
      FreeAndNil( selector );
    end;
end;

procedure TPutReportsForm.GenerateCommonDailyReport( adate : TDateTime; akind : integer );
	var
      flVal : real;
      list  : TPutList;
      i, j	: integer;
      cmpN	: string;
      field : TFloatField;

      progress  	: TProgressDisplayerForm;
begin
  progress := TProgressDisplayerForm.Create( self );
  try
      progress.Show();

      if( not ktbFlowRateStore.Active )then
          ktbFlowRateStore.Open();

      ktbFlowRateStore.DeleteTable();
      ktbFlowRateStore.Open();

      list := PutSettingsLoader.ReadPutSettings(false);
      progress.TaskMax := list.Count;
      for i := 1 to list.count do
      begin
      	  progress.TaskProgress := i;

          ktbFlowRateStore.Append();
          ktbFlowRateStore.FieldByName('caption').AsString := list[i].Caption;

          progress.WorkMessage 	:= Format( '��������� %s ����...', [ list[i].Caption ]);
          progress.MaxValue 	:=	list[i].count;

          for j := 1 to list[i].Count do
          begin
          	  progress.progress := j;

              flVal	:= GetAverageDayValueFor( i, j, adate, akind );

              cmpN   	:= 	Format( 'q%d', [j] );
              field 	:=	ktbFlowRateStore.FieldByName( cmpN ) as TFloatField;

              if( assigned( field ))and(flVal > 0)then
              begin
                field.AsFloat := RoundTo(flVal, -2);
              end;

              if( progress.IsCanceled )then
              begin
                abort;
              end;
          end;

          ktbFlowRateStore.Post();
      end;
  finally
    	FreeAndNil( progress );
  end;


    report.LoadFromFile( '.\reports\common_daily_report.frf' );
    if( report.PrepareReport() )then
    	report.ShowPreparedReport();

    //TODO: Rename table ktbFlowRate to ktbCommonValues
end;

function TPutReportsForm.GetAverageDayValueFor( putId : integer; chanelId : integer; adate : TDateTime; akind : integer ) : real;
const
	c_sql =
          'with ' +
          '    cnt(n) as ( ' +
          '        select count(*) ' +
          '        from put ' +
          '    ), ' +
          '    pid(id) as ( ' +
          '        select array( ' +
          '            select put.id ' +
          '            from put ' +
          '			   where put.active = true ' +
          '            order by put.id ' +
          '        ) ' +
          '    ), ' +
          ' ' +
          '    cid(n, c) as ( ' +
          '        select chanel.id, chanel.caption ' +
          '        from chanel, pid ' +
          '        where chanel.put_id = pid.id[ %0:d ] ' +
          '    ), ' +
          ' ' +
          '    aid(id) as ( ' +
          '        select array( ' +
          '            select n ' +
          '            from cid ' +
          '            order by n ' +
          '        ) ' +
          '    ), ' +
          ' ' +
          '    chld(chid, chcapt,puid,pcapt) as ( ' +
          '        select chanel.id as chid, chanel.caption as chcapt, put.id as puid, put.caption as pcapt ' +
          '        from chanel, aid, put ' +
          '            where ' +
          '                (chanel.id = aid.id[ %1:d ]) ' +
          '                and (put.id = chanel.put_id) ' +
          '    ) ' +
          ' ' +
          'select chld.chid, pcapt, avg(value) as value ' +
          'from ' +
          '    chld, params ' +
          'where ' +
          '   (chld.chid = params.chanel_id) ' +
          '    and ( date_trunc( ''day'', date_query ) = %2:s ) ' +
          '    and (params.param_kind_id = %3:d) ' +
          ' group by chid,pcapt ';

    VALUE_FIELD = 2;


begin
  result := -1;

  executorQuery.SQL.Text := Format( c_sql, [putId, chanelId, '''' + FormatDateTime('yyyy-mm-dd',adate) + '''' , akind] ) ;

  executorQuery.Open();
  if( executorQuery.RecordCount = 0 )then
  	exit;

  result := executorQuery.Fields[ VALUE_FIELD ].Value;
end;

procedure TPutReportsForm.mniComparePutValuesClick(Sender: TObject);
	var
      	list, selectedList : TPutList;
begin
	list := PutSettingsLoader.ReadPutSettings(false);
    selectedList := TPutList.Create();

    try
      if( SelectPutList( list, selectedList ) )then
          ShowComparePutValueReport( selectedList );
    finally
      	FreeAndNil( selectedList );
        FreeAndNil( list );
    end;
end;

function TPutReportsForm.SelectPutList( const sourceList : TPutList; var selectedList : TPutList ): boolean;
	var
      	putListSelector : TPutListSelectorForm;
begin
	result := false;

    putListSelector := TPutListSelectorForm.Create(self);
    try
      	putListSelector.PutList := sourceList;

        if( putListSelector.ShowModal() = mrOk )then
        begin
			selectedList.Assign( putListSelector.SelectedPuts );
            
            m_chanelNo      := putListSelector.ChanelNo;
            m_parmKind		:= putListSelector.ParamKind;
            m_dateStart		:= putListSelector.DateStart;
            m_dateEnd		:= putListSelector.DateEnd + 1;

            Result := true;
        end;
    finally
      	if( assigned( putListSelector ))then
			FreeAndNil( putListSelector );
    end;
end;


procedure TPutReportsForm.ShowComparePutValueReport( const list : TPutList );
	var
      	matrix : TValueMatrix;
begin
	matrix := GetValueMatrix( list, m_dateStart, m_dateEnd, m_parmKind );
    ShowGraphFor( matrix );
end;

procedure TPutReportsForm.ShowGraphFor( const matrix : TValueMatrix );
	var
        form   : TGraphViewForm;
begin
    form := TGraphViewForm.Create(self);
    try
		form.ParamCaption := '������� ������ �� �����';

		form.ValueMatrix := matrix;

        form.ShowModal();
    finally
		if assigned( form ) then
        	FreeAndNil( form );
    end;
end;


function TPutReportsForm.GetParmsKindStr( parmKind : integer ): string;
begin
	case parmKind of
    	TEMPERATURE : result := 'T';
        PRESSURE	: result := 'P';
        FLOW_RATE	: result := 'Q';
    end;
end;


function TPutReportsForm.GetValueListFor(
			chanelID 	: integer;
            startDate,
            endDate 	: TDateTime;
            parmKind 	: integer
		): TValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %0:d ) and (date_query >= %1:s) and (date_query <= %2:s) and (param_kind_id = %3:d) order by date_query;';
    var
    	adata : TValue;
begin
	result := nil;

    with msDataModule do
    begin
    	queryData.SQL.Text :=
        	Format(
            	c_sql,
                [
                	chanelId,
                    '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',
                    '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''',
                    parmKind
                ]
            );

        queryData.Open();
        if( queryData.RecordCount > 0 )then
        begin
        	result := TValueList.Create();
		    result.Title := GetParmsKindStr( parmKind ) + IntToStr( m_chanelNo );

			//TODO: Extract business logic for data store to additional classes
            //TODO: Extract reports into report generation classes

            while not queryData.eof do
            begin
				adata := TValue.Create();
                adata.Date := queryData.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := queryData.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                queryData.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;


function TPutReportsForm.GetValueMatrix( const list : TPutList; startDate, endDate : TDateTime; parmKind : integer ): TValueMatrix;
  var
    adata : TValueList;
    value : TValueMatrix;
    i	 : integer;
begin
  	assert( assigned( list ), 'Put list must be assigned' );
    Assert( startDate <= endDate, 'Date range must be defined successfully' );
    
 	value := TValueMatrix.Create();

    for i := 1 to list.Count do
    begin
        try
          adata 		:= GetValueListFor( list.Put[i].Chanels[m_chanelNo].Index, startDate, endDate, parmKind );
          adata.Title 	:= list.Put[i].Caption + ' - ' + GetParmsKindStr( parmKind ) + IntToStr(m_chanelNo);

          value.Add( adata );
        except
            on e:EListError do
                Abort;//Log error message
        end;
    end;
    result := value;
end;


end.
