unit interface_wellData;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_wellChannelData
        ;

    const
        IID_WellData : TGUID = '{C4DEFCBF-18CA-4957-8EF6-319FEE486B75}';

    type
        IWellData = interface( IMnemoschemeItemData )
            ['{C4DEFCBF-18CA-4957-8EF6-319FEE486B75}']
                function GetChanels(index: integer): IWellChannelData;
                procedure SetChanels(index: integer; const Value: IWellChannelData);

                property Chanels[ index : integer ] : IWellChannelData read GetChanels write SetChanels;default;
        end;

implementation

end.
 