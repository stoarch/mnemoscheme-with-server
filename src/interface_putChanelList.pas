unit interface_putChanelList;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelList,
        interface_putChanelData
        ;

const
    IID_IPutChanelList : TGUID = '{C84824E4-4EA7-46EE-A33B-BBB3B3687E87}';
type
    IPutChanelList = interface( IMnemoschemeItemChanelList )
        ['{C84824E4-4EA7-46EE-A33B-BBB3B3687E87}']
    end;

implementation

end.
 