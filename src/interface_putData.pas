unit interface_putData;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemData,
        interface_putChanelData
        ;

const
    IID_IPutData : TGUID = '{6902378D-C8B8-41DF-AD77-FD0EBC5455B4}';

type
    IPutData = interface( IMnemoschemeItemData )
        ['{6902378D-C8B8-41DF-AD77-FD0EBC5455B4}']
                function GetChanels(index: integer): IPutChanelData;

                procedure SetChanels(index: integer; const Value: IPutChanelData);

                property Chanels[ index : integer ] : IPutChanelData read GetChanels write SetChanels;default;
    end;
implementation

end.
