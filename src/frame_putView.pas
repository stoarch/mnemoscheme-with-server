unit frame_putView;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Mask,
  sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit, A3nalogGauge, sGroupBox,
  math, sFrameAdapter, ImgList, ComCtrls, ToolWin, sToolBar,
  Menus, iComponent, iVCLComponent, iCustomComponent,
  iLCDMatrix,
  //--[ classes ]--
  class_valueList,
  class_valueMatrix,
  //--[ data ]--
  data_put,
  //--[ interfaces ]--
  interface_commonFactory,
  interface_valueMatrix,
  interface_putData,
  interface_value,
  interface_valueList,
  //--[ frames ]--
  frame_channelView
  ;

type
  TPutViewFrame = class(TFrame)
    Channel1_View: TChannelViewFrame;
    sFrameAdapter1: TsFrameAdapter;
    sToolBar1: TsToolBar;
    graphToolButton: TToolButton;
    ImageList: TImageList;
    tableToolButton: TToolButton;
    graphPopupMenu: TPopupMenu;
    temperatureGraphMenuItem: TMenuItem;
    pressureGraphPopupMenu: TMenuItem;
    FlowRateGraphMenuItem: TMenuItem;
    tablePopupMenu: TPopupMenu;
    temperatureTableMenuItem: TMenuItem;
    pressureTableMenuItem: TMenuItem;
    FlowRateTableMenuItem: TMenuItem;
    lcmTitle: TiLCDMatrix;
    Channel2_View: TChannelViewFrame;
    Channel4_View: TChannelViewFrame;
    Channel3_view: TChannelViewFrame;
    Channel6_view: TChannelViewFrame;
    Channel5_view: TChannelViewFrame;
    procedure temperatureGraphMenuItemClick(Sender: TObject);
    procedure pressureGraphPopupMenuClick(Sender: TObject);
    procedure FlowRateGraphMenuItemClick(Sender: TObject);
    procedure HeatRateGraphMenuItemClick(Sender: TObject);
    procedure temperatureTableMenuItemClick(Sender: TObject);
    procedure pressureTableMenuItemClick(Sender: TObject);
    procedure FlowRateTableMenuItemClick(Sender: TObject);
    procedure HeatRateTableMenuItemClick(Sender: TObject);
  private
    FCount: integer;
    FActive: boolean;
    FPutCaption: string;
    FData: IPutData;
    FNeedUpdateView: boolean;
    procedure SetCount(const Value: integer);
    function getChannelView(index: integer): TChannelViewFrame;
    procedure SetPutCaption(const Value: string);
    function getAlarmOn: boolean;
    procedure SetData(const Value: IPutData);
    procedure ShowTemperatureGraph;
    function GetTemperatureListFor(chanelID: integer; startDate, endDate : TDateTime): IValueList;
    function GetTemperatureMatrix(startDate, endDate : TDateTime): IValueMatrix;
    procedure ShowGraph(value: IValueMatrix; acaption, atitle: string; parmCaption : string );
    function GetPressureMatrix(startDate, endDate : TDateTime): IValueMatrix;
    function GetPressureListFor(chanelID: integer; startDate, endDate : TDateTime ): IValueList;
    function GetFlowRateListFor(chanelID: integer; startDate, endDate : TDateTime ): IValueList;
    function GetFlowRateMatrix( startDate, endDate : TDateTime  ): IValueMatrix;
    function GetHeatRateListFor(chanelID: integer; startDate, endDate : TDateTime ): IValueList;
    function GetHeatRateMatrix( startDate, endDate : TDateTime ): IValueMatrix;
    procedure GetDateRange(var startDate, endDate: TDateTime);
    procedure ShowTable(value: IValueMatrix; acaption, atitle: string; startDate, endDate : TDateTime; paramCaption : string);
    procedure SetNeedUpdateView(const Value: boolean);
    function GetData: IPutData;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent; acount : integer );reintroduce;overload;
    destructor Destroy();override;


    procedure UpdateAlarmState();
    procedure UpdateView();

    //--[ properties ]--
    property Active : boolean read FActive;
    property AlarmOn : boolean read getAlarmOn;
    property ChannelView[ index : integer ] : TChannelViewFrame read getChannelView;
    property Count : integer read FCount write SetCount;
    property PutCaption : string read FPutCaption write SetPutCaption;

    property PutData : IPutData read GetData write SetData;

    procedure UpdateData(const adata: IPutData);

    property NeedUpdateView : boolean read FNeedUpdateView write SetNeedUpdateView;
  end;

implementation

uses
    //--[ classes ]--
    class_value,
    //--[ constants ]--
    const_guids,
    const_put,
    //--[ data modules ]--
    module_dataMain,
    //--[ forms ]--
  	form_dateRangeSelectorForm,
	form_graphViewForm,
    form_tableView,
    //--[ tools ]--
    tool_environment,
    tool_systemLog
    , tool_ms_factories;

{$R *.dfm}

{ TPutViewFrame }
const
    MAX_COUNT = 6;


constructor TPutViewFrame.Create(AOwner: TComponent; acount : integer);
var
  i: Integer;
begin
  inherited Create( aowner );

  FData := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IPutData;

  Count := acount;

  for i := 1 to Count do
  begin
    with ChannelView[ i ] do
    begin
    	ChannelCaption := IntToStr( i );
        ChanelNo := i;
    end;
  end;
end;

destructor TPutViewFrame.Destroy;
begin
    FData := nil;
    
  inherited;
end;

function TPutViewFrame.getAlarmOn: boolean;
	var
    	i : integer;
begin
   result := false;
   for i := 1 to Count do
   begin
     if( ChannelView[i].AlarmOn )then
     begin
     	result := true;
        exit;
     end;
   end;
end;

function TPutViewFrame.getChannelView(index: integer): TChannelViewFrame;
begin
    result := self.FindComponent( Format( 'Channel%d_View', [index] )) as TChannelViewFrame;
end;

//TODO: Implement dynamic view creation
procedure TPutViewFrame.SetCount(const Value: integer);
	var
    	i : integer;
begin
  FCount := Max( 0, Min( Value, MAX_COUNT ) );

  for i := 1 to value do
  begin
    ChannelView[ i ].Show();
  end;

  for i := Value + 1 to MAX_COUNT do
  begin
    ChannelView[ i ].Hide();
  end;
end;

procedure TPutViewFrame.SetData(const Value: IPutData);
	var
    	i : integer;
begin
  FData.Assign( Value );

  PutCaption := FData.Caption;
  FActive := FData.Active;

  SetCount( Value.Count );

  //TODO: Assign only ranges. OPC does not exist at this point
  for i := 1 to Count do
  begin
	ChannelView[i].OpcData.Assign(FData[i]);
  end;

  lcmTitle.Text := Value.Caption;
end;

procedure TPutViewFrame.SetPutCaption(const Value: string);
begin
  FPutCaption := Value;
end;

procedure TPutViewFrame.UpdateAlarmState;
var
  j: Integer;
begin
	for j := 1 to Count do
    begin
      ChannelView[j].UpdateAlarmState();
    end;
end;

procedure TPutViewFrame.UpdateView;
var
  i: Integer;
begin
    for i := 1 to Count do
    begin
      ChannelView[ i ].UpdateView();
    end;

    Refresh();
end;

procedure TPutViewFrame.UpdateData( const adata : IPutData );
	var
    	i : integer;
begin
	PutData.Assign( adata );

	for i := 1 to count do
    begin
		ChannelView[i].ParamValues.Assign( PutData.Chanels[i].Value );
    end;
end;


procedure TPutViewFrame.temperatureGraphMenuItemClick(Sender: TObject);
begin
	ShowTemperatureGraph();
end;


//TODO: Move to PutDataReceiver.Get...List
//TODO: Refactor get...ListFor
function TPutViewFrame.GetTemperatureListFor( chanelID : integer; startDate, endDate : TDateTime ): IValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %d ) and (date_query >= %s) and (date_query <= %s) and (param_kind_id = 1) order by date_query;';
    var
    	adata : IValue;
        factory : ICommonFactory;
begin
	result := nil;

    factory := GetCommonFactory();

    with msDataModule do
    begin
    	queryData.SQL.Text := Format( c_sql, [chanelId, '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',  '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''' ] );
        queryData.Open();
        if( queryData.RecordCount > 0 )then
        begin
        	result := factory.CreateInstance( CLSID_ValueList ) as IValueList;

		    result.Title := '�' + IntToStr(chanelId);

            while not queryData.eof do
            begin
				adata := factory.CreateInstance( CLSID_Value ) as IValue;

                adata.Date := queryData.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := queryData.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                queryData.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;

function TPutViewFrame.GetPressureListFor( chanelID : integer; startDate, endDate : TDateTime ): IValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %d ) and (date_query >= %s) and (date_query <= %s) and (param_kind_id = 2) order by date_query;';
    var
    	adata : IValue;
        factory : ICommonFactory;
begin
	result := nil;

    factory := GetCommonFactory();

    with msDataModule do
    begin
    	queryData.SQL.Text := Format( c_sql, [chanelId, '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',  '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''' ] );
        queryData.Open();
        if( queryData.RecordCount > 0 )then
        begin
        	result := factory.CreateInstance( CLSID_ValueList ) as IValueList;
		    result.Title := 'P' + IntToStr(chanelId);

            while not queryData.eof do
            begin
				adata := factory.CreateInstance( CLSID_Value ) as IValue;
                adata.Date := queryData.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := queryData.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                queryData.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;

function TPutViewFrame.GetFlowRateListFor( chanelID : integer; startDate, endDate : TDateTime ): IValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %d ) and (date_query >= %s) and (date_query <= %s) and (param_kind_id = 3) order by date_query;';
    var
    	adata : IValue;
        factory : ICommonFactory;
begin
	result := nil;

    //TODO: Remove duplication for methods Get...ListFor
    factory := GetCommonFactory();

    with msDataModule do
    begin
    	queryData.SQL.Text := Format( c_sql, [chanelId, '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',  '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''' ] );
        queryData.Open();
        if( queryData.RecordCount > 0 )then
        begin
        	result := factory.CreateInstance( CLSID_ValueList ) as IValueList;
		    result.Title := 'Q' + IntToStr(chanelId);

            while not queryData.eof do
            begin
				adata := factory.CreateInstance( CLSID_Value ) as IValue;
                adata.Date := queryData.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := queryData.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                queryData.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;

function TPutViewFrame.GetHeatRateListFor( chanelID : integer; startDate, endDate : TDateTime ): IValueList;
	const
    	c_sql = 'select * from params where ( chanel_id = %d ) and (date_query >= %s) and (date_query <= %s) and (param_kind_id = 4) order by date_query;';
    var
    	adata : IValue;
        factory: ICommonFactory;
begin
	result := nil;

    factory := GetCommonFactory();

    with msDataModule do
    begin
    	queryData.SQL.Text := Format( c_sql, [chanelId, '''' + FormatDateTime( 'yyyy-mm-dd', startDate ) + '''',  '''' + FormatDateTime( 'yyyy-mm-dd', endDate ) + '''' ] );
        queryData.Open();
        if( queryData.RecordCount > 0 )then
        begin
        	result := factory.CreateInstance( CLSID_ValueList ) as IValueList;
		    result.Title := 'H' + IntToStr(chanelId);

            while not queryData.eof do
            begin
				adata := factory.CreateInstance( CLSID_Value ) as IValue;
                adata.Date := queryData.FieldByName( 'date_query' ).AsDateTime;
                adata.Value := queryData.FieldByName( 'value' ).asFloat;
                result.Add( adata );

                queryData.Next();
            end;
        end;
    end;

    if( result = nil )then
    	raise EListError.Create('No data is present');
end;


//TODO: Move to IPutDataRetriever.Get...Matrix
function TPutViewFrame.GetTemperatureMatrix( startDate, endDate : TDateTime ): IValueMatrix;
  var
    adata : IValueList;
    value : IValueMatrix;
    i	 : integer;
    errCount : integer;
begin
 	value := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;

    errCount := 0;

    for i := 1 to Count do
    begin
        try
          adata := GetTemperatureListFor( PutData.Chanels[i].Index, startDate, endDate );
          adata.Title := 'T' + IntToStr(i);

          value.Add( adata );
        except
            on e:EListError do
            begin
                SystemLog.Write(e.Message);
                errCount := errCount + 1;
            end;
        end;
    end;

    if( errCount = count )then
    begin
      raise Exception.Create( '������ ��� �� ��������� ������!' );
    end;

    result := value;
end;

function TPutViewFrame.GetPressureMatrix(startDate, endDate : TDateTime): IValueMatrix;
  var
    adata : IValueList;
    value : IValueMatrix;
    i	 : integer;
begin
 	value := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;

    for i := 1 to Count do
    begin
        try
          adata := GetPressureListFor( PutData.Chanels[i].Index, startDate, endDate );
          adata.Title := 'P' + IntToStr(i);

          value.Add( adata );
        except
            on e:EListError do
                Abort;//Log error message
        end;
    end;
    result := value;
end;

function TPutViewFrame.GetFlowRateMatrix( startDate, endDate : TDateTime ): IValueMatrix;
  var
    adata : IValueList;
    value : IValueMatrix;
    i	 : integer;
begin
 	value := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;

    for i := 1 to Count do
    begin
        try
          adata := GetFlowRateListFor( PutData.Chanels[i].Index, startDate, endDate );
          adata.Title := 'Q' + IntToStr(i);

          value.Add( adata );
        except
            on e:EListError do
                Abort;//Log error message
        end;
    end;
    result := value;
end;

function TPutViewFrame.GetHeatRateMatrix( startDate, endDate : TDateTime ): IValueMatrix;
  var
    adata : IValueList;
    value : IValueMatrix;
    i	 : integer;
begin
 	value := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;

    for i := 1 to Count do
    begin
        try
          adata := GetHeatRateListFor( PutData.Chanels[i].Index, startDate, endDate );
          adata.Title := 'T' + IntToStr(i);

          value.Add( adata );
        except
            on e:EListError do
                Abort;//Log error message
        end;
    end;
    result := value;
end;

procedure TPutViewFrame.ShowTemperatureGraph();
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

    //TODO: Show progress indicator for loading

    try
	    ShowGraph( GetTemperatureMatrix( startDate, endDate ), '������� �����������', '����������� �� ������� ��� - ' + PutData.Caption, 'T' );
    except
        on e:Exception do
            Application.MessageBox( PAnsiChar('��������'#$0D#$0A'�������� ������ ��� ��������� ������' + #$0D#$0A + e.Message + #$0D#$0A), '��������', MB_OK + MB_ICONEXCLAMATION );
    end;
end;

procedure TPutViewFrame.ShowGraph( value : IValueMatrix; acaption : string; atitle : string; parmCaption : string );
	var
        graphForm : TGraphViewForm;
        i : integer;
begin
    graphForm := TGraphViewForm.Create(self);
    try
        for i := 1 to value.Count do
        begin
          value.List[i].Title := parmCaption + IntToStr(i);
        end;

        graphForm.ValueMatrix := value;
        graphForm.Caption := acaption;

        //TODO: Remove indirection
        graphForm.graphChart.Title.Text.Text := atitle;
        graphForm.ParamCaption := parmCaption;

        graphForm.ShowModal();
    finally
      FreeAndNil( graphForm );
    end;
end;

procedure TPutViewFrame.GetDateRange( var startDate, endDate : TDateTime ) ;
 var
    selector : TSelectDateRangeForm;
begin
	selector := TSelectDateRangeForm.Create(self);
    try
    	selector.startDate := now;
        selector.endDate := now + 1;
    	if( selector.ShowModal() = mrOk )then
        begin
			startDate := selector.StartDate;
            endDate := selector.EndDate;
        end
        else
        	Abort();
    finally
      FreeAndNil( selector );
    end;
end;

procedure TPutViewFrame.pressureGraphPopupMenuClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph( GetPressureMatrix( startDate, endDate ), '������� ��������', '�������� �� ������� ��� - ' + PutData.Caption, 'P' );
end;

procedure TPutViewFrame.FlowRateGraphMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph( GetFlowRateMatrix( startDate, endDate ), '������� �������', '������ �� ������� ��� - ' + PutData.Caption, 'Q' );
end;

procedure TPutViewFrame.HeatRateGraphMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowGraph( GetHeatRateMatrix( startDate, endDate ), '������� �����', '����� �� ������� ��� - ' + PutData.Caption, 'W' );
end;

procedure TPutViewFrame.temperatureTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowTable( GetTemperatureMatrix( startDate, endDate ), '������� �����������', '��� - ' + PutData.Caption, startDate, endDate, 'T' );
end;

procedure TPutViewFrame.ShowTable( value : IValueMatrix; acaption : string; atitle : string; startDate, endDate : TDateTime; paramCaption : string  );
	var
        tableForm : TTableViewForm;
begin
    tableForm := TTableViewForm.Create(self);
    try
        tableForm.ValueMatrix := value;
        tableForm.Caption := acaption;
        tableForm.tableLabel.caption := atitle;
        tableForm.StartDate := startDate;
        tableForm.EndDate := endDate;
        tableForm.ParamCaption := ParamCaption;
        tableForm.ShowModal();
    finally
      FreeAndNil( tableForm );
    end;
end;


procedure TPutViewFrame.pressureTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowTable( GetPressureMatrix( startDate, endDate ), '������� ��������', '��� - ' + PutData.Caption, startDate, endDate, 'P' );
end;

procedure TPutViewFrame.FlowRateTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowTable( GetFlowRateMatrix( startDate, endDate ), '������� ��������', '��� - ' + PutData.Caption, startDate, endDate, 'Q' );
end;

procedure TPutViewFrame.HeatRateTableMenuItemClick(Sender: TObject);
	var
    	startDate, endDate : TDateTime;
begin
    GetDateRange( startDate, endDate );

	ShowTable( GetHeatRateMatrix( startDate, endDate ), '������� �����', '��� - ' + PutData.Caption, startDate, endDate, 'W' );
end;

procedure TPutViewFrame.SetNeedUpdateView(const Value: boolean);
	var
    	i : integer;
begin
  FNeedUpdateView := Value;

  for i := 1 to count do
  begin
  	ChannelView[i].NeedUpdateView := value;
  end;
end;

function TPutViewFrame.GetData: IPutData;
begin
    RESULT := FData;
end;


end.
