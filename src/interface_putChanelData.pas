unit interface_putChanelData;

interface
    uses
      //--[ interfaces ]--
      interface_channelParams,
      interface_mnemoschemeItemChanelData
      ;

  const
      IID_IPutChanelData : TGUID ='{7E546710-2BB6-44C9-8AF6-743AC189EF06}';

  type
        IPutChanelData = interface( IMnemoschemeItemChanelData )
            ['{7E546710-2BB6-44C9-8AF6-743AC189EF06}']
              function GetflowRateOPC: string;
              function GetHeatRateOPC: string;
              function GetParams: IChannelParams;
              function GetPressureOPC: string;
              function GetTemperatureOPC: string;

              procedure SetflowRateOPC(const Value: string);
              procedure SetpressureOPC(const Value: string);
              procedure SettemperatureOPC(const Value: string);
              procedure SetParams(const Value: IChannelParams);
              procedure SetheatRateOPC(const Value: string);

              property temperatureOPC : string read GetTemperatureOPC write SettemperatureOPC;
              property pressureOPC : string read GetPressureOPC write SetpressureOPC;
              property flowRateOPC : string read GetflowRateOPC write SetflowRateOPC;
              property heatRateOPC : string read GetHeatRateOPC write SetheatRateOPC;

              property Params : IChannelParams read GetParams write SetParams;
        end;

implementation

end.
 