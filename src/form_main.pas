unit form_main;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleCtrls, DBOleCtl,  sSkinProvider, sSkinManager,
  OleServer, sComboBoxes, StdCtrls, sComboBox, Buttons,
  sBitBtn, ComCtrls, sStatusBar, ExtCtrls,
  Math, sLabel, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, Series, TeEngine, TeeProcs, Chart,
  sGroupBox, IniFiles, acProgressBar, sEdit, sSpinEdit, sPanel,
  //--[ io comps ]--
  iComponent,
  iVCLComponent, iCustomComponent, iAnalogDisplay, shellApi,
  sMemo, iLCDMatrix, iGaugeComponent, iAngularGauge,
  iPositionComponent, iScaleComponent, iThermometer, iSevenSegmentDisplay,
  iSevenSegmentAnalog, iPipe, iPipeJoint, iMotor, iTank, iValve, iLed,
  iLedRound, iSwitchLed, iSwitchMultiPosition,
  iSwitchRotary,
  Menus, AMAdvLed, ZDataset, sGauge,
  jclSysUtils, sSplitter, FlexBase, ImgList, ToolWin, sToolBar,
  FlexControls, FlexUtils, FlexProps, sPageControl,
  //--[ classes ]--
  class_channelParams,
  class_key,
  class_wellChannelParams,
  //--[ containers ]--
  XPCollections,
  //--[ Controls ]--
  control_putMnemoscheme,
  control_WaterIntakeMnemoscheme,
  //--[ data ]--
  data_put,
  data_putChanel,
  data_well,
  data_wellChanel,
  //--[ frames ]--
  frame_putInfoView,
  frame_putView,
  frame_wellView,
  //--[ interfaces ]--
  interface_commonFactory,
  interface_mnemoschemePreparer,
  interface_putChanelData,
  interface_putData,
  interface_putDataLoader,
  interface_putChanelList,
  interface_putList,
  interface_uiStatus,
  interface_wellChannelData,
  interface_wellData,
  interface_wellDataLoader,
  interface_wellList,
  interface_wellsCalculator,
  //--[ list ]--
  list_put,
  list_putChanel,
  list_well,
  //--[ opc ]--

  //--[ storage ]--
  storage_objectHashTable,
  //--[ types ]--
  type_tasks
  ;

type

  TWorkMode = ( wmSummer, wmWinter );
  TDisplayKind	= ( dkPressure, dkFlowRate, dkTemperature );

  TMnemoscheme = class(TForm)
    RefreshTimer: TTimer;
    alarmTimer: TTimer;
    detailsTimer: TTimer;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Exit1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    tickTimer: TTimer;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ilFuncs: TImageList;
    mainPages: TsPageControl;
    sPanel1: TsPanel;
    queryButton: TsBitBtn;
    timeGauge: TsGauge;
    time: TsTimePicker;
    memoLog: TsMemo;

    procedure RefreshTimerTimer(Sender: TObject);
    procedure put9_LEDMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure alarmTimerTimer(Sender: TObject);
    procedure queryAllNowButtonClick(Sender: TObject);
    procedure tickTimerTimer(Sender: TObject);
    procedure queryButtonClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure spPanelToggleCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure rgpActiveParmsChanging(Sender: TObject; NewIndex: Integer;
      var AllowChange: Boolean);


  private

    fStatus : IUIStatus;
    FCurrentChannel: IPutChanelData;
    FDisplayKind: TDisplayKind;

    procedure LogWrite(message: string);
    procedure CoverFullScreen;

    procedure ShowAboutForm;
    function GetPutName(index: integer): string;
    function getPutCount: integer;

    procedure SetStatus(const value: string);


	 procedure SetDisplayKind(const Value: TDisplayKind);


    function GetWellName(index: integer): string;

    function getWellView(index: integer): TWellViewFrame;

    procedure SetCurrentChannel(const Value: IPutChanelData);

    procedure CheckStatus;
    procedure Handle_QueryDataFinished(sender : tobject);
    procedure Handle_QueryDataStarted(sender : tobject);

    protected
		m_activePutLed: TiLedRound;
		m_alarmPutLed: TiLedRound;
		m_querying: Boolean;

		  m_iniFile : TMemIniFile;

		  m_putList : IPutList;


          m_putPreparer : IMnemoschemePreparer;
          m_wellPreparer : IMnemoschemePreparer;


        startTime : TTime;

        FOldDate : TDateTime;

	 function getPutView(index: integer): TPutViewFrame;

      function getIniFile(): TMemIniFile;
      procedure SetupOPCDA;
    { Private declarations }
  public
    { Public declarations }
    constructor CreateForTest();

    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;



    procedure LogError( errorMessage : string );


    property CurrentChannel: IPutChanelData read FCurrentChannel write
        SetCurrentChannel;

    property WellView[ index : integer ] : TWellViewFrame read getWellView;

    property PutCount : integer read getPutCount;

    property DisplayKind : TDisplayKind read FDisplayKind write SetDisplayKind;
  end;

var
  Mnemoscheme: TMnemoscheme;

implementation

uses
    //--[ classes ]--
    class_creatableObject,
	//--[ const ]--
    const_put,
    //--[ common ]--
  ActiveX,
	//--[ classes ]--
  class_opcItem,
  	//--[ data ]--
  module_dataMain,
  	//--[ forms ]--
  form_mnemoschemeAbout,
  //--[ other ]--
  tool_systemLog,
  tool_environment,
  const_guids,
  tool_putSettingsLoader,
  const_well,
  class_putDataLoader,
  class_wellDataLoader, JclAlgorithms, JclBase, JclContainerIntf, JclVectors,
  class_wellCalculator, tool_ms_factories, class_putMnemoschemePreparer
  ;


{$R *.dfm}
const
	//PUT_COUNT = 12;

	 settings_file = 'ms.settings';

	 WINTER_MODEL	=	'.\models\MainPipeline.fxd';
	 SUMMER_MODEL	=	'.\models\MainPipeline_summer.fxd';

     WATER_INTAKE_MODEL = '.\models\WaterIntake.fxd';


{TMnemoscheme}
procedure TMnemoscheme.put9_LEDMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  m_activePutLed := Sender as TiLedRound;
  detailsTimer.Enabled := true;
end;

procedure TMnemoscheme.alarmTimerTimer(Sender: TObject);
begin
	time.Value := now;
end;

constructor TMnemoscheme.Create(AOwner: TComponent);
	const
    	QUERYINTERVAL_ITEM = 'QueryInterval';
        MNEMOSCHEME_SECTION = 'Mnemoscheme';
begin
  inherited;


  FDisplayKind	:=	dkFlowRate;


  tickTimer.Enabled		:=	false;
  alarmTimer.Enabled	:=	false;
  RefreshTimer.Enabled	:=	false;
  detailsTimer.Enabled	:=	false;

  SetStatus( '�������������...' );


  timeGauge.MinValue := 0;
  timeGauge.MaxValue := RefreshTimer.Interval div TickTimer.Interval;


  SetStatus( '�������� ���������� �����...' );

  m_putPreparer := GetPutFactory().CreateInstance( CLSID_PutMnemoschemePreparer ) as IMnemoschemePreparer;

  m_putPreparer.Pages := mainPages;
  m_putPreparer.OnQueryDataStarted := Handle_QueryDataStarted;
  m_putPreparer.OnQueryDataFinished := Handle_QueryDataFinished;

  m_putPreparer.Execute();

  SetStatus( '�������� ���������� �������...' );

  m_wellPreparer := GetWellFactory().CreateInstance( CLSID_WellMnemoschemePreparer ) as IMnemoschemePreparer;

  m_wellPreparer.Pages := mainPages;
  m_wellPreparer.OnQueryDataStarted := Handle_QueryDataStarted;
  m_wellPreparer.OnQueryDataFinished := Handle_QueryDataFinished;

  m_wellPreparer.Execute();

  SetStatus( '���������� ���...' );
  FOldDate := 0;

  CoverFullScreen();


  RefreshTimer.Interval := 600000; //ms

  tickTimer.Enabled		:=	true;
  alarmTimer.Enabled	:=	true;
  RefreshTimer.Enabled	:=	true;
  detailsTimer.Enabled	:=	true;

  SetStatus( '�������� ���...' );


  mainPages.ActivePageIndex := 0;
end;


procedure TMnemoscheme.Handle_QueryDataStarted(sender : tobject);
begin
	queryButton.Enabled := false;
end;


procedure TMnemoscheme.Handle_QueryDataFinished(sender : tobject);
begin
    queryButton.Enabled := true;
end;

procedure TMnemoscheme.SetStatus( const value : string );
begin
    memoLog.Lines.Add( value );

    CheckStatus();

    if( not assigned( fstatus ))then
    	exit;

	fstatus.Status := value;
end;

procedure TMnemoscheme.CheckStatus();
begin
    if( not assigned( fStatus ) )then
        fstatus := Environment.getInstance( CLSID_UIStatus ) as IUIStatus;
end;

procedure TMnemoscheme.CoverFullScreen();
begin
	SetBounds( Screen.WorkAreaLeft, Screen.WorkAreaTop, Screen.WorkAreaWidth, Screen.WorkAreaHeight);
end;



procedure TMnemoscheme.SetupOPCDA();
begin
end;

destructor TMnemoscheme.Destroy;
begin
	AlarmTimer.Enabled := false;
	RefreshTimer.Enabled := false;

  inherited;

    FreeAndNil( m_iniFile );
end;


function TMnemoscheme.getIniFile(): TMemIniFile;
begin
	if( m_iniFile = nil )then
         m_iniFile := TMemIniFile.Create(ExtractFilePath(ParamStr(0))+settings_file);

    result := m_iniFile;
end;




function TMnemoscheme.getPutView(index: integer): TPutViewFrame;
begin
  result := FindComponent( GetPutName(index) ) as TPutViewFrame;
end;

function TMnemoscheme.getWellView(index: integer): TWellViewFrame;
begin
  result := FindComponent( GetWellName(index) ) as TWellViewFrame;
end;

function TMnemoscheme.GetPutName( index : integer ): string;
begin
	result := Format( 'Put%d_View', [index] );
end;

function TMnemoscheme.GetWellName( index : integer ): string;
begin
	result := Format( 'Well%dView', [index] );
end;


procedure TMnemoscheme.RefreshTimerTimer(Sender: TObject);
begin
  refreshTimer.Enabled := false;
  m_putPreparer.Update();

  m_wellPreparer.Update();
  
  timeGauge.Progress := 0;
  refreshTimer.Enabled := true;
end;



procedure TMnemoscheme.LogError( errorMessage : string );
begin
    LogWrite( '������:' + ErrorMessage );
end;

procedure TMnemoscheme.LogWrite( message : string);
begin
	SystemLog.Write( message );
end;



procedure TMnemoscheme.queryAllNowButtonClick(Sender: TObject);
begin
    m_putPreparer.Update();
end;

//TODO: Make data as field









function TMnemoscheme.getPutCount: integer;
begin
	result := m_putList.Count;
end;

procedure TMnemoscheme.SetDisplayKind(const Value: TDisplayKind);
begin
  FDisplayKind := Value;
end;

constructor TMnemoscheme.CreateForTest;
begin
  inherited Create(nil);
  //do nothing
end;


procedure TMnemoscheme.tickTimerTimer(Sender: TObject);
	const
    	c_day_secs = ( 24*60*60 );
	var
    	time : TDateTime;
begin
	if( RefreshTimer.Enabled )then
    begin
    	timeGauge.Progress := timeGauge.Progress + 1;

        time := ( refreshTimer.Interval - Cardinal(timeGauge.Progress)*tickTimer.Interval ) / (c_day_secs*1000);

        timeGauge.Hint := '�� ���������� ������ �������� ' + FormatDateTime('hh:nn:ss', time );
    end;

    Caption := Format( '���������� ���3 -- %s - %s', [DateToStr( Now ), TimeToStr( Now )] );
end;

procedure TMnemoscheme.queryButtonClick(Sender: TObject);
begin
	m_putPreparer.Update();
    m_wellPreparer.Update();
end;

procedure TMnemoscheme.Exit1Click(Sender: TObject);
begin
	Close();
end;

procedure TMnemoscheme.About1Click(Sender: TObject);
begin
	ShowAboutForm();
end;

procedure TMnemoscheme.ShowAboutForm();
	var
    	form : TMnemoschemeAboutForm;
begin
	form := TMnemoschemeAboutForm.Create(self);
    try
		form.ShowModal();
    finally
      FreeAndNil( form );
    end;
end;


procedure TMnemoscheme.spPanelToggleCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
	Accept := false;
end;

procedure TMnemoscheme.rgpActiveParmsChanging(Sender: TObject;
  NewIndex: Integer; var AllowChange: Boolean);
begin
	DisplayKind	:=	TDisplayKind( NewIndex );
end;

procedure TMnemoscheme.SetCurrentChannel(const Value: IPutChanelData);
begin
  FCurrentChannel := Value;
end;

initialization

finalization

end.


