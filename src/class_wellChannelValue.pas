unit class_wellChannelValue;

interface
    uses
        //--[ classes ]--
        class_parameterValue
        ;

	type
    	TWellChannelValue = class
            private
              FFlowRate: TParameterValue;
              FBackwardFlowRate : TParameterValue;
              FCurrentFlowRate : TParameterValue;

              function GetFlowRate: TParameterValue;
              procedure SetFlowRate(const Value: TParameterValue);

              function GetBackwardFlowRate: TParameterValue;
              procedure SetBackwardFlowRate(const Value: TParameterValue);
              function GetCurrentFlowRate: TParameterValue;
              procedure SetCurrentFlowRate(const Value: TParameterValue);
        	public
                constructor Create();

            	procedure Assign( const data : TWellChannelValue );

                property BackwardFlowRate : TParameterValue read GetBackwardFlowRate write SetBackwardFlowRate;
                property CurrentFlowRate : TParameterValue read GetCurrentFlowRate write SetCurrentFlowRate;
                property FlowRate : TParameterValue read GetFlowRate write SetFlowRate;

                function toString() : string;
                procedure Clear();
        end;

implementation

	uses
    	sysUtils
        , tool_systemLog;

{ TWellChannelValue }

procedure TWellChannelValue.Assign(const data: TWellChannelValue);
begin
    FFlowRate.Assign( data.FlowRate );
    FBackwardFlowRate.Assign( data.BackwardFlowRate );
    FCurrentFlowRate.Assign( data.CurrentFlowRate );
end;

procedure TWellChannelValue.Clear;
begin
    FlowRate.Clear();
    BackwardFlowRate.Clear();
    CurrentFlowRate.Clear();
end;

constructor TWellChannelValue.Create;
begin
    inherited;

    FFlowRate := TParameterValue.Create();
    FBackwardFlowRate := TParameterValue.Create();
    FCurrentFlowRate := TParameterValue.Create();
end;

function TWellChannelValue.GetBackwardFlowRate: TParameterValue;
begin
    result := FBackwardFlowRate;
end;

function TWellChannelValue.GetCurrentFlowRate: TParameterValue;
begin
    result := FCurrentFlowRate;
end;

function TWellChannelValue.GetFlowRate: TParameterValue;
begin
    result := FFlowRate;
end;


procedure TWellChannelValue.SetBackwardFlowRate(
  const Value: TParameterValue);
begin
    FBackwardFlowRate := value;
end;


procedure TWellChannelValue.SetCurrentFlowRate(
  const Value: TParameterValue);
begin
    FCurrentFlowRate := value;
end;

procedure TWellChannelValue.SetFlowRate(const Value: TParameterValue);
begin
   FFlowRate := Value;
end;



function TWellChannelValue.toString: string;
begin
	result := '';
end;

end.
 