unit interface_channelParams;

interface
    uses
        //--[ interfaces ]--
        interface_realRange
        ;

    const
        IID_IChannelParams : TGUID = '{599AD80B-64CD-444A-BF94-7DD8F169797F}';

    type
        IChannelParams = interface
            ['{599AD80B-64CD-444A-BF94-7DD8F169797F}']
            
            procedure Assign( const data : IChannelParams );

            function GetFlowRateRange: IRealRange;
            function GetHeatRateRange: IRealRange;
            function GetPressureRange: IRealRange;
            function GetTemperatureRange: IRealRange;

            property TemperatureRange : IRealRange read GetTemperatureRange;
            property PressureRange : IRealRange read GetPressureRange;
            property FlowRateRange : IRealRange read GetFlowRateRange;
            property HeatRateRange : IRealRange read GetHeatRateRange;
        end;
implementation

end.
 