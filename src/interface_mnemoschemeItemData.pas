unit interface_mnemoschemeItemData;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData
        ;

    const
        IID_IMnemoschemeItemData : TGUID = '{FD98F5F1-5868-49A6-9A35-766EA3C42B8C}';

    type
        IMnemoschemeItemData = interface
            ['{FD98F5F1-5868-49A6-9A35-766EA3C42B8C}']

                procedure Add( const adata : IMnemoschemeItemChanelData );
                procedure Assign( const data : IMnemoschemeItemData );
                procedure Clear();
                function FindById( index : integer ) : IMnemoschemeItemChanelData;
                function ToString() : string;


                function GetActive() : boolean;
                function GetCaption() : string;
                function GetCount(): integer;
                function GetIndex() : integer;
                function GetName() : string;

                procedure SetActive(const Value: boolean);
                procedure SetCaption(const Value: string);
                procedure SetIndex(const Value: integer);
                procedure SetName(const Value: string);

            	property Active : boolean read GetActive write SetActive;
                property Index : integer read GetIndex write SetIndex;
                property Caption : string read GetCaption write SetCaption;
                property Count : integer read getCount;
                property Name : string read GetName write SetName;

                function GetChanels(index: integer): IMnemoschemeItemChanelData;
                procedure SetChanels(index: integer; const Value: IMnemoschemeItemChanelData);

                property Chanels[ index : integer ] : IMnemoschemeItemChanelData read GetChanels write SetChanels;default;

                function Equals( source : IMnemoschemeItemData ) : boolean;
        end;

implementation

end.
 