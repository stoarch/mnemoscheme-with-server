program putScaner;

{%ToDo 'putScaner.todo'}
{%File 'src\const_put.dpk'}

uses
  Forms,
  sysUtils,
  form_wellScanerMain in 'src\form_wellScanerMain.pas' {WellScanerMainForm},
  opc_queryExecutor in '..\Lib\opc_queryExecutor.pas',
  class_opcDataAccess in 'src\class_opcDataAccess.pas',
  XPCollections in '..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  OPCutils in '..\..\library_new\OPC\Client\OPCutils.pas',
  class_realRange in 'src\class_realRange.pas',
  class_opcItem in 'src\class_opcItem.pas',
  OPCAutomation_TLB in '..\..\library_new\OPCAutomation_TLB.pas',
  XPFramework in '..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  OPCtypes in '..\..\library_new\OPC\OPCtypes.pas',
  OPC_AE in '..\..\library_new\OPC\OPC_AE.pas',
  OPCCOMN in '..\..\library_new\OPC\OPCCOMN.pas',
  OPCDA in '..\..\library_new\OPC\OPCDA.pas',
  OpcError in '..\..\library_new\OPC\OPCerror.pas',
  OPCHDA in '..\..\library_new\OPC\OPCHDA.pas',
  OPCSEC in '..\..\library_new\OPC\OPCSEC.pas',
  opc_error_strings in '..\..\library_new\OPC\opc_error_strings.pas',
  data_put in 'src\data_put.pas',
  data_putChanel in 'src\data_putChanel.pas',
  list_putChanel in 'src\list_putChanel.pas',
  list_put in 'src\list_put.pas',
  class_channelParams in 'src\class_channelParams.pas',
  module_dataMain in 'src\module_dataMain.pas' {MSDataModule: TDataModule},
  tool_systemLog in 'src\tool_systemLog.pas',
  tool_putSettingsLoader in 'src\tool_putSettingsLoader.pas',
  tool_putDataSaver in 'src\tool_putDataSaver.pas',
  form_aboutPutScaner in 'src\form_aboutPutScaner.pas' {AboutForm},
  class_channelValue in 'src\class_channelValue.pas',
  const_put in 'src\const_put.pas',
  tool_functions in 'src\tool_functions.pas',
  EzdslPQu in '..\..\library\Collections\EZDSL\EZDSLPQU.PAS',
  EZIntQue in '..\..\library\Collections\EZDSL\EZINTQUE.PAS',
  heap_putData in '..\..\library\irlib\storage\heap\heap_putData.pas',
  tool_settings in '..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\library\irlib\tools\tool_localSystem.pas',
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm};

{$R *.res}

begin
  	Settings.Open('putScaner.settings');

	SystemLog.Open( Format( 'putScaner%s.log', [formatDateTime('yyyymmddhhnnss', now)]));
    SystemLog.Write( 'Program started' );
    SystemLog.Write( 'Trying to open database...' );

    try
      	msDataModule.LoadSettings();
    	msDataModule.Open();
  		SystemLog.Write( 'Successfully' );
    except
      	on e:exception do
        begin
        	SystemLog.Write( 'Error:' + e.Message );
            Halt(1);
        end;
    end;


    try
      Application.Initialize;
      Application.Title := 'Put scaner server';

  		Application.HelpFile := 'D:\work\Mnemoscheme\doc\PUTSCANER.HLP';
  		Application.CreateForm(TWellScanerMainForm, WellScanerMainForm);
  Application.Run;
	except
    	on e:exception do
    		SystemLog.Write( 'CRITICAL ERROR:' + e.message );
    end;

    SystemLog.Write( 'Program halted' );
end.
