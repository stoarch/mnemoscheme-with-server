unit list_putChanel;

interface
	uses
        //--[ Classes ]--
        class_creatableInterfaceList,
        class_mnemoschemeItemChanelList,
    	//--[ common ]--
        xpCollections,
        //--[ data ]--
        data_putChanel,
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChanelList,
        interface_putChanelData,
        interface_putChanelList
        ;

    type
	  	TPutChanelList = class( TMnemoschemeItemChanelList, IPutChanelList )
            private
            protected
                function MakeData: IMnemoschemeItemChanelData;override;
        	public

        end;


implementation

uses tool_ms_factories, interface_commonFactory, const_put, const_guids;

{ TPutChanelList }


function TPutChanelList.MakeData(): IMnemoschemeItemChanelData;
begin
    result := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemChanelData ) as IPutChanelData;
end;





end.
