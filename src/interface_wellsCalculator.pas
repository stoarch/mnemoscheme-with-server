unit interface_wellsCalculator;

interface

    uses
        //--[ interfaces ]--
        interface_wellList
      ;

const
    IID_IWellsCalculator : TGUID  = '{85A058D4-851F-4448-8E29-F592FB8AA1B4}';

type
    IWellsCalculator = interface
        ['{85A058D4-851F-4448-8E29-F592FB8AA1B4}']

        function GetWells() : IWellList;
        procedure SetWells( const value : IWellList );

        property Wells : IWellList read GetWells write SetWells;

        function GetTotalSumFlowRate() : double;
        function GetTotalCurrentFlowRate() : double;
    end;


implementation

end.
 