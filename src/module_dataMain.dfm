object MSDataModule: TMSDataModule
  OldCreateOrder = False
  Left = 524
  Top = 158
  Height = 630
  Width = 885
  object msConnection: TZConnection
    Protocol = 'postgresql-8'
    HostName = '192.168.254.125'
    Port = 5432
    Database = 'ACSTP_HEF'
    User = 'postgres'
    Password = '1'
    Properties.Strings = (
      'codepage=UTF8')
    Left = 21
    Top = 22
  end
  object put_table: TZTable
    Connection = msConnection
    SortedFields = 'id'
    TableName = 'public.put'
    IndexFieldNames = 'id Asc'
    Left = 30
    Top = 112
    object put_tableid: TIntegerField
      DisplayWidth = 15
      FieldName = 'id'
      Required = True
    end
    object put_tablename: TStringField
      DisplayWidth = 15
      FieldName = 'name'
      Size = 50
    end
    object put_tablecaption: TStringField
      DisplayWidth = 13
      FieldName = 'caption'
      Size = 50
    end
    object put_tableactive: TBooleanField
      FieldName = 'active'
    end
  end
  object put_DataSource: TDataSource
    DataSet = put_table
    Left = 31
    Top = 179
  end
  object chanel_table: TZTable
    Connection = msConnection
    SortedFields = 'ID'
    TableName = 'public.chanel'
    MasterFields = 'id'
    MasterSource = put_DataSource
    LinkedFields = 'put_id'
    IndexFieldNames = 'ID Asc'
    Left = 122
    Top = 189
    object chanel_tableid: TIntegerField
      DisplayWidth = 15
      FieldName = 'id'
      Required = True
    end
    object chanel_tablecaption: TStringField
      DisplayWidth = 12
      FieldName = 'caption'
      Size = 50
    end
    object chanel_tableput_id: TIntegerField
      DisplayWidth = 15
      FieldName = 'put_id'
    end
    object chanel_tableactive: TBooleanField
      FieldName = 'active'
    end
  end
  object chanel_dataSource: TDataSource
    DataSet = chanel_table
    Left = 121
    Top = 267
  end
  object params_table: TZTable
    Connection = msConnection
    TableName = 'public.params'
    MasterFields = 'id'
    MasterSource = chanel_dataSource
    LinkedFields = 'chanel_id'
    Left = 226
    Top = 292
    object params_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object params_tableparam_caption: TStringField
      FieldKind = fkLookup
      FieldName = 'param_caption'
      LookupDataSet = param_kind_table
      LookupKeyFields = 'id'
      LookupResultField = 'caption'
      KeyFields = 'param_kind_id'
      Size = 50
      Lookup = True
    end
    object params_tablevalue: TFloatField
      FieldName = 'value'
    end
    object params_tablechanel_id: TIntegerField
      FieldName = 'chanel_id'
    end
    object params_tableparam_kind_id: TIntegerField
      FieldName = 'param_kind_id'
    end
    object params_tabledate_query: TDateTimeField
      FieldName = 'date_query'
    end
    object params_tablevalid: TBooleanField
      FieldName = 'valid'
    end
  end
  object params_DataSource: TDataSource
    DataSet = params_table
    Left = 226
    Top = 356
  end
  object opc_params_table: TZTable
    Connection = msConnection
    TableName = 'public.opc_params'
    MasterFields = 'id'
    MasterSource = chanel_dataSource
    LinkedFields = 'chanel_id'
    Left = 337
    Top = 295
    object opc_params_tableid: TIntegerField
      DisplayWidth = 12
      FieldName = 'id'
      Required = True
    end
    object opc_params_tableparam_caption: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'param_caption'
      LookupDataSet = param_kind_table
      LookupKeyFields = 'id'
      LookupResultField = 'caption'
      KeyFields = 'param_kind_id'
      Size = 50
      Lookup = True
    end
    object opc_params_tablevalue: TStringField
      DisplayWidth = 14
      FieldName = 'value'
      Size = 50
    end
    object opc_params_tablechanel_id: TIntegerField
      DisplayWidth = 12
      FieldName = 'chanel_id'
    end
    object opc_params_tableparam_kind_id: TIntegerField
      DisplayWidth = 14
      FieldName = 'param_kind_id'
    end
  end
  object opc_params_Datasource: TDataSource
    DataSet = opc_params_table
    Left = 342
    Top = 359
  end
  object param_kind_table: TZTable
    Connection = msConnection
    TableName = 'public.param_kind'
    Left = 63
    Top = 452
    object param_kind_tableid: TIntegerField
      DisplayWidth = 12
      FieldName = 'id'
      Required = True
    end
    object param_kind_tablecaption: TStringField
      DisplayWidth = 42
      FieldName = 'caption'
      Size = 50
    end
  end
  object param_kind_DataSource: TDataSource
    DataSet = param_kind_table
    Left = 64
    Top = 515
  end
  object chanel_control_settings_table: TZTable
    Connection = msConnection
    TableName = 'public.chanel_control_settings'
    MasterFields = 'id'
    MasterSource = chanel_dataSource
    LinkedFields = 'chanel_id'
    Left = 471
    Top = 322
    object chanel_control_settings_tableid: TIntegerField
      DisplayWidth = 8
      FieldName = 'id'
      Required = True
    end
    object chanel_control_settings_tableparam_caption: TStringField
      DisplayWidth = 21
      FieldKind = fkLookup
      FieldName = 'param_caption'
      LookupDataSet = param_kind_table
      LookupKeyFields = 'id'
      LookupResultField = 'caption'
      KeyFields = 'param_kind_id'
      Size = 50
      Lookup = True
    end
    object chanel_control_settings_tablemin: TFloatField
      DisplayWidth = 12
      FieldName = 'min'
    end
    object chanel_control_settings_tablemax: TFloatField
      DisplayWidth = 12
      FieldName = 'max'
    end
    object chanel_control_settings_tablechanel_id: TIntegerField
      DisplayWidth = 12
      FieldName = 'chanel_id'
    end
    object chanel_control_settings_tableparam_kind_id: TIntegerField
      DisplayWidth = 14
      FieldName = 'param_kind_id'
    end
  end
  object chanel_control_settings_DataSource: TDataSource
    DataSet = chanel_control_settings_table
    Left = 472
    Top = 386
  end
  object queryData: TZQuery
    Connection = msConnection
    Params = <>
    Left = 91
    Top = 30
  end
  object wiConnection: TZConnection
    Protocol = 'postgresql-8'
    HostName = '192.168.254.125'
    Port = 5432
    Database = 'wells_hef'
    User = 'postgres'
    Password = '1'
    Properties.Strings = (
      'codepage=UTF8')
    Left = 685
    Top = 26
  end
  object well_table: TZTable
    Connection = wiConnection
    SortedFields = 'id'
    TableName = 'public."object"'
    IndexFieldNames = 'id Asc'
    Left = 686
    Top = 124
    object well_tablecaption: TStringField
      FieldName = 'caption'
      Size = 50
    end
    object well_tableactive: TBooleanField
      FieldName = 'active'
    end
    object well_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object well_tablename: TStringField
      FieldName = 'name'
      Size = 50
    end
  end
  object well_source: TDataSource
    DataSet = well_table
    Left = 687
    Top = 191
  end
  object wellChannel_table: TZTable
    Connection = wiConnection
    SortedFields = 'ID'
    TableName = 'public.chanel'
    MasterFields = 'id'
    MasterSource = well_source
    LinkedFields = 'obj_id'
    IndexFieldNames = 'ID Asc'
    Left = 778
    Top = 201
    object wellChannel_tablecaption: TStringField
      FieldName = 'caption'
      Size = 50
    end
    object wellChannel_tableobj_id: TIntegerField
      FieldName = 'obj_id'
    end
    object wellChannel_tableactive: TBooleanField
      FieldName = 'active'
    end
    object wellChannel_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object wellChannel_tableobj_ch_id: TIntegerField
      FieldName = 'obj_ch_id'
    end
  end
  object wellChannel_source: TDataSource
    DataSet = wellChannel_table
    Left = 777
    Top = 279
  end
  object wellParams_table: TZTable
    Connection = wiConnection
    TableName = 'public.params'
    MasterFields = 'id'
    MasterSource = wellChannel_source
    LinkedFields = 'chanel_id'
    Left = 882
    Top = 304
    object wellParams_tablechanel_id: TIntegerField
      FieldName = 'chanel_id'
    end
    object wellParams_tableparam_kind_id: TIntegerField
      FieldName = 'param_kind_id'
    end
    object wellParams_tablevalue: TFloatField
      FieldName = 'value'
    end
    object wellParams_tabledate_query: TDateTimeField
      FieldName = 'date_query'
    end
    object wellParams_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
    object wellParams_tablevalid: TBooleanField
      FieldName = 'valid'
    end
  end
  object wellParams_source: TDataSource
    DataSet = wellParams_table
    Left = 882
    Top = 368
  end
  object wellOPC_params_table: TZTable
    Connection = wiConnection
    TableName = 'public.opc_params'
    MasterFields = 'id'
    MasterSource = wellChannel_source
    LinkedFields = 'chanel_id'
    Left = 993
    Top = 307
    object wellOPC_params_tablechanel_id: TIntegerField
      FieldName = 'chanel_id'
    end
    object wellOPC_params_tableparam_kind_id: TIntegerField
      FieldName = 'param_kind_id'
    end
    object wellOPC_params_tablevalue: TStringField
      FieldName = 'value'
      Size = 50
    end
    object wellOPC_params_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
  end
  object wellOPC_params_source: TDataSource
    DataSet = wellOPC_params_table
    Left = 998
    Top = 371
  end
  object wellParamKind_table: TZTable
    Connection = wiConnection
    TableName = 'public.param_kind'
    Left = 719
    Top = 464
    object wellParamKind_tablecaption: TStringField
      FieldName = 'caption'
      Size = 50
    end
    object wellParamKind_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
  end
  object wellParamKind_source: TDataSource
    DataSet = wellParamKind_table
    Left = 720
    Top = 527
  end
  object wellChannel_settings_table: TZTable
    Connection = wiConnection
    TableName = 'public.chanel_control_settings'
    MasterFields = 'id'
    MasterSource = wellChannel_source
    LinkedFields = 'chanel_id'
    Left = 1127
    Top = 334
    object wellChannel_settings_tablechanel_id: TIntegerField
      FieldName = 'chanel_id'
    end
    object wellChannel_settings_tableparam_kind_id: TIntegerField
      FieldName = 'param_kind_id'
    end
    object wellChannel_settings_tablemin: TFloatField
      FieldName = 'min'
    end
    object wellChannel_settings_tablemax: TFloatField
      FieldName = 'max'
    end
    object wellChannel_settings_tableid: TIntegerField
      FieldName = 'id'
      Required = True
    end
  end
  object wellChannel_settings_source: TDataSource
    DataSet = wellChannel_settings_table
    Left = 1128
    Top = 398
  end
  object wellQuery: TZQuery
    Connection = wiConnection
    Params = <>
    Left = 795
    Top = 50
  end
end
