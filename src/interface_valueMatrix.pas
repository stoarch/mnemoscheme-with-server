unit interface_valueMatrix;

interface

    uses
        interface_list,
        interface_value,
        interface_valueList
        ;

    const
        IID_IValueMatrix : TGUID = '{09c71efd-3c8f-4890-98ae-a7afce4ec693}';

    type
        IValueMatrix = interface( IList )
            ['{09c71efd-3c8f-4890-98ae-a7afce4ec693}']
				 //--[ methods ]--

				 procedure Add( const list : IValueList );
				 procedure Assign( const matrix : IValueMatrix );

				 //--[ property accessors ]--
				 function getList(index: integer): IValueList;

				 //--[ properties ]--
				 property List[ index : integer ] : IValueList read getList;
        end;


implementation

end.
 