unit class_opcDataAccess;

interface

uses
  //--[ classes ]--
  class_opcItem,
  //--[ common ]--
  classes, ComObj, SysUtils, Variants, ActiveX, Windows,
  //--[ dcl ]--
  XPCollections,
  //--[ opc ]--
  OPCutils, OPCDA, opcTypes
 ;

type
    EOPCDataAccess = class( Exception );
	EOPCAddGroup = class( EOPCDataAccess );
    EOPCAddItem = class( EOPCDataAccess );
    EOPCAdviseError = class( EOPCDataAccess );
    EOPCNotGoodItem = class( EOPCDataAccess );
    EOPCReadItem = class( EOPCDataAccess );
    EOPCDCOMInitializationError = class( EOPCDataAccess );
    EOPCRemoteServerError = class( EOPCDataAccess );

	TOPCDataAccess = class
    	private
        	m_server : OPCDA.IOPCServer;
        	m_group : IOPCItemMgt;
//            m_items  : Variant;
            m_adviseSink : IAdviseSink;
            m_asyncConnection : integer;

            m_opcItems : TXDList;

            groupHandle : cardinal;
    		FQueryInterval: integer;

            m_connected : boolean;


    		function getCount: integer;
    		function getItem(i : integer): Variant;
    		procedure SetQueryInterval(const Value: integer);

    		function getOPCItem( index : integer ): TOPCItem;
    		function getConnected: boolean;

        public
        	constructor Create();
            destructor Destroy();override;

            procedure AddItem( opcCaption : string );
            procedure RemoveItem( opcCaption : string );

            procedure Clear();
            procedure Connect( serverName : string );
            procedure Disconnect();
            procedure Advise();
            procedure UnAdvise();

            procedure StartServer();

            property Connected : boolean read getConnected;
            property Count : integer read getCount;
            property Item[ i : integer ] : Variant read getItem;
            property OPCItem[ index : integer ] : TOPCItem read getOPCItem;
            property QueryInterval : integer read FQueryInterval write SetQueryInterval;
    end;

implementation

uses opc_error_strings;

	const
        QUERY_INTERVAL = 60000;//3000 - needed

type
  // class to receive IDataObject data change advises
  TOPCAdviseSink = class(TInterfacedObject, IAdviseSink)
  public
    procedure OnDataChange(const formatetc: TFormatEtc;
                            const stgmed: TStgMedium); stdcall;
    procedure OnViewChange(dwAspect: Longint; lindex: Longint); stdcall;
    procedure OnRename(const mk: IMoniker); stdcall;
    procedure OnSave; stdcall;
    procedure OnClose; stdcall;
  end;

  // class to receive IConnectionPointContainer data change callbacks
  TOPCDataCallback = class(TInterfacedObject, IOPCDataCallback)
  public
    function OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
      phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
      pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
      pErrors: PResultList): HResult; stdcall;
    function OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
      hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
      pErrors: PResultList): HResult; stdcall;
    function OnCancelComplete(dwTransid: DWORD; hGroup: OPCHANDLE):
      HResult; stdcall;
  end;

// TOPCAdviseSink methods

// OPC standard says this is the only method we need to fill in
procedure TOPCAdviseSink.OnDataChange(const formatetc: TFormatEtc;
                                      const stgmed: TStgMedium);
var
  PG: POPCGROUPHEADER;
  PI1: POPCITEMHEADER1ARRAY;
  PI2: POPCITEMHEADER2ARRAY;
  PV: POleVariant;
  I: Integer;
  PStr: PWideChar;
  NewValue: string;
  WithTime: Boolean;
  ClientHandle: OPCHANDLE;
  Quality: Word;
begin
  // the rest of this method assumes that the item header array uses
  // OPCITEMHEADER1 or OPCITEMHEADER2 records,
  // so check this first to be defensive
  if (formatetc.cfFormat <> OPCSTMFORMATDATA) and
      (formatetc.cfFormat <> OPCSTMFORMATDATATIME) then Exit;
  // does the data stream provide timestamps with each value?
  WithTime := formatetc.cfFormat = OPCSTMFORMATDATATIME;

  PG := GlobalLock(stgmed.hGlobal);
  if PG <> nil then
  begin
    // we will only use one of these two values, according to whether
    // WithTime is set:
    PI1 := Pointer(PAnsiChar(PG) + SizeOf(OPCGROUPHEADER));
    PI2 := Pointer(PI1);
    if Succeeded(PG.hrStatus) then
    begin
      for I := 0 to PG.dwItemCount - 1 do
      begin
        if WithTime then
        begin
          PV := POleVariant(PAnsiChar(PG) + PI1[I].dwValueOffset);
          ClientHandle := PI1[I].hClient;
          Quality := (PI1[I].wQuality and OPC_QUALITY_MASK);
        end
        else begin
          PV := POleVariant(PAnsiChar(PG) + PI2[I].dwValueOffset);
          ClientHandle := PI2[I].hClient;
          Quality := (PI2[I].wQuality and OPC_QUALITY_MASK);
        end;
        if Quality = OPC_QUALITY_GOOD then
        begin
          // this test assumes we're not dealing with array data
          if TVarData(PV^).VType <> VT_BSTR then
          begin
            NewValue := VarToStr(PV^);
          end
          else begin
            // for BSTR data, the BSTR image follows immediately in the data
            // stream after the variant union;  the BSTR begins with a DWORD
            // character count, which we skip over as the BSTR is also
            // NULL-terminated
            PStr := PWideChar(PAnsiChar(PV) + SizeOf(OleVariant) + 4);
            NewValue := WideString(PStr);
          end;
          if WithTime then
          begin
            Writeln('New value for item ', ClientHandle, ' advised:  ',
                    NewValue, ' (with timestamp)');
          end
          else begin
            Writeln('New value for item ', ClientHandle, ' advised:  ',
                    NewValue);
          end;
        end
        else begin
          Writeln('Advise received for item ', ClientHandle,
                  ' , but quality not good');
        end;
      end;
    end;
    GlobalUnlock(stgmed.hGlobal);
  end;
end;

procedure TOPCAdviseSink.OnViewChange(dwAspect: Longint; lindex: Longint);
begin
end;

procedure TOPCAdviseSink.OnRename(const mk: IMoniker);
begin
end;

procedure TOPCAdviseSink.OnSave;
begin
end;

procedure TOPCAdviseSink.OnClose;
begin
end;

// TOPCDataCallback methods

function TOPCDataCallback.OnDataChange(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
var
  ClientItems: POPCHANDLEARRAY;
  Values: POleVariantArray;
  Qualities: PWORDARRAY;
  I: Integer;
  NewValue: string;
begin
  Result := S_OK;
  ClientItems := POPCHANDLEARRAY(phClientItems);
  Values := POleVariantArray(pvValues);
  Qualities := PWORDARRAY(pwQualities);
  for I := 0 to dwCount - 1 do
  begin
    if Qualities[I] = OPC_QUALITY_GOOD then
    begin
      NewValue := VarToStr(Values[I]);
      Writeln('New callback for item ', ClientItems[I], ' received, value:  ',
              NewValue);
    end
    else begin
      Writeln('Callback received for item ', ClientItems[I],
              ' , but quality not good');
    end;
  end;
end;

function TOPCDataCallback.OnReadComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMasterquality: HResult; hrMastererror: HResult; dwCount: DWORD;
  phClientItems: POPCHANDLEARRAY; pvValues: POleVariantArray;
  pwQualities: PWordArray; pftTimeStamps: PFileTimeArray;
  pErrors: PResultList): HResult;
begin
  Result := OnDataChange(dwTransid, hGroup, hrMasterquality, hrMastererror,
    dwCount, phClientItems, pvValues, pwQualities, pftTimeStamps, pErrors);
end;

function TOPCDataCallback.OnWriteComplete(dwTransid: DWORD; hGroup: OPCHANDLE;
  hrMastererr: HResult; dwCount: DWORD; pClienthandles: POPCHANDLEARRAY;
  pErrors: PResultList): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;

function TOPCDataCallback.OnCancelComplete(dwTransid: DWORD;
  hGroup: OPCHANDLE): HResult;
begin
  // we don't use this facility
  Result := S_OK;
end;




{ TOPCDataAccess }
procedure TOPCDataAccess.AddItem(opcCaption: string);
    var
        hr : HRESULT;
        index : integer;
        handle : cardinal;
        itemType : word;
begin
	index := m_opcItems.Add( refobj(TOPCItem.Create(opcCaption), true)) + 1;

    HR := GroupAddItem(m_group, opcCaption, 0, VT_EMPTY, handle, ItemType);

    if Failed(HR) then
    begin
      raise EOPCAddItem.Create( 'Unable to add to group.'#10#13'Error:'+OPCErrorCodeToString(HR) );
    end;

    OPCItem[Index].Handle := handle;
end;

procedure TOPCDataAccess.Advise;
	var
    	HR : HRESULT;
begin
  m_adviseSink := TOPCAdviseSink.Create();
  HR := GroupAdviseTime(m_group, m_adviseSink, m_asyncConnection);
  if Failed(HR) then
  begin
 	raise EOPCAdviseError.Create( 'Unable to advise' );
  end;
end;

procedure TOPCDataAccess.Clear;
    var
        i : integer;
        hr : HRESULT;
begin
    for i := 1 to m_opcItems.Count do
    begin
        hr := GroupRemoveItem(m_group, OPCItem[i].handle);
        if( FAILED(hr) )then
            raise EOPCDataAccess.Create('Failed to remove item ' + OPCItem[i].Caption);
    end;

	m_opcItems.Clear();
end;

//TODO: Refactor the method TOPCDataAccess.Connect:: Split it onto smaller
procedure TOPCDataAccess.Connect(serverName: string);
    const
        IID_IEnumString : TGuid = '{00000101-0000-0000-C000-000000000046}';

    label
        addNew, exists;
	var
    	HR : HRESULT;
        fetched : ULONG;
        itemIDEnum : IEnumString;
        browser : IOPCBrowseServerAddressSpace;
        itemID : LPWSTR;
        tempStr : string;
        itemsOnServer : TStringList;
begin
  m_connected := false;

  if( not assigned( m_server ))then
  begin
  	m_server := CreateComObject(ProgIDToClassID(serverName)) as IOPCServer;
  end;

  itemsOnServer := TStringList.Create();
  try
    hr := m_server.QueryInterface(IID_IOPCBrowseServerAddressSpace, browser);
    if( SUCCEEDED( hr ))then
    begin
      hr := browser.BrowseOPCItemIDs(OPC_FLAT,'',VT_EMPTY,0,itemIDEnum);
      if( Failed( hr ))then
        raise EOPCDataAccess.Create( 'Unable to enumerate id''s'#10#13'Error:'+SysErrorMessage(hr) );

      SetLength( tempStr, 255 );
      itemID := StringToOleStr(tempStr);
      repeat
        hr := itemIDEnum.Next(1,itemID,@fetched);

        if( hr = S_OK )then
          itemsOnServer.Add( itemId );
      until( hr = S_FALSE );
    end;

    HR := ServerAddGroup(m_server,'Data',True,QueryInterval,0, m_group, groupHandle);
    if Failed(HR) then
    begin
      raise EOPCAddGroup.Create( 'Unable to add to group'#10#13'Error:'+OPCErrorCodeToString(HR) );
    end;

  finally
    FreeAndNil( itemsOnServer );
  end;


  m_connected := true;
end;

constructor TOPCDataAccess.Create;
begin
	FQueryInterval := QUERY_INTERVAL;
    m_opcItems := TXDList.Create(TXDObject);
end;

destructor TOPCDataAccess.Destroy;
begin
    m_server := nil;

	FreeAndNil( m_opcItems );

	inherited;
end;

procedure TOPCDataAccess.Disconnect;
begin
	if( assigned( m_server ) )then
		m_server.RemoveGroup(groupHandle,false);

    m_connected := false;

    m_server := nil;
end;

function TOPCDataAccess.getConnected: boolean;
begin
	result := m_connected;
end;

function TOPCDataAccess.getCount: integer;
begin
	result := m_opcItems.Count;
end;

function TOPCDataAccess.getOPCItem( index : integer ): TOPCItem;
	var
    	xdItem : TXDObject;
begin
	m_opcItems.Item[ index-1 ].GetTyped(TXDObject, xdItem);//1 based!!

	result := xdItem.value as TOPCItem;
end;                            

function TOPCDataAccess.getItem(i : integer): Variant;
	var
    	HR : HRESULT;
        handle : cardinal;
        itemQuality : word;
        itemValue : string;
        opcItem : TOPCItem;
begin
    opcItem := getOPCItem( i );
    opcItem.Quality := ovqUncertain;

    handle := opcItem.Handle;

    HR := ReadOPCGroupItemValue(m_group, handle, ItemValue, ItemQuality);
    if Succeeded(HR) then
    begin
        if (ItemQuality and OPC_QUALITY_MASK) = OPC_QUALITY_GOOD then
        begin
    	    result := itemValue;
            opcItem.Value := itemValue;
            opcItem.Quality := ovqGood;

            exit;
        end
        else
        begin
            opcItem.Quality := ovqBad;

            raise EOPCNotGoodItem.Create( 'Item is not good' );
        end;
    end
    else
        begin
            raise EOPCReadItem.Create( 'Unable to read the item' );
        end;

  result := itemValue;
end;

procedure TOPCDataAccess.SetQueryInterval(const Value: integer);
begin
  FQueryInterval := Value;
end;

procedure TOPCDataAccess.UnAdvise;
begin
    GroupUnadvise(m_group, m_AsyncConnection);
end;

procedure TOPCDataAccess.RemoveItem(opcCaption: string);
    var
        i, index : integer;
        hr : HRESULT;
begin
  index := -1;
  for i := 1 to Count do
  begin
    if( OPCItem[i].Caption = opcCaption )then
    begin
        index := i;
        break;
    end;
  end;

  if( index = -1 )then
  begin
    raise EOPCDataAccess.Create('Item with caption ' + opcCaption + ' not found' );
  end;

  hr := GroupRemoveItem(m_group, OPCItem[index].handle);

  if( FAILED(hr) )then
      raise EOPCDataAccess.Create('Failed to remove item ' + OPCItem[index].Caption);

  m_opcItems.RemoveElement( m_opcItems.Item[ index-1 ] );

end;

procedure TOPCDataAccess.StartServer;
	var
      status : OPCSERVERSTATUS;
      pstatus : POPCSERVERSTATUS;
begin
	pstatus := @status;

  	m_server.GetStatus(pstatus);
end;

end.
