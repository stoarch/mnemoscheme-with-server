unit class_opcItem;

interface

	type
        TOPCValueQuality = ( ovqUncertain, ovqBad, ovqGood );
        
    	TOPCItem = class
            private
              FHandle: cardinal;
              FCaption: string;
              FQuality: TOPCValueQuality;
              FValue: variant;

              procedure SetCaption(const Value: string);
              procedure SetHandle(const Value: cardinal);
              procedure SetQuality(const Value: TOPCValueQuality);
              procedure SetValue(const Value: variant);

            published
            
        	public
            	//--[ methods ]--
                constructor Create( acaption : string );

            	//--[ properties ]--
            	property Caption : string read FCaption write SetCaption;
                property Handle : cardinal read FHandle write SetHandle;
                property Value : variant read FValue write SetValue;
                property Quality : TOPCValueQuality read FQuality write SetQuality;
        end;

implementation

{ TOPCItem }

constructor TOPCItem.Create(acaption: string);
begin
    Caption := acaption;
end;

procedure TOPCItem.SetCaption(const Value: string);
begin
  FCaption := Value;
end;

procedure TOPCItem.SetHandle(const Value: cardinal);
begin
  FHandle := Value;
end;

procedure TOPCItem.SetQuality(const Value: TOPCValueQuality);
begin
  FQuality := Value;
end;

procedure TOPCItem.SetValue(const Value: variant);
begin
  FValue := Value;
end;

end.
