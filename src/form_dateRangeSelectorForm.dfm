object SelectDateRangeForm: TSelectDateRangeForm
  Left = 563
  Top = 306
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1080#1072#1087#1087#1072#1079#1086#1085' '#1076#1072#1090
  ClientHeight = 140
  ClientWidth = 293
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object selectButton: TsButton
    Left = 8
    Top = 93
    Width = 111
    Height = 40
    Caption = #1042#1099#1073#1088#1072#1090#1100
    TabOrder = 0
    OnClick = selectButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object startDateEdit: TsDateEdit
    Left = 8
    Top = 20
    Width = 275
    Height = 21
    AutoSize = False
    EditMask = '!99/99/9999;1; '
    MaxLength = 10
    TabOrder = 1
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1053#1072#1095#1072#1083#1086':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object endDateEdit: TsDateEdit
    Left = 8
    Top = 63
    Width = 275
    Height = 21
    AutoSize = False
    EditMask = '!99/99/9999;1; '
    MaxLength = 10
    TabOrder = 2
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1050#1086#1085#1077#1094':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clWindowText
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object discardButton: TsButton
    Left = 172
    Top = 93
    Width = 111
    Height = 40
    Caption = #1054#1090#1082#1072#1079#1072#1090#1100#1089#1103
    TabOrder = 3
    OnClick = discardButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 475
    Top = 267
  end
end
