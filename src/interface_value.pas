unit interface_value;

interface

	const
		IID_IValue : TGuid = '{C97683EF-57F9-4046-8CCD-ABCC93A44552}';

	type
		IValue	=	interface
			['{1DE5041F-2164-4308-9E8B-7626E75356B8}']

			//--[ methods ]--
			procedure Assign( const value : IValue );

			//--[ property accessors ]--
			function GetDate() 	:	TDateTime;
			function GetValue() 	:	Variant;

			//--[ property setters ]--
			procedure setDate( const date : TDateTime );
			procedure setValue( const value : Variant );

			//--[ properties ]--
			property Date : TDateTime 	read GetDate 	write SetDate;
			property Value: variant 	read GetValue	write SetValue;
		end;

implementation

end.
 