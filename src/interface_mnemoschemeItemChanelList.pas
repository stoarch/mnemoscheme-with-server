unit interface_mnemoschemeItemChanelList;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData
        ;

    const
        IID_IMnemoschemeItemChanelList : TGUID = '{C65F2861-9F7E-4338-8B21-35CC37A7BA69}';
        
    type
        IMnemoschemeItemChanelList = interface
            ['{C65F2861-9F7E-4338-8B21-35CC37A7BA69}']
            
          procedure Add( value : IMnemoschemeItemChanelData );

          procedure Assign( const data : IMnemoschemeItemChanelList);
          procedure Clear();
          function FindById( index : integer ) : IMnemoschemeItemChanelData;

          function GetCount() : integer;
          function getChanelData(index: integer): IMnemoschemeItemChanelData;

          procedure setChanelData(index: integer; const Value: IMnemoschemeItemChanelData);

          property Count : integer read GetCount;
          property Chanels[ index : integer ]:IMnemoschemeItemChanelData read getChanelData write setChanelData;default;
        end;
implementation

end.
 