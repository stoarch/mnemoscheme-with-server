object DateSelectorForm: TDateSelectorForm
  Left = 735
  Top = 278
  BorderStyle = bsDialog
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1076#1083#1103' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 171
  ClientWidth = 281
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  DesignSize = (
    281
    171)
  PixelsPerInch = 96
  TextHeight = 13
  object applyButton: TsButton
    Left = 21
    Top = 124
    Width = 99
    Height = 37
    Anchors = [akLeft, akBottom]
    Caption = #1055#1088#1080#1085#1103#1090#1100
    Default = True
    ModalResult = 1
    TabOrder = 2
    SkinData.SkinSection = 'BUTTON'
  end
  object dateSelector: TsDateEdit
    Left = 10
    Top = 27
    Width = 262
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Color = clWhite
    EditMask = '!99/99/9999;1; '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 10
    ParentFont = False
    TabOrder = 0
    Text = '  .  .    '
    BoundLabel.Active = True
    BoundLabel.Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1077#1085#1100' '#1076#1083#1103' '#1086#1090#1095#1077#1090#1072':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
    SkinData.SkinSection = 'EDIT'
    GlyphMode.Blend = 0
    GlyphMode.Grayed = False
  end
  object discardButton: TsButton
    Left = 169
    Top = 124
    Width = 95
    Height = 37
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = #1054#1090#1082#1072#1079#1072#1090#1100#1089#1103
    ModalResult = 2
    TabOrder = 3
    SkinData.SkinSection = 'BUTTON'
  end
  object tpkTime: TsTimePicker
    Left = 10
    Top = 76
    Width = 125
    Height = 21
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '00:00:00'
    SkinData.SkinSection = 'EDIT'
    BoundLabel.Active = True
    BoundLabel.Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1088#1077#1084#1103':'
    BoundLabel.Indent = 0
    BoundLabel.Font.Charset = DEFAULT_CHARSET
    BoundLabel.Font.Color = clBlack
    BoundLabel.Font.Height = -11
    BoundLabel.Font.Name = 'MS Sans Serif'
    BoundLabel.Font.Style = []
    BoundLabel.Layout = sclTopLeft
    BoundLabel.MaxWidth = 0
    BoundLabel.UseSkinColor = True
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 203
    Top = 229
  end
end
