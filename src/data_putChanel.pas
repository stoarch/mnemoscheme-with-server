unit data_putChanel;

interface
	uses
    	//--[ data ]--
        class_mnemoschemeItemChannelData,
        //--[ interfaces ]--
        interface_channelParams,
        interface_mnemoschemeItemChanelData,
        interface_putChanelData
        ;

//TODO: Introduce list of channel values distinct from channel params
	type
      TPutChanelData = class(TMnemoschemeItemChannelData, IPutChanelData)
            private
              FpressureOPC: string;
              FflowRateOPC: string;
              FtemperatureOPC: string;
              FParams: IChannelParams;
              FheatRateOPC: string;

        	public
              constructor Create();override;
              destructor Destroy();override;

              procedure Assign( const value : IMnemoschemeItemChanelData );override;

              function GetflowRateOPC: string;
              function GetHeatRateOPC: string;
              function GetParams: IChannelParams;
              function GetPressureOPC: string;
              function GetTemperatureOPC: string;

              procedure SetflowRateOPC(const Value: string);
              procedure SetpressureOPC(const Value: string);
              procedure SettemperatureOPC(const Value: string);
              procedure SetParams(const Value: IChannelParams);
              procedure SetheatRateOPC(const Value: string);

              property temperatureOPC : string read GetTemperatureOPC write SettemperatureOPC;
              property pressureOPC : string read GetPressureOPC write SetpressureOPC;
              property flowRateOPC : string read GetflowRateOPC write SetflowRateOPC;
              property heatRateOPC : string read GetHeatRateOPC write SetheatRateOPC;

              property Params : IChannelParams read GetParams write SetParams;

              function ToString() : string;
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils
        , tool_ms_factories, interface_commonFactory, const_put;


{ TPutChanelData }

procedure TPutChanelData.Assign(const value: IMnemoschemeItemChanelData);
    var
      data : IPutChanelData;
begin
    inherited Assign( value );

    data := value as IPutChanelData;

    begin
        FflowRateOPC := data.flowRateOPC;
        FpressureOPC := data.PressureOPC;
        FtemperatureOPC := data.temperatureOPC;
        FheatRateOPC := data.heatRateOPC;

        FParams.Assign( data.Params );
    end;
end;

constructor TPutChanelData.Create;
begin
    inherited;

	FParams := GetPutFactory().CreateInstance( CLSID_PutChanelParams ) as IChannelParams;
end;

destructor TPutChanelData.Destroy;
begin
  inherited;
end;



function TPutChanelData.GetflowRateOPC: string;
begin
    result := FflowRateOPC;
end;

function TPutChanelData.GetHeatRateOPC: string;
begin
    Result := FheatRateOPC;
end;

function TPutChanelData.GetParams: IChannelParams;
begin
    result := FParams;
end;

function TPutChanelData.GetPressureOPC: string;
begin
    result := FpressureOPC;
end;

function TPutChanelData.GetTemperatureOPC: string;
begin
    result := FtemperatureOPC;
end;

procedure TPutChanelData.SetflowRateOPC(const Value: string);
begin
  FflowRateOPC := Value;
end;

procedure TPutChanelData.SetheatRateOPC(const Value: string);
begin
  FheatRateOPC := Value;
end;

procedure TPutChanelData.SetParams(const Value: IChannelParams);
begin
  FParams := Value;
end;

procedure TPutChanelData.SetpressureOPC(const Value: string);
begin
  FpressureOPC := Value;
end;

procedure TPutChanelData.SettemperatureOPC(const Value: string);
begin
  FtemperatureOPC := Value;
end;

function TPutChanelData.ToString: string;
begin
    result := Format( 'Chanel:%s Index:%d', [Caption, Index] );
end;

end.
