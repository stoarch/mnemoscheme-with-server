program ms;
{$DEFINE RELEASE}

//TODO: Commonize mnemoscheme preparers
//TODO: Remove from mnemoscheme preparer data loader
//TODO: Make loader with self preparation (not need to move to another class)

uses
  Forms,
  Dialogs,
  sysUtils,
  ActiveX,
  windows,
  ShellAPI,
  form_main in 'src\form_main.pas' {Mnemoscheme},
  class_channelParams in 'src\class_channelParams.pas',
  class_putView in 'src\class_putView.pas',
  frame_channelView in 'src\frame_channelView.pas' {ChannelViewFrame: TFrame},
  frame_putView in 'src\frame_putView.pas' {PutViewFrame: TFrame},
  frame_channelInfoView in 'src\frame_channelInfoView.pas' {ChannelInfoViewFrame: TFrame},
  frame_putInfoView in 'src\frame_putInfoView.pas' {PutInfoViewFrame: TFrame},
  class_realRange in 'src\class_realRange.pas',
  data_alarms in 'src\data_alarms.pas',
  class_opcItem in 'src\class_opcItem.pas',
  XPFramework in '..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  XPCollections in '..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  form_logo in 'src\form_logo.pas' {LogoForm},
  module_dataMain in 'src\module_dataMain.pas' {MSDataModule: TDataModule},
  list_wellChanel in 'src\list_wellChanel.pas',
  data_put in 'src\data_put.pas',
  data_wellChanel in 'src\data_wellChanel.pas',
  list_put in 'src\list_put.pas',
  AMAdvLed in '..\..\library_new\AmadvLed\AMAdvLed.pas',
  A3nalogGauge in '..\..\library_new\A3Nalog\A3nalogGauge.pas',
  storage_ObjectHashTable in '..\..\library\irlib\storage\hash\storage_ObjectHashTable.pas',
  storage_HashTable in '..\..\library\irlib\storage\hash\storage_HashTable.pas',
  DeCAL in '..\..\library\Collections\decal\DeCAL.pas',
  SuperStream in '..\..\library\Collections\decal\SuperStream.pas',
  DeCALIO in '..\..\library\Collections\decal\DeCALIO.pas',
  mwFixedRecSort in '..\..\library\Collections\decal\mwFixedRecSort.pas',
  storage_iterator in '..\..\library\irlib\storage\iterators\storage_iterator.pas',
  form_mnemoschemeAbout in 'src\form_mnemoschemeAbout.pas' {MnemoschemeAboutForm},
  class_mnemoschemeItemChannelValue in 'src\class_mnemoschemeItemChannelValue.pas',
  const_well in 'src\const_well.pas',
  class_valueMatrix in 'src\class_valueMatrix.pas',
  class_valueList in 'src\class_valueList.pas',
  class_value in 'src\class_value.pas',
  form_graphViewForm in 'src\form_graphViewForm.pas' {GraphViewForm},
  form_dateRangeSelectorForm in 'src\form_dateRangeSelectorForm.pas' {SelectDateRangeForm},
  form_tableView in 'src\form_tableView.pas' {TableViewForm},
  tool_systemLog in 'src\tool_systemLog.pas',
  tool_environment in '..\..\library\irlib\tools\tool_environment.pas',
  interface_uiStatus in 'src\interface_uiStatus.pas',
  const_guids in 'src\const_guids.pas',
  type_tasks in 'src\type_tasks.pas',
  tool_settings in '..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\library\irlib\tools\tool_localSystem.pas',
  JclFileUtils,
  form_monthDaySelector in 'src\form_monthDaySelector.pas' {MonthDaySelectorForm},
  tool_wellSettingsLoader in 'src\tool_wellSettingsLoader.pas',
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm},
  class_creatableObject in '..\..\library\irlib\classes\class_creatableObject.pas',
  interface_value in 'src\interface_value.pas',
  interface_valueList in 'src\interface_valueList.pas',
  interface_valueMatrix in 'src\interface_valueMatrix.pas',
  data_putChanel in 'src\data_putChanel.pas',
  list_putChanel in 'src\list_putChanel.pas',
  const_put in 'src\const_put.pas',
  tool_putSettingsLoader in 'src\tool_putSettingsLoader.pas',
  data_well in 'src\data_well.pas',
  list_well in 'src\list_well.pas',
  class_putParams in 'src\class_putParams.pas',
  class_wellChannelParams in 'src\class_wellChannelParams.pas',
  form_aboutPutScaner in 'src\form_aboutPutScaner.pas' {AboutForm},
  frame_wellView in 'src\frame_wellView.pas' {WellViewFrame: TFrame},
  frame_wellChannelView in 'src\frame_wellChannelView.pas' {WellChannelViewFrame: TFrame},
  class_putChannelDataLoader in 'src\class_putChannelDataLoader.pas',
  class_key in 'src\class_key.pas',
  class_putDataLoader in 'src\class_putDataLoader.pas',
  interface_putDataLoader in 'src\interface_putDataLoader.pas',
  class_wellDataLoader in 'src\class_wellDataLoader.pas',
  interface_wellDataLoader in 'src\interface_wellDataLoader.pas',
  class_universalDataLoader in 'src\class_universalDataLoader.pas',
  class_mnemoschemeItemChannelData in 'src\class_mnemoschemeItemChannelData.pas',
  class_parameterValue in 'src\class_parameterValue.pas',
  interface_parameterValue in 'src\interface_parameterValue.pas',
  classes_chanelParameterLoaders in 'src\classes_chanelParameterLoaders.pas',
  class_channelValueLoader in 'src\class_channelValueLoader.pas',
  interface_channelValueLoader in 'src\interface_channelValueLoader.pas',
  interface_universalDataLoader in 'src\interface_universalDataLoader.pas',
  class_wellCalculator in 'src\class_wellCalculator.pas',
  interface_wellsCalculator in 'src\interface_wellsCalculator.pas',
  control_putMnemoscheme in 'src\control_putMnemoscheme.pas',
  control_WaterIntakeMnemoscheme in 'src\control_WaterIntakeMnemoscheme.pas',
  control_commonMnemoscheme in 'src\control_commonMnemoscheme.pas',
  interface_putChanelData in 'src\interface_putChanelData.pas',
  interface_channelParams in 'src\interface_channelParams.pas',
  interface_realRange in 'src\interface_realRange.pas',
  interface_mnemoschemeItemChannelValue in 'src\interface_mnemoschemeItemChannelValue.pas',
  interface_putData in 'src\interface_putData.pas',
  interface_putChanelList in 'src\interface_putChanelList.pas',
  interface_wellChannelData in 'src\interface_wellChannelData.pas',
  interface_wellChannelParams in 'src\interface_wellChannelParams.pas',
  class_creatableInterfaceList in '..\..\library\irlib\classes\class_creatableInterfaceList.pas',
  interface_commonIterator in '..\..\library\irlib\interfaces\iterators\interface_commonIterator.pas',
  interface_interfaceList in '..\..\library\irlib\interfaces\interface_interfaceList.pas',
  interface_list in '..\..\library\irlib\interfaces\lists\interface_list.pas',
  storage_interfaceList in '..\..\library\irlib\storage\list\storage_interfaceList.pas',
  storage_commonList in '..\..\library\irlib\storage\list\storage_commonList.pas',
  interface_commonFactory in '..\..\library\irlib\interfaces\factory\interface_commonFactory.pas',
  factory_common in '..\..\library\irlib\tools\factories\factory_common.pas',
  storage_classesHashTable in '..\..\library\irlib\storage\hash\storage_classesHashTable.pas',
  storage_interfaceHashTable in '..\..\library\irlib\storage\hash\storage_interfaceHashTable.pas',
  interface_wellList in 'src\interface_wellList.pas',
  interface_wellData in 'src\interface_wellData.pas',
  interface_wellChanelList in 'src\interface_wellChanelList.pas',
  interface_putList in 'src\interface_putList.pas',
  class_uiLogger in 'src\class_uiLogger.pas',
  tool_ms_factories in 'src\tool_ms_factories.pas',
  interface_parameterValueLoader in 'src\interface_parameterValueLoader.pas',
  class_putMnemoschemePreparer in 'src\class_putMnemoschemePreparer.pas',
  interface_mnemoschemePreparer in 'src\interface_mnemoschemePreparer.pas',
  class_wellMnemoschemePreparer in 'src\class_wellMnemoschemePreparer.pas',
  class_mnemoschemePrearer in 'src\class_mnemoschemePrearer.pas',
  interface_mnemoschemeItemData in 'src\interface_mnemoschemeItemData.pas',
  interface_mnemoschemeItemChanelData in 'src\interface_mnemoschemeItemChanelData.pas',
  interface_mnemoschemeItemChanelList in 'src\interface_mnemoschemeItemChanelList.pas',
  class_mnemoschemeItemChanelList in 'src\class_mnemoschemeItemChanelList.pas',
  interface_mnemoschemeItemList in 'src\interface_mnemoschemeItemList.pas',
  class_mnemoschemeItemList in 'src\class_mnemoschemeItemList.pas',
  class_mnemoschemeItemData in 'src\class_mnemoschemeItemData.pas',
  xpAdditions in '..\..\library\irlib\storage\XpCollections\xpAdditions.pas';

{$R *.res}

	const
      PATH_ITEM	=	'path';

      INSTALL_SECTION	=	'install';
	const
      RPC_C_AUTHN_LEVEL_NONE = 1;
      RPC_C_IMP_LEVEL_IMPERSONATE = 3;
      EOAC_NONE = 0;


      procedure RegisterCommonClasses();
        var
            factory : TCommonFactory;
      begin
        factory := TCommonFactory.Create();

        factory.registerClass( CLSID_ChannelValue, TMnemoschemeItemChannelValue );
        factory.registerClass( CLSID_ParameterValue, TParameterValue );
        factory.registerClass( CLSID_RealRange, TRealRange );

        factory.registerClass( CLSID_Value, TValue );
        factory.registerClass( CLSID_ValueList, TValueList );
        factory.registerClass( CLSID_ValueMatrix, TValueMatrix );

        factory.registerClass( CLSID_ChannelValueLoader, TChannelValueLoader );

        factory.registerClass( CLSID_FlowRateValueLoader, TFlowRateValueLoader );
        factory.registerClass( CLSID_HeatRateValueLoader, THeatRateValueLoader );
        factory.registerClass( CLSID_PressureValueLoader, TPressureValueLoader );
        factory.registerClass( CLSID_TemperatureValueLoader, TTemperatureValueLoader );

        factory.registerClass( CLSID_BackwardFlowRateValueLoader, TBackwardFlowRateValueLoader );
        factory.registerClass( CLSID_CurrentFlowRateValueLoader, TCurrentFlowRateValueLoader );

        factory.registerClass( CLSID_MnemoschemeItemData, TMnemoschemeItemData );
        factory.registerClass( CLSID_MnemoschemeItemChanelList, TMnemoschemeItemChanelList );

        Environment.setInstance( CLSID_CommonFactory, factory );
      end;

      procedure RegisterPutClasses();
        var
            factory : TCommonFactory;
      begin
        factory := TCommonFactory.Create();

        factory.registerClass( CLSID_MnemoschemeDataLoader, TPutDataLoader );

        factory.registerClass( CLSID_MnemoschemeItemChanelData, TPutChanelData );
        factory.registerClass( CLSID_MnemoschemeItemChanelList, TPutChanelList );
        factory.registerClass( CLSID_MnemoschemeItemChanelValue, TMnemoschemeItemChannelValue );
        factory.registerClass( CLSID_MnemoschemeItemData, TPutData );

        factory.registerClass( CLSID_PutChanelParams, TChannelParams );

        factory.registerClass( CLSID_PutList, TPutList );

        factory.registerClass( CLSID_PutMnemoschemePreparer, TPutMnemoschemePreparer );

        Environment.setInstance( CLSID_PutFactory, factory );
      end;

      procedure RegisterWellClasses();
        var
            factory : TCommonFactory;
      begin
        factory := TCommonFactory.Create();

        factory.registerClass( CLSID_WellsCalculator, TWellsCalculator );

        factory.registerClass( CLSID_MnemoschemeDataLoader, TWellDataLoader );

        factory.registerClass( CLSID_MnemoschemeItemChanelData, TWellChanelData );
        factory.registerClass( CLSID_MnemoschemeItemData, TWellData );
        factory.registerClass( CLSID_MnemoschemeItemChanelList, TWellChanelList );
        factory.registerClass( CLSID_MnemoschemeItemChanelValue, TMnemoschemeItemChannelValue );

        factory.registerClass( CLSID_WellChanelParams, TWellChannelParams );
        factory.registerClass( CLSID_WellList, TWellList );

        factory.registerClass( CLSID_WellMnemoschemePreparer, TWellMnemoschemePreparer );

        Environment.setInstance( CLSID_WELLSFACTORY, factory );
      end;

      procedure RegisterClasses();
      begin
        RegisterCommonClasses();
        RegisterPutClasses();
        RegisterWellClasses();
      end;


    var
        uiStatus : IUIStatus;
begin
  SetThreadLocale(1049);

  Settings.Open( 'ms.settings' );

  SystemLog.Open( '.\logs\' + ExtractFileName( ParamStr(0) ) + FormatDateTime('yyyymmddhhnnsss', now ) + '.log' );

  uiStatus := TUILogger.Create();
  Environment.setInstance( CLSID_UIStatus, uiStatus );

  RegisterClasses();

  Application.Initialize;

  SystemLog.Write( 'Mnemoscheme starting...' );
  uiStatus.setStatus( '������ ����������...'  );

  try
	uiStatus.setStatus( '�������� ��������...' );
    msDataModule.LoadSettings();

    try

      SystemLog.Write( 'Creating main form...' );
      uiStatus.setStatus( '������ ����������...' );

      Application.Title := 'HEF3 Mnemoscheme';
      Application.CreateForm(TMnemoscheme, Mnemoscheme);
  Mnemoscheme.Show();
      Mnemoscheme.Update();

      Environment.removeInstance( CLSID_UIStatus );
      uiStatus := nil;
    finally
    end;
  except
    on e:exception do
    begin
      ShowMessage( '��������, ��������� ��������� ������.'+#10#13+'���������� � �������������, ����������.' +#10#13+ e.message );
      SystemLog.Write( 'ERROR:' + e.message);
      Halt(1);
    end;
  end;

  try
  	SystemLog.Write('Succesfully started. Running...');
  	Application.Run;

    FreeAndNil( Mnemoscheme );
  except
  	on e:exception do
    	begin
        	ShowMessage( '��������, ��������� ��������� ������.'+#10#13+'���������� � �������������, ����������.' +#10#13+ e.message );
            SystemLog.Write( 'ERROR:' + e.message);
        end;
  end;

  SystemLog.Write( 'Successfully exited' );
end.
