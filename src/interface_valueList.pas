unit interface_valueList;

interface

    uses
        interface_list,
        interface_value
        ;

    const
        IID_IValueList : TGUID = '{c8ce5db4-fc41-4827-ba62-2aef5ff42435}';

    type
        IValueList = interface( IList )
            ['{c8ce5db4-fc41-4827-ba62-2aef5ff42435}']

            //--[ methods ]--
            procedure Add( const value : IValue );
            procedure Assign( const alist : IValueList );

            //--[ property accessors ]--
            function getValue(index: integer): IValue;
            function getTitle() : string;

            procedure SetTitle(const Value: string);

            //--[ properties ]--
            property Title : string read GetTitle write SetTitle;
            property Value[ index : integer ] : IValue read getValue;
        end;

implementation

end.
 