unit class_wellMnemoschemePreparer;

interface
    uses
        //--[ Classes ]--
        class_creatableObject,
        //--[ common ]--
        classes, ComCtrls, controls, spageControl, sysUtils,
        //--[ controls ]--
        control_WaterIntakeMnemoscheme,
        //--[ interfaces ]--
        interface_mnemoschemePreparer,
        interface_wellChannelData,
        interface_wellData,
        interface_wellDataLoader,
        interface_wellList
        ;

    type
        TWellMnemoschemePreparer = class( TCreatableObject, IMnemoschemePreparer )
        private
            m_wells : IWellList;

            FQuerying : boolean;

            FPages : TPageControl;

            FOnQueryDataFinished: TNotifyEvent;
            FOnQueryDataStarted : TNotifyEvent;

            FWaterIntakeMnemoscheme :  TWaterIntakeMnemoscheme;
            FWellLastRecord : integer;

            function ReadWellsSettings: IWellList;
            procedure ReadWellViewSettings(data: IWellData);
            function CreateWellDataLoader: IWellDataLoader;
            function LoadWellRecordsToLast: boolean;
            procedure CreateWaterIntakeMnemoscheme;
            procedure QueryWellData;

            procedure Fire_OnQueryDataFinished;
            procedure Fire_OnQueryDataStarted;
        public
          function Execute() : boolean;
          procedure Update();

          function GetOnQueryDataFinished: TNotifyEvent;
          function GetOnQueryDataStarted: TNotifyEvent;

          procedure SetOnQueryDataFinished(const Value: TNotifyEvent);
          procedure SetOnQueryDataStarted(const Value: TNotifyEvent);

          property OnQueryDataFinished : TNotifyEvent read GetOnQueryDataFinished write SetOnQueryDataFinished;
          property OnQueryDataStarted : TNotifyEvent read GetOnQueryDataStarted write SetOnQueryDataStarted;


          function GetPages: TPageControl;
          procedure SetPages(const Value: TPageControl);
          property Pages : TPageControl read GetPages write SetPages;
        end;

implementation

uses module_dataMain, tool_ms_factories, interface_commonFactory,
  const_guids, const_well;

{ TWellMnemoschemePreparer }

function TWellMnemoschemePreparer.Execute: boolean;
begin
  m_wells := ReadWellsSettings;
  CreateWaterIntakeMnemoscheme();

  FWaterIntakeMnemoscheme.GenerateViews;
  FWaterIntakeMnemoscheme.UpdateSettings;

  QueryWellData();  

  result := true;
end;

function TWellMnemoschemePreparer.GetOnQueryDataFinished: TNotifyEvent;
begin
    result := FOnQueryDataFinished;
end;

function TWellMnemoschemePreparer.GetOnQueryDataStarted: TNotifyEvent;
begin
    result := FOnQueryDataStarted;
end;

function TWellMnemoschemePreparer.GetPages: TPageControl;
begin
    result := FPages;
end;

procedure TWellMnemoschemePreparer.SetOnQueryDataFinished(
  const Value: TNotifyEvent);
begin
    FOnQueryDataFinished := value;
end;

procedure TWellMnemoschemePreparer.SetOnQueryDataStarted(
  const Value: TNotifyEvent);
begin
    FOnQueryDataStarted := Value;
end;

procedure TWellMnemoschemePreparer.SetPages(const Value: TPageControl);
begin
    FPages := value;
end;

procedure TWellMnemoschemePreparer.Update;
begin
    QueryWellData();
end;


function TWellMnemoschemePreparer.ReadWellsSettings() : IWellList;
const
	WELL_CAPTION_ITEM = 'Caption';
    WELL_ACTIVE_ITEM  = 'Active';
    WELL_COUNT_ITEM   = 'Count';

    COMMON_SECTION = 'Mnemoscheme';
var
  i: Integer;
  wellCount : integer;
  data : IWellData;
begin
	try
		msDataModule.wellParams_table.Close();

		result := GetWellFactory().CreateInstance( CLSID_WellList ) as IWellList;

		if( not MsDataModule.well_table.Active )then
		begin
			msDataModule.well_table.Open();
		end;

		wellCount := MSDataModule.well_table.RecordCount;

		for i := 1 to wellCount do
		begin
		  data := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IWellData;

		  result.Add( data );

		  data.Name 	 := msDataModule.well_table.fieldByName('name').AsString;
		  data.Caption := msDataModule.well_table.fieldByName('caption').AsString;
		  data.Active	 := msDataModule.well_table.fieldByName('active').AsBoolean;
		  data.Index 	 := msDataModule.well_table.fieldByName('id').AsInteger;

		  ReadWellViewSettings( data );

		  msDataModule.well_table.Next();
		end;
	 except
		on e:exception do
			raise Exception.Create( 'TPutScanerMainForm.ReadPutSettings->'+#10#13+ e.message );
	 end;
end;


procedure TWellMnemoschemePreparer.ReadWellViewSettings( data : IWellData );
var
  i : Integer;
  chData : IWellChannelData;
begin
    //TODO: Extract methods
	try
		with MSDataModule do
		begin
  		     if( not wellChannel_table.Active )then
			 begin
				wellChannel_table.Open();
			 end;

			 if( not wellChannel_settings_table.Active )then
			 begin
				wellChannel_settings_table.Open();
			 end;

			 if( not wellOPC_params_table.Active )then
			 begin
				wellOPC_params_table.Open();
			 end;

                wellChannel_table.First();

                chData := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemChanelData ) as IWellChannelData;


				data.Clear();
				for i := 1 to wellChannel_table.RecordCount do
				begin

				  chData.caption := wellChannel_table.FieldByName('caption').AsString;
				  chData.active  := wellChannel_table.FieldByName('active').AsBoolean;
				  chData.Index 	:= wellChannel_table.FieldByName( 'id' ).AsInteger;

				  wellChannel_settings_table.First();
				  while not wellChannel_settings_table.Eof do
				  begin
					 case( wellChannel_settings_table.FieldByName('param_kind_id').asInteger )of
						  WELL_FLOWRATE_ID :
								with chData.Params.FlowRateRange do
								begin
									 Min := wellChannel_settings_table.FieldByName('min').AsFloat;
									 Max := wellChannel_settings_table.FieldByName('max').AsFloat;
								end;
						  WELL_BACKWARD_FLOWRATE_ID :
								with chData.Params.BackwardFlowRateRange do
								begin
									 Min := wellChannel_settings_table.FieldByName('min').AsFloat;
									 Max := wellChannel_settings_table.FieldByName('max').AsFloat;
								end;
					 end;//case

					 wellChannel_settings_table.Next;
				  end;//while

				  wellOPC_params_table.First();
				  while not wellOPC_params_table.Eof do
				  begin
					 case wellOPC_params_table.FieldByName('param_kind_id').AsInteger of
						  WELL_FLOWRATE_ID :
								begin
								  chData.flowRateOPC := wellOPC_params_table.FieldByName( 'value' ).AsString;
								end;
						  WELL_BACKWARD_FLOWRATE_ID :
								begin
								  chData.backwardFlowRateOPC := wellOPC_params_table.FieldByName( 'value' ).AsString;
								end;
					 end;

					 wellOPC_params_table.Next();
				  end;

				  data.Add(chData);

				  wellChannel_table.Next();
				end;//for
		end;
	 except
		on e:exception do
			raise Exception.Create('TPutScanerMainForm.ReadPutViewSettings->'+#10#13+e.message);
	 end;
end;

function TWellMnemoschemePreparer.LoadWellRecordsToLast() : boolean;
    var
      loader : IWellDataLoader;
begin
    loader := CreateWellDataLoader();

    loader.LastRecord := FWellLastRecord;
    loader.Wells := m_wells;

    result := loader.Execute();

    FWellLastRecord := loader.LastRecord;
end;

function TWellMnemoschemePreparer.CreateWellDataLoader: IWellDataLoader;
begin
  Result := GetWellFactory().CreateInstance( CLSID_MnemoschemeDataLoader ) as IWellDataLoader;
end;

procedure TWellMnemoschemePreparer.CreateWaterIntakeMnemoscheme;
    var
        tabMs : TsTabSheet;
begin
  FWaterIntakeMnemoscheme := TWaterIntakeMnemoscheme.Create( Pages );

  FWaterIntakeMnemoscheme.WellList :=  m_wells;

   tabMs := TsTabSheet.Create(Pages);
  tabMs.Caption := 'Водозабор';
  tabMs.PageControl := Pages;

  FWaterIntakeMnemoscheme.Parent := tabMs; //TODO: move to mainPages when complete
  FWaterIntakeMnemoscheme.Align := alClient;
end;


procedure TWellMnemoschemePreparer.QueryWellData();
begin
    Fire_OnQueryDataStarted();

    if( FQuerying )then
    	exit;

	FQuerying := true;

    if( LoadWellRecordsToLast() )then
    begin
        FWaterIntakeMnemoscheme.Update;
    end;

    FQuerying := false;

    Fire_OnQueryDataFinished();
end;

procedure TWellMnemoschemePreparer.Fire_OnQueryDataStarted();
begin
    if assigned( FOnQueryDataStarted )then
        FOnQueryDataStarted( self );
end;

procedure TWellMnemoschemePreparer.Fire_OnQueryDataFinished();
begin
    if assigned( FOnQueryDataFinished ) then
        FOnQueryDataFinished( self );
end;



end.
