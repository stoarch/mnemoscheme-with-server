unit interface_mnemoschemeItemChanelData;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChannelValue
        ;

    const
        IID_IMnemoschemeItemChanelData : TGUID = '{A38BA1A9-08D0-498F-9816-CEBBA4F46A86}';

    type
        IMnemoschemeItemChanelData = interface
            ['{A38BA1A9-08D0-498F-9816-CEBBA4F46A86}']
            procedure Assign( const value : IMnemoschemeItemChanelData );

            function GetActive: boolean;
            function GetCaption: string;
            function GetIndex: integer;
            function GetValue() : IMnemoschemeItemChannelValue;

            procedure SetActive(const Value: boolean);
            procedure SetCaption(const Value: string);
            procedure SetIndex(const Value: integer);
            procedure SetValue(const Value: IMnemoschemeItemChannelValue);

            property Active : boolean read GetActive write SetActive;
            property Caption : string read GetCaption write SetCaption;
            property Index : integer read GetIndex write SetIndex;

            function Equals( source : IMnemoschemeItemChanelData ) : boolean;

            property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
        end;

implementation

end.
 