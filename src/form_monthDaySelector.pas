unit form_monthDaySelector;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, ExtCtrls, sPanel, sMonthCalendar,
  sSkinProvider;

type
  TMonthDaySelectorForm = class(TForm)
    sSkinProvider1: TsSkinProvider;
    mclDay: TsMonthCalendar;
    bbtSelect: TsBitBtn;
    bbtDiscard: TsBitBtn;
  private
    function getDate: TDateTime;
    procedure setDate(const Value: TDateTime);
    { Private declarations }
  public
    { Public declarations }
    property Date : TDateTime read getDate write setDate;
  end;

var
  MonthDaySelectorForm: TMonthDaySelectorForm;

implementation

{$R *.dfm}

{ TMonthDaySelectorForm }

function TMonthDaySelectorForm.getDate: TDateTime;
begin
	result := mclDay.CalendarDate;
end;

procedure TMonthDaySelectorForm.setDate(const Value: TDateTime);
begin
	mclDay.CalendarDate := value;
end;

end.
