unit test_scanePutMain;

interface
	uses
        //--[ common ]--
        classes,
        //--[ forms ]--
        form_scanerPutMain,
    	//--[ testing ]--
        GuiTesting, TestFramework
        ;

    type
    	TScanerPutMainFormAccessor = class( TPutScanerMainForm );

    	TScanerPutMainFormTest = class( TGuiTestCase )
        	private
            	m_form : TScanerPutMainFormAccessor;

    			procedure CheckLoadServers;

            protected
            	procedure SetUp();override;

            public
            	procedure TearDown();override;


            published
                procedure TestLoadServerList();
            	procedure TestReceiveSettings();
                procedure TesteSaveOfData();
        end;

implementation

	uses
    	//--[ common ]--
        module_dataMain, sysUtils,
        //--[ data ]--
        data_put, data_putChanel,
        //--[ list ]--
        list_put
        ;

const
    TEMPERATURE_ID = 1;
    PRESSURE_ID = 2;
    FLOWRATE_ID = 3;

{ TScanerPutMainFormTest }

procedure TScanerPutMainFormTest.SetUp;
begin
  inherited;

  m_form := TScanerPutMainFormAccessor.Create(nil);
end;

procedure TScanerPutMainFormTest.TearDown;
begin
	FreeAndNil( m_form );

  inherited;
end;

procedure TScanerPutMainFormTest.TestLoadServerList;
begin
	CheckException( CheckLoadServers, Exception );
end;

procedure TScanerPutMainFormTest.CheckLoadServers();
begin
	m_form.LoadServerList();
end;

procedure TScanerPutMainFormTest.TestReceiveSettings;
	var
    	list : TPutList;
        i,j	 : integer;
begin
	list := m_form.ReadPutSettings();

    with MSDataModule do
    begin
    	put_table.First();

      	for i := 1 to list.count do
      	begin
        	CheckEquals( list[i].Name, put_table.fieldByName('name').AsString );
            CheckEquals( list[i].Caption, put_table.fieldByName('caption').AsString );
            CheckEquals( list[i].Active, put_table.fieldByName('active').AsBoolean );

            chanel_table.First();
            for j := 1 to list[i].Count do
            begin
            	CheckEquals( list[i][j].Caption, chanel_table.FieldByName('caption').asString);
            	CheckEquals( list[i][j].Active, chanel_table.FieldByName('active').asBoolean);

            	chanel_table.Next();
            end;

        	put_table.Next();
      	end;
    end;
end;

procedure TScanerPutMainFormTest.TesteSaveOfData;
	var
    	data : TPutData;
        ch : TPutChanelData;
begin
	data := TPutData.Create();

	data.Name := 'PUT1';
    data.Caption := 'PUT1';

    ch := data.AddNew();
    ch.Caption := 'CH11-1';

    ch.Params.Temperature := 123;
    ch.Params.Pressure := 345;
    ch.Params.FlowRate := 678;

	m_form.SaveCurrentPutData( data );

    with msDataModule do
    begin
    	CheckEquals( data.Name, put_tablename.AsString );
        CheckEquals( data.Caption, put_tableCaption.AsString );

		CheckEquals( ch.Caption, chanel_tablecaption.AsString );

        params_table.First();
        while not params_table.Eof do
        begin
        	CheckEquals( Trunc(Now()), Trunc(params_tabledate_query.AsDateTime) );

        	case( params_tableparam_kind_id.AsInteger )of
            	TEMPERATURE_ID : CheckEquals( ch.params.Temperature, params_tablevalue.AsFloat );
                PRESSURE_ID : CheckEquals( ch.Params.Pressure, params_tablevalue.AsFloat );
                FLOWRATE_ID : CheckEquals( ch.Params.FlowRate, params_tablevalue.AsFloat );
                else
                	Fail( 'Invalid kind id' );
            end;

            params_table.Delete();
        end;
    end;
end;

initialization
	RegisterTest( 'TScanerPutMainForm', TScanerPutMainFormTest.Suite );

end.
