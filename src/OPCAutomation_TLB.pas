unit OPCAutomation_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 23.04.2009 11:34:41 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Documents and Settings\ROOT\��� ���������\TestMS\OPCDAAuto.dll (1)
// LIBID: {28E68F91-8D75-11D1-8DC3-3C302A000000}
// LCID: 0
// Helpfile: 
// HelpString: OPC DA Automation Wrapper 2.02
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// Errors:
//   Error creating palette bitmap of (TOPCGroups) : Server C:\WINDOWS\system32\OPCDAAuto.dll contains no icons
//   Error creating palette bitmap of (TOPCGroup) : Server C:\WINDOWS\system32\OPCDAAuto.dll contains no icons
//   Error creating palette bitmap of (TOPCActivator) : Server C:\WINDOWS\system32\OPCDAAuto.dll contains no icons
//   Error creating palette bitmap of (TOPCServer) : Server C:\WINDOWS\system32\OPCDAAuto.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  OPCAutomationMajorVersion = 1;
  OPCAutomationMinorVersion = 0;

  LIBID_OPCAutomation: TGUID = '{28E68F91-8D75-11D1-8DC3-3C302A000000}';

  IID_IOPCAutoServer: TGUID = '{28E68F92-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCGroups: TGUID = '{28E68F95-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCGroupsEvent: TGUID = '{28E68F9D-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCGroup: TGUID = '{28E68F96-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCGroupEvent: TGUID = '{28E68F97-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCItems: TGUID = '{28E68F98-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCItem: TGUID = '{28E68F99-8D75-11D1-8DC3-3C302A000000}';
  CLASS_OPCGroup: TGUID = '{28E68F9B-8D75-11D1-8DC3-3C302A000000}';
  CLASS_OPCGroups: TGUID = '{28E68F9E-8D75-11D1-8DC3-3C302A000000}';
  IID_OPCBrowser: TGUID = '{28E68F94-8D75-11D1-8DC3-3C302A000000}';
  DIID_DIOPCServerEvent: TGUID = '{28E68F93-8D75-11D1-8DC3-3C302A000000}';
  IID_IOPCActivator: TGUID = '{860A4800-46A4-478B-A776-7F3A019369E3}';
  CLASS_OPCActivator: TGUID = '{860A4801-46A4-478B-A776-7F3A019369E3}';
  CLASS_OPCServer: TGUID = '{28E68F9A-8D75-11D1-8DC3-3C302A000000}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum OPCNamespaceTypes
type
  OPCNamespaceTypes = TOleEnum;
const
  OPCHierarchical = $00000001;
  OPCFlat = $00000002;

// Constants for enum OPCDataSource
type
  OPCDataSource = TOleEnum;
const
  OPCCache = $00000001;
  OPCDevice = $00000002;

// Constants for enum OPCAccessRights
type
  OPCAccessRights = TOleEnum;
const
  OPCReadable = $00000001;
  OPCWritable = $00000002;

// Constants for enum OPCServerState
type
  OPCServerState = TOleEnum;
const
  OPCRunning = $00000001;
  OPCFailed = $00000002;
  OPCNoconfig = $00000003;
  OPCSuspended = $00000004;
  OPCTest = $00000005;
  OPCDisconnected = $00000006;

// Constants for enum OPCErrors
type
  OPCErrors = TOleEnum;
const
  OPCInvalidHandle = $C0040001;
  OPCBadType = $C0040004;
  OPCPublic = $C0040005;
  OPCBadRights = $C0040006;
  OPCUnknownItemID = $C0040007;
  OPCInvalidItemID = $C0040008;
  OPCInvalidFilter = $C0040009;
  OPCUnknownPath = $C004000A;
  OPCRange = $C004000B;
  OPCDuplicateName = $C004000C;
  OPCUnsupportedRate = $0004000D;
  OPCClamp = $0004000E;
  OPCInuse = $0004000F;
  OPCInvalidConfig = $C0040010;
  OPCNotFound = $C0040011;
  OPCInvalidPID = $C0040203;

// Constants for enum OPCQuality
type
  OPCQuality = TOleEnum;
const
  OPCQualityMask = $000000C0;
  OPCQualityBad = $00000000;
  OPCQualityUncertain = $00000040;
  OPCQualityGood = $000000C0;

// Constants for enum OPCQualityStatus
type
  OPCQualityStatus = TOleEnum;
const
  OPCStatusMask = $000000FC;
  OPCStatusConfigError = $00000004;
  OPCStatusNotConnected = $00000008;
  OPCStatusDeviceFailure = $0000000C;
  OPCStatusSensorFailure = $00000010;
  OPCStatusLastKnown = $00000014;
  OPCStatusCommFailure = $00000018;
  OPCStatusOutOfService = $0000001C;
  OPCStatusLastUsable = $00000044;
  OPCStatusSensorCal = $00000050;
  OPCStatusEGUExceeded = $00000054;
  OPCStatusSubNormal = $00000058;
  OPCStatusLocalOverride = $000000D8;

// Constants for enum OPCQualityLimits
type
  OPCQualityLimits = TOleEnum;
const
  OPCLimitMask = $00000003;
  OPCLimitOk = $00000000;
  OPCLimitLow = $00000001;
  OPCLimitHigh = $00000002;
  OPCLimitConst = $00000003;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IOPCAutoServer = interface;
  IOPCAutoServerDisp = dispinterface;
  IOPCGroups = interface;
  IOPCGroupsDisp = dispinterface;
  DIOPCGroupsEvent = dispinterface;
  IOPCGroup = interface;
  IOPCGroupDisp = dispinterface;
  DIOPCGroupEvent = dispinterface;
  OPCItems = interface;
  OPCItemsDisp = dispinterface;
  OPCItem = interface;
  OPCItemDisp = dispinterface;
  OPCBrowser = interface;
  OPCBrowserDisp = dispinterface;
  DIOPCServerEvent = dispinterface;
  IOPCActivator = interface;
  IOPCActivatorDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  OPCGroup = IOPCGroup;
  OPCGroups = IOPCGroups;
  OPCActivator = IOPCActivator;
  OPCServer = IOPCAutoServer;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PPSafeArray1 = ^PSafeArray; {*}
  PPSafeArray2 = ^PSafeArray; {*}
  PPSafeArray3 = ^PSafeArray; {*}
  PPSafeArray4 = ^PSafeArray; {*}


// *********************************************************************//
// Interface: IOPCAutoServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F92-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCAutoServer = interface(IDispatch)
    ['{28E68F92-8D75-11D1-8DC3-3C302A000000}']
    function Get_StartTime: TDateTime; safecall;
    function Get_CurrentTime: TDateTime; safecall;
    function Get_LastUpdateTime: TDateTime; safecall;
    function Get_MajorVersion: Smallint; safecall;
    function Get_MinorVersion: Smallint; safecall;
    function Get_BuildNumber: Smallint; safecall;
    function Get_VendorInfo: WideString; safecall;
    function Get_ServerState: Integer; safecall;
    function Get_ServerName: WideString; safecall;
    function Get_ServerNode: WideString; safecall;
    function Get_ClientName: WideString; safecall;
    procedure Set_ClientName(const ClientName: WideString); safecall;
    function Get_LocaleID: Integer; safecall;
    procedure Set_LocaleID(LocaleID: Integer); safecall;
    function Get_Bandwidth: Integer; safecall;
    function Get_OPCGroups: OPCGroups; safecall;
    function Get_PublicGroupNames: OleVariant; safecall;
    function GetOPCServers(Node: OleVariant): OleVariant; safecall;
    procedure Connect(const ProgID: WideString; Node: OleVariant); safecall;
    procedure Disconnect; safecall;
    function CreateBrowser: OPCBrowser; safecall;
    function GetErrorString(ErrorCode: Integer): WideString; safecall;
    function QueryAvailableLocaleIDs: OleVariant; safecall;
    procedure QueryAvailableProperties(const ItemID: WideString; out Count: Integer; 
                                       out PropertyIDs: PSafeArray; out Descriptions: PSafeArray; 
                                       out DataTypes: PSafeArray); safecall;
    procedure GetItemProperties(const ItemID: WideString; Count: Integer; 
                                var PropertyIDs: PSafeArray; out PropertyValues: PSafeArray; 
                                out Errors: PSafeArray); safecall;
    procedure LookupItemIDs(const ItemID: WideString; Count: Integer; var PropertyIDs: PSafeArray; 
                            out NewItemIDs: PSafeArray; out Errors: PSafeArray); safecall;
    property StartTime: TDateTime read Get_StartTime;
    property CurrentTime: TDateTime read Get_CurrentTime;
    property LastUpdateTime: TDateTime read Get_LastUpdateTime;
    property MajorVersion: Smallint read Get_MajorVersion;
    property MinorVersion: Smallint read Get_MinorVersion;
    property BuildNumber: Smallint read Get_BuildNumber;
    property VendorInfo: WideString read Get_VendorInfo;
    property ServerState: Integer read Get_ServerState;
    property ServerName: WideString read Get_ServerName;
    property ServerNode: WideString read Get_ServerNode;
    property ClientName: WideString read Get_ClientName write Set_ClientName;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property Bandwidth: Integer read Get_Bandwidth;
    property OPCGroups: OPCGroups read Get_OPCGroups;
    property PublicGroupNames: OleVariant read Get_PublicGroupNames;
  end;

// *********************************************************************//
// DispIntf:  IOPCAutoServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F92-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCAutoServerDisp = dispinterface
    ['{28E68F92-8D75-11D1-8DC3-3C302A000000}']
    property StartTime: TDateTime readonly dispid 1610743808;
    property CurrentTime: TDateTime readonly dispid 1610743809;
    property LastUpdateTime: TDateTime readonly dispid 1610743810;
    property MajorVersion: Smallint readonly dispid 1610743811;
    property MinorVersion: Smallint readonly dispid 1610743812;
    property BuildNumber: Smallint readonly dispid 1610743813;
    property VendorInfo: WideString readonly dispid 1610743814;
    property ServerState: Integer readonly dispid 1610743815;
    property ServerName: WideString readonly dispid 1610743816;
    property ServerNode: WideString readonly dispid 1610743817;
    property ClientName: WideString dispid 1610743818;
    property LocaleID: Integer dispid 1610743820;
    property Bandwidth: Integer readonly dispid 1610743822;
    property OPCGroups: OPCGroups readonly dispid 0;
    property PublicGroupNames: OleVariant readonly dispid 1610743824;
    function GetOPCServers(Node: OleVariant): OleVariant; dispid 1610743825;
    procedure Connect(const ProgID: WideString; Node: OleVariant); dispid 1610743826;
    procedure Disconnect; dispid 1610743827;
    function CreateBrowser: OPCBrowser; dispid 1610743828;
    function GetErrorString(ErrorCode: Integer): WideString; dispid 1610743829;
    function QueryAvailableLocaleIDs: OleVariant; dispid 1610743830;
    procedure QueryAvailableProperties(const ItemID: WideString; out Count: Integer; 
                                       out PropertyIDs: {??PSafeArray}OleVariant; 
                                       out Descriptions: {??PSafeArray}OleVariant; 
                                       out DataTypes: {??PSafeArray}OleVariant); dispid 1610743831;
    procedure GetItemProperties(const ItemID: WideString; Count: Integer; 
                                var PropertyIDs: {??PSafeArray}OleVariant; 
                                out PropertyValues: {??PSafeArray}OleVariant; 
                                out Errors: {??PSafeArray}OleVariant); dispid 1610743832;
    procedure LookupItemIDs(const ItemID: WideString; Count: Integer; 
                            var PropertyIDs: {??PSafeArray}OleVariant; 
                            out NewItemIDs: {??PSafeArray}OleVariant; 
                            out Errors: {??PSafeArray}OleVariant); dispid 1610743833;
  end;

// *********************************************************************//
// Interface: IOPCGroups
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F95-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroups = interface(IDispatch)
    ['{28E68F95-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: IOPCAutoServer; safecall;
    function Get_DefaultGroupIsActive: WordBool; safecall;
    procedure Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool); safecall;
    function Get_DefaultGroupUpdateRate: Integer; safecall;
    procedure Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer); safecall;
    function Get_DefaultGroupDeadband: Single; safecall;
    procedure Set_DefaultGroupDeadband(DefaultGroupDeadband: Single); safecall;
    function Get_DefaultGroupLocaleID: Integer; safecall;
    procedure Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer); safecall;
    function Get_DefaultGroupTimeBias: Integer; safecall;
    procedure Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): OPCGroup; safecall;
    function Add(Name: OleVariant): OPCGroup; safecall;
    function GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup; safecall;
    procedure RemoveAll; safecall;
    procedure Remove(ItemSpecifier: OleVariant); safecall;
    function ConnectPublicGroup(const Name: WideString): OPCGroup; safecall;
    procedure RemovePublicGroup(ItemSpecifier: OleVariant); safecall;
    property Parent: IOPCAutoServer read Get_Parent;
    property DefaultGroupIsActive: WordBool read Get_DefaultGroupIsActive write Set_DefaultGroupIsActive;
    property DefaultGroupUpdateRate: Integer read Get_DefaultGroupUpdateRate write Set_DefaultGroupUpdateRate;
    property DefaultGroupDeadband: Single read Get_DefaultGroupDeadband write Set_DefaultGroupDeadband;
    property DefaultGroupLocaleID: Integer read Get_DefaultGroupLocaleID write Set_DefaultGroupLocaleID;
    property DefaultGroupTimeBias: Integer read Get_DefaultGroupTimeBias write Set_DefaultGroupTimeBias;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  IOPCGroupsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F95-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroupsDisp = dispinterface
    ['{28E68F95-8D75-11D1-8DC3-3C302A000000}']
    property Parent: IOPCAutoServer readonly dispid 1610743808;
    property DefaultGroupIsActive: WordBool dispid 1610743809;
    property DefaultGroupUpdateRate: Integer dispid 1610743811;
    property DefaultGroupDeadband: Single dispid 1610743813;
    property DefaultGroupLocaleID: Integer dispid 1610743815;
    property DefaultGroupTimeBias: Integer dispid 1610743817;
    property Count: Integer readonly dispid 1610743819;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): OPCGroup; dispid 0;
    function Add(Name: OleVariant): OPCGroup; dispid 1610743822;
    function GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup; dispid 1610743823;
    procedure RemoveAll; dispid 1610743824;
    procedure Remove(ItemSpecifier: OleVariant); dispid 1610743825;
    function ConnectPublicGroup(const Name: WideString): OPCGroup; dispid 1610743826;
    procedure RemovePublicGroup(ItemSpecifier: OleVariant); dispid 1610743827;
  end;

// *********************************************************************//
// DispIntf:  DIOPCGroupsEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F9D-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCGroupsEvent = dispinterface
    ['{28E68F9D-8D75-11D1-8DC3-3C302A000000}']
    procedure GlobalDataChange(TransactionID: Integer; GroupHandle: Integer; NumItems: Integer; 
                               var ClientHandles: {??PSafeArray}OleVariant; 
                               var ItemValues: {??PSafeArray}OleVariant; 
                               var Qualities: {??PSafeArray}OleVariant; 
                               var TimeStamps: {??PSafeArray}OleVariant); dispid 1;
  end;

// *********************************************************************//
// Interface: IOPCGroup
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F96-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroup = interface(IDispatch)
    ['{28E68F96-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: IOPCAutoServer; safecall;
    function Get_Name: WideString; safecall;
    procedure Set_Name(const Name: WideString); safecall;
    function Get_IsPublic: WordBool; safecall;
    function Get_IsActive: WordBool; safecall;
    procedure Set_IsActive(IsActive: WordBool); safecall;
    function Get_IsSubscribed: WordBool; safecall;
    procedure Set_IsSubscribed(IsSubscribed: WordBool); safecall;
    function Get_ClientHandle: Integer; safecall;
    procedure Set_ClientHandle(ClientHandle: Integer); safecall;
    function Get_ServerHandle: Integer; safecall;
    function Get_LocaleID: Integer; safecall;
    procedure Set_LocaleID(LocaleID: Integer); safecall;
    function Get_TimeBias: Integer; safecall;
    procedure Set_TimeBias(TimeBias: Integer); safecall;
    function Get_DeadBand: Single; safecall;
    procedure Set_DeadBand(DeadBand: Single); safecall;
    function Get_UpdateRate: Integer; safecall;
    procedure Set_UpdateRate(UpdateRate: Integer); safecall;
    function Get_OPCItems: OPCItems; safecall;
    procedure SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                       out Values: PSafeArray; out Errors: PSafeArray; out Qualities: OleVariant; 
                       out TimeStamps: OleVariant); safecall;
    procedure SyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray; 
                        out Errors: PSafeArray); safecall;
    procedure AsyncRead(NumItems: Integer; var ServerHandles: PSafeArray; out Errors: PSafeArray; 
                        TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray; 
                         out Errors: PSafeArray; TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer); safecall;
    procedure AsyncCancel(CancelID: Integer); safecall;
    property Parent: IOPCAutoServer read Get_Parent;
    property Name: WideString read Get_Name write Set_Name;
    property IsPublic: WordBool read Get_IsPublic;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property IsSubscribed: WordBool read Get_IsSubscribed write Set_IsSubscribed;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property ServerHandle: Integer read Get_ServerHandle;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property TimeBias: Integer read Get_TimeBias write Set_TimeBias;
    property DeadBand: Single read Get_DeadBand write Set_DeadBand;
    property UpdateRate: Integer read Get_UpdateRate write Set_UpdateRate;
    property OPCItems: OPCItems read Get_OPCItems;
  end;

// *********************************************************************//
// DispIntf:  IOPCGroupDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F96-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  IOPCGroupDisp = dispinterface
    ['{28E68F96-8D75-11D1-8DC3-3C302A000000}']
    property Parent: IOPCAutoServer readonly dispid 1610743808;
    property Name: WideString dispid 1610743809;
    property IsPublic: WordBool readonly dispid 1610743811;
    property IsActive: WordBool dispid 1610743812;
    property IsSubscribed: WordBool dispid 1610743814;
    property ClientHandle: Integer dispid 1610743816;
    property ServerHandle: Integer readonly dispid 1610743818;
    property LocaleID: Integer dispid 1610743819;
    property TimeBias: Integer dispid 1610743821;
    property DeadBand: Single dispid 1610743823;
    property UpdateRate: Integer dispid 1610743825;
    property OPCItems: OPCItems readonly dispid 0;
    procedure SyncRead(Source: Smallint; NumItems: Integer; 
                       var ServerHandles: {??PSafeArray}OleVariant; 
                       out Values: {??PSafeArray}OleVariant; out Errors: {??PSafeArray}OleVariant; 
                       out Qualities: OleVariant; out TimeStamps: OleVariant); dispid 1610743828;
    procedure SyncWrite(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                        var Values: {??PSafeArray}OleVariant; out Errors: {??PSafeArray}OleVariant); dispid 1610743829;
    procedure AsyncRead(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                        out Errors: {??PSafeArray}OleVariant; TransactionID: Integer; 
                        out CancelID: Integer); dispid 1610743830;
    procedure AsyncWrite(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                         var Values: {??PSafeArray}OleVariant; 
                         out Errors: {??PSafeArray}OleVariant; TransactionID: Integer; 
                         out CancelID: Integer); dispid 1610743831;
    procedure AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer); dispid 1610743832;
    procedure AsyncCancel(CancelID: Integer); dispid 1610743833;
  end;

// *********************************************************************//
// DispIntf:  DIOPCGroupEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F97-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCGroupEvent = dispinterface
    ['{28E68F97-8D75-11D1-8DC3-3C302A000000}']
    procedure DataChange(TransactionID: Integer; NumItems: Integer; 
                         var ClientHandles: {??PSafeArray}OleVariant; 
                         var ItemValues: {??PSafeArray}OleVariant; 
                         var Qualities: {??PSafeArray}OleVariant; 
                         var TimeStamps: {??PSafeArray}OleVariant); dispid 1;
    procedure AsyncReadComplete(TransactionID: Integer; NumItems: Integer; 
                                var ClientHandles: {??PSafeArray}OleVariant; 
                                var ItemValues: {??PSafeArray}OleVariant; 
                                var Qualities: {??PSafeArray}OleVariant; 
                                var TimeStamps: {??PSafeArray}OleVariant; 
                                var Errors: {??PSafeArray}OleVariant); dispid 2;
    procedure AsyncWriteComplete(TransactionID: Integer; NumItems: Integer; 
                                 var ClientHandles: {??PSafeArray}OleVariant; 
                                 var Errors: {??PSafeArray}OleVariant); dispid 3;
    procedure AsyncCancelComplete(CancelID: Integer); dispid 4;
  end;

// *********************************************************************//
// Interface: OPCItems
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F98-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItems = interface(IDispatch)
    ['{28E68F98-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: OPCGroup; safecall;
    function Get_DefaultRequestedDataType: Smallint; safecall;
    procedure Set_DefaultRequestedDataType(DefaultRequestedDataType: Smallint); safecall;
    function Get_DefaultAccessPath: WideString; safecall;
    procedure Set_DefaultAccessPath(const DefaultAccessPath: WideString); safecall;
    function Get_DefaultIsActive: WordBool; safecall;
    procedure Set_DefaultIsActive(DefaultIsActive: WordBool); safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): OPCItem; safecall;
    function GetOPCItem(ServerHandle: Integer): OPCItem; safecall;
    function AddItem(const ItemID: WideString; ClientHandle: Integer): OPCItem; safecall;
    procedure AddItems(NumItems: Integer; var ItemIDs: PSafeArray; var ClientHandles: PSafeArray; 
                       out ServerHandles: PSafeArray; out Errors: PSafeArray; 
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); safecall;
    procedure Remove(NumItems: Integer; var ServerHandles: PSafeArray; out Errors: PSafeArray); safecall;
    procedure Validate(NumItems: Integer; var ItemIDs: PSafeArray; out Errors: PSafeArray; 
                       RequestedDataTypes: OleVariant; AccessPaths: OleVariant); safecall;
    procedure SetActive(NumItems: Integer; var ServerHandles: PSafeArray; ActiveState: WordBool; 
                        out Errors: PSafeArray); safecall;
    procedure SetClientHandles(NumItems: Integer; var ServerHandles: PSafeArray; 
                               var ClientHandles: PSafeArray; out Errors: PSafeArray); safecall;
    procedure SetDataTypes(NumItems: Integer; var ServerHandles: PSafeArray; 
                           var RequestedDataTypes: PSafeArray; out Errors: PSafeArray); safecall;
    property Parent: OPCGroup read Get_Parent;
    property DefaultRequestedDataType: Smallint read Get_DefaultRequestedDataType write Set_DefaultRequestedDataType;
    property DefaultAccessPath: WideString read Get_DefaultAccessPath write Set_DefaultAccessPath;
    property DefaultIsActive: WordBool read Get_DefaultIsActive write Set_DefaultIsActive;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  OPCItemsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F98-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItemsDisp = dispinterface
    ['{28E68F98-8D75-11D1-8DC3-3C302A000000}']
    property Parent: OPCGroup readonly dispid 1610743808;
    property DefaultRequestedDataType: Smallint dispid 1610743809;
    property DefaultAccessPath: WideString dispid 1610743811;
    property DefaultIsActive: WordBool dispid 1610743813;
    property Count: Integer readonly dispid 1610743815;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): OPCItem; dispid 0;
    function GetOPCItem(ServerHandle: Integer): OPCItem; dispid 1610743818;
    function AddItem(const ItemID: WideString; ClientHandle: Integer): OPCItem; dispid 1610743819;
    procedure AddItems(NumItems: Integer; var ItemIDs: {??PSafeArray}OleVariant; 
                       var ClientHandles: {??PSafeArray}OleVariant; 
                       out ServerHandles: {??PSafeArray}OleVariant; 
                       out Errors: {??PSafeArray}OleVariant; RequestedDataTypes: OleVariant; 
                       AccessPaths: OleVariant); dispid 1610743820;
    procedure Remove(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                     out Errors: {??PSafeArray}OleVariant); dispid 1610743821;
    procedure Validate(NumItems: Integer; var ItemIDs: {??PSafeArray}OleVariant; 
                       out Errors: {??PSafeArray}OleVariant; RequestedDataTypes: OleVariant; 
                       AccessPaths: OleVariant); dispid 1610743822;
    procedure SetActive(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                        ActiveState: WordBool; out Errors: {??PSafeArray}OleVariant); dispid 1610743823;
    procedure SetClientHandles(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                               var ClientHandles: {??PSafeArray}OleVariant; 
                               out Errors: {??PSafeArray}OleVariant); dispid 1610743824;
    procedure SetDataTypes(NumItems: Integer; var ServerHandles: {??PSafeArray}OleVariant; 
                           var RequestedDataTypes: {??PSafeArray}OleVariant; 
                           out Errors: {??PSafeArray}OleVariant); dispid 1610743825;
  end;

// *********************************************************************//
// Interface: OPCItem
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F99-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItem = interface(IDispatch)
    ['{28E68F99-8D75-11D1-8DC3-3C302A000000}']
    function Get_Parent: OPCGroup; safecall;
    function Get_ClientHandle: Integer; safecall;
    procedure Set_ClientHandle(ClientHandle: Integer); safecall;
    function Get_ServerHandle: Integer; safecall;
    function Get_AccessPath: WideString; safecall;
    function Get_AccessRights: Integer; safecall;
    function Get_ItemID: WideString; safecall;
    function Get_IsActive: WordBool; safecall;
    procedure Set_IsActive(IsActive: WordBool); safecall;
    function Get_RequestedDataType: Smallint; safecall;
    procedure Set_RequestedDataType(RequestedDataType: Smallint); safecall;
    function Get_Value: OleVariant; safecall;
    function Get_Quality: Integer; safecall;
    function Get_TimeStamp: TDateTime; safecall;
    function Get_CanonicalDataType: Smallint; safecall;
    function Get_EUType: Smallint; safecall;
    function Get_EUInfo: OleVariant; safecall;
    procedure Read(Source: Smallint; out Value: OleVariant; out Quality: OleVariant; 
                   out TimeStamp: OleVariant); safecall;
    procedure Write(Value: OleVariant); safecall;
    property Parent: OPCGroup read Get_Parent;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property ServerHandle: Integer read Get_ServerHandle;
    property AccessPath: WideString read Get_AccessPath;
    property AccessRights: Integer read Get_AccessRights;
    property ItemID: WideString read Get_ItemID;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property RequestedDataType: Smallint read Get_RequestedDataType write Set_RequestedDataType;
    property Value: OleVariant read Get_Value;
    property Quality: Integer read Get_Quality;
    property TimeStamp: TDateTime read Get_TimeStamp;
    property CanonicalDataType: Smallint read Get_CanonicalDataType;
    property EUType: Smallint read Get_EUType;
    property EUInfo: OleVariant read Get_EUInfo;
  end;

// *********************************************************************//
// DispIntf:  OPCItemDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F99-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCItemDisp = dispinterface
    ['{28E68F99-8D75-11D1-8DC3-3C302A000000}']
    property Parent: OPCGroup readonly dispid 1610743808;
    property ClientHandle: Integer dispid 1610743809;
    property ServerHandle: Integer readonly dispid 1610743811;
    property AccessPath: WideString readonly dispid 1610743812;
    property AccessRights: Integer readonly dispid 1610743813;
    property ItemID: WideString readonly dispid 1610743814;
    property IsActive: WordBool dispid 1610743815;
    property RequestedDataType: Smallint dispid 1610743817;
    property Value: OleVariant readonly dispid 0;
    property Quality: Integer readonly dispid 1610743820;
    property TimeStamp: TDateTime readonly dispid 1610743821;
    property CanonicalDataType: Smallint readonly dispid 1610743822;
    property EUType: Smallint readonly dispid 1610743823;
    property EUInfo: OleVariant readonly dispid 1610743824;
    procedure Read(Source: Smallint; out Value: OleVariant; out Quality: OleVariant; 
                   out TimeStamp: OleVariant); dispid 1610743825;
    procedure Write(Value: OleVariant); dispid 1610743826;
  end;

// *********************************************************************//
// Interface: OPCBrowser
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F94-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCBrowser = interface(IDispatch)
    ['{28E68F94-8D75-11D1-8DC3-3C302A000000}']
    function Get_Organization: Integer; safecall;
    function Get_Filter: WideString; safecall;
    procedure Set_Filter(const Filter: WideString); safecall;
    function Get_DataType: Smallint; safecall;
    procedure Set_DataType(DataType: Smallint); safecall;
    function Get_AccessRights: Integer; safecall;
    procedure Set_AccessRights(AccessRights: Integer); safecall;
    function Get_CurrentPosition: WideString; safecall;
    function Get_Count: Integer; safecall;
    function Get__NewEnum: IUnknown; safecall;
    function Item(ItemSpecifier: OleVariant): WideString; safecall;
    procedure ShowBranches; safecall;
    procedure ShowLeafs(Flat: OleVariant); safecall;
    procedure MoveUp; safecall;
    procedure MoveToRoot; safecall;
    procedure MoveDown(const Branch: WideString); safecall;
    procedure MoveTo(var Branches: PSafeArray); safecall;
    function GetItemID(const Leaf: WideString): WideString; safecall;
    function GetAccessPaths(const ItemID: WideString): OleVariant; safecall;
    property Organization: Integer read Get_Organization;
    property Filter: WideString read Get_Filter write Set_Filter;
    property DataType: Smallint read Get_DataType write Set_DataType;
    property AccessRights: Integer read Get_AccessRights write Set_AccessRights;
    property CurrentPosition: WideString read Get_CurrentPosition;
    property Count: Integer read Get_Count;
    property _NewEnum: IUnknown read Get__NewEnum;
  end;

// *********************************************************************//
// DispIntf:  OPCBrowserDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {28E68F94-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  OPCBrowserDisp = dispinterface
    ['{28E68F94-8D75-11D1-8DC3-3C302A000000}']
    property Organization: Integer readonly dispid 1610743808;
    property Filter: WideString dispid 1610743809;
    property DataType: Smallint dispid 1610743811;
    property AccessRights: Integer dispid 1610743813;
    property CurrentPosition: WideString readonly dispid 1610743815;
    property Count: Integer readonly dispid 1610743816;
    property _NewEnum: IUnknown readonly dispid -4;
    function Item(ItemSpecifier: OleVariant): WideString; dispid 1610743818;
    procedure ShowBranches; dispid 1610743819;
    procedure ShowLeafs(Flat: OleVariant); dispid 1610743820;
    procedure MoveUp; dispid 1610743821;
    procedure MoveToRoot; dispid 1610743822;
    procedure MoveDown(const Branch: WideString); dispid 1610743823;
    procedure MoveTo(var Branches: {??PSafeArray}OleVariant); dispid 1610743824;
    function GetItemID(const Leaf: WideString): WideString; dispid 1610743825;
    function GetAccessPaths(const ItemID: WideString): OleVariant; dispid 1610743826;
  end;

// *********************************************************************//
// DispIntf:  DIOPCServerEvent
// Flags:     (4224) NonExtensible Dispatchable
// GUID:      {28E68F93-8D75-11D1-8DC3-3C302A000000}
// *********************************************************************//
  DIOPCServerEvent = dispinterface
    ['{28E68F93-8D75-11D1-8DC3-3C302A000000}']
    procedure ServerShutDown(const Reason: WideString); dispid 1;
  end;

// *********************************************************************//
// Interface: IOPCActivator
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {860A4800-46A4-478B-A776-7F3A019369E3}
// *********************************************************************//
  IOPCActivator = interface(IDispatch)
    ['{860A4800-46A4-478B-A776-7F3A019369E3}']
    function Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IOPCActivatorDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {860A4800-46A4-478B-A776-7F3A019369E3}
// *********************************************************************//
  IOPCActivatorDisp = dispinterface
    ['{860A4800-46A4-478B-A776-7F3A019369E3}']
    function Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer; dispid 1610743808;
  end;

// *********************************************************************//
// The Class CoOPCGroup provides a Create and CreateRemote method to          
// create instances of the default interface IOPCGroup exposed by              
// the CoClass OPCGroup. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOPCGroup = class
    class function Create: IOPCGroup;
    class function CreateRemote(const MachineName: string): IOPCGroup;
  end;

  TOPCGroupDataChange = procedure(ASender: TObject; TransactionID: Integer; NumItems: Integer; 
                                                    var ClientHandles: {??PSafeArray}OleVariant; 
                                                    var ItemValues: {??PSafeArray}OleVariant; 
                                                    var Qualities: {??PSafeArray}OleVariant; 
                                                    var TimeStamps: {??PSafeArray}OleVariant) of object;
  TOPCGroupAsyncReadComplete = procedure(ASender: TObject; TransactionID: Integer; 
                                                           NumItems: Integer; 
                                                           var ClientHandles: {??PSafeArray}OleVariant; 
                                                           var ItemValues: {??PSafeArray}OleVariant; 
                                                           var Qualities: {??PSafeArray}OleVariant; 
                                                           var TimeStamps: {??PSafeArray}OleVariant; 
                                                           var Errors: {??PSafeArray}OleVariant) of object;
  TOPCGroupAsyncWriteComplete = procedure(ASender: TObject; TransactionID: Integer; 
                                                            NumItems: Integer; 
                                                            var ClientHandles: {??PSafeArray}OleVariant; 
                                                            var Errors: {??PSafeArray}OleVariant) of object;
  TOPCGroupAsyncCancelComplete = procedure(ASender: TObject; CancelID: Integer) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOPCGroup
// Help String      : OPC Automation Group
// Default Interface: IOPCGroup
// Def. Intf. DISP? : No
// Event   Interface: DIOPCGroupEvent
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TOPCGroupProperties= class;
{$ENDIF}
  TOPCGroup = class(TOleServer)
  private
    FOnDataChange: TOPCGroupDataChange;
    FOnAsyncReadComplete: TOPCGroupAsyncReadComplete;
    FOnAsyncWriteComplete: TOPCGroupAsyncWriteComplete;
    FOnAsyncCancelComplete: TOPCGroupAsyncCancelComplete;
    FIntf:        IOPCGroup;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TOPCGroupProperties;
    function      GetServerProperties: TOPCGroupProperties;
{$ENDIF}
    function      GetDefaultInterface: IOPCGroup;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function Get_Parent: IOPCAutoServer;
    function Get_Name: WideString;
    procedure Set_Name(const Name: WideString);
    function Get_IsPublic: WordBool;
    function Get_IsActive: WordBool;
    procedure Set_IsActive(IsActive: WordBool);
    function Get_IsSubscribed: WordBool;
    procedure Set_IsSubscribed(IsSubscribed: WordBool);
    function Get_ClientHandle: Integer;
    procedure Set_ClientHandle(ClientHandle: Integer);
    function Get_ServerHandle: Integer;
    function Get_LocaleID: Integer;
    procedure Set_LocaleID(LocaleID: Integer);
    function Get_TimeBias: Integer;
    procedure Set_TimeBias(TimeBias: Integer);
    function Get_DeadBand: Single;
    procedure Set_DeadBand(DeadBand: Single);
    function Get_UpdateRate: Integer;
    procedure Set_UpdateRate(UpdateRate: Integer);
    function Get_OPCItems: OPCItems;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IOPCGroup);
    procedure Disconnect; override;
    procedure SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                       out Values: PSafeArray; out Errors: PSafeArray); overload;
    procedure SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                       out Values: PSafeArray; out Errors: PSafeArray; out Qualities: OleVariant); overload;
    procedure SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                       out Values: PSafeArray; out Errors: PSafeArray; out Qualities: OleVariant; 
                       out TimeStamps: OleVariant); overload;
    procedure SyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray; 
                        out Errors: PSafeArray);
    procedure AsyncRead(NumItems: Integer; var ServerHandles: PSafeArray; out Errors: PSafeArray; 
                        TransactionID: Integer; out CancelID: Integer);
    procedure AsyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; var Values: PSafeArray; 
                         out Errors: PSafeArray; TransactionID: Integer; out CancelID: Integer);
    procedure AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer);
    procedure AsyncCancel(CancelID: Integer);
    property DefaultInterface: IOPCGroup read GetDefaultInterface;
    property Parent: IOPCAutoServer read Get_Parent;
    property IsPublic: WordBool read Get_IsPublic;
    property ServerHandle: Integer read Get_ServerHandle;
    property OPCItems: OPCItems read Get_OPCItems;
    property Name: WideString read Get_Name write Set_Name;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property IsSubscribed: WordBool read Get_IsSubscribed write Set_IsSubscribed;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property TimeBias: Integer read Get_TimeBias write Set_TimeBias;
    property DeadBand: Single read Get_DeadBand write Set_DeadBand;
    property UpdateRate: Integer read Get_UpdateRate write Set_UpdateRate;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TOPCGroupProperties read GetServerProperties;
{$ENDIF}
    property OnDataChange: TOPCGroupDataChange read FOnDataChange write FOnDataChange;
    property OnAsyncReadComplete: TOPCGroupAsyncReadComplete read FOnAsyncReadComplete write FOnAsyncReadComplete;
    property OnAsyncWriteComplete: TOPCGroupAsyncWriteComplete read FOnAsyncWriteComplete write FOnAsyncWriteComplete;
    property OnAsyncCancelComplete: TOPCGroupAsyncCancelComplete read FOnAsyncCancelComplete write FOnAsyncCancelComplete;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TOPCGroup
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TOPCGroupProperties = class(TPersistent)
  private
    FServer:    TOPCGroup;
    function    GetDefaultInterface: IOPCGroup;
    constructor Create(AServer: TOPCGroup);
  protected
    function Get_Parent: IOPCAutoServer;
    function Get_Name: WideString;
    procedure Set_Name(const Name: WideString);
    function Get_IsPublic: WordBool;
    function Get_IsActive: WordBool;
    procedure Set_IsActive(IsActive: WordBool);
    function Get_IsSubscribed: WordBool;
    procedure Set_IsSubscribed(IsSubscribed: WordBool);
    function Get_ClientHandle: Integer;
    procedure Set_ClientHandle(ClientHandle: Integer);
    function Get_ServerHandle: Integer;
    function Get_LocaleID: Integer;
    procedure Set_LocaleID(LocaleID: Integer);
    function Get_TimeBias: Integer;
    procedure Set_TimeBias(TimeBias: Integer);
    function Get_DeadBand: Single;
    procedure Set_DeadBand(DeadBand: Single);
    function Get_UpdateRate: Integer;
    procedure Set_UpdateRate(UpdateRate: Integer);
    function Get_OPCItems: OPCItems;
  public
    property DefaultInterface: IOPCGroup read GetDefaultInterface;
  published
    property Name: WideString read Get_Name write Set_Name;
    property IsActive: WordBool read Get_IsActive write Set_IsActive;
    property IsSubscribed: WordBool read Get_IsSubscribed write Set_IsSubscribed;
    property ClientHandle: Integer read Get_ClientHandle write Set_ClientHandle;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
    property TimeBias: Integer read Get_TimeBias write Set_TimeBias;
    property DeadBand: Single read Get_DeadBand write Set_DeadBand;
    property UpdateRate: Integer read Get_UpdateRate write Set_UpdateRate;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoOPCGroups provides a Create and CreateRemote method to          
// create instances of the default interface IOPCGroups exposed by              
// the CoClass OPCGroups. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOPCGroups = class
    class function Create: IOPCGroups;
    class function CreateRemote(const MachineName: string): IOPCGroups;
  end;

  TOPCGroupsGlobalDataChange = procedure(ASender: TObject; TransactionID: Integer; 
                                                           GroupHandle: Integer; NumItems: Integer; 
                                                           var ClientHandles: {??PSafeArray}OleVariant; 
                                                           var ItemValues: {??PSafeArray}OleVariant; 
                                                           var Qualities: {??PSafeArray}OleVariant; 
                                                           var TimeStamps: {??PSafeArray}OleVariant) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOPCGroups
// Help String      : OPC Automation Groups Collection
// Default Interface: IOPCGroups
// Def. Intf. DISP? : No
// Event   Interface: DIOPCGroupsEvent
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TOPCGroupsProperties= class;
{$ENDIF}
  TOPCGroups = class(TOleServer)
  private
    FOnGlobalDataChange: TOPCGroupsGlobalDataChange;
    FIntf:        IOPCGroups;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TOPCGroupsProperties;
    function      GetServerProperties: TOPCGroupsProperties;
{$ENDIF}
    function      GetDefaultInterface: IOPCGroups;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function Get_Parent: IOPCAutoServer;
    function Get_DefaultGroupIsActive: WordBool;
    procedure Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool);
    function Get_DefaultGroupUpdateRate: Integer;
    procedure Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer);
    function Get_DefaultGroupDeadband: Single;
    procedure Set_DefaultGroupDeadband(DefaultGroupDeadband: Single);
    function Get_DefaultGroupLocaleID: Integer;
    procedure Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer);
    function Get_DefaultGroupTimeBias: Integer;
    procedure Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer);
    function Get_Count: Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IOPCGroups);
    procedure Disconnect; override;
    function Item(ItemSpecifier: OleVariant): OPCGroup;
    function Add: OPCGroup; overload;
    function Add(Name: OleVariant): OPCGroup; overload;
    function GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup;
    procedure RemoveAll;
    procedure Remove(ItemSpecifier: OleVariant);
    function ConnectPublicGroup(const Name: WideString): OPCGroup;
    procedure RemovePublicGroup(ItemSpecifier: OleVariant);
    property DefaultInterface: IOPCGroups read GetDefaultInterface;
    property Parent: IOPCAutoServer read Get_Parent;
    property Count: Integer read Get_Count;
    property DefaultGroupIsActive: WordBool read Get_DefaultGroupIsActive write Set_DefaultGroupIsActive;
    property DefaultGroupUpdateRate: Integer read Get_DefaultGroupUpdateRate write Set_DefaultGroupUpdateRate;
    property DefaultGroupDeadband: Single read Get_DefaultGroupDeadband write Set_DefaultGroupDeadband;
    property DefaultGroupLocaleID: Integer read Get_DefaultGroupLocaleID write Set_DefaultGroupLocaleID;
    property DefaultGroupTimeBias: Integer read Get_DefaultGroupTimeBias write Set_DefaultGroupTimeBias;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TOPCGroupsProperties read GetServerProperties;
{$ENDIF}
    property OnGlobalDataChange: TOPCGroupsGlobalDataChange read FOnGlobalDataChange write FOnGlobalDataChange;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TOPCGroups
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TOPCGroupsProperties = class(TPersistent)
  private
    FServer:    TOPCGroups;
    function    GetDefaultInterface: IOPCGroups;
    constructor Create(AServer: TOPCGroups);
  protected
    function Get_Parent: IOPCAutoServer;
    function Get_DefaultGroupIsActive: WordBool;
    procedure Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool);
    function Get_DefaultGroupUpdateRate: Integer;
    procedure Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer);
    function Get_DefaultGroupDeadband: Single;
    procedure Set_DefaultGroupDeadband(DefaultGroupDeadband: Single);
    function Get_DefaultGroupLocaleID: Integer;
    procedure Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer);
    function Get_DefaultGroupTimeBias: Integer;
    procedure Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer);
    function Get_Count: Integer;
  public
    property DefaultInterface: IOPCGroups read GetDefaultInterface;
  published
    property DefaultGroupIsActive: WordBool read Get_DefaultGroupIsActive write Set_DefaultGroupIsActive;
    property DefaultGroupUpdateRate: Integer read Get_DefaultGroupUpdateRate write Set_DefaultGroupUpdateRate;
    property DefaultGroupDeadband: Single read Get_DefaultGroupDeadband write Set_DefaultGroupDeadband;
    property DefaultGroupLocaleID: Integer read Get_DefaultGroupLocaleID write Set_DefaultGroupLocaleID;
    property DefaultGroupTimeBias: Integer read Get_DefaultGroupTimeBias write Set_DefaultGroupTimeBias;
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoOPCActivator provides a Create and CreateRemote method to          
// create instances of the default interface IOPCActivator exposed by              
// the CoClass OPCActivator. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOPCActivator = class
    class function Create: IOPCActivator;
    class function CreateRemote(const MachineName: string): IOPCActivator;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOPCActivator
// Help String      : OPC Automation Server Activator
// Default Interface: IOPCActivator
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TOPCActivatorProperties= class;
{$ENDIF}
  TOPCActivator = class(TOleServer)
  private
    FIntf:        IOPCActivator;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TOPCActivatorProperties;
    function      GetServerProperties: TOPCActivatorProperties;
{$ENDIF}
    function      GetDefaultInterface: IOPCActivator;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IOPCActivator);
    procedure Disconnect; override;
    function Attach(const Server: IUnknown; const ProgID: WideString): IOPCAutoServer; overload;
    function Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer; overload;
    property DefaultInterface: IOPCActivator read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TOPCActivatorProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TOPCActivator
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TOPCActivatorProperties = class(TPersistent)
  private
    FServer:    TOPCActivator;
    function    GetDefaultInterface: IOPCActivator;
    constructor Create(AServer: TOPCActivator);
  protected
  public
    property DefaultInterface: IOPCActivator read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoOPCServer provides a Create and CreateRemote method to          
// create instances of the default interface IOPCAutoServer exposed by              
// the CoClass OPCServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoOPCServer = class
    class function Create: IOPCAutoServer;
    class function CreateRemote(const MachineName: string): IOPCAutoServer;
  end;

  TOPCServerServerShutDown = procedure(ASender: TObject; const Reason: WideString) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TOPCServer
// Help String      : OPC Automation Server
// Default Interface: IOPCAutoServer
// Def. Intf. DISP? : No
// Event   Interface: DIOPCServerEvent
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TOPCServerProperties= class;
{$ENDIF}
  TOPCServer = class(TOleServer)
  private
    FOnServerShutDown: TOPCServerServerShutDown;
    FIntf:        IOPCAutoServer;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TOPCServerProperties;
    function      GetServerProperties: TOPCServerProperties;
{$ENDIF}
    function      GetDefaultInterface: IOPCAutoServer;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function Get_StartTime: TDateTime;
    function Get_CurrentTime: TDateTime;
    function Get_LastUpdateTime: TDateTime;
    function Get_MajorVersion: Smallint;
    function Get_MinorVersion: Smallint;
    function Get_BuildNumber: Smallint;
    function Get_VendorInfo: WideString;
    function Get_ServerState: Integer;
    function Get_ServerName: WideString;
    function Get_ServerNode: WideString;
    function Get_ClientName: WideString;
    procedure Set_ClientName(const ClientName: WideString);
    function Get_LocaleID: Integer;
    procedure Set_LocaleID(LocaleID: Integer);
    function Get_Bandwidth: Integer;
    function Get_OPCGroups: OPCGroups;
    function Get_PublicGroupNames: OleVariant;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IOPCAutoServer);
    procedure Disconnect; override;
    function GetOPCServers: OleVariant; overload;
    function GetOPCServers(Node: OleVariant): OleVariant; overload;
    procedure Connect1(const ProgID: WideString); overload;
    procedure Connect1(const ProgID: WideString; Node: OleVariant); overload;
    procedure Disconnect1;
    function CreateBrowser: OPCBrowser;
    function GetErrorString(ErrorCode: Integer): WideString;
    function QueryAvailableLocaleIDs: OleVariant;
    procedure QueryAvailableProperties(const ItemID: WideString; out Count: Integer; 
                                       out PropertyIDs: PSafeArray; out Descriptions: PSafeArray; 
                                       out DataTypes: PSafeArray);
    procedure GetItemProperties(const ItemID: WideString; Count: Integer; 
                                var PropertyIDs: PSafeArray; out PropertyValues: PSafeArray; 
                                out Errors: PSafeArray);
    procedure LookupItemIDs(const ItemID: WideString; Count: Integer; var PropertyIDs: PSafeArray; 
                            out NewItemIDs: PSafeArray; out Errors: PSafeArray);
    property DefaultInterface: IOPCAutoServer read GetDefaultInterface;
    property StartTime: TDateTime read Get_StartTime;
    property CurrentTime: TDateTime read Get_CurrentTime;
    property LastUpdateTime: TDateTime read Get_LastUpdateTime;
    property MajorVersion: Smallint read Get_MajorVersion;
    property MinorVersion: Smallint read Get_MinorVersion;
    property BuildNumber: Smallint read Get_BuildNumber;
    property VendorInfo: WideString read Get_VendorInfo;
    property ServerState: Integer read Get_ServerState;
    property ServerName: WideString read Get_ServerName;
    property ServerNode: WideString read Get_ServerNode;
    property Bandwidth: Integer read Get_Bandwidth;
    property OPCGroups: OPCGroups read Get_OPCGroups;
    property PublicGroupNames: OleVariant read Get_PublicGroupNames;
    property ClientName: WideString read Get_ClientName write Set_ClientName;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TOPCServerProperties read GetServerProperties;
{$ENDIF}
    property OnServerShutDown: TOPCServerServerShutDown read FOnServerShutDown write FOnServerShutDown;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TOPCServer
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TOPCServerProperties = class(TPersistent)
  private
    FServer:    TOPCServer;
    function    GetDefaultInterface: IOPCAutoServer;
    constructor Create(AServer: TOPCServer);
  protected
    function Get_StartTime: TDateTime;
    function Get_CurrentTime: TDateTime;
    function Get_LastUpdateTime: TDateTime;
    function Get_MajorVersion: Smallint;
    function Get_MinorVersion: Smallint;
    function Get_BuildNumber: Smallint;
    function Get_VendorInfo: WideString;
    function Get_ServerState: Integer;
    function Get_ServerName: WideString;
    function Get_ServerNode: WideString;
    function Get_ClientName: WideString;
    procedure Set_ClientName(const ClientName: WideString);
    function Get_LocaleID: Integer;
    procedure Set_LocaleID(LocaleID: Integer);
    function Get_Bandwidth: Integer;
    function Get_OPCGroups: OPCGroups;
    function Get_PublicGroupNames: OleVariant;
  public
    property DefaultInterface: IOPCAutoServer read GetDefaultInterface;
  published
    property ClientName: WideString read Get_ClientName write Set_ClientName;
    property LocaleID: Integer read Get_LocaleID write Set_LocaleID;
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoOPCGroup.Create: IOPCGroup;
begin
  Result := CreateComObject(CLASS_OPCGroup) as IOPCGroup;
end;

class function CoOPCGroup.CreateRemote(const MachineName: string): IOPCGroup;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCGroup) as IOPCGroup;
end;

procedure TOPCGroup.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{28E68F9B-8D75-11D1-8DC3-3C302A000000}';
    IntfIID:   '{28E68F96-8D75-11D1-8DC3-3C302A000000}';
    EventIID:  '{28E68F97-8D75-11D1-8DC3-3C302A000000}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOPCGroup.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as IOPCGroup;
  end;
end;

procedure TOPCGroup.ConnectTo(svrIntf: IOPCGroup);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TOPCGroup.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TOPCGroup.GetDefaultInterface: IOPCGroup;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TOPCGroup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TOPCGroupProperties.Create(Self);
{$ENDIF}
end;

destructor TOPCGroup.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TOPCGroup.GetServerProperties: TOPCGroupProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TOPCGroup.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    1: if Assigned(FOnDataChange) then
         FOnDataChange(Self,
                       Params[0] {Integer},
                       Params[1] {Integer},
                       {??PSafeArray}OleVariant((TVarData(Params[2]).VPointer)^) {var  ??PSafeArray OleVariant},
                       {??PSafeArray}OleVariant((TVarData(Params[3]).VPointer)^) {var  ??PSafeArray OleVariant},
                       {??PSafeArray}OleVariant((TVarData(Params[4]).VPointer)^) {var  ??PSafeArray OleVariant},
                       {??PSafeArray}OleVariant((TVarData(Params[5]).VPointer)^) {var  ??PSafeArray OleVariant});
    2: if Assigned(FOnAsyncReadComplete) then
         FOnAsyncReadComplete(Self,
                              Params[0] {Integer},
                              Params[1] {Integer},
                              {??PSafeArray}OleVariant((TVarData(Params[2]).VPointer)^) {var  ??PSafeArray OleVariant},
                              {??PSafeArray}OleVariant((TVarData(Params[3]).VPointer)^) {var  ??PSafeArray OleVariant},
                              {??PSafeArray}OleVariant((TVarData(Params[4]).VPointer)^) {var  ??PSafeArray OleVariant},
                              {??PSafeArray}OleVariant((TVarData(Params[5]).VPointer)^) {var  ??PSafeArray OleVariant},
                              {??PSafeArray}OleVariant((TVarData(Params[6]).VPointer)^) {var  ??PSafeArray OleVariant});
    3: if Assigned(FOnAsyncWriteComplete) then
         FOnAsyncWriteComplete(Self,
                               Params[0] {Integer},
                               Params[1] {Integer},
                               {??PSafeArray}OleVariant((TVarData(Params[2]).VPointer)^) {var  ??PSafeArray OleVariant},
                               {??PSafeArray}OleVariant((TVarData(Params[3]).VPointer)^) {var  ??PSafeArray OleVariant});
    4: if Assigned(FOnAsyncCancelComplete) then
         FOnAsyncCancelComplete(Self, Params[0] {Integer});
  end; {case DispID}
end;

function TOPCGroup.Get_Parent: IOPCAutoServer;
begin
    Result := DefaultInterface.Parent;
end;

function TOPCGroup.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TOPCGroup.Set_Name(const Name: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := Name;
end;

function TOPCGroup.Get_IsPublic: WordBool;
begin
    Result := DefaultInterface.IsPublic;
end;

function TOPCGroup.Get_IsActive: WordBool;
begin
    Result := DefaultInterface.IsActive;
end;

procedure TOPCGroup.Set_IsActive(IsActive: WordBool);
begin
  DefaultInterface.Set_IsActive(IsActive);
end;

function TOPCGroup.Get_IsSubscribed: WordBool;
begin
    Result := DefaultInterface.IsSubscribed;
end;

procedure TOPCGroup.Set_IsSubscribed(IsSubscribed: WordBool);
begin
  DefaultInterface.Set_IsSubscribed(IsSubscribed);
end;

function TOPCGroup.Get_ClientHandle: Integer;
begin
    Result := DefaultInterface.ClientHandle;
end;

procedure TOPCGroup.Set_ClientHandle(ClientHandle: Integer);
begin
  DefaultInterface.Set_ClientHandle(ClientHandle);
end;

function TOPCGroup.Get_ServerHandle: Integer;
begin
    Result := DefaultInterface.ServerHandle;
end;

function TOPCGroup.Get_LocaleID: Integer;
begin
    Result := DefaultInterface.LocaleID;
end;

procedure TOPCGroup.Set_LocaleID(LocaleID: Integer);
begin
  DefaultInterface.Set_LocaleID(LocaleID);
end;

function TOPCGroup.Get_TimeBias: Integer;
begin
    Result := DefaultInterface.TimeBias;
end;

procedure TOPCGroup.Set_TimeBias(TimeBias: Integer);
begin
  DefaultInterface.Set_TimeBias(TimeBias);
end;

function TOPCGroup.Get_DeadBand: Single;
begin
    Result := DefaultInterface.DeadBand;
end;

procedure TOPCGroup.Set_DeadBand(DeadBand: Single);
begin
  DefaultInterface.Set_DeadBand(DeadBand);
end;

function TOPCGroup.Get_UpdateRate: Integer;
begin
    Result := DefaultInterface.UpdateRate;
end;

procedure TOPCGroup.Set_UpdateRate(UpdateRate: Integer);
begin
  DefaultInterface.Set_UpdateRate(UpdateRate);
end;

function TOPCGroup.Get_OPCItems: OPCItems;
begin
    Result := DefaultInterface.OPCItems;
end;

procedure TOPCGroup.SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                             out Values: PSafeArray; out Errors: PSafeArray);
begin
  DefaultInterface.SyncRead(Source, NumItems, ServerHandles, Values, Errors, EmptyParam, EmptyParam);
end;

procedure TOPCGroup.SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                             out Values: PSafeArray; out Errors: PSafeArray; 
                             out Qualities: OleVariant);
begin
  DefaultInterface.SyncRead(Source, NumItems, ServerHandles, Values, Errors, Qualities, EmptyParam);
end;

procedure TOPCGroup.SyncRead(Source: Smallint; NumItems: Integer; var ServerHandles: PSafeArray; 
                             out Values: PSafeArray; out Errors: PSafeArray; 
                             out Qualities: OleVariant; out TimeStamps: OleVariant);
begin
  DefaultInterface.SyncRead(Source, NumItems, ServerHandles, Values, Errors, Qualities, TimeStamps);
end;

procedure TOPCGroup.SyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; 
                              var Values: PSafeArray; out Errors: PSafeArray);
begin
  DefaultInterface.SyncWrite(NumItems, ServerHandles, Values, Errors);
end;

procedure TOPCGroup.AsyncRead(NumItems: Integer; var ServerHandles: PSafeArray; 
                              out Errors: PSafeArray; TransactionID: Integer; out CancelID: Integer);
begin
  DefaultInterface.AsyncRead(NumItems, ServerHandles, Errors, TransactionID, CancelID);
end;

procedure TOPCGroup.AsyncWrite(NumItems: Integer; var ServerHandles: PSafeArray; 
                               var Values: PSafeArray; out Errors: PSafeArray; 
                               TransactionID: Integer; out CancelID: Integer);
begin
  DefaultInterface.AsyncWrite(NumItems, ServerHandles, Values, Errors, TransactionID, CancelID);
end;

procedure TOPCGroup.AsyncRefresh(Source: Smallint; TransactionID: Integer; out CancelID: Integer);
begin
  DefaultInterface.AsyncRefresh(Source, TransactionID, CancelID);
end;

procedure TOPCGroup.AsyncCancel(CancelID: Integer);
begin
  DefaultInterface.AsyncCancel(CancelID);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TOPCGroupProperties.Create(AServer: TOPCGroup);
begin
  inherited Create;
  FServer := AServer;
end;

function TOPCGroupProperties.GetDefaultInterface: IOPCGroup;
begin
  Result := FServer.DefaultInterface;
end;

function TOPCGroupProperties.Get_Parent: IOPCAutoServer;
begin
    Result := DefaultInterface.Parent;
end;

function TOPCGroupProperties.Get_Name: WideString;
begin
    Result := DefaultInterface.Name;
end;

procedure TOPCGroupProperties.Set_Name(const Name: WideString);
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := Name;
end;

function TOPCGroupProperties.Get_IsPublic: WordBool;
begin
    Result := DefaultInterface.IsPublic;
end;

function TOPCGroupProperties.Get_IsActive: WordBool;
begin
    Result := DefaultInterface.IsActive;
end;

procedure TOPCGroupProperties.Set_IsActive(IsActive: WordBool);
begin
  DefaultInterface.Set_IsActive(IsActive);
end;

function TOPCGroupProperties.Get_IsSubscribed: WordBool;
begin
    Result := DefaultInterface.IsSubscribed;
end;

procedure TOPCGroupProperties.Set_IsSubscribed(IsSubscribed: WordBool);
begin
  DefaultInterface.Set_IsSubscribed(IsSubscribed);
end;

function TOPCGroupProperties.Get_ClientHandle: Integer;
begin
    Result := DefaultInterface.ClientHandle;
end;

procedure TOPCGroupProperties.Set_ClientHandle(ClientHandle: Integer);
begin
  DefaultInterface.Set_ClientHandle(ClientHandle);
end;

function TOPCGroupProperties.Get_ServerHandle: Integer;
begin
    Result := DefaultInterface.ServerHandle;
end;

function TOPCGroupProperties.Get_LocaleID: Integer;
begin
    Result := DefaultInterface.LocaleID;
end;

procedure TOPCGroupProperties.Set_LocaleID(LocaleID: Integer);
begin
  DefaultInterface.Set_LocaleID(LocaleID);
end;

function TOPCGroupProperties.Get_TimeBias: Integer;
begin
    Result := DefaultInterface.TimeBias;
end;

procedure TOPCGroupProperties.Set_TimeBias(TimeBias: Integer);
begin
  DefaultInterface.Set_TimeBias(TimeBias);
end;

function TOPCGroupProperties.Get_DeadBand: Single;
begin
    Result := DefaultInterface.DeadBand;
end;

procedure TOPCGroupProperties.Set_DeadBand(DeadBand: Single);
begin
  DefaultInterface.Set_DeadBand(DeadBand);
end;

function TOPCGroupProperties.Get_UpdateRate: Integer;
begin
    Result := DefaultInterface.UpdateRate;
end;

procedure TOPCGroupProperties.Set_UpdateRate(UpdateRate: Integer);
begin
  DefaultInterface.Set_UpdateRate(UpdateRate);
end;

function TOPCGroupProperties.Get_OPCItems: OPCItems;
begin
    Result := DefaultInterface.OPCItems;
end;

{$ENDIF}

class function CoOPCGroups.Create: IOPCGroups;
begin
  Result := CreateComObject(CLASS_OPCGroups) as IOPCGroups;
end;

class function CoOPCGroups.CreateRemote(const MachineName: string): IOPCGroups;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCGroups) as IOPCGroups;
end;

procedure TOPCGroups.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{28E68F9E-8D75-11D1-8DC3-3C302A000000}';
    IntfIID:   '{28E68F95-8D75-11D1-8DC3-3C302A000000}';
    EventIID:  '{28E68F9D-8D75-11D1-8DC3-3C302A000000}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOPCGroups.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as IOPCGroups;
  end;
end;

procedure TOPCGroups.ConnectTo(svrIntf: IOPCGroups);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TOPCGroups.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TOPCGroups.GetDefaultInterface: IOPCGroups;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TOPCGroups.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TOPCGroupsProperties.Create(Self);
{$ENDIF}
end;

destructor TOPCGroups.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TOPCGroups.GetServerProperties: TOPCGroupsProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TOPCGroups.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    1: if Assigned(FOnGlobalDataChange) then
         FOnGlobalDataChange(Self,
                             Params[0] {Integer},
                             Params[1] {Integer},
                             Params[2] {Integer},
                             {??PSafeArray}OleVariant((TVarData(Params[3]).VPointer)^) {var  ??PSafeArray OleVariant},
                             {??PSafeArray}OleVariant((TVarData(Params[4]).VPointer)^) {var  ??PSafeArray OleVariant},
                             {??PSafeArray}OleVariant((TVarData(Params[5]).VPointer)^) {var  ??PSafeArray OleVariant},
                             {??PSafeArray}OleVariant((TVarData(Params[6]).VPointer)^) {var  ??PSafeArray OleVariant});
  end; {case DispID}
end;

function TOPCGroups.Get_Parent: IOPCAutoServer;
begin
    Result := DefaultInterface.Parent;
end;

function TOPCGroups.Get_DefaultGroupIsActive: WordBool;
begin
    Result := DefaultInterface.DefaultGroupIsActive;
end;

procedure TOPCGroups.Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool);
begin
  DefaultInterface.Set_DefaultGroupIsActive(DefaultGroupIsActive);
end;

function TOPCGroups.Get_DefaultGroupUpdateRate: Integer;
begin
    Result := DefaultInterface.DefaultGroupUpdateRate;
end;

procedure TOPCGroups.Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer);
begin
  DefaultInterface.Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate);
end;

function TOPCGroups.Get_DefaultGroupDeadband: Single;
begin
    Result := DefaultInterface.DefaultGroupDeadband;
end;

procedure TOPCGroups.Set_DefaultGroupDeadband(DefaultGroupDeadband: Single);
begin
  DefaultInterface.Set_DefaultGroupDeadband(DefaultGroupDeadband);
end;

function TOPCGroups.Get_DefaultGroupLocaleID: Integer;
begin
    Result := DefaultInterface.DefaultGroupLocaleID;
end;

procedure TOPCGroups.Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer);
begin
  DefaultInterface.Set_DefaultGroupLocaleID(DefaultGroupLocaleID);
end;

function TOPCGroups.Get_DefaultGroupTimeBias: Integer;
begin
    Result := DefaultInterface.DefaultGroupTimeBias;
end;

procedure TOPCGroups.Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer);
begin
  DefaultInterface.Set_DefaultGroupTimeBias(DefaultGroupTimeBias);
end;

function TOPCGroups.Get_Count: Integer;
begin
    Result := DefaultInterface.Count;
end;

function TOPCGroups.Item(ItemSpecifier: OleVariant): OPCGroup;
begin
  Result := DefaultInterface.Item(ItemSpecifier);
end;

function TOPCGroups.Add: OPCGroup;
begin
  Result := DefaultInterface.Add(EmptyParam);
end;

function TOPCGroups.Add(Name: OleVariant): OPCGroup;
begin
  Result := DefaultInterface.Add(Name);
end;

function TOPCGroups.GetOPCGroup(ItemSpecifier: OleVariant): OPCGroup;
begin
  Result := DefaultInterface.GetOPCGroup(ItemSpecifier);
end;

procedure TOPCGroups.RemoveAll;
begin
  DefaultInterface.RemoveAll;
end;

procedure TOPCGroups.Remove(ItemSpecifier: OleVariant);
begin
  DefaultInterface.Remove(ItemSpecifier);
end;

function TOPCGroups.ConnectPublicGroup(const Name: WideString): OPCGroup;
begin
  Result := DefaultInterface.ConnectPublicGroup(Name);
end;

procedure TOPCGroups.RemovePublicGroup(ItemSpecifier: OleVariant);
begin
  DefaultInterface.RemovePublicGroup(ItemSpecifier);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TOPCGroupsProperties.Create(AServer: TOPCGroups);
begin
  inherited Create;
  FServer := AServer;
end;

function TOPCGroupsProperties.GetDefaultInterface: IOPCGroups;
begin
  Result := FServer.DefaultInterface;
end;

function TOPCGroupsProperties.Get_Parent: IOPCAutoServer;
begin
    Result := DefaultInterface.Parent;
end;

function TOPCGroupsProperties.Get_DefaultGroupIsActive: WordBool;
begin
    Result := DefaultInterface.DefaultGroupIsActive;
end;

procedure TOPCGroupsProperties.Set_DefaultGroupIsActive(DefaultGroupIsActive: WordBool);
begin
  DefaultInterface.Set_DefaultGroupIsActive(DefaultGroupIsActive);
end;

function TOPCGroupsProperties.Get_DefaultGroupUpdateRate: Integer;
begin
    Result := DefaultInterface.DefaultGroupUpdateRate;
end;

procedure TOPCGroupsProperties.Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate: Integer);
begin
  DefaultInterface.Set_DefaultGroupUpdateRate(DefaultGroupUpdateRate);
end;

function TOPCGroupsProperties.Get_DefaultGroupDeadband: Single;
begin
    Result := DefaultInterface.DefaultGroupDeadband;
end;

procedure TOPCGroupsProperties.Set_DefaultGroupDeadband(DefaultGroupDeadband: Single);
begin
  DefaultInterface.Set_DefaultGroupDeadband(DefaultGroupDeadband);
end;

function TOPCGroupsProperties.Get_DefaultGroupLocaleID: Integer;
begin
    Result := DefaultInterface.DefaultGroupLocaleID;
end;

procedure TOPCGroupsProperties.Set_DefaultGroupLocaleID(DefaultGroupLocaleID: Integer);
begin
  DefaultInterface.Set_DefaultGroupLocaleID(DefaultGroupLocaleID);
end;

function TOPCGroupsProperties.Get_DefaultGroupTimeBias: Integer;
begin
    Result := DefaultInterface.DefaultGroupTimeBias;
end;

procedure TOPCGroupsProperties.Set_DefaultGroupTimeBias(DefaultGroupTimeBias: Integer);
begin
  DefaultInterface.Set_DefaultGroupTimeBias(DefaultGroupTimeBias);
end;

function TOPCGroupsProperties.Get_Count: Integer;
begin
    Result := DefaultInterface.Count;
end;

{$ENDIF}

class function CoOPCActivator.Create: IOPCActivator;
begin
  Result := CreateComObject(CLASS_OPCActivator) as IOPCActivator;
end;

class function CoOPCActivator.CreateRemote(const MachineName: string): IOPCActivator;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCActivator) as IOPCActivator;
end;

procedure TOPCActivator.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{860A4801-46A4-478B-A776-7F3A019369E3}';
    IntfIID:   '{860A4800-46A4-478B-A776-7F3A019369E3}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOPCActivator.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IOPCActivator;
  end;
end;

procedure TOPCActivator.ConnectTo(svrIntf: IOPCActivator);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TOPCActivator.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TOPCActivator.GetDefaultInterface: IOPCActivator;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TOPCActivator.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TOPCActivatorProperties.Create(Self);
{$ENDIF}
end;

destructor TOPCActivator.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TOPCActivator.GetServerProperties: TOPCActivatorProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TOPCActivator.Attach(const Server: IUnknown; const ProgID: WideString): IOPCAutoServer;
begin
  Result := DefaultInterface.Attach(Server, ProgID, EmptyParam);
end;

function TOPCActivator.Attach(const Server: IUnknown; const ProgID: WideString; NodeName: OleVariant): IOPCAutoServer;
begin
  Result := DefaultInterface.Attach(Server, ProgID, NodeName);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TOPCActivatorProperties.Create(AServer: TOPCActivator);
begin
  inherited Create;
  FServer := AServer;
end;

function TOPCActivatorProperties.GetDefaultInterface: IOPCActivator;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

class function CoOPCServer.Create: IOPCAutoServer;
begin
  Result := CreateComObject(CLASS_OPCServer) as IOPCAutoServer;
end;

class function CoOPCServer.CreateRemote(const MachineName: string): IOPCAutoServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_OPCServer) as IOPCAutoServer;
end;

procedure TOPCServer.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{28E68F9A-8D75-11D1-8DC3-3C302A000000}';
    IntfIID:   '{28E68F92-8D75-11D1-8DC3-3C302A000000}';
    EventIID:  '{28E68F93-8D75-11D1-8DC3-3C302A000000}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TOPCServer.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as IOPCAutoServer;
  end;
end;

procedure TOPCServer.ConnectTo(svrIntf: IOPCAutoServer);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TOPCServer.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TOPCServer.GetDefaultInterface: IOPCAutoServer;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TOPCServer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TOPCServerProperties.Create(Self);
{$ENDIF}
end;

destructor TOPCServer.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TOPCServer.GetServerProperties: TOPCServerProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TOPCServer.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    1: if Assigned(FOnServerShutDown) then
         FOnServerShutDown(Self, Params[0] {const WideString});
  end; {case DispID}
end;

function TOPCServer.Get_StartTime: TDateTime;
begin
    Result := DefaultInterface.StartTime;
end;

function TOPCServer.Get_CurrentTime: TDateTime;
begin
    Result := DefaultInterface.CurrentTime;
end;

function TOPCServer.Get_LastUpdateTime: TDateTime;
begin
    Result := DefaultInterface.LastUpdateTime;
end;

function TOPCServer.Get_MajorVersion: Smallint;
begin
    Result := DefaultInterface.MajorVersion;
end;

function TOPCServer.Get_MinorVersion: Smallint;
begin
    Result := DefaultInterface.MinorVersion;
end;

function TOPCServer.Get_BuildNumber: Smallint;
begin
    Result := DefaultInterface.BuildNumber;
end;

function TOPCServer.Get_VendorInfo: WideString;
begin
    Result := DefaultInterface.VendorInfo;
end;

function TOPCServer.Get_ServerState: Integer;
begin
    Result := DefaultInterface.ServerState;
end;

function TOPCServer.Get_ServerName: WideString;
begin
    Result := DefaultInterface.ServerName;
end;

function TOPCServer.Get_ServerNode: WideString;
begin
    Result := DefaultInterface.ServerNode;
end;

function TOPCServer.Get_ClientName: WideString;
begin
    Result := DefaultInterface.ClientName;
end;

procedure TOPCServer.Set_ClientName(const ClientName: WideString);
  { Warning: The property ClientName has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ClientName := ClientName;
end;

function TOPCServer.Get_LocaleID: Integer;
begin
    Result := DefaultInterface.LocaleID;
end;

procedure TOPCServer.Set_LocaleID(LocaleID: Integer);
begin
  DefaultInterface.Set_LocaleID(LocaleID);
end;

function TOPCServer.Get_Bandwidth: Integer;
begin
    Result := DefaultInterface.Bandwidth;
end;

function TOPCServer.Get_OPCGroups: OPCGroups;
begin
    Result := DefaultInterface.OPCGroups;
end;

function TOPCServer.Get_PublicGroupNames: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.PublicGroupNames;
end;

function TOPCServer.GetOPCServers: OleVariant;
begin
  Result := DefaultInterface.GetOPCServers(EmptyParam);
end;

function TOPCServer.GetOPCServers(Node: OleVariant): OleVariant;
begin
  Result := DefaultInterface.GetOPCServers(Node);
end;

procedure TOPCServer.Connect1(const ProgID: WideString);
begin
  DefaultInterface.Connect(ProgID, EmptyParam);
end;

procedure TOPCServer.Connect1(const ProgID: WideString; Node: OleVariant);
begin
  DefaultInterface.Connect(ProgID, Node);
end;

procedure TOPCServer.Disconnect1;
begin
  DefaultInterface.Disconnect;
end;

function TOPCServer.CreateBrowser: OPCBrowser;
begin
  Result := DefaultInterface.CreateBrowser;
end;

function TOPCServer.GetErrorString(ErrorCode: Integer): WideString;
begin
  Result := DefaultInterface.GetErrorString(ErrorCode);
end;

function TOPCServer.QueryAvailableLocaleIDs: OleVariant;
begin
  Result := DefaultInterface.QueryAvailableLocaleIDs;
end;

procedure TOPCServer.QueryAvailableProperties(const ItemID: WideString; out Count: Integer; 
                                              out PropertyIDs: PSafeArray; 
                                              out Descriptions: PSafeArray; 
                                              out DataTypes: PSafeArray);
begin
  DefaultInterface.QueryAvailableProperties(ItemID, Count, PropertyIDs, Descriptions, DataTypes);
end;

procedure TOPCServer.GetItemProperties(const ItemID: WideString; Count: Integer; 
                                       var PropertyIDs: PSafeArray; out PropertyValues: PSafeArray; 
                                       out Errors: PSafeArray);
begin
  DefaultInterface.GetItemProperties(ItemID, Count, PropertyIDs, PropertyValues, Errors);
end;

procedure TOPCServer.LookupItemIDs(const ItemID: WideString; Count: Integer; 
                                   var PropertyIDs: PSafeArray; out NewItemIDs: PSafeArray; 
                                   out Errors: PSafeArray);
begin
  DefaultInterface.LookupItemIDs(ItemID, Count, PropertyIDs, NewItemIDs, Errors);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TOPCServerProperties.Create(AServer: TOPCServer);
begin
  inherited Create;
  FServer := AServer;
end;

function TOPCServerProperties.GetDefaultInterface: IOPCAutoServer;
begin
  Result := FServer.DefaultInterface;
end;

function TOPCServerProperties.Get_StartTime: TDateTime;
begin
    Result := DefaultInterface.StartTime;
end;

function TOPCServerProperties.Get_CurrentTime: TDateTime;
begin
    Result := DefaultInterface.CurrentTime;
end;

function TOPCServerProperties.Get_LastUpdateTime: TDateTime;
begin
    Result := DefaultInterface.LastUpdateTime;
end;

function TOPCServerProperties.Get_MajorVersion: Smallint;
begin
    Result := DefaultInterface.MajorVersion;
end;

function TOPCServerProperties.Get_MinorVersion: Smallint;
begin
    Result := DefaultInterface.MinorVersion;
end;

function TOPCServerProperties.Get_BuildNumber: Smallint;
begin
    Result := DefaultInterface.BuildNumber;
end;

function TOPCServerProperties.Get_VendorInfo: WideString;
begin
    Result := DefaultInterface.VendorInfo;
end;

function TOPCServerProperties.Get_ServerState: Integer;
begin
    Result := DefaultInterface.ServerState;
end;

function TOPCServerProperties.Get_ServerName: WideString;
begin
    Result := DefaultInterface.ServerName;
end;

function TOPCServerProperties.Get_ServerNode: WideString;
begin
    Result := DefaultInterface.ServerNode;
end;

function TOPCServerProperties.Get_ClientName: WideString;
begin
    Result := DefaultInterface.ClientName;
end;

procedure TOPCServerProperties.Set_ClientName(const ClientName: WideString);
  { Warning: The property ClientName has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ClientName := ClientName;
end;

function TOPCServerProperties.Get_LocaleID: Integer;
begin
    Result := DefaultInterface.LocaleID;
end;

procedure TOPCServerProperties.Set_LocaleID(LocaleID: Integer);
begin
  DefaultInterface.Set_LocaleID(LocaleID);
end;

function TOPCServerProperties.Get_Bandwidth: Integer;
begin
    Result := DefaultInterface.Bandwidth;
end;

function TOPCServerProperties.Get_OPCGroups: OPCGroups;
begin
    Result := DefaultInterface.OPCGroups;
end;

function TOPCServerProperties.Get_PublicGroupNames: OleVariant;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.PublicGroupNames;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TOPCGroup, TOPCGroups, TOPCActivator, TOPCServer]);
end;

end.
