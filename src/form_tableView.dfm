object TableViewForm: TTableViewForm
  Left = 484
  Top = 77
  Width = 532
  Height = 563
  Caption = #1058#1072#1073#1083#1080#1094#1072' '#1079#1085#1072#1095#1077#1085#1080#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    524
    535)
  PixelsPerInch = 96
  TextHeight = 13
  object tableLabel: TsLabel
    Left = 3
    Top = 4
    Width = 46
    Height = 13
    Caption = #1058#1072#1073#1083#1080#1094#1072':'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 4276545
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object DBGrid1: TDBGrid
    Left = 5
    Top = 20
    Width = 512
    Height = 465
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object closeButton: TsBitBtn
    Left = 428
    Top = 491
    Width = 91
    Height = 31
    Anchors = [akRight, akBottom]
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 1
    OnClick = closeButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object printButton: TsBitBtn
    Left = 332
    Top = 491
    Width = 91
    Height = 31
    Anchors = [akRight, akBottom]
    Caption = #1055#1077#1095#1072#1090#1100
    TabOrder = 2
    OnClick = printButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object exportButton: TsBitBtn
    Left = 236
    Top = 491
    Width = 91
    Height = 31
    Anchors = [akRight, akBottom]
    Caption = #1069#1082#1089#1087#1086#1088#1090
    TabOrder = 3
    OnClick = exportButtonClick
    SkinData.SkinSection = 'BUTTON'
  end
  object sSkinProvider1: TsSkinProvider
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 384
    Top = 217
  end
  object report: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    OnGetValue = reportGetValue
    Left = 261
    Top = 225
    ReportForm = {17000000}
  end
  object dataSet: TfrDBDataSet
    DataSet = dataTable
    Left = 332
    Top = 262
  end
  object DataSource: TDataSource
    DataSet = dataTable
    Left = 329
    Top = 222
  end
  object dataTable: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    LoadedCompletely = False
    SavedCompletely = False
    FilterOptions = []
    Version = '5.52'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 328
    Top = 185
  end
  object headerDataset: TfrUserDataset
    RangeEnd = reCount
    Left = 254
    Top = 329
  end
  object rowDataset: TfrUserDataset
    RangeEnd = reCount
    Left = 291
    Top = 332
  end
  object dlgSelectExportFile: TsSaveDialog
    DefaultExt = '*.xls'
    Filter = 'Excel file|*.xls'
    InitialDir = 'c:\'
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1076#1083#1103' '#1101#1082#1089#1087#1086#1088#1090#1072
    Left = 252
    Top = 268
  end
end
