unit class_putMnemoschemePreparer;

interface
    uses
        //--[ classes ]--
        class_mnemoschemePrearer,
        //--[ common ]--
        classes, ComCtrls, controls, forms, SysUtils,
        //--[ controls ]--
        control_putMnemoscheme,
        //--[ data ]--
        ZDataset,
        //--[ interfaces ]--
        interface_channelParams,
        interface_mnemoschemePreparer,
        interface_putChanelData,
        interface_putData,
        interface_putList,
        interface_realRange
        ;

    type
        TPutMnemoschemePreparer = class( TMnemoschemePreparer, IMnemoschemePreparer )
        private
          function GetSettingsTable: TZTable;
          procedure FillSettingsRange(range: IRealRange);

          function GetCurrentParamKind: integer;
          procedure FillCurrentSettings( params : IChannelParams );
          procedure LoadSettings(data: IPutChanelData);
          function GetChanelsTable: TZTable;

          procedure FillChannelData(chData: IPutChanelData);
          function MakeChannelData: IPutChanelData;
          procedure LoadChannels(data: IPutData);

          function GetValuesTable: TZTable;

          procedure LoadChannelData(data: IPutData);
          function TryLoadChannelData(data: IPutData): boolean;
          procedure LoadList(list: IPutList);
          function LoadItem: IPutData;

          procedure SetPutMnemoscheme(const Value: TPutMnemoscheme);

          function GetPutMnemoscheme: TPutMnemoscheme;
        private
        protected
          m_putList : IPutList;

          procedure CreateMnemoscheme;override;
          procedure QueryData;override;

          function LoadRecordsToLast: boolean;
          function QueryLastRecord: integer;

          procedure Fire_OnQueryDataStarted;
          procedure Fire_OnQueryDataFinished;

          procedure LogWrite(message: string);

          function MakeData: IPutData;

          function GetDataTable() : TZTable;

          property CurrentParamKind : integer read GetCurrentParamKind;

          property ChanelsTable : TZTable read GetChanelsTable;
          property DataTable : TZTable read GetDataTable;
          property SettingsTable : TZTable read GetSettingsTable;
          property ValuesTable : TZTable read GetValuesTable;

          procedure FillData(data: IPutData);
          function MakeList: IPutList;
          function TryReadSettings: boolean;override;
          function ReadSettings: IPutList;
        public
            function Execute() : boolean;override;

            property PutMnemoscheme : TPutMnemoscheme read GetPutMnemoscheme write SetPutMnemoscheme;

        end;


implementation

    uses
       sPageControl, interface_putDataLoader, const_guids, const_put,
        interface_commonFactory, tool_ms_factories, module_dataMain,
        tool_systemLog, type_tasks;

{ TPutMnemoschemePreparer }

function TPutMnemoschemePreparer.Execute() : Boolean;
begin
  result := inherited Execute();

  PutMnemoscheme.DisplayKind := control_putMnemoscheme.dkFlowRate;
end;

procedure TPutMnemoschemePreparer.QueryData();
	var
        lastRecord : integer;
begin
    Fire_OnQueryDataStarted();

    if( FQuerying )then
    	exit;

	FQuerying := true;

    lastRecord := QueryLastRecord();

    //TODO: Split the ui and db access logic to another classes
    if( LoadRecordsToLast() )then
    begin
        PutMnemoscheme.Update();
    end;

    fLastRecord := lastRecord;

    FQuerying := false;

    Fire_OnQueryDataFinished();
end;

procedure TPutMnemoschemePreparer.Fire_OnQueryDataStarted();
begin
    if assigned( FOnQueryDataStarted ) then
        FOnQueryDataStarted( self );
end;

procedure TPutMnemoschemePreparer.Fire_OnQueryDataFinished();
begin
    if Assigned( FOnQueryDataFinished ) then
        FOnQueryDataFinished( self );
end;


function TPutMnemoschemePreparer.LoadRecordsToLast() : boolean;
    var
        loader : IPutDataLoader2;
begin
    loader := GetPutFactory().CreateInstance( CLSID_MnemoschemeDataLoader ) as IPutDataLoader2;

    loader.LoadActiveOnly := True;
    loader.LastRecord := FLastRecord;
    loader.PutList := m_putList;

    result := loader.Execute();

    FLastRecord := loader.LastRecord;
end;


function TPutMnemoschemePreparer.QueryLastRecord() : integer;
    var
      lastRecord : integer;
    const
        c_sql = 'select max(id) from params';
begin
    lastRecord := -1;

    //TODO: Move to another class: LastRecordRetriever
    with msDataModule do
    begin
        queryData.SQL.Text := c_sql;
        try
            queryData.Open();
            if( queryData.RecordCount = 1 )then
                lastRecord := queryData.Fields[0].Value;
        except
            on e:exception do
                LogWrite('Error:' + e.message);
        end;
    end;

    result := lastRecord;
end;

procedure TPutMnemoschemePreparer.LogWrite( message : string);
begin
	SystemLog.Write( message );
end;

procedure TPutMnemoschemePreparer.CreateMnemoscheme;
    var
        tabPutMs : TsTabSheet;
begin
  PutMnemoscheme := TPutMnemoscheme.Create( Pages );
  PutMnemoscheme.PutList := m_putList;

  tabPutMs := TsTabSheet.Create( Pages );
  tabPutMs.Caption := '������ ����� �����';
  tabPutMs.PageControl := pages;

  PutMnemoscheme.Parent := tabPutMs; 
  PutMnemoscheme.Align := alClient;
end;

function TPutMnemoschemePreparer.TryReadSettings() : boolean;
begin
	try
        m_putList := ReadSettings();

        result := true;
    except
		on e:exception do
        begin
			raise Exception.Create( 'TPutMnemoschemePreparer.ReadPutSettings->'+#10#13+ e.message );
        end;
	end;
end;

function TPutMnemoschemePreparer.ReadSettings(): IPutList;
begin
    result := MakeList();

    LoadList( result );
end;

procedure TPutMnemoschemePreparer.LoadList( list : IPutList );
begin
    ValuesTable.Close();

    if( not DataTable.Active )then
    begin
        DataTable.Open();
    end;

    list.Clear();

    while not DataTable.Eof do
    begin
      list.Add( LoadItem() );

      DataTable.Next();
    end;
end;

function TPutMnemoschemePreparer.LoadItem() : IPutData;
begin
    result := MakeData();

    FillData( result );
end;


function TPutMnemoschemePreparer.MakeList(): IPutList;
begin
    result := GetPutFactory().CreateInstance( CLSID_PutList ) as IPutList;
end;

procedure TPutMnemoschemePreparer.FillData( data : IPutData );
const
    PUT_ACTIVE_ITEM  = 'Active';
	PUT_CAPTION_ITEM = 'Caption';
    PUT_NAME_ITEM = 'Name';
    PUT_ID_ITEM = 'Id';
begin
    data.Name 	 := DataTable.fieldByName( PUT_NAME_ITEM ).AsString;
    data.Caption :=  DataTable.fieldByName( PUT_CAPTION_ITEM ).AsString;
    data.Active	 := DataTable.fieldByName( PUT_ACTIVE_ITEM ).AsBoolean;
    data.Index 	 := DataTable.fieldByName( PUT_ID_ITEM ).AsInteger;

    TryLoadChannelData( data );
end;

function TPutMnemoschemePreparer.MakeData() : IPutData;
begin
    result := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IPutData;
end;

function TPutMnemoschemePreparer.TryLoadChannelData( data : IPutData ) : boolean;
begin
	try
      LoadChannelData( data );

      result := true;
    except
		on e:exception do
        begin
			raise Exception.Create('TPutMnemoschemePreparer.FillViewSettings->'+#10#13+e.message);
        end;
	end;
end;

procedure TPutMnemoschemePreparer.LoadChannelData( data : IPutData );
begin
    data.Clear();

    LoadChannels( data );
end;

procedure TPutMnemoschemePreparer.LoadChannels( data : IPutData );
var
  chData : IPutChanelData;
begin
    if( not ChanelsTable.Active )then
    begin
       ChanelsTable.Open();
    end;

    ChanelsTable.First();
    while not ChanelsTable.Eof do
    begin
      chData := MakeChannelData();

      FillChannelData( chData );

      data.Add( chData );

      ChanelsTable.Next();
    end;//for
end;

function TPutMnemoschemePreparer.MakeChannelData(): IPutChanelData;
begin
    result := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemChanelData ) as IPutChanelData;
end;
                                                                  
procedure TPutMnemoschemePreparer.FillChannelData( chData : IPutChanelData );
begin
    chData.caption := ChanelsTable.FieldByName('caption').AsString;
    chData.active  := ChanelsTable.FieldByName('active').AsBoolean;
    chData.Index 	:= ChanelsTable.FieldByName( 'id' ).AsInteger;

    LoadSettings( chData );
end;

procedure TPutMnemoschemePreparer.LoadSettings( data : IPutChanelData );
begin
  if( not SettingsTable.Active )then
  begin
     SettingsTable.Open();
  end;

  SettingsTable.First();
  while not SettingsTable.Eof do
  begin
     FillCurrentSettings( Data.Params );

     SettingsTable.Next;
  end;//while
end;


procedure TPutMnemoschemePreparer.FillCurrentSettings( params : IChannelParams );
begin
    //TODO: Split case by classes
     case( CurrentParamKind )of
          TEMPERATURE_ID :
                FillSettingsRange( Params.TemperatureRange );

          PRESSURE_ID :
                FillSettingsRange( Params.PressureRange );

          FLOWRATE_ID :
                FillSettingsRange( Params.FlowRateRange );
     end;//case
end;


procedure TPutMnemoschemePreparer.FillSettingsRange( range : IRealRange );
begin
    range.Min := SettingsTable.FieldByName('min').AsFloat;
    range.Max := SettingsTable.FieldByName('max').AsFloat;
end;

function TPutMnemoschemePreparer.GetDataTable: TZTable;
begin
    result := msDataModule.put_table;
end;

function TPutMnemoschemePreparer.GetSettingsTable: TZTable;
begin
    result := msDataModule.chanel_control_settings_table;
end;

function TPutMnemoschemePreparer.GetCurrentParamKind: integer;
begin
    result := SettingsTable.FieldByName('param_kind_id').asInteger;
end;

function TPutMnemoschemePreparer.GetChanelsTable: TZTable;
begin
    Result := msDataModule.chanel_table;
end;

function TPutMnemoschemePreparer.GetValuesTable: TZTable;
begin
    result := msDataModule.params_table;
end;




function TPutMnemoschemePreparer.GetPutMnemoscheme: TPutMnemoscheme;
begin
    result := FMnemoscheme as TPutMnemoscheme;
end;

procedure TPutMnemoschemePreparer.SetPutMnemoscheme(
  const Value: TPutMnemoscheme);
begin
    FMnemoscheme := value;
end;

end.
