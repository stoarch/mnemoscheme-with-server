unit class_putView;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls;

type
  TPutView = class(TPanel)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('IrLib', [TPutView]);
end;

end.
