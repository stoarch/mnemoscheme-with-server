unit interface_wellChannelData;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData,
        interface_wellChannelParams
        ;

    const
        IID_IWellChannelData : TGUID ='{288E2A69-7C4E-4C9B-B072-B297449F2BBC}';

    type
        IWellChannelData = interface( IMnemoschemeItemChanelData )
            ['{288E2A69-7C4E-4C9B-B072-B297449F2BBC}']
              function GetBackwardFlowRateOPC: string;
              function GetCurrentFlowRateOPC: string;
              function GetFlowRateOPC: string;
              function GetParams: IWellChannelParams;

              procedure SetflowRateOPC(const Value: string);
              procedure SetBackwardFlowRateOPC( const Value : string );
              procedure SetParams(const Value: IWellChannelParams);
              procedure SetcurrentFlowRateOPC(const Value: string);

              property flowRateOPC : string read GetFlowRateOPC write SetflowRateOPC;
              property backwardFlowRateOPC : string read GetBackwardFlowRateOPC write SetBackwardFlowRateOPC;
              property currentFlowRateOPC : string read GetCurrentFlowRateOPC write SetcurrentFlowRateOPC;

              property Params : IWellChannelParams read GetParams write SetParams;

              function ToString() : string;
        end;

implementation

end.
 