unit class_channelValue;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        class_parameterValue,
        //--[ interfaces ]--
        interface_mnemoschemeItemChannelValue,
        interface_parameterValue
        ;

	type
    	TChannelValue = class( TCreatableObject, IMnemoschemeItemChannelValue )
            private
              FTemperature: IParameterValue;
              FPressure : IParameterValue;
              FFlowRate: IParameterValue;
              FHeatRate: IParameterValue;
              FBackwardFlowRate : IParameterValue;
              FCurrentFlowRate : IParameterValue;


        	public

                constructor Create();override;
                destructor Destroy();override;

                function GetFlowRate: IParameterValue;
                function GetHeatRate: IParameterValue;
                function GetPressure: IParameterValue;
                function GetTemperature: IParameterValue;

                procedure SetTemperature(const Value: IParameterValue);
                procedure SetPressure(const Value: IParameterValue);
                procedure SetFlowRate(const Value: IParameterValue);
                procedure SetHeatRate(const Value: IParameterValue);
                function GetBackwardFlowRate: IParameterValue;
                function GetCurrentFlowRate: IParameterValue;
                procedure SetBackwardFlowRate(const Value: IParameterValue);
                procedure SetCurrentFlowRate(const Value: IParameterValue);

                property Temperature : IParameterValue read GetTemperature write SetTemperature;
                property Pressure : IParameterValue read GetPressure write SetPressure;
                property FlowRate : IParameterValue read GetFlowRate write SetFlowRate;
                property HeatRate : IParameterValue read GetHeatRate write SetHeatRate;
                property BackwardFlowRate : IParameterValue read GetBackwardFlowRate write SetBackwardFlowRate;
                property CurrentFlowRate : IParameterValue read GetCurrentFlowRate write SetCurrentFlowRate;

            	procedure Assign( const data : IChannelValue );
                procedure Clear();
                function ToString() : string;
        end;

implementation

	uses
    	sysUtils
        , tool_systemLog, tool_ms_factories, interface_commonFactory,
  const_guids;

{ TChannelValue }

procedure TChannelValue.Assign(const data: IChannelValue);
begin
    FTemperature.Assign( data.Temperature );
    FPressure.Assign( data.Pressure );
    FFlowRate.Assign( data.FlowRate );
    FHeatRate.Assign( data.HeatRate );
    FBackwardFlowRate.Assign( data.BackwardFlowRate );
    FCurrentFlowRate.Assign( data.CurrentFlowRate );
end;

procedure TChannelValue.Clear;
begin
    Temperature.Clear();
    Pressure.Clear();
    FlowRate.Clear();
    HeatRate.Clear();
    BackwardFlowRate.Clear();
    CurrentFlowRate.Clear();
end;

constructor TChannelValue.Create;
    var
        factory : ICommonFactory;
begin
    inherited;

    factory := GetCommonFactory();

    FTemperature := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FPressure := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FHeatRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FBackwardFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FCurrentFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
end;

destructor TChannelValue.Destroy;
begin

  inherited;
end;


function TChannelValue.GetBackwardFlowRate: IParameterValue;
begin
    result := FBackwardFlowRate;
end;

function TChannelValue.GetCurrentFlowRate: IParameterValue;
begin
    result := FCurrentFlowRate;
end;

function TChannelValue.GetFlowRate: IParameterValue;
begin
    Result := FFlowRate;
end;

function TChannelValue.GetHeatRate: IParameterValue;
begin
    Result := FHeatRate;
end;

function TChannelValue.GetPressure: IParameterValue;
begin
    result := FPressure;
end;

function TChannelValue.GetTemperature: IParameterValue;
begin
    result := FTemperature;
end;

procedure TChannelValue.SetBackwardFlowRate(const Value: IParameterValue);
begin
    FBackwardFlowRate := value;
end;

procedure TChannelValue.SetCurrentFlowRate(const Value: IParameterValue);
begin
    FCurrentFlowRate := value;
end;

procedure TChannelValue.SetFlowRate(const Value: IParameterValue);
begin
  FFlowRate := Value;
end;

procedure TChannelValue.SetHeatRate(const Value: IParameterValue);
begin
  FHeatRate := Value;
end;

procedure TChannelValue.SetPressure(const Value: IParameterValue);
begin
  FPressure := Value;
end;

procedure TChannelValue.SetTemperature(const Value: IParameterValue);
begin
  FTemperature := Value;
end;

function TChannelValue.toString: string;
begin
	result := '';
end;

end.
