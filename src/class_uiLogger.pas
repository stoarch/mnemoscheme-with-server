unit class_uiLogger;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ interfaces ]--
        interface_uiStatus,
        //--[ Forms ]--
        form_logo,
        //--[ types ]--
        type_tasks
        ;

    type
        TUILogger = class( TCreatableObject, IUIStatus )
        private
            FLogo : TLogoForm;

            procedure CheckLogo;
        public
            constructor Create();override;
            destructor Destroy();override;

            function getSubTaskProgress() : TTaskProgress;
            function getTaskProgress() : TTaskProgress;

            procedure setStatus( const value : string );
            procedure setSubTaskProgress( const value : TTaskProgress );
            procedure setTaskProgress( const value : TTaskProgress );

            procedure stepSubTaskProgress();
            procedure stepTaskProgress();

            property Status : string write setStatus;

            property SubTaskProgress : TTaskProgress read getSubTaskProgress write setSubTaskProgress;
            property TaskProgress : TTaskProgress read getTaskProgress write setTaskProgress;
        end;

implementation

    uses
        //--[ common ]--
        forms, SysUtils
        ;

{ TUILogger }

constructor TUILogger.Create;
begin
  inherited;
end;

destructor TUILogger.Destroy;
begin
  Assert( FRefCount = 0, 'Destroying of usable object' );
  
  if( Assigned( FLogo ))then
    FreeAndNil( FLogo );

  inherited;
end;

function TUILogger.getSubTaskProgress: TTaskProgress;
begin
  CheckLogo();

  result := FLogo.getSubTaskProgress();
end;

procedure TUILogger.CheckLogo();
begin
    if( not Assigned( FLogo ))then
    begin
        FLogo := TLogoForm.Create( Application );
        FLogo.Show();
        FLogo.Update();
    end;
end;

function TUILogger.getTaskProgress: TTaskProgress;
begin
    CheckLogo();

    result := FLogo.getTaskProgress();
end;

procedure TUILogger.setStatus(const value: string);
begin
    CheckLogo();

    FLogo.setStatus( value );
end;

procedure TUILogger.setSubTaskProgress(const value: TTaskProgress);
begin
    CheckLogo();

    FLogo.setSubTaskProgress(value);
end;

procedure TUILogger.setTaskProgress(const value: TTaskProgress);
begin
    CheckLogo();

    FLogo.setTaskProgress( value );
end;

procedure TUILogger.stepSubTaskProgress;
begin
    CheckLogo();

    FLogo.stepSubTaskProgress();
end;

procedure TUILogger.stepTaskProgress;
begin
    CheckLogo();

    FLogo.stepTaskProgress();
end;

end.
