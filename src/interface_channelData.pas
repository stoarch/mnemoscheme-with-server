unit interface_channelData;

interface
    uses
      //--[ interfaces ]--
      interface_channelValue
      ;

const
    IID_IChannelData : TGUID = '{24769645-296C-44FC-921D-2AB613AC12C9}';

type
    IChannelData = interface
        ['{24769645-296C-44FC-921D-2AB613AC12C9}']
        procedure Assign( const value : IChannelData );

        function GetActive: boolean;
        function GetCaption: string;
        function GetIndex: integer;
        function GetValue() : IChannelValue;

        procedure SetActive(const Value: boolean);
        procedure SetCaption(const Value: string);
        procedure SetIndex(const Value: integer);
        procedure SetValue(const Value: IChannelValue);

        property Active : boolean read GetActive write SetActive;
        property Caption : string read GetCaption write SetCaption;
        property Index : integer read GetIndex write SetIndex;

        property Value : IChannelValue read GetValue write SetValue;
    end;

implementation

end.
 