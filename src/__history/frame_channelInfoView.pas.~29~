unit frame_channelInfoView;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, A3nalogGauge, sGroupBox, sFrameAdapter,
  ExtCtrls,
  //--[ classes ]--
  class_channelParams,
  //--[ data ]--
  data_alarms
  ;

type
  TChannelInfoViewFrame = class(TFrame)
    temperatureGroup: TsGroupBox;
    temperatureGauge: TA3nalogGauge;
    temperatureEdit: TsCurrencyEdit;
    pressureGroup: TsGroupBox;
    pressureGauge: TA3nalogGauge;
    pressureEdit: TsCurrencyEdit;
    flowRateGroup: TsGroupBox;
    FlowRateGauge: TA3nalogGauge;
    flowRateEdit: TsCurrencyEdit;
    sFrameAdapter1: TsFrameAdapter;
    alarmTimer: TTimer;
    procedure alarmTimerTimer(Sender: TObject);
  private
    FParams: TChannelParams;
    FChannelCaption: string;
    FAlarmState: TAlarmStateSet;
    procedure SetChannelCaption(const Value: string);
    procedure SetParams(const Value: TChannelParams);
    procedure SetFlowRate;
    procedure SetPressure;
    procedure SetTemperature;
    procedure UpdateParams;
    procedure SetTemperatureRange;
    procedure SetPressureRange;
    procedure SetFlowRateRange;
    procedure CheckAlarmState;
    procedure CheckFlowRateAlarm;
    procedure CheckPressureAlarm;
    procedure CheckTemperatureAlarm;
    procedure UpdateFlowRateAlarm;
    procedure UpdatePressureAlarm;
    procedure UpdateTemperatureAlarm;
    procedure SetAlarmState(const Value: TAlarmStateSet);
    { Private declarations }
  public
    { Public declarations }
    //--[ methods ]--
    procedure UpdateView;

    //--[ properties ]--
    property AlarmState : TAlarmStateSet read FAlarmState write SetAlarmState;
    property ChannelCaption : string read FChannelCaption write SetChannelCaption;
    property Params : TChannelParams read FParams write SetParams;
  end;

implementation

{$R *.dfm}

{ TChannelInfoViewFrame }

procedure TChannelInfoViewFrame.alarmTimerTimer(Sender: TObject);
begin
	UpdateAlarms();
end;

procedure TChannelInfoViewFrame.UpdateAlarms;
begin
  UpdateTemperatureAlarm;
  UpdatePressureAlarm;
  UpdateFlowRateAlarm;
end;


procedure TChannelInfoViewFrame.CheckAlarmState;
begin
  CheckTemperatureAlarm;
  CheckPressureAlarm;
  CheckFlowRateAlarm;
end;

procedure TChannelInfoViewFrame.CheckTemperatureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.TemperatureRange.Contains(Params.Temperature)) then
    Exclude(state, asTemperature)
  else
    Include(state, asTemperature);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.CheckPressureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.PressureRange.Contains(Params.Pressure)) then
    Exclude(state, asPressure)
  else
    Include(state, asPressure);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.CheckFlowRateAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.FlowRateRange.Contains(Params.FlowRate)) then
    Exclude(state, asFlowRate)
  else
    Include(state, asFlowRate);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.UpdateFlowRateAlarm;
begin
  if (asFlowRate in AlarmState) then
  begin
    if (FlowRateGauge.CenterColor = clGray) then
      FlowRateGauge.CenterColor := clRed
    else
      FlowRateGauge.CenterColor := clGray;
  end
  else
  begin
    FlowRateGauge.CenterColor := clGray;
  end;
end;

procedure TChannelInfoViewFrame.UpdatePressureAlarm;
begin
  if (asPressure in AlarmState) then
  begin
    if (pressureGauge.CenterColor = clGray) then
      pressureGauge.CenterColor := clRed
    else
      pressureGauge.CenterColor := clGray;
  end
  else
  begin
    pressureGauge.CenterColor := clGray;
  end;
end;

procedure TChannelInfoViewFrame.UpdateTemperatureAlarm;
begin
  if (asTemperature in AlarmState) then
  begin
    if (temperatureGauge.CenterColor = clGray) then
      temperatureGauge.CenterColor := clRed
    else
      temperatureGauge.CenterColor := clGray;
  end
  else
  begin
    temperatureGauge.CenterColor := clGray;
  end;
end;


procedure TChannelInfoViewFrame.SetAlarmState(const Value: TAlarmStateSet);
begin
  FAlarmState := Value;

  alarmTimer.Enabled := FAlarmState <> [];
end;

procedure TChannelInfoViewFrame.SetChannelCaption(const Value: string);
begin
  FChannelCaption := Value;

  temperatureGroup.Caption := Format('����������� (%s)',[Value]);
  pressureGroup.Caption := Format('�������� (%s)',[Value]);
  flowRateGroup.Caption := Format('������ (%s)',[Value]);
end;

procedure TChannelInfoViewFrame.SetParams(const Value: TChannelParams);
begin
  FParams := Value;

  UpdateParams();
end;

procedure TChannelInfoViewFrame.UpdateParams();
begin
  SetPressure();
  SetTemperature();
  SetFlowRate();

  SetTemperatureRange();
  SetPressureRange();
  SetFlowRateRange();

  CheckAlarmState();
end;

procedure TChannelInfoViewFrame.SetPressure();
begin
  pressureGauge.Position := Params.Pressure;
  pressureEdit.Value := Params.Pressure;
end;

procedure TChannelInfoViewFrame.SetTemperature();
begin
  temperatureGauge.Position := Params.Temperature;
  temperatureEdit.Value := Params.Temperature;
end;

procedure TChannelInfoViewFrame.UpdateView;
begin
  UpdateParams();
end;

procedure TChannelInfoViewFrame.SetTemperatureRange;
begin
  temperatureGauge.IndMinimum := Round( Params.TemperatureRange.Min );
  temperatureGauge.IndMaximum := Round( Params.TemperatureRange.Max );
end;

procedure TChannelInfoViewFrame.SetPressureRange;
begin
  pressureGauge.IndMinimum := Round( Params.PressureRange.Min );
  pressureGauge.IndMaximum := Round( Params.PressureRange.Max );
end;

procedure TChannelInfoViewFrame.SetFlowRateRange;
begin
  FlowRateGauge.IndMinimum := Round( Params.FlowRateRange.Min );
  FlowRateGauge.IndMaximum := Round( Params.FlowRateRange.Max );
end;


procedure TChannelInfoViewFrame.SetFlowRate();
begin
  flowRateGauge.Position := Params.FlowRate;
  flowRateEdit.Value := Params.FlowRate;
end;

end.
