unit class_mnemoschemeItemData;

interface
    uses
      //--[ Classes ]--
      class_creatableObject,
      //--[ interfaces ]--
      interface_mnemoschemeItemData,
      interface_mnemoschemeItemChanelData,
      interface_mnemoschemeItemChanelList
      ;

    type
        TMnemoschemeItemData = class( TCreatableObject, IMnemoschemeItemData )
        private
              FActive: boolean;
              FCaption: string;
              FChanels: IMnemoschemeItemChanelList;
              FName: string;
              FIndex: integer;

            function EqualChanels(source: IMnemoschemeItemData): boolean;

        protected
            function MakeList() : IMnemoschemeItemChanelList;virtual;
        public
                function GetActive() : boolean;
                function GetCaption() : string;
                function GetIndex() : integer;
                function GetName() : string;
                function GetCount(): integer;
                function GetChanels(index: integer): IMnemoschemeItemChanelData;

                procedure SetActive(const Value: boolean);
                procedure SetCaption(const Value: string);
                procedure SetChanels(index: integer; const Value: IMnemoschemeItemChanelData);
                procedure SetName(const Value: string);
                procedure SetIndex(const Value: integer);

                procedure Assign( const data : IMnemoschemeItemData );
                procedure Add( const adata : IMnemoschemeItemChanelData );
                procedure Clear();
                function FindById( index : integer ) : IMnemoschemeItemChanelData;

            	property Active : boolean read GetActive write SetActive;
                property Caption : string read GetCaption write SetCaption;

                property Index : integer read GetIndex write SetIndex;

                property Count : integer read getCount;
                property Name : string read GetName write SetName;

                function ToString() : string;

                function Equals( source : IMnemoschemeItemData ) : boolean;virtual;

                property Chanels[ index : integer ] : IMnemoschemeItemChanelData read GetChanels write SetChanels;default;

                constructor Create; override;
                destructor Destroy; override;
        end;



implementation

uses const_guids, classes, tool_ms_factories, sysutils;

{ TMnemoschemeItemData }
procedure TMnemoschemeItemData.Add(const adata: IMnemoschemeItemChanelData);
begin
    FChanels.Add( adata );
end;

procedure TMnemoschemeItemData.Assign(const data: IMnemoschemeItemData);
    var
        i : integer;
begin
	FActive := data.Active;
    FCaption := data.Caption;
    FName := data.Name;

    FChanels.Clear();
    for i := 1 to data.Count do
    begin
        FChanels.Add( data.Chanels[i] );
    end;


    FIndex := data.Index;
end;

procedure TMnemoschemeItemData.Clear;
begin
	FChanels.Clear();
end;

constructor TMnemoschemeItemData.Create;
begin
  	inherited;

	FChanels := MakeList();
end;


destructor TMnemoschemeItemData.Destroy;
begin
    Assert( FRefCount = 0, 'Destroying of usable interfaced object' );

    FChanels := nil;

  inherited;
end;


function TMnemoschemeItemData.Equals(
  source: IMnemoschemeItemData): boolean;
begin
    result :=
        (
              ( Active = source.Active )
          and ( Caption = source.Caption )
          and ( EqualChanels( source ) )
          and ( Index = source.Index )
          and ( Name = source.Name )
        );
end;

function TMnemoschemeItemData.EqualChanels( source : IMnemoschemeItemData ): boolean;
    var
        i : integer;
begin
    result := false;

    if( Count <> source.Count ) then
        exit;

    for i := 1 to Count do
    begin
      if( not Chanels[ i ].Equals( source.Chanels[ i ] ) )then
        exit;
    end;

    result := true;
end;


function TMnemoschemeItemData.FindById(index: integer): IMnemoschemeItemChanelData;
begin
	result := FChanels.FindById( index );

    if( result = nil )then
    	raise EListError.Create( 'Item is not found' );
end;

function TMnemoschemeItemData.GetActive: boolean;
begin
   result := FActive;
end;

function TMnemoschemeItemData.GetCaption: string;
begin
    result := FCaption;
end;

function TMnemoschemeItemData.GetChanels(index: integer): IMnemoschemeItemChanelData;
begin
	result := FChanels[ index ];
end;

function TMnemoschemeItemData.getCount: integer;
begin
	result := FChanels.Count;
end;

function TMnemoschemeItemData.GetIndex: integer;
begin
    result := FIndex;
end;

function TMnemoschemeItemData.GetName: string;
begin
    result := FName;
end;

function TMnemoschemeItemData.MakeList: IMnemoschemeItemChanelList;
begin
    result := GetCommonFactory().CreateInstance( CLSID_MnemoschemeItemChanelList ) as IMnemoschemeItemChanelList;
end;

procedure TMnemoschemeItemData.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

procedure TMnemoschemeItemData.SetCaption(const Value: string);
begin
  FCaption := Value;
end;

procedure TMnemoschemeItemData.SetChanels(index: integer; const Value: IMnemoschemeItemChanelData);
begin
	FChanels.Chanels[index] := value;
end;

procedure TMnemoschemeItemData.SetIndex(const Value: integer);
begin
  FIndex := Value;
end;

procedure TMnemoschemeItemData.SetName(const Value: string);
begin
  FName := Value;
end;

function TMnemoschemeItemData.ToString: string;
begin
    result := Format(
                      'Put:%s Active:%s Count:%d Index:%d Name:%s',
                        [Caption, BoolToStr( Active ), Count, Index, Name]
                     );
end;

end.
 