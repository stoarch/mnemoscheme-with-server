unit tool_wellDataSaver;

interface
	uses
    	//--[ data ]--
        data_Well
        ;

	type
    	WellDataSaver = class
        	class procedure Save( data : TWellData );
        end;

implementation

	uses
    	//--[ common ]--
        db, sysUtils, windows,
        //--[ constants ]--
        const_Well,
    	//--[ data ]--
        module_dataMain
        ;



{ WellDataSaver }

class procedure WellDataSaver.Save(data: TWellData);
    const
      	c_sql = 'insert into params (chanel_id, param_kind_id, "value", date_query, "valid") ' +
        		' values ( %0:d, %1:d, %2:5.2f, ''%3:s'', %4:s ) ';

    function GetBoolStr( value : boolean ) : string;
    begin
      result := 'False';
      if value then
      	result	:= 'True';
    end;

    procedure SaveParams( index : integer; kind : integer; value : Single; isValid : boolean );
    	var
          	settings : TFormatSettings;
    begin
    	settings.DecimalSeparator := '.';

      with msDataModule, wellQuery do
      begin
        SQL.Text := Format( c_sql,
                      [
                          index,
                          kind,
                          value,
                          FormatDateTime( 'dd.mm.yyyy hh:nn:ss.zzz', Now ),
                          GetBoolStr( isValid )
                      ],
                      settings
                    );
        ExecSQL();
      end;
    end;

	var
    	i : integer;
begin
  	for i := 1 to data.Count do
    with data.Chanels[i], Value do
    begin
      	SaveParams( Index, WELL_FLOWRATE_ID, 	FlowRate, 		IsValidFlowRate		);
        SaveParams( Index, WELL_BACKWARD_FLOWRATE_ID, BackwardFlowRate, IsValidBackwardFlowRate );
        SaveParams( index, WELL_CURRENT_FLOWRATE_ID, CurrentFlowRate, IsValidCurrentFlowRate );
    end;
end;

end.
