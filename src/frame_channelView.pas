unit frame_channelView;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Mask,
  sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit, A3nalogGauge, sGroupBox,
  sFrameAdapter, iThermometer, iSevenSegmentDisplay, iSevenSegmentAnalog,
  iComponent, iVCLComponent, iCustomComponent, iPositionComponent,
  iScaleComponent, iGaugeComponent, iAngularGauge, iAngularLogGauge,
  iLinearGauge, iLed, iLedDiamond, ImgList, sLabel, math, PDJRotoLabel,
  //--[ data ]--
  data_alarms,
  data_putChanel,
  //--[ interfaces ]--
  interface_channelParams,
  interface_commonFactory,
  interface_mnemoschemeItemChannelValue,
  interface_putChanelData
  ;

type

  TChannelViewFrame = class(TFrame)
    temperatureGroup: TsGroupBox;
    pressureGroup: TsGroupBox;
    flowRateGroup: TsGroupBox;
    sFrameAdapter1: TsFrameAdapter;
    pressureGauge: TiAngularGauge;
    pressureEdit: TiSevenSegmentAnalog;
    temperatureEdit: TiSevenSegmentAnalog;
    temperatureGauge: TiThermometer;
    flowRateEdit: TiSevenSegmentAnalog;
    flowRateGauge: TiLinearGauge;
    temperatureValidLED: TiLedDiamond;
    pressureValidLED: TiLedDiamond;
    flowRateValidLED: TiLedDiamond;
    ImageList: TImageList;
    temperatureTimeLabel: TsLabel;
    pressureTimeLabel: TsLabel;
    flowRateLabel: TsLabel;
    lblCaption: TPDJRotoLabel;
    procedure alarmTimerTimer(Sender: TObject);
  private
    FParams: IChannelParams;
    FChannelCaption: string;
    FAlarmState: TAlarmStateSet;
    FActive: boolean;
    FTemperatureOPC: string;
    FFlowRateOPC: string;
    FPressureOPC: string;
    FData: IPutChanelData;
    FChanelNo: integer;
    FValue : IMnemoschemeItemChannelValue;
    FNeedUpdateView: boolean;

    procedure SetChannelCaption(const Value: string);
    procedure SetPressure;
    procedure SetTemperature;
    procedure SetFlowRate;
    procedure UpdateParams;
    procedure SetFlowRateRange;
    procedure SetPressureRange;
    procedure SetTemperatureRange;
    procedure SetAlarmState(const Value: TAlarmStateSet);
    procedure UpdateTemperatureAlarm;
    procedure UpdatePressureAlarm;
    procedure UpdateFlowRateAlarm;
    procedure CheckTemperatureAlarm;
    procedure CheckPressureAlarm;
    procedure CheckFlowRateAlarm;
    procedure CheckAlarmState;
    procedure UpdateAlarms;
    procedure SetActive(const Value: boolean);
    procedure SetTemperatureOPC(const Value: string);
    procedure SetFlowRateOPC(const Value: string);
    procedure SetPressureOPC(const Value: string);
    procedure SetData(const Value: IPutChanelData);
    procedure SetChanelNo(const Value: integer);
    procedure UpdateValue;
    procedure SetHeatRateRange;
    procedure SetHeatRate;
    procedure CheckHeatRateAlarm;
    procedure UpdateHeatRateAlarm;
    function GetActive: boolean;
    procedure SetNeedUpdateView(const Value: boolean);
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    //--[ methods ]--
    function AlarmOn(): boolean;

    function TemperatureBreakLo() : boolean;
    function TemperatureBreakHi() : boolean;

    procedure UpdateAlarmState();
    procedure UpdateView();

    //--[ properties ]--
    property Active : boolean read GetActive write SetActive;
    property AlarmState : TAlarmStateSet read FAlarmState write SetAlarmState;
    property ChannelCaption : string read FChannelCaption write SetChannelCaption;
    property ChanelNo : integer read FChanelNo write SetChanelNo;
    property ParamRanges : IChannelParams read FParams;

    property ParamValues : IMnemoschemeItemChannelValue read FValue;

    property FlowRateOPC : string read FFlowRateOPC write SetFlowRateOPC;
	property PressureOPC : string read FPressureOPC write SetPressureOPC;
    property TemperatureOPC : string read FTemperatureOPC write SetTemperatureOPC;

    property OpcData : IPutChanelData read FData write SetData;

    property NeedUpdateView : boolean read FNeedUpdateView write SetNeedUpdateView;
  end;

implementation

uses
    //--[ constants ]--
    const_put,
    //--[ tools ]--
    tool_environment,
    tool_systemLog
    , tool_ms_factories, const_guids;

{$R *.dfm}
	const
    	DEFAULT_SEGMENT_COLOR : TColor = $0039FB82;

{ TFrame1 }

function TChannelViewFrame.AlarmOn: boolean;
begin
    CheckAlarmState();
	result := AlarmState <> [];
end;

procedure TChannelViewFrame.alarmTimerTimer(Sender: TObject);
begin
  UpdateAlarms;
end;

constructor TChannelViewFrame.Create(AOwner: TComponent);
    var
        factory : ICommonFactory;
begin
  inherited;

  //TODO: Commonize place for GetPutFactory(), and other factories: MnemoschemeEnvironment

  factory := GetPutFactory();

  FData := factory.CreateInstance( CLSID_MnemoschemeItemChanelData ) as IPutChanelData;

  FParams := factory.CreateInstance( CLSID_PutChanelParams ) as IChannelParams;

  FValue := factory.CreateInstance( CLSID_MnemoschemeItemChanelValue ) as IMnemoschemeItemChannelValue;
end;

destructor TChannelViewFrame.Destroy;
begin
    FData := nil;
    FParams := nil;
    FValue := nil;
    
  inherited;
end;

procedure TChannelViewFrame.SetActive(const Value: boolean);
begin
  FActive := Value;

  UpdateAlarms();
end;

procedure TChannelViewFrame.SetAlarmState(const Value: TAlarmStateSet);
begin
  FAlarmState := Value;
end;

procedure TChannelViewFrame.SetChannelCaption(const Value: string);
begin
  FChannelCaption := Value;

  temperatureGroup.Caption := Format('T (%s)',[Value]);
  pressureGroup.Caption := Format('P (%s)',[Value]);
  flowRateGroup.Caption := Format('Q (%s)',[Value]);
end;


procedure TChannelViewFrame.UpdateParams();
begin
  SetTemperatureRange();
  SetPressureRange();
  SetFlowRateRange();
  SetHeatRateRange();

  CheckAlarmState();
end;

procedure TChannelViewFrame.SetPressure();
begin
	//TODO: Commonize params displaying and setup
  pressureGroup.Caption := Format('P(%d) - ',[ChanelNo]);
  pressureTimeLabel.Caption := FormatDateTime( 'hh:nn:ss', ParamValues.Pressure.DateReceived );
  pressureValidLED.Active := ParamValues.Pressure.IsValid;

  pressureGauge.Position := ParamValues.Pressure.Value;
  pressureEdit.Value := ParamValues.Pressure.Value;
end;

procedure TChannelViewFrame.SetPressureOPC(const Value: string);
begin
  FPressureOPC := Value;
end;

procedure TChannelViewFrame.SetTemperature();
begin
  temperatureGroup.Caption := Format('T(%d) - ',[ChanelNo]);
  temperatureTimeLabel.Caption := FormatDateTime( 'hh:nn:ss', ParamValues.Temperature.DateReceived );
  temperatureValidLED.Active := ParamValues.Temperature.IsValid;

  temperatureGauge.Position := ParamValues.Temperature.Value;
  temperatureEdit.Value := ParamValues.Temperature.Value;
end;

procedure TChannelViewFrame.SetHeatRate();
begin
end;


procedure TChannelViewFrame.SetTemperatureOPC(const Value: string);
begin
  FTemperatureOPC := Value;
end;

function TChannelViewFrame.TemperatureBreakHi: boolean;
begin
	result := ParamValues.Temperature.Value >= temperatureGauge.CurrentMax;
end;

function TChannelViewFrame.TemperatureBreakLo: boolean;
begin
   result := ParamValues.Temperature.Value <= temperatureGauge.CurrentMin;
end;

procedure TChannelViewFrame.UpdateView;
begin
  UpdateParams();
  UpdateValue();

  lblCaption.Caption :=	opcData.Caption;

  Refresh();
end;

procedure TChannelViewFrame.UpdateValue();
begin
  SetPressure();
  SetTemperature();
  SetFlowRate();
  SetHeatRate();

  UpdateAlarms();
end;

procedure TChannelViewFrame.UpdateAlarms;
begin
  UpdateTemperatureAlarm;
  UpdatePressureAlarm;
  UpdateFlowRateAlarm;
  UpdateHeatRateAlarm;
end;

procedure TChannelViewFrame.UpdateAlarmState;
begin
	UpdateAlarms();
end;

procedure TChannelViewFrame.CheckAlarmState;
begin
  CheckTemperatureAlarm;
  CheckPressureAlarm;
  CheckFlowRateAlarm;
  CheckHeatRateAlarm();
end;

procedure TChannelViewFrame.CheckHeatRateAlarm;
begin
end;

procedure TChannelViewFrame.CheckTemperatureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (ParamRanges.TemperatureRange.Contains(ParamValues.Temperature.Value)) then
    Exclude(state, asTemperature)
  else
    Include(state, asTemperature);

  AlarmState := state;
end;

procedure TChannelViewFrame.CheckPressureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (ParamRanges.PressureRange.Contains(ParamValues.Pressure.Value)) then
    Exclude(state, asPressure)
  else
    Include(state, asPressure);

  AlarmState := state;
end;

procedure TChannelViewFrame.CheckFlowRateAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (ParamRanges.FlowRateRange.Contains(ParamValues.FlowRate.Value)) then
    Exclude(state, asFlowRate)
  else
    Include(state, asFlowRate);

  AlarmState := state;
end;

procedure TChannelViewFrame.UpdateFlowRateAlarm;
begin
  if (asFlowRate in AlarmState) and (Active) then
  begin
    if (flowRateEdit.SegmentColor = DEFAULT_SEGMENT_COLOR) then
      flowRateEdit.SegmentColor := clRed
    else
      flowRateEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;

    if (FlowRateGauge.PointerColor = clGray) then
      FlowRateGauge.PointerColor := clRed
    else
      FlowRateGauge.PointerColor := clGray;
  end
  else
  begin
    FlowRateGauge.PointerColor := clGray;
    flowRateEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;

procedure TChannelViewFrame.UpdatePressureAlarm;
begin
  if (asPressure in AlarmState) and (Active) then
  begin
    if (pressureEdit.SegmentColor = DEFAULT_SEGMENT_COLOR) then
      pressureEdit.SegmentColor := clRed
    else
      pressureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;

    if (pressureGauge.HubColor = clGray) then
      pressureGauge.HubColor := clRed
    else
      pressureGauge.HubColor := clGray;
  end
  else
  begin
    pressureGauge.HubColor := clGray;
    pressureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;

procedure TChannelViewFrame.UpdateTemperatureAlarm;
begin
  if (asTemperature in AlarmState) and (Active) then
  begin
    if (temperatureEdit.SegmentColor = DEFAULT_SEGMENT_COLOR) then
      temperatureEdit.SegmentColor := clRed
    else
      temperatureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;

    if (temperatureGauge.IndicatorColor = clGray) then
      temperatureGauge.IndicatorColor := clRed
    else
      temperatureGauge.IndicatorColor := clGray;
  end
  else
  begin
    temperatureGauge.IndicatorColor := clGray;
    temperatureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;

procedure TChannelViewFrame.UpdateHeatRateAlarm;
begin
end;

procedure TChannelViewFrame.SetFlowRate();
begin
  flowRateGroup.Caption := Format('Q(%d) - ', [ChanelNo]);
  flowRateLabel.Caption := FormatDateTime( 'hh:nn:ss', ParamValues.FlowRate.DateReceived );
  flowRateValidLED.Active := ParamValues.FlowRate.IsValid;

  flowRateGauge.Position := ParamValues.FlowRate.Value;
  flowRateEdit.Value := ParamValues.FlowRate.Value;
end;

procedure TChannelViewFrame.SetFlowRateOPC(const Value: string);
begin
  FFlowRateOPC := Value;
end;

procedure TChannelViewFrame.SetTemperatureRange;
begin
  temperatureGauge.CurrentMin := Min( 0, ParamRanges.TemperatureRange.Min );
  temperatureGauge.CurrentMax := Max( ParamRanges.TemperatureRange.Max, temperatureGauge.PositionMax );
end;

procedure TChannelViewFrame.SetHeatRateRange;
begin
end;

procedure TChannelViewFrame.SetPressureRange;
begin
  pressureGauge.SectionEnd1 := ParamRanges.PressureRange.Min;
  pressureGauge.SectionEnd2 := ParamRanges.PressureRange.Max;

  pressureGauge.PositionMin := Min( 0, ParamRanges.PressureRange.Min );
  pressureGauge.PositionMax := ParamRanges.PressureRange.Max;
end;

procedure TChannelViewFrame.SetFlowRateRange;
begin
  FlowRateGauge.SectionEnd1 := ( ParamRanges.FlowRateRange.Min );
  FlowRateGauge.SectionEnd2 := ( ParamRanges.FlowRateRange.Max );

  FlowRateGauge.PositionMin := Min( 0, ParamRanges.FlowRateRange.Min );
  FlowRateGauge.PositionMax := ( ParamRanges.FlowRateRange.Max );
end;

procedure TChannelViewFrame.SetData(const Value: IPutChanelData);
begin
  FData.Assign( Value );

  if( NeedUpdateView )then
  	UpdateView();
end;

procedure TChannelViewFrame.SetChanelNo(const Value: integer);
begin
  FChanelNo := Value;
end;

function TChannelViewFrame.GetActive: boolean;
begin
	result := FData.Active;
end;

procedure TChannelViewFrame.SetNeedUpdateView(const Value: boolean);
begin
  FNeedUpdateView := Value;
end;

end.
