unit frame_channelInfoView;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, sMaskEdit, sCustomComboEdit, sCurrEdit,
  sCurrencyEdit, A3nalogGauge, sGroupBox, sFrameAdapter,
  ExtCtrls, iThermometer, iAngularGauge, iSevenSegmentDisplay,
  iSevenSegmentAnalog, iComponent, iVCLComponent, iCustomComponent,
  iPositionComponent, iScaleComponent, iGaugeComponent, iLinearGauge, iLed,
  iLedDiamond,
  //--[ data ]--
  data_alarms,
  //--[ interfaces ]--
  interface_commonFactory,
  interface_channelParams,
  interface_mnemoschemeItemChannelValue
  ;

type
  TChannelInfoViewFrame = class(TFrame)
    temperatureGroup: TsGroupBox;
    pressureGroup: TsGroupBox;
    flowRateGroup: TsGroupBox;
    sFrameAdapter1: TsFrameAdapter;
    flowRateEdit: TiSevenSegmentAnalog;
    pressureEdit: TiSevenSegmentAnalog;
    temperatureEdit: TiSevenSegmentAnalog;
    temperatureValidLED: TiLedDiamond;
    pressureValidLED: TiLedDiamond;
    flowRateValidLED: TiLedDiamond;
    procedure alarmTimerTimer(Sender: TObject);
  private
    FParams: IChannelParams;
    FChannelCaption: string;
    FAlarmState: TAlarmStateSet;
    FActive: boolean;
    FChanelNo: integer;
    FValue: IMnemoschemeItemChannelValue;
    FCurrentMin: real;
    FCurrentMax: real;
    procedure SetChannelCaption(const Value: string);
    procedure SetFlowRate;
    procedure SetPressure;
    procedure SetTemperature;
    procedure UpdateParams;
    procedure SetTemperatureRange;
    procedure SetPressureRange;
    procedure SetFlowRateRange;
    procedure CheckAlarmState;
    procedure CheckFlowRateAlarm;
    procedure CheckPressureAlarm;
    procedure CheckTemperatureAlarm;
    procedure UpdateFlowRateAlarm;
    procedure UpdatePressureAlarm;
    procedure UpdateTemperatureAlarm;
    procedure SetAlarmState(const Value: TAlarmStateSet);
    procedure UpdateAlarms;
    procedure SetActive(const Value: boolean);
    procedure SetChanelNo(const Value: integer);
    procedure UpdateHeatRateAlarm;
    procedure CheckHeatRateAlarm;
    procedure SetCurrentMax(const Value: real);
    procedure SetCurrentMin(const Value: real);
    { Private declarations }
  public
    { Public declarations }
    constructor Create(aowner : TComponent);override;
    destructor Destroy();override;

    //--[ methods ]--
    procedure UpdateAlarmState();
    procedure UpdateView();

    //--[ properties ]--
    property Active : boolean read FActive write SetActive;
    property AlarmState : TAlarmStateSet read FAlarmState write SetAlarmState;
    property ChannelCaption : string read FChannelCaption write SetChannelCaption;
    property ChanelNo : integer read FChanelNo write SetChanelNo;
    property Params : IChannelParams read FParams;
    property Value : IMnemoschemeItemChannelValue read FValue;

    property CurrentMin : Real read FCurrentMin write SetCurrentMin;
    property CurrentMax : Real read FCurrentMax write SetCurrentMax;
  end;

implementation
    uses
        //--[ constants ]--
        const_put,
        //--[ tools ]--
        tool_environment
        , tool_ms_factories, const_guids;

	const
    	DEFAULT_SEGMENT_COLOR : TColor = $0039FB82;

{$R *.dfm}

{ TChannelInfoViewFrame }

procedure TChannelInfoViewFrame.alarmTimerTimer(Sender: TObject);
begin
	UpdateAlarms();
end;

procedure TChannelInfoViewFrame.UpdateAlarms;
begin
  UpdateTemperatureAlarm;
  UpdatePressureAlarm;
  UpdateFlowRateAlarm;
  UpdateHeatRateAlarm;
end;

procedure TChannelInfoViewFrame.UpdateHeatRateAlarm;
begin
end;

procedure TChannelInfoViewFrame.UpdateAlarmState;
begin
  UpdateAlarms();
end;

procedure TChannelInfoViewFrame.CheckAlarmState;
begin
  CheckTemperatureAlarm;
  CheckPressureAlarm;
  CheckFlowRateAlarm;
  CheckHeatRateAlarm;
end;

procedure TChannelInfoViewFrame.CheckHeatRateAlarm;
begin
end;

procedure TChannelInfoViewFrame.CheckTemperatureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  //TODO: Simplify this
  if (Params.TemperatureRange.Contains(Value.Temperature.Value)) then
    Exclude(state, asTemperature)
  else
    Include(state, asTemperature);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.CheckPressureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.PressureRange.Contains(Value.Pressure.Value)) then
    Exclude(state, asPressure)
  else
    Include(state, asPressure);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.CheckFlowRateAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.FlowRateRange.Contains(Value.FlowRate.Value)) then
    Exclude(state, asFlowRate)
  else
    Include(state, asFlowRate);

  AlarmState := state;
end;

procedure TChannelInfoViewFrame.UpdateFlowRateAlarm;
begin
  if (asFlowRate in AlarmState) and (Active) then
  begin
    if (flowRateEdit.SegmentColor = DEFAULT_SEGMENT_COLOR) then
      flowRateEdit.SegmentColor := clRed
    else
      flowRateEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end
  else
  begin
  	flowRateEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;

procedure TChannelInfoViewFrame.UpdatePressureAlarm;
begin
  if (asPressure in AlarmState) and (Active) then
  begin
    if (pressureEdit.SegmentColor = DEFAULT_SEGMENT_COLOR) then
      pressureEdit.SegmentColor := clRed
    else
      pressureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end
  else
  begin
    pressureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;

procedure TChannelInfoViewFrame.UpdateTemperatureAlarm;
begin
  if (asTemperature in AlarmState) and (Active) then
  begin
    if ( temperatureEdit.SegmentColor = DEFAULT_SEGMENT_COLOR ) then
      temperatureEdit.SegmentColor := clRed
    else
      temperatureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end
  else
  begin
    temperatureEdit.SegmentColor := DEFAULT_SEGMENT_COLOR;
  end;
end;


procedure TChannelInfoViewFrame.SetActive(const Value: boolean);
begin
  FActive := Value;

  UpdateAlarms();
end;

procedure TChannelInfoViewFrame.SetAlarmState(const Value: TAlarmStateSet);
begin
  FAlarmState := Value;
end;

procedure TChannelInfoViewFrame.SetChannelCaption(const Value: string);
begin
  FChannelCaption := Value;

  temperatureGroup.Caption := Format('T (%s)',[Value]);
  pressureGroup.Caption := Format('P (%s)',[Value]);
  flowRateGroup.Caption := Format('Q (%s)',[Value]);
end;

procedure TChannelInfoViewFrame.UpdateParams();
begin
  SetPressure();
  SetTemperature();
  SetFlowRate();

  SetTemperatureRange();
  SetPressureRange();
  SetFlowRateRange();

  CheckAlarmState();
end;

procedure TChannelInfoViewFrame.SetPressure();
begin
  pressureGroup.Caption := Format('P(%d) - ',[ChanelNo]);
  pressureValidLED.Active := Value.Pressure.IsValid;

  pressureEdit.Value := Value.Pressure.Value;
end;

procedure TChannelInfoViewFrame.SetTemperature();
begin
  temperatureGroup.Caption := Format('T(%d) - ',[ChanelNo]);
  temperatureValidLED.Active := Value.Temperature.IsValid;

  temperatureEdit.Value := Value.Temperature.Value;
end;

procedure TChannelInfoViewFrame.UpdateView;
begin
  UpdateParams();
end;

procedure TChannelInfoViewFrame.SetTemperatureRange;
begin
end;

procedure TChannelInfoViewFrame.SetPressureRange;
begin
end;

procedure TChannelInfoViewFrame.SetFlowRateRange;
begin
end;


procedure TChannelInfoViewFrame.SetFlowRate();
begin
  flowRateGroup.Caption := Format('Q(%d) - ', [ChanelNo]);
  flowRateValidLED.Active := Value.FlowRate.IsValid;

  flowRateEdit.Value := Value.FlowRate.Value;
end;

procedure TChannelInfoViewFrame.SetChanelNo(const Value: integer);
begin
  FChanelNo := Value;
end;


constructor TChannelInfoViewFrame.Create(aowner: TComponent);
    var
        factory : ICommonFactory;
begin
  inherited;

  factory := GetPutFactory();

  FValue := factory.CreateInstance( CLSID_MnemoschemeItemChanelValue ) as IMnemoschemeItemChannelValue;
  FParams := factory.CreateInstance( CLSID_PutChanelParams ) as IChannelParams;
end;

destructor TChannelInfoViewFrame.Destroy;
begin
  inherited;
end;

procedure TChannelInfoViewFrame.SetCurrentMax(const Value: real);
begin
  FCurrentMax := Value;
end;

procedure TChannelInfoViewFrame.SetCurrentMin(const Value: real);
begin
  FCurrentMin := Value;
end;

end.
