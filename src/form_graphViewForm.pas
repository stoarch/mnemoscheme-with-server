unit form_graphViewForm;

interface

uses
	//--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TeeProcs, TeEngine, Chart,
	//--[ classes ]--
  class_valueMatrix, Series, StdCtrls, Buttons, sBitBtn, sSkinProvider, DB,
  Grids, DBGrids, kbmMemTable, sCheckBox,
  //--[interfaces]--
  interface_valueMatrix
  ;

type
  TGraphViewForm = class(TForm)
    graphChart: TChart;
    sSkinProvider1: TsSkinProvider;
    sBitBtn1: TsBitBtn;
    printButton: TsBitBtn;
    dataGrid: TDBGrid;
    dataSource: TDataSource;
    dataTable: TkbmMemTable;
    graphValsVisible: TsCheckBox;
    marksVisible: TsCheckBox;
    Series1: TLineSeries;
    procedure printButtonClick(Sender: TObject);
    procedure graphValsVisibleClick(Sender: TObject);
    procedure marksVisibleClick(Sender: TObject);
  private
    FValueMatrix: IValueMatrix;
    FParamCaption: string;
    procedure CreateSeries(const Value: IValueMatrix);
    procedure FillDataTable(const Value: IValueMatrix);
    procedure PrepareDataTable(const Value: IValueMatrix);
    procedure SetValueMatrix(const Value: IValueMatrix);
    procedure PrintGraph;
    procedure SetParamCaption(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    constructor Create( Aowner : TComponent );override;
    destructor Destroy();override;

    property ParamCaption 	: string 		read FParamCaption 	write SetParamCaption;
    property ValueMatrix 	: IValueMatrix 	read FValueMatrix 	write SetValueMatrix;
  end;

var
  GraphViewForm: TGraphViewForm;

implementation

uses const_guids, tool_ms_factories, interface_commonFactory;

{$R *.dfm}

{ TGraphViewForm }

constructor TGraphViewForm.Create(Aowner: TComponent);
begin
  inherited;

  FValueMatrix := GetCommonFactory().CreateInstance( CLSID_ValueMatrix ) as IValueMatrix;
end;

destructor TGraphViewForm.Destroy;
begin
  inherited;
end;

procedure TGraphViewForm.SetValueMatrix(const Value: IValueMatrix);
begin
  FValueMatrix.Assign( Value );

  PrepareDataTable(Value);

  FillDataTable(Value);

  CreateSeries(Value);
end;

procedure TGraphViewForm.CreateSeries(const Value: IValueMatrix);
    const
        COUNT = 6;
        c_lineColors: Array[ 1..COUNT ] of TColor = (clRed, clNavy, clRed, clNavy, clGreen, clBlack );
        c_lineStyles : Array[ 1..COUNT ] of TPenStyle = (psSolid, psSolid, psDash, psDash, psDot, psDot );
var
  i: integer;
  j: integer;
  lineColor: TColor;
  lineStyle: TPenStyle;
  series: TLineSeries;
begin
  graphChart.SeriesList.Clear();

  for i := 1 to value.Count do
  begin
    if( i < COUNT )then
     begin
        lineColor := c_lineColors[i];
        lineStyle := c_lineStyles[i];
     end
    else
     begin
        lineColor := c_lineColors[COUNT];
        lineStyle := c_lineStyles[COUNT];
     end;


    series := TLineSeries.Create(self);
    series.Title := value.List[i].Title;
    series.XValues.DateTime := true;

    series.LinePen.Width := 1;
    series.LinePen.Style :=  lineStyle;
    series.LinePen.Color := lineColor;

    series.Pointer.Style := psCircle;
    series.Pointer.HorizSize := 3;
    series.Pointer.VertSize := 3;

    for j := 1 to value.List[i].count do
    begin
        series.AddXY( value.List[i].Value[j].Date, value.List[i].Value[j].Value, '', lineColor );
    end;



    graphChart.AddSeries(series);
  end;
end;

procedure TGraphViewForm.FillDataTable(const Value: IValueMatrix);
var
  i: integer;
  j: integer;
begin
  dataTable.Active := true;

  dataTable.First();
  for j := 1 to value.List[1].Count do
  begin
    dataTable.Append();
    dataTable.FieldByName('Date').AsDateTime := value.List[1].Value[j].Date;

    for i := 1 to value.count do
    begin
        dataTable.FieldByName('Value'+intToStr(i)).AsFloat := value.List[i].Value[j].Value;
    end;

    dataTable.Post();
  end;
end;

procedure TGraphViewForm.printButtonClick(Sender: TObject);
begin
	PrintGraph();
end;

procedure TGraphViewForm.PrintGraph();
begin
	graphChart.PrintLandscape();
end;


procedure TGraphViewForm.graphValsVisibleClick(Sender: TObject);
	var
    	i : integer;
begin
	for i := 1 to graphChart.SeriesList.Count do
    begin
    	graphChart.SeriesList[i-1].Marks.Visible := graphValsVisible.Checked;
    end;
end;

procedure TGraphViewForm.marksVisibleClick(Sender: TObject);
	var
    	i : integer;
begin
	for i := 1 to graphChart.SeriesList.Count do
    begin
		( graphChart.SeriesList[i-1] as TLineSeries ).Pointer.Visible := marksVisible.Checked; 
    end;
end;

procedure TGraphViewForm.PrepareDataTable(const Value: IValueMatrix);
var
  i: integer;
begin
  dataTable.Fields.Clear();
  dataTable.FieldDefs.Clear();
  dataTable.FieldDefs.Add('Date', ftDateTime );
  for i := 1 to value.count do
  begin
    dataTable.FieldDefs.Add( 'Value' + intToStr(i), ftFloat );
  end;
  dataTable.CreateTable();
  with(dataTable.FieldByName('Date') as TDateTimeField)do
  begin
    DisplayFormat := 'hh:nn  dd.mm.yyyy';
    DisplayLabel := '�����';
  end;

  for i := 1 to value.count do
  begin
  with( dataTable.FieldByName( 'Value' + intToStr(i) ) as TFloatField )do
    begin
        DisplayFormat := '###.##';
        DisplayLabel := '����. ' + intToStr(i);
    end;
  end;
end;

procedure TGraphViewForm.SetParamCaption(const Value: string);
begin
  FParamCaption := Value;
end;

end.
