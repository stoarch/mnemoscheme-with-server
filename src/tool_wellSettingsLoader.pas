unit tool_wellSettingsLoader;

interface
	uses
    	//--[ data ]--
        data_Well,
        data_WellChanel,
        //--[ forms ]--
		form_progressDisplayer,
        //--[ interfaces ]--
        interface_wellChannelData,
        interface_wellData,
        interface_wellList,
    	//--[ list ]--
        list_Well
        ;

	type
      	TWellSettingsOption = ( psoChanel, psoOPC, psoLimits );
        TWellSettingsOptions = set of TWellSettingsOption;

    	WellSettingsLoader = class
//TODO: Optimize performance of reading
        	class function ReadWellSettings(
            					WellCount : integer;
                                options  : TWellSettingsOptions = [psoChanel,psoOPC,psoLimits];
                                needInactive : Boolean = True
            				) : IWellList;overload;

        	class function ReadWellSettings(
            				options  : TWellSettingsOptions = [psoChanel,psoOPC,psoLimits];
                            needInactive : boolean = true
                           ) : IWellList;overload;

          private
            class procedure ReadChanelControlSettings(chData: IWellChannelData);
            class procedure ReadOPCSettings(chData: IWellChannelData);
            class procedure ReadWellViewSettings(data: IWellData; progress : TProgressDisplayerForm; options  : TWellSettingsOptions = [psoOPC,psoLimits]);
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils,
        //--[ constants ]--
        const_Well,
        //--[ data ]--
		module_dataMain,
        tool_ms_factories,
        const_guids;


{ WellSettingsLoader }

class function WellSettingsLoader.ReadWellSettings(
									options  	 : TWellSettingsOptions = [psoChanel,psoOPC,psoLimits];
									needInactive : boolean = true
								): IWellList;
begin
	result := ReadWellSettings( MSDataModule.Well_table.RecordCount, options, needInactive );
end;


class procedure WellSettingsLoader.ReadChanelControlSettings( chData : IWellChannelData );
begin
	with msDatamodule do
    begin
    	if( not wellChannel_settings_table.Active )then
        	wellChannel_settings_table.Open();

      wellChannel_settings_table.First();
      while not wellChannel_settings_table.Eof do
      begin
        case( wellChannel_settings_table.FieldByName('param_kind_id').asInteger )of
            WELL_FLOWRATE_ID :
                with chData.Params.FlowRateRange do
                begin
                    Min := wellChannel_settings_table.FieldByName('min').AsFloat;
                    Max := wellChannel_settings_table.FieldByName('max').AsFloat;
                end;
        end;

        wellChannel_settings_table.Next;
      end;
    end;
end;

class procedure WellSettingsLoader.ReadOPCSettings( chData : IWellChannelData );
begin
  with msDataModule do
  begin
  	if( not wellOpc_params_table.Active )then
    	wellOpc_params_table.Open();

    wellOpc_params_table.First();
    while not wellOpc_params_table.Eof do
    begin
      case wellOpc_params_table.FieldByName('param_kind_id').AsInteger of
          WELL_FLOWRATE_ID :
              begin
                chData.flowRateOPC := wellOpc_params_table.FieldByName( 'value' ).AsString;
              end;
          WELL_BACKWARD_FLOWRATE_ID :
              begin
                chData.backwardFlowRateOPC := wellOpc_params_table.FieldByName( 'value' ).AsString;
              end;
          WELL_CURRENT_FLOWRATE_ID:
              begin
                chData.currentFlowRateOPC := wellOpc_params_table.FieldByName( 'value' ).AsString;
              end;
      end;

      wellOpc_params_table.Next();
    end;
  end;
end;

class procedure WellSettingsLoader.ReadWellViewSettings( data : IWellData; progress : TProgressDisplayerForm; options  : TWellSettingsOptions = [psoOPC,psoLimits] );
var
  i : Integer;
  chData : IWellChannelData;
  oldOPCActive, oldLimitsActive : boolean;
begin
	try
      with MSDataModule do
      begin
      		if( not wellChannel_table.Active )then
            	wellChannel_table.Open();

            oldLimitsActive := wellChannel_settings_table.Active;
            oldOPCActive	:=	wellOpc_params_table.Active;

            if( not (psoOpc in options))then
            	wellOpc_params_table.Close();

            if( not (psoLimits in options))then
            	wellChannel_settings_table.Close();

      	try
            wellChannel_table.First();

              chData := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemChanelData ) as IWellChannelData;
              data.Clear();

              progress.MaxValue := wellChannel_table.RecordCount;

              for i := 1 to wellChannel_table.RecordCount do
              begin
                  if( progress.IsCanceled )then
                  begin
                      Abort;
                  end;

                  progress.progress := i;
                  progress.SubMessage := '�������� ����� ' + IntToStr( i ) +  ' �� ' + IntToStr( wellChannel_table.RecordCount );

                  chData.caption := wellChannel_table.FieldByName('caption').AsString;
                  chData.active  := wellChannel_table.FieldByName('active').AsBoolean;
                  chData.index   := wellChannel_table.FieldByName('id').AsInteger;

                  if( psoOPC in options )then
                  	ReadOPCSettings( chData );

                  if( psoLimits in options )then
                      ReadChanelControlSettings( chData );

                  data.Add( chData );

                  wellChannel_table.next();
              end;
        finally
          	wellChannel_settings_table.Active        := oldLimitsActive;
            wellOpc_params_table.Active 			 := oldOPCActive;
        end;
      end;
    except
      	on e:EAbort do
        begin

        	raise;
        end;

    	on e:exception do
        	raise Exception.Create('TWellScanerMainForm.ReadWellViewSettings->'+#10#13+e.message);
    end;
end;

class function WellSettingsLoader.ReadWellSettings(WellCount: integer; options  : TWellSettingsOptions; needInactive: Boolean): IWellList;
const
	Well_CAPTION_ITEM = 'Caption';
    Well_ACTIVE_ITEM  = 'Active';
    Well_COUNT_ITEM   = 'Count';

    COMMON_SECTION = 'Mnemoscheme';
var
  i		: Integer;
  data 	: IWellData;
  progress	: TProgressDisplayerForm;
  oldParamsActive : boolean;
begin
  	result 		:= nil;
    oldParamsActive := msDataModule.wellParams_table.Active;

  	progress	:= TProgressDisplayerForm.Create(nil);
    try
      	progress.Show();
        progress.WorkMessage := '�������� ������ �������...';

        with msDataModule do
        try
          result := GetWellFactory().CreateInstance( CLSID_WellList ) as IWellList;

          wellParams_table.Close();

          if( not msDataModule.Well_table.Active )then
            Well_table.Open();

          if( not ( psoChanel in options ))then
          	Well_table.DisableControls();

          i := 0;
          msDataModule.Well_table.First();

          //TODO: Add progress indicator
          progress.TaskMax := WellCount;
          while ( not msDataModule.Well_table.eof ) and ( i < WellCount )  do
          begin
            if( progress.IsCanceled )then
            begin
                Abort();
            end;

            if not( ( msDataModule.Well_table.fieldByName('active').AsBoolean )
                    or( needInactive ) )
            then
                begin
                    msDataModule.Well_table.Next();
                    continue;
                end;

          	progress.TaskProgress := i;

            i	 := i + 1;
            data := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemData ) as IWellData;

            data.Name 	 := msDataModule.Well_table.fieldByName('name').AsString;
            data.Caption := msDataModule.Well_table.fieldByName('caption').AsString;
            data.Active	 := msDataModule.Well_table.fieldByName('active').AsBoolean;
            data.Index	 := msDataModule.Well_table.fieldByName('id').AsInteger;

            progress.WorkMessage := '�������� ������ ' + data.Name + '...';

            if( psoChanel in options ) then
            	ReadWellViewSettings( data, progress, options );

            result.Add( data );

            msDataModule.Well_table.Next();
          end;
        except
          	on e:EAbort do
            begin
                result := nil;
                raise;
            end;

            on e:exception do
                raise Exception.Create( 'TWellScanerMainForm.ReadWellSettings->'+#10#13+ e.message );
        end;
    finally
      	msDataModule.Well_table.EnableControls;

        msDataModule.wellParams_table.Active := oldParamsActive;

		if( assigned( progress ) )then
      		FreeAndNil( progress );
    end;
end;


end.
