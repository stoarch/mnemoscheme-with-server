unit class_channelValueLoader;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ data ]--
        ZDataset,
        //--[ interfaces ]--
        interface_channelValueLoader,
        interface_mnemoschemeItemChannelValue,
        interface_parameterValueLoader,
        //--[ storages ]--
        storage_interfaceHashTable
        ;

type
    TChannelValueLoader = class( TCreatableObject, IChannelValueLoader )
    private
        FLoaders : TIntegerInterfaceHashTable;
        FKind: integer;
        FValue: IMnemoschemeItemChannelValue;
        FQuery: TZQuery;

    public
        constructor Create();override;
        destructor Destroy();override;

        function Execute() : boolean;virtual;

        function GetKind: integer;
        function GetQuery: TZQuery;
        function GetValue: IMnemoschemeItemChannelValue;

        procedure SetKind(const Value: integer);
        procedure SetQuery(const Value: TZQuery);
        procedure SetValue(const Value: IMnemoschemeItemChannelValue);

        procedure AddParameterLoader( const paramId : integer; const loader : IParameterValueLoader );

        property Kind : integer read GetKind write SetKind;

        property Query : TZQuery read GetQuery write SetQuery;

        property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
    end;

implementation

uses
  SysUtils;

{ TChannelValueLoader }

procedure TChannelValueLoader.AddParameterLoader(const paramId: integer;
  const loader: IParameterValueLoader);
begin
    FLoaders.put( paramId, loader );
end;

constructor TChannelValueLoader.Create;
begin
  inherited;

  FLoaders := TIntegerInterfaceHashTable.Create();
end;

destructor TChannelValueLoader.Destroy;
begin
    if assigned( FLoaders ) then
        FreeAndNil( FLoaders );

  inherited;
end;

function TChannelValueLoader.Execute: boolean;
    var
        loader : IParameterValueLoader;
begin
    loader := FLoaders.get( Kind ) as IParameterValueLoader;
    
    loader.Value := Value;
    loader.Query := Query;

    result := loader.Execute();
end;

function TChannelValueLoader.GetKind: integer;
begin
    result := FKind;
end;

function TChannelValueLoader.GetQuery: TZQuery;
begin
    result := FQuery;
end;

function TChannelValueLoader.GetValue: IMnemoschemeItemChannelValue;
begin
    result := FValue;
end;

procedure TChannelValueLoader.SetKind(const Value: integer);
begin
  FKind := Value;
end;

procedure TChannelValueLoader.SetQuery(const Value: TZQuery);
begin
  FQuery := Value;
end;

procedure TChannelValueLoader.SetValue(const Value: IMnemoschemeItemChannelValue);
begin
  FValue := Value;
end;

end.
 