object ChannelInfoViewFrame: TChannelInfoViewFrame
  Left = 0
  Top = 0
  Width = 260
  Height = 68
  TabOrder = 0
  object temperatureGroup: TsGroupBox
    Left = 2
    Top = 1
    Width = 83
    Height = 65
    Caption = 'T (1)'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    SkinData.SkinSection = 'GROUPBOX'
    object temperatureEdit: TiSevenSegmentAnalog
      Left = 5
      Top = 30
      Width = 73
      Height = 30
      AutoSize = False
      DigitSpacing = 8
      SegmentColor = 3799938
      SegmentOffColor = 2960685
      AutoSegmentOffColor = False
      Precision = 1
      ShowSign = False
    end
    object temperatureValidLED: TiLedDiamond
      Left = 66
      Top = 2
      Width = 14
      Height = 14
      Hint = #1047#1077#1083#1077#1085#1099#1081' - '#1074#1077#1088#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
      ShowHint = True
    end
  end
  object pressureGroup: TsGroupBox
    Left = 88
    Top = 1
    Width = 83
    Height = 65
    Caption = 'P (1)'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    SkinData.SkinSection = 'GROUPBOX'
    object pressureEdit: TiSevenSegmentAnalog
      Left = 5
      Top = 30
      Width = 73
      Height = 30
      AutoSize = False
      DigitSpacing = 8
      SegmentColor = 3799938
      SegmentOffColor = 2960685
      AutoSegmentOffColor = False
      Precision = 3
      ShowSign = False
    end
    object pressureValidLED: TiLedDiamond
      Left = 67
      Top = 2
      Width = 14
      Height = 14
      Hint = #1047#1077#1083#1077#1085#1099#1081' - '#1074#1077#1088#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
      ShowHint = True
    end
  end
  object flowRateGroup: TsGroupBox
    Left = 174
    Top = 1
    Width = 83
    Height = 65
    Caption = 'Q (1)'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    SkinData.SkinSection = 'GROUPBOX'
    object flowRateEdit: TiSevenSegmentAnalog
      Left = 5
      Top = 31
      Width = 73
      Height = 30
      AutoSize = False
      DigitSpacing = 8
      SegmentColor = 3799938
      SegmentOffColor = 2960685
      AutoSegmentOffColor = False
      Precision = 0
      ShowSign = False
    end
    object flowRateValidLED: TiLedDiamond
      Left = 66
      Top = 2
      Width = 14
      Height = 14
      Hint = #1047#1077#1083#1077#1085#1099#1081' - '#1074#1077#1088#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
      ShowHint = True
    end
  end
  object sFrameAdapter1: TsFrameAdapter
    SkinData.SkinSection = 'GROUPBOX'
    Left = 559
    Top = 91
  end
end
