unit class_valueMatrix;

interface

	uses
		  //--[ classes ]--
		  class_creatableInterfaceList,
		  class_valueList,
		  //--[ interfaces ]--
		  interface_valueList,
		  interface_valueMatrix,
		  //--[ collections ]--
		  xpCollections
		  ;

	type
		TValueMatrix = class( TCreatableInterfaceList, IValueMatrix )
			private
			public
				 constructor Create();override;
                 destructor Destroy();override;

				 //--[ methods ]--
				 procedure Add( const list : IValueList );reintroduce;overload;
				 procedure Assign( const matrix : IValueMatrix );

				 //--[ property accessors ]--
				 function getList(index: integer): IValueList;virtual;

				 //--[ properties ]--
				 property List[ index : integer ] : IValueList read getList;
		  end;

implementation

uses storage_interfaceList, interface_commonFactory, tool_ms_factories,
  const_put, const_guids;

{ TValueMatrix }

procedure TValueMatrix.Add(const list: IValueList);
begin
	inherited add( list );
end;

procedure TValueMatrix.Assign(const matrix: IValueMatrix);
	var
		i : integer;
		  data : IValueList;
          factory : ICommonFactory;
begin
	Clear();

    factory := GetCommonFactory();

	 for i := 1 to matrix.count do
	 begin
		  data := factory.CreateInstance( CLSID_ValueList ) as IValueList;
		  data.Assign( matrix.List[i] );

		  Add( data );
	 end;
end;

constructor TValueMatrix.Create;
begin
	inherited Create();
end;

destructor TValueMatrix.Destroy;
begin
  inherited;
end;

function TValueMatrix.getList(index: integer): IValueList;
begin
	result := inherited Values[ index ] as IValueList;
end;

end.
