﻿select * from
(SELECT avg( params.value ) as temp_val, extract( hour from date_query ) as hr, params.param_kind_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_trunc( 'day', date_query ) = '2009-08-19')
	and ( put.id = 6 ) and ( params.param_kind_id = 1 )
group by hr, params.param_kind_id
order by hr
) as temp_query,
(SELECT avg( params.value ) as pres_val, extract( hour from date_query ) as hr, params.param_kind_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_trunc( 'day', date_query ) = '2009-08-19')
	and ( put.id = 6 ) and ( params.param_kind_id = 2 )
group by hr, params.param_kind_id
order by hr
) as pres_query,
(SELECT avg( params.value ) as fl_val, extract( hour from date_query ) as hr, params.param_kind_id
  FROM put, chanel, params, param_kind
  where ( chanel.put_id = put.id ) and (params.chanel_id = chanel.id) and (params.param_kind_id = param_kind.id) and 
	( date_trunc( 'day', date_query ) = '2009-08-19')
	and ( put.id = 6 ) and ( params.param_kind_id = 3 )
group by hr, params.param_kind_id
order by hr
)  as flr_query
where (temp_query.hr = pres_query.hr ) and ( pres_query.hr = flr_query.hr );
