unit data_wellChanel;

interface
	uses
    	//--[ data ]--
        class_mnemoschemeItemChannelData,
        //--[ interfaces ]--
        interface_commonFactory,
        interface_mnemoschemeItemChanelData,
        interface_wellChannelData,
        interface_wellChannelParams
        ;

//TODO: Introduce list of channel values distinct from channel params
	type
    	TWellChanelData = class(TMnemoschemeItemChannelData, IWellChannelData)
            private

              //HACK: Think about common patterns and multivals
              FflowRateOPC: string;
              FbackwardFlowRateOPC : string;

              FParams: IWellChannelParams;
              FcurrentFlowRateOPC: string;


        	public
              constructor Create();override;
              destructor Destroy();override;

              procedure Assign( const value : IMnemoschemeItemChanelData );override;

              function GetBackwardFlowRateOPC: string;
              function GetCurrentFlowRateOPC: string;
              function GetFlowRateOPC: string;
              function GetParams: IWellChannelParams;

              procedure SetflowRateOPC(const Value: string);
              procedure SetBackwardFlowRateOPC( const Value : string );
              procedure SetParams(const Value: IWellChannelParams);
              procedure SetcurrentFlowRateOPC(const Value: string);

              property flowRateOPC : string read GetFlowRateOPC write SetflowRateOPC;
              property backwardFlowRateOPC : string read GetBackwardFlowRateOPC write SetBackwardFlowRateOPC;
              property currentFlowRateOPC : string read GetCurrentFlowRateOPC write SetcurrentFlowRateOPC;

              property Params : IWellChannelParams read GetParams write SetParams;

              function ToString() : string;
        end;

implementation

	uses
    	//--[ common ]--
        sysUtils,
        //--[ constants ]--
        const_well,
        //--[ tools ]--
        tool_environment
        , tool_ms_factories;


{ TWellChanelData }

procedure TWellChanelData.Assign(const value: IMnemoschemeItemChanelData);
var
  data: IWellChannelData;
begin
    inherited Assign( value );

    data := value as IWellChannelData;

    begin
        FflowRateOPC := data.flowRateOPC;
        FbackwardFlowRateOPC := data.backwardFlowRateOPC;
        FcurrentFlowRateOPC := data.currentFlowRateOPC;

        FParams.Assign( data.Params );
    end;
end;


constructor TWellChanelData.Create;
begin
    inherited;
    
	FParams := GetWellFactory().CreateInstance( CLSID_WellChanelParams ) as IWellChannelParams;
end;

destructor TWellChanelData.Destroy;
begin
  inherited;
end;

function TWellChanelData.GetBackwardFlowRateOPC: string;
begin
    result := FbackwardFlowRateOPC;
end;

function TWellChanelData.GetCurrentFlowRateOPC: string;
begin
    result := FcurrentFlowRateOPC;
end;
function TWellChanelData.GetFlowRateOPC: string;
begin
    result := FflowRateOPC;
end;

function TWellChanelData.GetParams: IWellChannelParams;
begin
    result := FParams;
end;

procedure TWellChanelData.SetBackwardFlowRateOPC(const Value: string);
begin
    FbackwardFlowRateOPC := value;
end;

procedure TWellChanelData.SetcurrentFlowRateOPC(const Value: string);
begin
  FcurrentFlowRateOPC := Value;
end;

procedure TWellChanelData.SetflowRateOPC(const Value: string);
begin
  FflowRateOPC := Value;
end;



procedure TWellChanelData.SetParams(const Value: IWellChannelParams);
begin
  FParams := Value;
end;

function TWellChanelData.ToString: string;
begin
    result := Format( 'Chanel:%s Index:%d', [Caption, Index] );
end;

end.
