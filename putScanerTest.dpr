program putScanerTest;

uses
  Forms,
  sysUtils,
  TestFrameWork,
  GuiTestRunner,
  test_scanePutMain in 'src\test\test_scanePutMain.pas',
  form_scanerPUTMain in 'src\form_scanerPUTMain.pas' {PutScanerMainForm},
  data_put in 'src\data_put.pas',
  XPFramework in '..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  XPCollections in '..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  data_putChanel in 'src\data_putChanel.pas',
  class_channelParams in 'src\class_channelParams.pas',
  class_realRange in 'src\class_realRange.pas',
  list_putChanel in 'src\list_putChanel.pas',
  list_put in 'src\list_put.pas',
  class_opcDataAccess in 'src\class_opcDataAccess.pas',
  class_opcItem in 'src\class_opcItem.pas',
  OPCutils in '..\..\library_new\OPC\Client\OPCutils.pas',
  tool_systemLog in 'src\tool_systemLog.pas',
  form_aboutPutScaner in 'src\form_aboutPutScaner.pas' {AboutForm},
  module_dataMain in 'src\module_dataMain.pas' {MSDataModule: TDataModule},
  tool_putSettingsLoader in 'src\tool_putSettingsLoader.pas',
  tool_putDataSaver in 'src\tool_putDataSaver.pas',
  class_channelValue in 'src\class_channelValue.pas',
  const_put in 'src\const_put.pas',
  tool_functions in 'src\tool_functions.pas',
  EzdslPQu in '..\..\library\Collections\EZDSL\EZDSLPQU.PAS',
  EZIntQue in '..\..\library\Collections\EZDSL\EZINTQUE.PAS',
  heap_putData in '..\..\library\irlib\storage\heap\heap_putData.pas',
  tool_settings in '..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\library\irlib\tools\tool_localSystem.pas',
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm};
  ;

{$R *.res}

begin
  Application.Initialize;

  SystemLog.Open('putScanerTest.log');

  Application.Title := 'Test of put scaner form';
  msDataModule := TMSDataModule.Create(nil);

  GuiTestRunner.RunRegisteredTests();

  FreeAndNil( msDataModule );
end.
