program wellScanner;

{%ToDo 'wellScanner.todo'}

uses
  Forms,
  sysUtils,
  form_wellScanerMain in 'form_wellScanerMain.pas' {WellScanerMainForm},
  opc_queryExecutor in '..\..\Lib\opc_queryExecutor.pas',
  class_opcDataAccess in 'class_opcDataAccess.pas',
  XPCollections in '..\..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  OPCutils in '..\..\..\library_new\OPC\Client\OPCutils.pas',
  class_realRange in 'class_realRange.pas',
  class_opcItem in 'class_opcItem.pas',
  OPCAutomation_TLB in '..\..\..\library_new\OPCAutomation_TLB.pas',
  XPFramework in '..\..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  OPCtypes in '..\..\..\library_new\OPC\OPCtypes.pas',
  OPC_AE in '..\..\..\library_new\OPC\OPC_AE.pas',
  OPCCOMN in '..\..\..\library_new\OPC\OPCCOMN.pas',
  OPCDA in '..\..\..\library_new\OPC\OPCDA.pas',
  OpcError in '..\..\..\library_new\OPC\OPCerror.pas',
  OPCHDA in '..\..\..\library_new\OPC\OPCHDA.pas',
  OPCSEC in '..\..\..\library_new\OPC\OPCSEC.pas',
  opc_error_strings in '..\..\..\library_new\OPC\opc_error_strings.pas',
  data_put in 'data_put.pas',
  data_wellChanel in 'data_wellChanel.pas',
  list_wellChanel in 'list_wellChanel.pas',
  list_put in 'list_put.pas',
  class_channelParams in 'class_channelParams.pas',
  module_dataMain in 'module_dataMain.pas' {MSDataModule: TDataModule},
  tool_systemLog in 'tool_systemLog.pas',
  tool_wellSettingsLoader in 'tool_wellSettingsLoader.pas',
  tool_wellDataSaver in 'tool_wellDataSaver.pas',
  form_aboutWellScaner in 'form_aboutWellScaner.pas' {AboutForm},
  class_mnemoschemeItemChannelValue in 'class_mnemoschemeItemChannelValue.pas',
  const_well in 'const_well.pas',
  tool_functions in 'tool_functions.pas',
  EzdslPQu in '..\..\..\library\Collections\EZDSL\EZDSLPQU.PAS',
  EZIntQue in '..\..\..\library\Collections\EZDSL\EZINTQUE.PAS',
  heap_wellData in 'heap_wellData.pas',
  tool_settings in '..\..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\..\library\irlib\tools\tool_localSystem.pas',
  form_progressDisplayer in 'form_progressDisplayer.pas' {ProgressDisplayerForm},
  list_well in 'list_well.pas',
  data_well in 'data_well.pas',
  class_wellChannelValue in 'class_wellChannelValue.pas',
  class_wellChannelParams in 'class_wellChannelParams.pas',
  class_wellDataLoader in 'class_wellDataLoader.pas';

{$R *.res}

begin
  	Settings.Open(ExtractFilePath(ParamStr(0)) + 'wellScaner.settings');

	SystemLog.Open( Format( 'wellScaner%s.log', [formatDateTime('yyyymmddhhnnss', now)]));
    SystemLog.Write( 'Program started' );
    SystemLog.Write( 'Trying to open database...' );

    try
      	msDataModule.LoadSettings();
    	msDataModule.OpenWells();
  		SystemLog.Write( 'Successfully' );
    except
      	on e:exception do
        begin
        	SystemLog.Write( 'Error:' + e.Message );
            Halt(1);
        end;
    end;


    try
      Application.Initialize;
      Application.Title := 'Well scaner server';

  		Application.HelpFile := 'D:\work\Mnemoscheme\doc\wellSCANER.HLP';
  		Application.CreateForm(TWellScanerMainForm, WellScanerMainForm);
  Application.Run;
	except
    	on e:exception do
    		SystemLog.Write( 'CRITICAL ERROR:' + e.message );
    end;

    SystemLog.Write( 'Program halted' );
end.
