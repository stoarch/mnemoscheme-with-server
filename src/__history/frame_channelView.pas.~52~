unit frame_channelView;

interface

uses
  //--[ common ]--
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Mask,
  sMaskEdit, sCustomComboEdit, sCurrEdit, sCurrencyEdit, A3nalogGauge, sGroupBox,
  //--[ classes ]--
  class_channelParams, sFrameAdapter
  ;

type
	TAlarmState = ( asTemperature, asPressure, asFlowRate );
	TAlarmStateSet = set of TAlarmState;

  TChannelViewFrame = class(TFrame)
    temperatureGroup: TsGroupBox;
    temperatureGauge: TA3nalogGauge;
    temperatureEdit: TsCurrencyEdit;
    temperatureChart: TChart;
    pressureGroup: TsGroupBox;
    pressureChart: TChart;
    pressureGauge: TA3nalogGauge;
    pressureEdit: TsCurrencyEdit;
    flowRateGroup: TsGroupBox;
    flowRateChart: TChart;
    FlowRateGauge: TA3nalogGauge;
    flowRateEdit: TsCurrencyEdit;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    sFrameAdapter1: TsFrameAdapter;
    alarmTimer: TTimer;
    procedure alarmTimerTimer(Sender: TObject);
  private
    FParams: TChannelParams;
    FChannelCaption: string;
    FAlarmState: TAlarmStateSet;
    procedure SetParams(const Value: TChannelParams);
    procedure SetChannelCaption(const Value: string);
    procedure SetPressure;
    procedure SetTemperature;
    procedure SetFlowRate;
    procedure UpdateParams;
    procedure SetFlowRateRange;
    procedure SetPressureRange;
    procedure SetTemperatureRange;
    procedure SetAlarmState(const Value: TAlarmStateSet);
    procedure UpdateTemperatureAlarm;
    procedure UpdatePressureAlarm;
    procedure UpdateFlowRateAlarm;
    procedure CheckTemperatureAlarm;
    procedure CheckPressureAlarm;
    procedure CheckFlowRateAlarm;
    procedure CheckAlarmState;
    { Private declarations }
  public
    { Public declarations }
    constructor Create( AOwner : TComponent );override;
    destructor Destroy();override;

    //--[ methods ]--
    function AlarmOn(): boolean;

    function TemperatureBreakLo() : boolean;
    function TemperatureBreakHi() : boolean;

    procedure UpdateView();

    //--[ properties ]--
    property AlarmState : TAlarmStateSet read FAlarmState write SetAlarmState;
    property ChannelCaption : string read FChannelCaption write SetChannelCaption;
    property Params : TChannelParams read FParams write SetParams;
  end;

implementation

{$R *.dfm}

{ TFrame1 }

function TChannelViewFrame.AlarmOn: boolean;
begin
    CheckAlarmState();
	result := AlarmState <> [];
end;

procedure TChannelViewFrame.alarmTimerTimer(Sender: TObject);
begin
  UpdateTemperatureAlarm;
  UpdatePressureAlarm;
  UpdateFlowRateAlarm;
end;

constructor TChannelViewFrame.Create(AOwner: TComponent);
begin
  inherited;

  FParams := TChannelParams.Create();
end;

destructor TChannelViewFrame.Destroy;
begin
  FreeAndNil( FParams );

  inherited;
end;

procedure TChannelViewFrame.SetAlarmState(const Value: TAlarmStateSet);
begin
  FAlarmState := Value;

  alarmTimer.Enabled := FAlarmState <> [];
end;

procedure TChannelViewFrame.SetChannelCaption(const Value: string);
begin
  FChannelCaption := Value;

  temperatureGroup.Caption := Format('����������� (%s)',[Value]);
  pressureGroup.Caption := Format('�������� (%s)',[Value]);
  flowRateGroup.Caption := Format('������ (%s)',[Value]);
end;

procedure TChannelViewFrame.SetParams(const Value: TChannelParams);
begin
  FParams := Value;

  UpdateParams();
end;

procedure TChannelViewFrame.UpdateParams();
begin
  SetPressure();
  SetTemperature();
  SetFlowRate();

  SetTemperatureRange();
  SetPressureRange();
  SetFlowRateRange();

  CheckAlarmState();
end;

procedure TChannelViewFrame.SetPressure();
begin
  pressureGauge.Position := Params.Pressure;
  pressureEdit.Value := Params.Pressure;

  pressureChart.SeriesList[0].AddXY(Frac(Now), Params.Pressure);
end;

procedure TChannelViewFrame.SetTemperature();
begin
  temperatureGauge.Position := Params.Temperature;
  temperatureEdit.Value := Params.Temperature;

  temperatureChart.SeriesList[0].AddXY(Frac(Now), Params.Temperature);
end;

function TChannelViewFrame.TemperatureBreakHi: boolean;
begin
	result := Params.Temperature >= temperatureGauge.IndMaximum;
end;

function TChannelViewFrame.TemperatureBreakLo: boolean;
begin
   result := Params.Temperature <= temperatureGauge.IndMinimum;
end;

procedure TChannelViewFrame.UpdateView;
begin
  UpdateParams();
end;

procedure TChannelViewFrame.CheckAlarmState;
begin
  CheckTemperatureAlarm;
  CheckPressureAlarm;
  CheckFlowRateAlarm;
end;

procedure TChannelViewFrame.CheckTemperatureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.TemperatureRange.Contains(Params.Temperature)) then
    Exclude(state, asTemperature)
  else
    Include(state, asTemperature);

  AlarmState := state;
end;

procedure TChannelViewFrame.CheckPressureAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.PressureRange.Contains(Params.Pressure)) then
    Exclude(state, asPressure)
  else
    Include(state, asPressure);

  AlarmState := state;
end;

procedure TChannelViewFrame.CheckFlowRateAlarm;
var
  state: TAlarmStateSet;
begin
  state := AlarmState;

  if (Params.FlowRateRange.Contains(Params.FlowRate)) then
    Exclude(state, asFlowRate)
  else
    Include(state, asFlowRate);

  AlarmState := state;
end;

procedure TChannelViewFrame.UpdateFlowRateAlarm;
begin
  if (asFlowRate in AlarmState) then
  begin
    if (FlowRateGauge.CenterColor = clGray) then
      FlowRateGauge.CenterColor := clRed
    else
      FlowRateGauge.CenterColor := clGray;
  end
  else
  begin
    FlowRateGauge.CenterColor := clGray;
  end;
end;

procedure TChannelViewFrame.UpdatePressureAlarm;
begin
  if (asPressure in AlarmState) then
  begin
    if (pressureGauge.CenterColor = clGray) then
      pressureGauge.CenterColor := clRed
    else
      pressureGauge.CenterColor := clGray;
  end
  else
  begin
    pressureGauge.CenterColor := clGray;
  end;
end;

procedure TChannelViewFrame.UpdateTemperatureAlarm;
begin
  if (asTemperature in AlarmState) then
  begin
    if (temperatureGauge.CenterColor = clGray) then
      temperatureGauge.CenterColor := clRed
    else
      temperatureGauge.CenterColor := clGray;
  end
  else
  begin
    temperatureGauge.CenterColor := clGray;
  end;
end;

procedure TChannelViewFrame.SetFlowRate();
begin
  flowRateGauge.Position := Params.FlowRate;
  flowRateEdit.Value := Params.FlowRate;

  flowRateChart.SeriesList[0].AddXY(Frac(Now), Params.FlowRate);
end;


procedure TChannelViewFrame.SetTemperatureRange;
begin
  temperatureGauge.IndMinimum := Round( Params.TemperatureRange.Min );
  temperatureGauge.IndMaximum := Round( Params.TemperatureRange.Max );
end;

procedure TChannelViewFrame.SetPressureRange;
begin
  pressureGauge.IndMinimum := Round( Params.PressureRange.Min );
  pressureGauge.IndMaximum := Round( Params.PressureRange.Max );
end;

procedure TChannelViewFrame.SetFlowRateRange;
begin
  FlowRateGauge.IndMinimum := Round( Params.FlowRateRange.Min );
  FlowRateGauge.IndMaximum := Round( Params.FlowRateRange.Max );
end;

end.
