program putReport;

uses
  Forms,
  form_reportMain in 'src\form_reportMain.pas' {PutReportsForm},
  module_dataMain in 'src\module_dataMain.pas' {MSDataModule: TDataModule},
  data_put in 'src\data_put.pas',
  XPFramework in '..\..\library_new\xpframework\XPFramework\XPFramework.pas' {DialogFramework},
  XPCollections in '..\..\library_new\xpframework\XPFramework\XPCollections.pas',
  data_wellChanel in 'src\data_wellChanel.pas',
  class_mnemoschemeItemChannelValue in 'src\class_mnemoschemeItemChannelValue.pas',
  class_channelParams in 'src\class_channelParams.pas',
  class_realRange in 'src\class_realRange.pas',
  list_wellChanel in 'src\list_wellChanel.pas',
  form_data_putSelector in 'src\form_data_putSelector.pas' {DateSelectorForm},
  list_put in 'src\list_put.pas',
  tool_wellSettingsLoader in 'src\tool_wellSettingsLoader.pas',
  const_well in 'src\const_well.pas',
  form_putDateRangeSelectorForm in 'src\form_putDateRangeSelectorForm.pas' {PutDateRangeSelectorForm},
  form_putMonthSelectorForm in 'src\form_putMonthSelectorForm.pas' {PutMonthSelectorForm},
  storage_ObjectHashTable in '..\..\library\irlib\storage\hash\storage_ObjectHashTable.pas',
  storage_HashTable in '..\..\library\irlib\storage\hash\storage_HashTable.pas',
  storage_interfaceHashTable in '..\..\library\irlib\storage\hash\storage_interfaceHashTable.pas',
  tool_settings in '..\..\library\irlib\tools\tool_settings.pas',
  tool_localSystem in '..\..\library\irlib\tools\tool_localSystem.pas',
  tool_systemLog in 'src\tool_systemLog.pas',
  tool_environment in '..\..\library\irlib\tools\tool_environment.pas',
  mwFixedRecSort in '..\..\library\Collections\decal\mwFixedRecSort.pas',
  DeCAL in '..\..\library\Collections\decal\DeCAL.pas',
  DeCALIO in '..\..\library\Collections\decal\DeCALIO.pas',
  storage_iterator in '..\..\library\irlib\storage\iterators\storage_iterator.pas',
  SuperStream in '..\..\library\Collections\decal\SuperStream.pas',
  form_putYearSelectorForm in 'src\form_putYearSelectorForm.pas' {PutYearSelectorForm},
  form_putSelector in 'src\form_putSelector.pas' {PutSelectorForm},
  form_progressDisplayer in 'src\form_progressDisplayer.pas' {ProgressDisplayerForm},
  form_monthDaySelector in 'src\form_monthDaySelector.pas' {MonthDaySelectorForm},
  form_putListSelector in 'src\form_putListSelector.pas' {PutListSelectorForm},
  class_valueMatrix in 'src\class_valueMatrix.pas',
  class_value in 'src\class_value.pas',
  class_valueList in 'src\class_valueList.pas',
  form_graphViewForm in 'src\form_graphViewForm.pas' {GraphViewForm},
  class_creatableObject in '..\..\library\irlib\classes\class_creatableObject.pas',
  interface_value in 'src\interface_value.pas';

{$R *.res}

begin
  Settings.open('putReport.settings');

  msDataModule.LoadSettings();
  msDataModule.Open();
  
  Application.Initialize;
  Application.Title := 'Put Reports Form';
  Application.CreateForm(TPutReportsForm, PutReportsForm);
  Application.Run;
end.
