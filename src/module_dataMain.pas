
unit module_dataMain;

interface

uses
  SysUtils, Classes, ZSqlMonitor, DB, ZAbstractRODataset, ZAbstractDataset,
  ZAbstractTable, ZDataset, ZConnection, DBLogDlg, JclSysInfo;

type
  TMSDataModule = class(TDataModule)
    msConnection: TZConnection;
    put_table: TZTable;
    put_DataSource: TDataSource;         
    put_tableid: TIntegerField;
    put_tablecaption: TStringField;
    chanel_table: TZTable;
    chanel_dataSource: TDataSource;
    params_table: TZTable;
    params_DataSource: TDataSource;
    opc_params_table: TZTable;
    opc_params_Datasource: TDataSource;
    chanel_tableid: TIntegerField;
    chanel_tablecaption: TStringField;
    chanel_tableput_id: TIntegerField;
    params_tableid: TIntegerField;
    params_tablechanel_id: TIntegerField;
    params_tableparam_kind_id: TIntegerField;
    params_tablevalue: TFloatField;
    opc_params_tableid: TIntegerField;
    opc_params_tablechanel_id: TIntegerField;
    opc_params_tableparam_kind_id: TIntegerField;
    opc_params_tablevalue: TStringField;
    param_kind_table: TZTable;
    param_kind_DataSource: TDataSource;
    chanel_control_settings_table: TZTable;
    chanel_control_settings_DataSource: TDataSource;
    chanel_control_settings_tableid: TIntegerField;
    chanel_control_settings_tablechanel_id: TIntegerField;
    chanel_control_settings_tableparam_kind_id: TIntegerField;
    chanel_control_settings_tablemin: TFloatField;
    chanel_control_settings_tablemax: TFloatField;
    param_kind_tableid: TIntegerField;
    param_kind_tablecaption: TStringField;
    params_tableparam_caption: TStringField;
    opc_params_tableparam_caption: TStringField;
    chanel_control_settings_tableparam_caption: TStringField;
    put_tablename: TStringField;
    params_tabledate_query: TDateTimeField;
    queryData: TZQuery;
    put_tableactive: TBooleanField;
    chanel_tableactive: TBooleanField;
    params_tablevalid: TBooleanField;
    wiConnection: TZConnection;
    well_table: TZTable;
    well_source: TDataSource;
    wellChannel_table: TZTable;
    wellChannel_source: TDataSource;
    wellParams_table: TZTable;
    wellParams_source: TDataSource;
    wellOPC_params_table: TZTable;
    wellOPC_params_source: TDataSource;
    wellParamKind_table: TZTable;
    wellParamKind_source: TDataSource;
    wellChannel_settings_table: TZTable;
    wellChannel_settings_source: TDataSource;
    well_tablecaption: TStringField;
    well_tableactive: TBooleanField;
    well_tableid: TIntegerField;
    well_tablename: TStringField;
    wellParams_tablechanel_id: TIntegerField;
    wellParams_tableparam_kind_id: TIntegerField;
    wellParams_tablevalue: TFloatField;
    wellParams_tabledate_query: TDateTimeField;
    wellParams_tableid: TIntegerField;
    wellParams_tablevalid: TBooleanField;
    wellOPC_params_tablechanel_id: TIntegerField;
    wellOPC_params_tableparam_kind_id: TIntegerField;
    wellOPC_params_tablevalue: TStringField;
    wellOPC_params_tableid: TIntegerField;
    wellChannel_settings_tablechanel_id: TIntegerField;
    wellChannel_settings_tableparam_kind_id: TIntegerField;
    wellChannel_settings_tablemin: TFloatField;
    wellChannel_settings_tablemax: TFloatField;
    wellChannel_settings_tableid: TIntegerField;
    wellParamKind_tablecaption: TStringField;
    wellParamKind_tableid: TIntegerField;
    wellChannel_tablecaption: TStringField;
    wellChannel_tableobj_id: TIntegerField;
    wellChannel_tableactive: TBooleanField;
    wellChannel_tableid: TIntegerField;
    wellChannel_tableobj_ch_id: TIntegerField;
    wellQuery: TZQuery;
  private
    { Private declarations }
  public
    { Public declarations }

    procedure 	LoadSettings();
    procedure	Open();
    procedure OpenWells();
  end;

var
  msDataModule: TMSDataModule;

implementation

	uses
    	Dialogs
        , tool_settings;

{$R *.dfm}

procedure TMSDataModule.LoadSettings;
	const
      	IP_ADDRESS = 'address';

        SERVER_SECTION = 'server';

    var
      	address : string;
begin
	address := Settings.getString( IP_ADDRESS, SERVER_SECTION, '127.0.0.1' );

	msConnection.HostName := address;
end;

procedure TMsDatamodule.Open();
begin
	msConnection.Connected := true;

    put_table.Open();
    chanel_table.Open();
    opc_params_table.Open();
    chanel_control_settings_table.Open();
    param_kind_table.Open();
end;

procedure TMSDataModule.OpenWells;
begin
    wiConnection.Connected := true;

    well_table.Open();
    wellChannel_table.Open();
    wellOPC_params_table.Open();
    wellChannel_settings_table.Open();
    wellParamKind_table.Open();
end;

initialization
	try
		msDataModule := TMSDataModule.Create(nil);
    except
    	on e:exception do
        	ShowMessage( '��������, �������� ������:' + e.message + #10#13 + '���������� � �������������, ����������.');
    end;

finalization
	FreeAndNil( msDataModule );
end.
