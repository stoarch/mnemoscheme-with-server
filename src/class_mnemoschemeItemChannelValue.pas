unit class_mnemoschemeItemChannelValue;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        class_parameterValue,
        //--[ interfaces ]--
        interface_mnemoschemeItemChannelValue,
        interface_parameterValue
        ;

	type
    	TMnemoschemeItemChannelValue = class( TCreatableObject, IMnemoschemeItemChannelValue )
            private
              FTemperature: IParameterValue;
              FPressure : IParameterValue;
              FFlowRate: IParameterValue;
              FHeatRate: IParameterValue;
              FBackwardFlowRate : IParameterValue;
              FCurrentFlowRate : IParameterValue;


        	public

                constructor Create();override;
                destructor Destroy();override;

                function GetFlowRate: IParameterValue;
                function GetHeatRate: IParameterValue;
                function GetPressure: IParameterValue;
                function GetTemperature: IParameterValue;

                procedure SetTemperature(const Value: IParameterValue);
                procedure SetPressure(const Value: IParameterValue);
                procedure SetFlowRate(const Value: IParameterValue);
                procedure SetHeatRate(const Value: IParameterValue);
                function GetBackwardFlowRate: IParameterValue;
                function GetCurrentFlowRate: IParameterValue;
                procedure SetBackwardFlowRate(const Value: IParameterValue);
                procedure SetCurrentFlowRate(const Value: IParameterValue);

                property Temperature : IParameterValue read GetTemperature write SetTemperature;
                property Pressure : IParameterValue read GetPressure write SetPressure;
                property FlowRate : IParameterValue read GetFlowRate write SetFlowRate;
                property HeatRate : IParameterValue read GetHeatRate write SetHeatRate;
                property BackwardFlowRate : IParameterValue read GetBackwardFlowRate write SetBackwardFlowRate;
                property CurrentFlowRate : IParameterValue read GetCurrentFlowRate write SetCurrentFlowRate;

            	procedure Assign( const data : IMnemoschemeItemChannelValue );
                procedure Clear();
                function Equals( source : IMnemoschemeItemChannelValue ) : Boolean;
                function ToString() : string;
        end;

implementation

	uses
    	sysUtils
        , tool_systemLog, tool_ms_factories, interface_commonFactory,
  const_guids;

{ TMnemoschemeItemChannelValue }

procedure TMnemoschemeItemChannelValue.Assign(const data: IMnemoschemeItemChannelValue);
begin
    FTemperature.Assign( data.Temperature );
    FPressure.Assign( data.Pressure );
    FFlowRate.Assign( data.FlowRate );
    FHeatRate.Assign( data.HeatRate );
    FBackwardFlowRate.Assign( data.BackwardFlowRate );
    FCurrentFlowRate.Assign( data.CurrentFlowRate );
end;

procedure TMnemoschemeItemChannelValue.Clear;
begin
    Temperature.Clear();
    Pressure.Clear();
    FlowRate.Clear();
    HeatRate.Clear();
    BackwardFlowRate.Clear();
    CurrentFlowRate.Clear();
end;

constructor TMnemoschemeItemChannelValue.Create;
    var
        factory : ICommonFactory;
begin
    inherited;

    factory := GetCommonFactory();

    FTemperature := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FPressure := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FHeatRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FBackwardFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
    FCurrentFlowRate := factory.CreateInstance( CLSID_ParameterValue ) as IParameterValue;
end;

destructor TMnemoschemeItemChannelValue.Destroy;
begin

  inherited;
end;


function TMnemoschemeItemChannelValue.Equals(
  source: IMnemoschemeItemChannelValue): Boolean;
begin
    Result  :=
        (
            (BackwardFlowRate = source.BackwardFlowRate)
           and ( CurrentFlowRate = source.CurrentFlowRate )
           and ( FlowRate = source.FlowRate )
           and ( HeatRate = source.HeatRate )
           and ( Pressure = source.Pressure )
           and ( Temperature = source.Temperature )
        );
end;

function TMnemoschemeItemChannelValue.GetBackwardFlowRate: IParameterValue;
begin
    result := FBackwardFlowRate;
end;

function TMnemoschemeItemChannelValue.GetCurrentFlowRate: IParameterValue;
begin
    result := FCurrentFlowRate;
end;

function TMnemoschemeItemChannelValue.GetFlowRate: IParameterValue;
begin
    Result := FFlowRate;
end;

function TMnemoschemeItemChannelValue.GetHeatRate: IParameterValue;
begin
    Result := FHeatRate;
end;

function TMnemoschemeItemChannelValue.GetPressure: IParameterValue;
begin
    result := FPressure;
end;

function TMnemoschemeItemChannelValue.GetTemperature: IParameterValue;
begin
    result := FTemperature;
end;

procedure TMnemoschemeItemChannelValue.SetBackwardFlowRate(const Value: IParameterValue);
begin
    FBackwardFlowRate := value;
end;

procedure TMnemoschemeItemChannelValue.SetCurrentFlowRate(const Value: IParameterValue);
begin
    FCurrentFlowRate := value;
end;

procedure TMnemoschemeItemChannelValue.SetFlowRate(const Value: IParameterValue);
begin
  FFlowRate := Value;
end;

procedure TMnemoschemeItemChannelValue.SetHeatRate(const Value: IParameterValue);
begin
  FHeatRate := Value;
end;

procedure TMnemoschemeItemChannelValue.SetPressure(const Value: IParameterValue);
begin
  FPressure := Value;
end;

procedure TMnemoschemeItemChannelValue.SetTemperature(const Value: IParameterValue);
begin
  FTemperature := Value;
end;

function TMnemoschemeItemChannelValue.toString: string;
begin
	result := '';
end;

end.
