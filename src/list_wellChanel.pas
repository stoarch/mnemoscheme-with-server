unit list_wellChanel;

interface
	uses
        //--[ classes ]--
        class_mnemoschemeItemChanelList,
    	//--[ common ]--
        xpCollections,
        //--[ data ]--
        data_WellChanel,
        //--[ interfaces ]--
        interface_commonFactory,
        interface_mnemoschemeItemChanelData,
        interface_wellChannelData,
        interface_wellChanelList
        ;

    type
	  	TWellChanelList = class( TMnemoschemeItemChanelList, IWellChanelList )
            private
            protected
                function MakeData() : IMnemoschemeItemChanelData;override;
        	public
        end;


implementation
    uses
        //--[ constants ]--
        const_well,
        //--[ tools ]--
        tool_environment
        , tool_ms_factories, const_guids;

{ TWellChanelList }

function TWellChanelList.MakeData: IMnemoschemeItemChanelData;
begin
    result := GetWellFactory().CreateInstance( CLSID_MnemoschemeItemChanelData ) as IMnemoschemeItemChanelData;
end;

end.
