unit class_parameterValue;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ interfaces ]--
        interface_parameterValue
        ;

type
    TParameterValue = class( TCreatableObject, IParameterValue )
    private
        FValue : Single;
        FDateReceived : TDateTime;
        FIsValid : boolean;
    public
        procedure Assign( const data : IParameterValue );
        procedure Clear();

        function GetValue() : Single;
        procedure SetValue( const avalue : Single );

        function GetDateReceived() : TDateTime;
        procedure SetDateReceived( const avalue : TDateTime );

        function GetIsValid() : boolean;
        procedure SetIsValid( const avalue : boolean );


        property DateReceived : TDateTime read GetDateReceived write SetDateReceived;
        property IsValid : boolean read GetIsValid write SetIsValid;
        property Value : Single read GetValue write SetValue;
    end;

implementation

{ TParameterValue }

procedure TParameterValue.Assign(const data: IParameterValue);
begin
    FValue := data.Value;
    FIsValid := data.IsValid;
    FDateReceived := data.DateReceived;
end;

procedure TParameterValue.Clear;
begin
    FValue := 0;
    FIsValid := false;
    FDateReceived := 0;
end;

function TParameterValue.GetDateReceived: TDateTime;
begin
    result := FDateReceived;
end;

function TParameterValue.GetIsValid: boolean;
begin
    result := FIsValid;
end;

function TParameterValue.GetValue: Single;
begin
    result := FValue;
end;

procedure TParameterValue.SetDateReceived(const avalue: TDateTime);
begin
    FDateReceived := avalue;
end;

procedure TParameterValue.SetIsValid(const avalue: boolean);
begin
    FIsValid := avalue;
end;

procedure TParameterValue.SetValue(const avalue: Single);
begin
    FValue := avalue;
end;

end.
 