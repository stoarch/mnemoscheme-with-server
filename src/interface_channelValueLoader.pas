unit interface_channelValueLoader;

interface
    uses
        //--[ classes ]--
        classes_chanelParameterLoaders,
        //--[ data ]--
        ZDataset,
        //--[ interfaces ]--
        interface_mnemoschemeItemChannelValue,
        interface_parameterValueLoader
        ;

const
    IID_IChannelValueLoader : TGUID = '{1AD81A2C-BAEA-440D-A916-A4DF4155705F}';

type
    IChannelValueLoader = interface
        ['{1AD81A2C-BAEA-440D-A916-A4DF4155705F}']

        function Execute() : boolean;

        function GetKind: integer;
        function GetQuery: TZQuery;
        function GetValue: IMnemoschemeItemChannelValue;

        procedure SetKind(const Value: integer);
        procedure SetQuery(const Value: TZQuery);
        procedure SetValue(const Value: IMnemoschemeItemChannelValue);

        procedure AddParameterLoader( const paramId : integer; const loader : IParameterValueLoader );

        property Kind : integer read GetKind write SetKind;

        property Query : TZQuery read GetQuery write SetQuery;

        property Value : IMnemoschemeItemChannelValue read GetValue write SetValue;
    end;

implementation

end.
 