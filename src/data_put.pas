unit data_put;

interface
	uses
        //--[ classes ]--
        class_mnemoschemeItemData,
        //--[ data ]--
        data_putChanel,
        //--[ interfaces ]--
        interface_commonFactory,
        interface_mnemoschemeItemData,
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChanelList,
        interface_putChanelList,
        interface_putData,
        interface_putChanelData,
        //--[ list ]--
        list_putChanel
        ;

	type
    	TPutData = class( TMnemoschemeItemData, IPutData )
            private
            protected
                function MakeList() : IMnemoschemeItemChanelList;override;

        	public

                function IPutData.GetChanels = GetPutChanels;
                procedure IPutData.SetChanels = SetPutChanels;

                function GetPutChanels(index: integer): IPutChanelData;
                procedure SetPutChanels(index: integer; const Value: IPutChanelData);


        end;

implementation

	uses
    	//--[ common ]--
        sysUtils, Classes,
        //--[ constants ]--
        const_put,
        //--[ tools ]--
        tool_environment
        , tool_ms_factories, const_guids;

{ TPutData }




function TPutData.GetPutChanels(index: integer): IPutChanelData;
begin
    result := Chanels[index] as IPutChanelData;
end;


function TPutData.MakeList: IMnemoschemeItemChanelList;
begin
    result := GetPutFactory().CreateInstance( CLSID_MnemoschemeItemChanelList ) as IMnemoschemeItemChanelList;
end;

procedure TPutData.SetPutChanels(index: integer;
  const Value: IPutChanelData);
begin
    Chanels[index] := value;
end;


end.
