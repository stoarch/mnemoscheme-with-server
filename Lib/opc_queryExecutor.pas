unit Copc_queryExecutor;

interface
	uses
    	//--[ common ]--
        classes, sysUtils,
        //--[ data ]--
        data_Chanel,
        data_Object,
        //--[ interface ]--
        interface_objectData,
        interface_objectList,
        //--[ list ]--
        list_object,
        //--[ opc ]--
        class_opcDataAccess,
        class_opcItem
        ;

type
  	EParamInvalid = class( Exception );

    TErrorHandler = procedure ( sender : tobject; const error : string ) of object;
    TInfoViewer = procedure ( sender : tObject; const info : string ) of object;

	TQueryExecutor = class( TThread )
      private
        m_opcda : TOPCDataAccess;
        FServerName: string;
        FErrorMessage: string;
    	FQueriedWell: string;
    	FOnError: TErrorHandler;
    	FWellList: IWellList;
    	FOnInitProgress: TNotifyEvent;
    	FOnUnableToConnect: TNotifyEvent;
    	FOnViewQueriedWell: TInfoViewer;
    	FOnStepProgress: TNotifyEvent;
    	FOnFinalProgress: TNotifyEvent;
	    FActiveWell: IWellData;
	    FCurrentWell: IWellData;
    	FOnLogOutWell: TInfoViewer;
        FMessage : string;

    	function getItem( index : integer ): Variant;
    	procedure CopyOPCDASettings(source, destination: TOPCDataAccess);

        procedure SetServerName(const Value: string);
        procedure Initialize;
        procedure SetErrorMessage(const Value: string);
    	procedure SetQueriedWell(const Value: string);

    	procedure SetOnError(const Value: TErrorHandler);
    	procedure SetWellList(const Value: IWellList);
    	procedure SetOnInitProgress(const Value: TNotifyEvent);
    	procedure SetOnUnableToConnect(const Value: TNotifyEvent);
    	procedure SetOnViewQueriedWell(const Value: TInfoViewer);
    	procedure SetOnStepProgress(const Value: TNotifyEvent);
    	procedure SetOnFinalProgress(const Value: TNotifyEvent);
    	procedure SetActiveWell(const Value: IWellData);

    	procedure SetCurrentWell(const Value: IWellData);

    	procedure Log(msg: string);
	    procedure SetOnLogOutWell(const Value: TInfoViewer);
    	procedure FireLogOutWell;

    	function QueryOPCItem(item, itemCaption: string; out valid : boolean ): real;

        function ReadItem( itemCaption : string; var valid : boolean ) : real;
    	procedure AppendItem(item, itemCaption: string);
    	procedure RemoveItem(item, itemCaption: string);

      protected
      	procedure Execute();override;

        procedure LogError();

        procedure FireInitProgress();
        procedure FireFinalProgress();
        procedure FireUnableToConnect();
        procedure FireStepProgress();

        procedure ViewQueriedWell();
      public
        constructor Create( CreateSuspended : boolean );

        procedure SetOPCDA( value : TOPCDataAccess );

        property ErrorMessage : string read FErrorMessage write SetErrorMessage;
        property QueriedWell : string read FQueriedWell write SetQueriedWell;
        property ServerName : string read FServerName write SetServerName;

        property ActiveWell : IWellData read FActiveWell write SetActiveWell;
        property CurrentWell : IWellData read FCurrentWell write SetCurrentWell;

        property OnError : TErrorHandler read FOnError write SetOnError;
        property OnInitProgress : TNotifyEvent read FOnInitProgress write SetOnInitProgress;
        property OnFinalProgress : TNotifyEvent read FOnFinalProgress write SetOnFinalProgress;
        property OnStepProgress : TNotifyEvent read FOnStepProgress write SetOnStepProgress;
        property OnUnableToConnect : TNotifyEvent read FOnUnableToConnect write SetOnUnableToConnect;
        property OnViewQueriedWell : TInfoViewer read FOnViewQueriedWell write SetOnViewQueriedWell;
        property OnLogOutWell : TInfoViewer read FOnLogOutWell write SetOnLogOutWell;

        property WellList : IWellList read FWellList write SetWellList;
    end;


var
  g_executor: TQueryExecutor;
  g_synchronizer : TMultiReadExclusiveWriteSynchronizer;

implementation

	uses
    	//--[ common ]--
        activex, variants, windows,
        Math,
        //--[ constants ]--
        const_object,
        heap_objectData;

{ TQueryExecutor }

constructor TQueryExecutor.Create(CreateSuspended: boolean);
begin
  inherited Create( True );

  m_opcda := TOPCDataAccess.Create();
end;

procedure TQueryExecutor.CopyOPCDASettings( source, destination : TOPCDataAccess);
	var
    	i : integer;
begin
    destination.Clear();
    for i := 1 to source.Count do
    begin
      destination.AddItem(source.OPCItem[i].Caption);
    end;
end;

function TQueryExecutor.getItem( index : integer ): Variant;
begin
   result := m_opcda.Item[index];
end;

procedure TQueryExecutor.SetServerName(const Value: string);
begin
  FServerName := Value;
end;

procedure TQueryExecutor.Initialize();
	const
      RPC_C_AUTHN_LEVEL_NONE = 1;
      RPC_C_IMP_LEVEL_IMPERSONATE = 3;
      EOAC_NONE = 0;

begin
      CoInitialize(nil);

end;


procedure TQueryExecutor.AppendItem( item, itemCaption : string );
begin
  try
  	m_opcda.AddItem( item );
  except
  	on e:EOPCAddItem do
	begin
      ErrorMessage := e.Message + '::' + ItemCaption + '::' + m_opcda.OPCItem[1].Caption;
      Synchronize( self, LogError );

      raise EWellParamInvalid.Create('Add item is failed');
    end;
  end;
end;


function TQueryExecutor.ReadItem( itemCaption : string; var valid : boolean ) : real;
    const
        MAX_RETRIES = 1;
    	RETRY_INTERVAL = 300;
	var
    	floatV : variant;
        retriesCount : integer;
  		asettings: sysUtils.TFormatSettings;
begin
  asettings.DecimalSeparator := ',';

  valid := false;
  result := 0;

  retriesCount := 1;

  repeat
      try
      	floatV := getItem(1);
      except
        on e:EOPCNotGoodItem do
        begin
          ErrorMessage := 'Not good item found ' + e.Message + '::' + ItemCaption + '::' + m_opcda.OPCItem[1].Caption;
          Synchronize( self, LogError );

          m_opcda.Clear;

          raise EWellParamInvalid.Create('Get item is failed');
        end;

        on e:EOPCAddItem do
        begin
          ErrorMessage := 'Get item failed ' + e.Message + '::' + ItemCaption + '::' + m_opcda.OPCItem[1].Caption;;
          Synchronize( self, LogError );

          m_opcda.Clear;

          raise EWellParamInvalid.Create('Get item is failed');
        end;

        on e:EOPCDataAccess do
        begin
          ErrorMessage := 'OPC Error detected ' + e.Message + '::' + ItemCaption + '::' + m_opcda.OPCItem[1].Caption;;
          Synchronize( self, LogError );

          m_opcda.Clear;

          raise EWellParamInvalid.Create('Get item is failed');
        end;

      end;

      inc( retriesCount );

      if( retriesCount < MAX_RETRIES )then
      if( floatV = Unassigned )then
      begin
          Sleep( RETRY_INTERVAL );
          continue;
      end
      else
      	break;

      result := sysUtils.StrToFloat( floatV, asettings );

  until( m_opcda.OPCItem[1].Quality = ovqGood )
        or( retriesCount >= MAX_RETRIES );

  valid := ( m_opcda.OPCItem[1].Quality = ovqGood );
end;


procedure TQueryExecutor.RemoveItem( item, itemCaption : string );
begin
  try
  	m_opcda.RemoveItem( item );
  except
  	on e:EOPCDataAccess do
	begin
      ErrorMessage := e.Message + '::' + ItemCaption;
      Synchronize( self, LogError );

      m_opcda.Clear;

      raise EWellParamInvalid.Create('Remove item is failed');
    end;
  end;
end;


function TQueryExecutor.QueryOPCItem( item, itemCaption : string; out valid : boolean ) : real;
    const
      WAIT_TIMEOUT = 60000;//ms
    var
      startTime : TDateTime;
      delta : double;
begin
  valid := false;

  if( not m_opcda.Connected )then
  	m_opcda.Connect(ServerName);

  result := 0;

  startTime := Now();

  if( item = '' )then
  	exit;

    repeat
      AppendItem( item, itemCaption );
      result := ReadItem( itemCaption, valid );
      RemoveItem( item, itemCaption );

      delta := Now() - startTime;
    until(( valid )or( delta = WAIT_TIMEOUT ));
end;

procedure TQueryExecutor.Log( msg : string );
begin
    FMessage :=  msg;
	synchronize( self, FireLogOutWell );
end;

procedure TQueryExecutor.FireLogOutWell();
begin
	if( assigned( FOnLogOutWell))then
    	FOnLogOutWell( self, FMessage );
end;

procedure TQueryExecutor.Execute;
const
    IN_BETWEEN_INTERVAL = 1000;
    PENALTY_FACTOR = 5;
    OPC_STARTUP_DELAY = 10000;//ms
var
  i, j, index : integer;
  fFlowRate, fBackwardFlowRate : real;
  fCurrentFlowRate : real;
  isValidFlowRate, IsValidBackwardFlowRate : boolean;
  isValidCurrentFlowRate : boolean;
  isQuerySuccess : boolean;
  heap : TWellHeap;
begin
  initialize();

  //Initialize heap
  heap := TWellHeap.Create();
  for i := 1 to WellList.Count do
  begin
	heap.Push(WellList[i], i);
  end;


  while True do
  begin
    try
      if( not m_opcda.Connected )then
        try
          Log( 'Connecting to server...' );
          m_opcda.Connect( ServerName );

          Sleep( OPC_STARTUP_DELAY );
        except
          on e:Exception do
          begin
          	ErrorMessage	:=	e.Message;

          	Synchronize( self, FireUnableToConnect  );
          	Suspend();
          	continue;
          end;
        end;


      if( Terminated )then
        break;

      Synchronize( self, FireInitProgress );

      Log( 'Starting query...' );

      for i := 1 to WellList.Count do
      begin
        CurrentWell := heap.Pop;
        index 	   := heap.TopPriority;

        isQuerySuccess := false;

        if( CurrentWell.Active )then
        begin
          ActiveWell := CurrentWell;
          QueriedWell := CurrentWell.Caption;
          Synchronize( self, ViewQueriedWell );
          try
            for j := 1 to CurrentWell.Count do
            begin
              if( not CurrentWell[j].Active )then
                continue;

              isQuerySuccess := False;

              if( Terminated )then
                Abort;

              fFlowRate := QueryOPCItem( CurrentWell[j].flowRateOPC, 'FlowRate', isValidFlowRate );

              if( Terminated )then
                Abort;

              fbackwardFlowRate := QueryOPCItem( CurrentWell[j].backwardflowRateOPC, 'BackwardFlowRate', isValidBackwardFlowRate);

              if( Terminated )then
                Abort;

              fCurrentFlowRate := QueryOPCItem( CurrentWell[j].currentFlowRateOPC, 'CurrentFlowRate', isValidCurrentFlowRate);

              g_synchronizer.BeginWrite();

              CurrentWell[j].Value.FlowRate.Value := fFlowRate;
              CurrentWell[j].Value.BackwardFlowRate.Value := fbackwardFlowRate;
              CurrentWell[j].Value.CurrentFlowRate.Value  := fCurrentFlowRate;

              CurrentWell[j].Value.FlowRate.IsValid := isValidFlowRate;
              CurrentWell[j].Value.BackwardFlowRate.IsValid := IsValidBackwardFlowRate;
              CurrentWell[j].Value.CurrentFlowRate.IsValid := isValidCurrentFlowRate;

              g_synchronizer.EndWrite();
            end;//for

            isQuerySuccess := true;
          except
            	on e:EWellParamInvalid do
                begin
                end;
          end;
        end;//if

        if( Terminated )then
          Abort;


        if( isQuerySuccess )then
        begin
        	heap.Push( CurrentWell, index + heap.count + 1 );
            Synchronize( self, FireStepProgress );
        end
        else
			heap.Push( CurrentWell, index + PENALTY_FACTOR*heap.count + 1 )
        ;

        if( isQuerySuccess )then
          Sleep( IN_BETWEEN_INTERVAL );


        if( Terminated )then
          Abort;
      end;//while

      if( Terminated )then
        Abort;

      Synchronize( self, FireFinalProgress );

      m_opcda.Disconnect();
      Suspend();

      if( Terminated )then
        Abort;

    except
    	on e:Eabort do
        begin
        	break;
        end;

    	on e:exception do
        begin
        	ErrorMessage := e.Message;
        	LogError();

            m_opcda.Disconnect;
            Suspend;
        end;
    end;//try-except
  end;//while forever

  m_opcda.Disconnect();
  CoUninitialize();

  FreeAndNil( heap );
end;

procedure TQueryExecutor.SetErrorMessage(const Value: string);
begin
  FErrorMessage := Value;
end;

procedure TQueryExecutor.SetOPCDA(value: TOPCDataAccess);
begin
	CopyOPCDASettings(value, m_opcda);
end;

procedure TQueryExecutor.SetQueriedWell(const Value: string);
begin
	FQueriedWell := value;
end;

procedure TQueryExecutor.LogError;
begin
	if( assigned( FOnError ))then
    	FOnError( self, ErrorMessage );
end;

procedure TQueryExecutor.SetOnError(const Value: TErrorHandler);
begin
  FOnError := Value;
end;

procedure TQueryExecutor.FireInitProgress;
begin
	if( assigned( FOnInitProgress ) )then
    	FOnInitProgress( self );
end;

procedure TQueryExecutor.FireUnableToConnect;
begin
	if( assigned( FOnUnableToConnect ) )then
    	FOnUnableToConnect( self );
end;

procedure TQueryExecutor.SetWellList(const Value: IWellList);
begin
  FWellList := Value;
end;

procedure TQueryExecutor.SetOnInitProgress(const Value: TNotifyEvent);
begin
  FOnInitProgress := Value;
end;

procedure TQueryExecutor.SetOnUnableToConnect(const Value: TNotifyEvent);
begin
  FOnUnableToConnect := Value;
end;

procedure TQueryExecutor.ViewQueriedWell;
begin
	if( assigned( FOnViewQueriedWell ))then
    	FOnViewQueriedWell( self, QueriedWell );
end;

procedure TQueryExecutor.SetOnViewQueriedWell(const Value: TInfoViewer);
begin
  FOnViewQueriedWell := Value;
end;

procedure TQueryExecutor.FireStepProgress;
begin
	if( assigned( FOnStepProgress ) )then
    	FOnStepProgress( self );
end;

procedure TQueryExecutor.SetOnStepProgress(const Value: TNotifyEvent);
begin
  FOnStepProgress := Value;
end;

procedure TQueryExecutor.FireFinalProgress;
begin
	if( assigned( FOnFinalProgress ))then
    	FOnFinalProgress( self );
end;

procedure TQueryExecutor.SetOnFinalProgress(const Value: TNotifyEvent);
begin
  FOnFinalProgress := Value;
end;

procedure TQueryExecutor.SetActiveWell(const Value: IWellData);
begin
  FActiveWell := Value;
end;

procedure TQueryExecutor.SetCurrentWell(const Value: IWellData);
begin
  FCurrentWell := Value;
end;

procedure TQueryExecutor.SetOnLogOutWell(const Value: TInfoViewer);
begin
  FOnLogOutWell := Value;
end;

initialization
	g_synchronizer := TMultiReadExclusiveWriteSynchronizer.Create();

finalization
	FreeAndNil( g_synchronizer );

end.
