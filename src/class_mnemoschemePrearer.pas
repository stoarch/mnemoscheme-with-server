unit class_mnemoschemePrearer;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ common ]--
        classes, ComCtrls, controls, forms, SysUtils,
        //--[ controls ]--
        control_commonMnemoscheme,
        //--[ interfaces ]--
        interface_mnemoschemePreparer
        ;

    type
        TMnemoschemePreparer = class( TCreatableObject, IMnemoschemePreparer )
        protected
          m_pages : TPageControl;

          FQuerying : boolean;
          FLastRecord : integer;
          FMnemoscheme: TCommonMnemoscheme;

          FOnQueryDataFinished : TNotifyEvent;
          FOnQueryDataStarted : TNotifyEvent;

          procedure CreateMnemoscheme;virtual;abstract;
          procedure QueryData;virtual;abstract;
          function TryReadSettings: boolean;virtual;abstract;

        public
            function Execute() : boolean;virtual;
            procedure Update();virtual;

            function GetOnQueryDataFinished: TNotifyEvent;
            function GetOnQueryDataStarted: TNotifyEvent;

            procedure SetOnQueryDataFinished(const Value: TNotifyEvent);
            procedure SetOnQueryDataStarted(const Value: TNotifyEvent);

            property OnQueryDataFinished : TNotifyEvent read GetOnQueryDataFinished write SetOnQueryDataFinished;
            property OnQueryDataStarted : TNotifyEvent read GetOnQueryDataStarted write SetOnQueryDataStarted;


            function GetPages: TPageControl;
            procedure SetPages(const Value: TPageControl);
            property Pages : TPageControl read GetPages write SetPages;
        end;

implementation

{ TMnemoschemePreparer }


function TMnemoschemePreparer.Execute: boolean;
begin
  TryReadSettings;
  CreateMnemoscheme();
  FMnemoscheme.GenerateViews();
  FMnemoscheme.UpdateSettings();
  QueryData();

  result := true;
end;

function TMnemoschemePreparer.GetOnQueryDataFinished: TNotifyEvent;
begin
    result := FOnQueryDataFinished;
end;

function TMnemoschemePreparer.GetOnQueryDataStarted: TNotifyEvent;
begin
    result := FOnQueryDataStarted;
end;

function TMnemoschemePreparer.GetPages: TPageControl;
begin
    result := m_pages;
end;


procedure TMnemoschemePreparer.SetOnQueryDataFinished(
  const Value: TNotifyEvent);
begin
    FOnQueryDataFinished := value;
end;

procedure TMnemoschemePreparer.SetOnQueryDataStarted(
  const Value: TNotifyEvent);
begin
    FOnQueryDataStarted := value;
end;

procedure TMnemoschemePreparer.SetPages(const Value: TPageControl);
begin
    m_pages := value;
end;

procedure TMnemoschemePreparer.Update;
begin
    QueryData();
end;

end.
