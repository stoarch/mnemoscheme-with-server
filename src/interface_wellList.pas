unit interface_wellList;

interface
    uses
        //--[ interfaces ]--
        interface_mnemoschemeItemList,
        interface_wellData
        ;

    const
        IID_IWellList : TGUID = '{DD7EEA41-17B9-4DF1-B67A-241016D50158}';

    type
        IWellList = interface( IMnemoschemeItemList )
            ['{DD7EEA41-17B9-4DF1-B67A-241016D50158}']
                function getWellData(index: integer): IWellData;

                property Well[ index : integer ] : IWellData read getWellData;default;
        end;

implementation

end.
 