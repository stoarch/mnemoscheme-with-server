unit class_channelData;

interface
    uses
        //--[ classes ]--
        class_creatableObject,
        //--[ interfaces ]--
        interface_mnemoschemeItemChanelData,
        interface_mnemoschemeItemChannelValue
        ;
type
    TMnemoschemeItemChannelData = class( TCreatableObject, IMnemoschemeItemChannelValue )
    private
        FActive: boolean;
        FCaption: string;
        FIndex: integer;
        FValue: IChannelValue;


    public
        constructor Create();override;
        destructor Destroy();override;


        procedure Assign( const value : IChannelData );virtual;

        function GetActive: boolean;
        function GetCaption: string;
        function GetIndex: integer;
        function GetValue: IChannelValue;

        procedure SetActive(const Value: boolean);
        procedure SetCaption(const Value: string);
        procedure SetIndex(const Value: integer);
        procedure SetValue(const Value: IChannelValue);

        property Active : boolean read GetActive write SetActive;
        property Caption : string read GetCaption write SetCaption;
        property Index : integer read GetIndex write SetIndex;

        property Value : IChannelValue read GetValue write SetValue;
    end;

implementation

uses tool_ms_factories, interface_commonFactory, const_put, const_guids;

{ TChannelData }

procedure TChannelData.Assign(const value: IChannelData);
begin
	Active := value.Active;
    Caption := value.Caption;
    Index := value.Index;
    FValue.Assign( value.Value );
end;

constructor TChannelData.Create;
begin
  inherited;

    FValue := GetCommonFactory().CreateInstance( CLSID_ChannelValue ) as IChannelValue;
end;

destructor TChannelData.Destroy;
begin
   Assert( FRefCount = 0, 'Destroying of usable interfaced object' );

  inherited;
end;

function TChannelData.GetActive: boolean;
begin
    result := FActive;
end;

function TChannelData.GetCaption: string;
begin
    result := FCaption;
end;

function TChannelData.GetIndex: integer;
begin
    result := FIndex;
end;

function TChannelData.GetValue: IChannelValue;
begin
    result := FValue;
end;

procedure TChannelData.SetActive(const Value: boolean);
begin
    FActive := value;
end;

procedure TChannelData.SetCaption(const Value: string);
begin
    FCaption := value;
end;

procedure TChannelData.SetIndex(const Value: integer);
begin
    FIndex := value;
end;

procedure TChannelData.SetValue(const Value: IChannelValue);
begin
  FValue := Value;
end;

end.
 