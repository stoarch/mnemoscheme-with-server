unit interface_wellChannelParams;

interface
    uses
        //--[ interfaces ]--
        interface_realRange
        ;

    const
        IID_IWellChannelParams : TGUID = '{4315BCF6-8A58-48F8-A91F-5B1D70C25345}';

    type
        IWellChannelParams = interface
            ['{4315BCF6-8A58-48F8-A91F-5B1D70C25345}']
            procedure Assign( const data : IWellChannelParams );

            function GetBackwardFlowRateRange: IRealRange;
            function GetCurrentFlowRateRange: IRealRange;
            function GetFlowRateRange: IRealRange;

            procedure SetCurrentFlowRateRange(const Value: IRealRange);

            property FlowRateRange : IRealRange read GetFlowRateRange;
            property BackwardFlowRateRange : IRealRange read GetBackwardFlowRateRange;
            property CurrentFlowRateRange : IRealRange read GetCurrentFlowRateRange write SetCurrentFlowRateRange;
        end;

implementation

end.
 