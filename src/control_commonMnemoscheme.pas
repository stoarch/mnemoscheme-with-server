unit control_commonMnemoscheme;

interface

    uses
        //--[ common ]--
        classes, ComCtrls, controls, graphics, ExtCtrls, SysUtils,
        XPCollections,
        //--[ alpha skins ]--
        sComboBox, slabel, sPanel, sGroupBox, sSplitter, sToolbar,
        sSkinProvider, sSkinManager, sCustomComboEdit, sConst,
        sedit,
        //--[ data ]--
        data_put,
        //--[ flex ]--
        FlexBase, FlexUtils, FlexControls, FlexProps,
        //--[ frames ]--
        frame_putView, frame_wellView,
        //--[ jedi ]--
        JclSysUtils, JclStrings, JclStringLists, JclContainerIntf,
        JclVectors, JclAlgorithms,
        //--[interfaces]--
        interface_commonFactory,
        interface_interfaceList,
        interface_wellsCalculator,
        //--[Tools]--
        tool_environment,
        //--[constants]--
        const_well
        ;

type
    TCommonMnemoscheme = class( TWinControl )
    private
    FCaptionList: IJclAnsiStrList;
    procedure SetCaptionList(const Value: IJclAnsiStrList);
    procedure SetupFunctionPages;
    procedure SetupMnemoschemeTab;
    procedure SetupPanelToggleSplitter;
    procedure SetupMnemoschemePanel;
    procedure SetupInfoPanel;
    procedure SetupHintLabel;
    procedure SetupZoomFactorSelector;

    procedure spPanelToggleCanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure spPanelToggleClick(Sender: TObject);
    procedure cbbZoomFactorExit(Sender: TObject);
    function GetDataList: IInterfaceList;

    protected
        functionPages: TPageControl;
        tabMnemoscheme: TTabSheet;
        spPanelToggle: TsSplitter;
        MnemoschemePanel: TFlexPanel;
        pnlInfo: TsPanel;
        lblHint: TsLabel;
        cbbZoomFactor: TsComboBox;

        FDataList : IInterfaceList;

        procedure CreateControls();virtual;

        function GetItemCaption( index : integer ): string;virtual;
        function GetItemCaptions: IJclAnsiStrList; virtual;
        function GetItemIndex(index: Integer): integer;virtual;

        function MakeViewTab( aindex : integer; acaption : String ): TTabSheet;virtual;

        procedure Handle_TabChanged(sender: TObject); virtual;

        procedure MnemoschemePanelMouseMove(Sender: TObject; Shift : TShiftState;
                    X,Y : integer );virtual;

        procedure MnemoschemePanelMouseUp(Sender: TObject;
                  Button: TMouseButton; Shift: TShiftState; X, Y: Integer); virtual;


        procedure SetDataList(const Value: IInterfaceList);

        procedure SetupControls(); virtual;

        procedure UpdateMnemoschemeView;virtual;
        procedure UpdateViews();virtual;

        procedure ZoomBy(factor: integer);

        property CaptionList : IJclAnsiStrList read FCaptionList write SetCaptionList;
    public
        property DataList : IInterfaceList read GetDataList write SetDataList;
        procedure GenerateViews();virtual;
        procedure Update();override;
        procedure UpdateSettings();virtual;

        constructor Create(AOwner: TComponent); override;
        destructor Destroy();override;
    end;

implementation

{ TCommonMnemoscheme }


constructor TCommonMnemoscheme.Create(AOwner: TComponent);

begin
  inherited;

  Font.Charset := 204;
  Font.Name := 'Tahoma';

  Parent := AOwner as TWinControl;

  CreateControls();

  SetupControls();
end;

procedure TCommonMnemoscheme.CreateControls;
begin
    functionPages := TPageControl.Create(Self);
    tabMnemoscheme := TTabSheet.Create(Self);
    spPanelToggle := TsSplitter.Create(Self);
    MnemoschemePanel := TFlexPanel.Create(Self);
    pnlInfo := TsPanel.Create(Self);
    lblHint := TsLabel.Create(Self);
    cbbZoomFactor := TsComboBox.Create(Self);
end;

procedure TCommonMnemoscheme.GenerateViews;
	var
    	i : integer;
begin
    CaptionList := GetItemCaptions();

    //QuickSort( CaptionList, 0, DataList.Count - 1, AnsiStrSimpleCompare);

    for i := 0 to CaptionList.Size - 1 do
    begin
        MakeViewTab(
            GetItemIndex( i ),
            CaptionList[i]
        );
    end;

    functionPages.ActivePageIndex := 0;
end;

function TCommonMnemoscheme.GetItemCaption(index: integer): string;
begin
    result := '';
end;

function TCommonMnemoscheme.GetItemCaptions: IJclAnsiStrList;
var
  i: integer;
  list: IJclAnsiStrList;
begin
  list := TJclAnsiStrVector.Create(DataList.Count);
  for i := 1 to DataList.Count do
  begin
      list.Add( GetItemCaption( i ) );
  end;
  Result := list;
end;

function TCommonMnemoscheme.GetItemIndex(index: Integer): integer;
begin
    result := -1;
end;

function TCommonMnemoscheme.MakeViewTab(aindex: integer;
  acaption: String): TTabSheet;
begin
    raise Exception.Create( 'Not implemented MakeViewTab' );
end;

procedure TCommonMnemoscheme.SetCaptionList(const Value: IJclAnsiStrList);
begin
  FCaptionList := Value;
end;

procedure TCommonMnemoscheme.SetDataList(const Value: IInterfaceList);
begin
  FDataList := Value;
end;

procedure TCommonMnemoscheme.SetupControls;
begin
  SetupFunctionPages;

  SetupMnemoschemeTab;
  SetupPanelToggleSplitter;
  SetupMnemoschemePanel;
  SetupInfoPanel;
  SetupHintLabel;
  SetupZoomFactorSelector;
end;

procedure TCommonMnemoscheme.Update;
begin
    UpdateSettings();
    UpdateMnemoschemeView;
    UpdateViews();
end;

procedure TCommonMnemoscheme.UpdateMnemoschemeView;
begin

end;

procedure TCommonMnemoscheme.UpdateSettings;
begin

end;

procedure TCommonMnemoscheme.UpdateViews;
begin

end;

procedure TCommonMnemoscheme.SetupFunctionPages;
begin
  functionPages.Parent := self;
  with functionPages do
  begin
    Name := 'functionPages';
    Left := 0;
    Top := 0;
    Width := 1008;
    Height := 692;
    ActivePage := tabMnemoscheme;
    Align := alClient;
    TabOrder := 0;
    ParentFont := true;
    OnChange := Handle_TabChanged;
  end;
end;

procedure TCommonMnemoscheme.SetupMnemoschemeTab;
begin
  with tabMnemoscheme do
  begin
    Name := 'tabMnemoscheme';
    Parent := functionPages;
    PageControl := functionPages;
    Caption := '����������';
    ImageIndex := 1;
    ParentFont := true;
  end;
end;

procedure TCommonMnemoscheme.SetupPanelToggleSplitter;
begin
  with spPanelToggle do
  begin
    Name := 'spPanelToggle';
    Parent := tabMnemoscheme;
    Left := 778;
    Top := 0;
    Width := 8;
    Height := 664;
    Align := alRight;
    Beveled := True;
    ResizeStyle := rsLine;
    OnCanResize := spPanelToggleCanResize;
    OnClick := spPanelToggleClick;
  end;
end;

procedure TCommonMnemoscheme.SetupMnemoschemePanel;
begin
  with MnemoschemePanel do
  begin
    Name := 'MnemoschemePanel';
    Parent := tabMnemoscheme;
    Left := 0;
    Top := 0;
    Width := 778;
    Height := 664;
    Scale := 75;
    DocWidth := 320000;
    DocHeight := 240000;
    DocFrameColor := clBlack;
    DocShadowColor := clGray;
    DocSpaceFill := False;
    GridStyle := gsDots;
    GridColor := clGray;
    GridPixColor := clSilver;
    GridHorizSize := 10000;
    GridVertSize := 10000;
    Align := alClient;
    TabOrder := 0;
    OnMouseUp := MnemoschemePanelMouseUp;
    OnMouseMove := MnemoschemePanelMouseMove;
  end;
end;

procedure TCommonMnemoscheme.SetupInfoPanel;
begin
  with pnlInfo do
  begin
    Name := 'pnlInfo';
    Caption := '';
    Parent := tabMnemoscheme;
    Left := 786;
    Top := 0;
    Width := 284;
    Height := 664;
    Align := alRight;
    TabOrder := 1;
  end;
end;

procedure TCommonMnemoscheme.SetupHintLabel;
begin
  with lblHint do
  begin
    Name := 'lblHint';
    Parent := pnlInfo;
    Left := 11;
    Top := 76;
    Width := 23;
    Height := 13;
    Caption := 'Hint:';
    ParentFont := False;
    Visible := False;
  end;
end;

procedure TCommonMnemoscheme.SetupZoomFactorSelector;
begin
  with cbbZoomFactor do
  begin
    Name := 'cbbZoomFactor';
    Parent := pnlInfo;
    Left := 8;
    Top := 17;
    Width := 73;
    Height := 21;
    Alignment := taLeftJustify;
    Color := clWhite;
    ItemHeight := 13;
    ItemIndex := 3;
    ParentFont := False;
    TabOrder := 0;
    Text := '75%';
    OnChange := cbbZoomFactorExit;
    Items.Clear;
    Items.Add('10%');
    Items.Add('20%');
    Items.Add('50%');
    Items.Add('75%');
    Items.Add('100%');
    Items.Add('150%');
    Items.Add('200%');
    Items.Add('300%');
    Items.Add('500%');

    BoundLabel.Active := True;
    BoundLabel.Caption := '�������:';
    BoundLabel.Indent := 0;
    BoundLabel.Font.Charset := 204;
    BoundLabel.Font.Color := 4276545;
    BoundLabel.Font.Height := -11;
    BoundLabel.Font.Name := 'Tahoma';
    BoundLabel.Font.Style := [];
    BoundLabel.Layout := sclTopLeft;
    BoundLabel.MaxWidth := 0;
    BoundLabel.UseSkinColor := True;
  end;
end;

procedure TCommonMnemoscheme.Handle_TabChanged( sender : TObject );
begin
    raise Exception.Create( 'Not implemented Handle_TabChanged' );
end;


procedure TCommonMnemoscheme.spPanelToggleCanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
begin
	Accept := false;
end;

procedure TCommonMnemoscheme.spPanelToggleClick(Sender: TObject);
begin
	pnlInfo.Visible := not pnlInfo.Visible;
end;

procedure TCommonMnemoscheme.MnemoschemePanelMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin

end;

procedure TCommonMnemoscheme.cbbZoomFactorExit(Sender: TObject);
begin
	ZoomBy( StrToIntDef(System.Copy(cbbZoomFactor.Text, 1, pos('%', cbbZoomFactor.Text)-1), 100) );
end;

procedure TCommonMnemoscheme.ZoomBy( factor : integer );
begin
	MnemoschemePanel.Zoom( factor, nil );
end;



procedure TCommonMnemoscheme.MnemoschemePanelMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: integer);
begin

end;

destructor TCommonMnemoscheme.Destroy;
begin
    FDataList := nil;
    FCaptionList := nil;

  inherited;
end;

function TCommonMnemoscheme.GetDataList: IInterfaceList;
begin
    result := FDataList;
end;

end.
